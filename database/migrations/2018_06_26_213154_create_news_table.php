<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique()->default('');
            $table->string('slug')->unique()->default(''); 
            $table->string('date',20)->default('');
            $table->string('reference',100)->default('');
            $table->string('event_date',100)->default('');
            $table->string('description',500)->default('');
            $table->string('short',1000)->default('');
            $table->string('detailed',5000)->default('');
            $table->string('thumb',250)->default('');
            $table->string('cover',250)->default('');
            $table->string('keywords',250)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}

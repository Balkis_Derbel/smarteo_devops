<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->default('');
            $table->string('coupon')->default('');
            $table->string('token_rent')->default('');
            $table->string('token_sell')->default('');
            $table->double('discount_rent')->default('0');
            $table->double('discount_sell')->default('0'); 
            $table->string('kit')->default('');
            $table->enum('status', array('live','inactive'))->default('inactive'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}

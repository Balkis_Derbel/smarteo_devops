<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcel_trackings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('orderid')->nullable()->default(0);
            $table->string('firstname')->nullable()->default('');
            $table->double('lastname')->nullable()->default(0);                        
            $table->string('email')->nullable()->default('');
            $table->string('phone')->nullable()->default('');
            $table->string('address')->nullable()->default('');
            $table->string('zipcode')->nullable()->default('');
            $table->string('city')->nullable()->default('');                                      
            $table->enum('status', array(
                'prepared',
                'sent',
                'delivered',
                'sent_back',
                'received',
                'archived'))->default('prepared');
            $table->string('transporter')->nullable()->default('');
            $table->string('tracking_id')->nullable()->default('');
            $table->string('date_sent')->nullable()->default('');
            $table->string('date_delivered')->nullable()->default('');                           
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcel_trackings');
    }
}

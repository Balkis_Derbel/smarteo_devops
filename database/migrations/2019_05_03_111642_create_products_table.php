<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->unique()->default('');
            $table->enum('type',array('none','product','workshop','pricing_plan'))->default('none');
            $table->string('title')->unique();
            $table->string('subtitle')->nullabe()->default('');
            $table->string('slug')->unique()->default('');
            $table->text('description');
            $table->string('thumb')->nullable()->default('');
            $table->integer('nb_images')->nullable()->default(0);            
            $table->integer('featured')->default(1);
            $table->string('tags')->nullable();
            $table->double('selling_price')->default(0);            
            $table->double('subscr_amount')->default(0); 
            $table->enum('subscr_frequency', array('none','weekly','monthly','yearly'))->default('none');
            $table->string('stripe_product_ref')->nullable();
            $table->string('stripe_subscr_plan')->nullable();
            $table->integer('inventory')->nullable()->default(0);            
            $table->enum('status',array('live','hidden'))->default('live');  
            $table->timestamps();
        });

        DB::statement('ALTER TABLE products ADD FULLTEXT full(reference, title, subtitle,slug,description,tags)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

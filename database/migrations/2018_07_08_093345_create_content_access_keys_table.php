<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentAccessKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_access_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->string('value')->default('');
            $table->string('startdate')->default('');
            $table->string('maxdate')->default('');
            $table->string('contentids')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_access_keys');
    }
}

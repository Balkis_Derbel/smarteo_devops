<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('keywords')->nullable();
            $table->string('tags')->nullable();
            $table->string('thumb')->nullable();
            $table->enum('status', array('live','hidden'))->default('hidden');
            $table->string('details',5000)->default('');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE subjects ADD FULLTEXT full(title, slug,keywords,tags,details)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}

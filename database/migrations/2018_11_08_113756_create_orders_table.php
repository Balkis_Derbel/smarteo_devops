<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();        
            $table->string('product_reference')->default('');
            $table->integer('amount')->default(0);
            $table->string('discount_code')->nullable()->default('');            
            $table->enum('order_type', array('pay_once','subscribe'))->default('pay_once');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->string('request_uri')->default('');
            $table->string('unique_key')->default('');
            $table->string('firstname',50)->default('');
            $table->string('lastname',50)->default('');
            $table->string('email')->default('');
            $table->string('phone')->default('');
            $table->string('address')->default('');
            $table->string('zipcode')->default('');
            $table->string('city')->default('');                                      
            $table->enum('status', array(
                'created',
                'checkout',
                'payment_success',
                'subscription_success',
                'product_sent',
                'product_delivered',
                'subscription_ongoing',
                'subscription_finished',
                'processed',
                'archived'))->default('created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

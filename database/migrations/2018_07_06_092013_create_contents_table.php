<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->default('');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('slug')->nullable();
            $table->string('keywords')->nullable();
            $table->string('tags')->nullable();
            $table->string('tags_1')->nullable();
            $table->string('thumb')->nullable();
            $table->enum('integration', array('iframe,','href','local'))->default('local');
            $table->string('parameters')->nullable()->default('');
            $table->enum('access_type', array('public','restricted'))->default('restricted');
            $table->enum('status', array('ready','inprogress','deleted'))->default('inprogress');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE contents ADD FULLTEXT full(reference, title, description,slug,keywords,tags,tags_1,parameters)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}

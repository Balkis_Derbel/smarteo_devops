<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->string('url')->unique()->default(''); 
            $table->string('date',20)->default('');
            $table->string('reference',100)->unique()->default('');
            $table->string('detailed',2000)->default('');
            $table->string('thumb',250)->default('');
            $table->string('keywords',250)->default('');
            $table->integer('ownerid');
            $table->string('author',250)->default('');
            $table->string('author_thumb',250)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

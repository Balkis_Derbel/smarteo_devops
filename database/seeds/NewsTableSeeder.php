<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('news')->delete();
        
        \DB::table('news')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Participation à la conférence TOTEM#EdTech',
                'slug' => 'participation-la-conference-totem-edtech',
                'date' => '20180209',
                'reference' => '180209-totem-edtech',
                'event_date' => '09/02/2018',
                'description' => 'A l’ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L’individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.',
                'short' => 'A l’ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L’individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.
                <br>
                <a href="https://blog.lehub.bpifrance.fr/edtech-retour-images/">https://blog.lehub.bpifrance.fr/edtech-retour-images/</a>',
                'detailed' => 'A l’ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L’individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.
                <br>
                <a href="https://blog.lehub.bpifrance.fr/edtech-retour-images/">https://blog.lehub.bpifrance.fr/edtech-retour-images/</a>',
                'thumb' => '20180209-totem-edtech/thumb-fresque_edtech.jpg',
                'cover' => '20180209-totem-edtech/fresque_edtech.jpg',
                'keywords' => 'EdTech,formation',
                'created_at' => '2018-06-29 08:37:41',
                'updated_at' => '2018-06-29 12:38:16',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Nous étions présents au Salon de la Formation et du Digital Learning',
                'slug' => 'nous-tions-pr-sents-au-salon-de-la-formation-et-du-digital-learning',
                'date' => '20180320',
                'reference' => '180322-elearning-expo',
                'event_date' => '20/03/2018 au 22/03/2018',
                'description' => 'Digital learning, mobile learning, social learning, serious games, moocs, gamification, réalité virtuelle, intelligence artificielle et immersive learning …   Le salon ELearning Expo était l\'occasion d\'échanger avec les différents acteurs sur les sujets de l\'éducation et du digital.',
                'short' => 'Digital learning, mobile learning, social learning, serious games, moocs, gamification, réalité virtuelle, intelligence artificielle et immersive learning …
                Le salon ELearning Expo était l\'occasion d\'échanger avec les différents acteurs sur les sujets de l\'éducation et du digital.
                <br>
                <a href="http://www.e-learning-expo.com/">http://www.e-learning-expo.com/</a>',
                'detailed' => 'Digital learning, mobile learning, social learning, serious games, moocs, gamification, réalité virtuelle, intelligence artificielle et immersive learning …
                Le salon ELearning Expo était l\'occasion d\'échanger avec les différents acteurs sur les sujets de l\'éducation et du digital.
                <br>
                <a href="http://www.e-learning-expo.com/">http://www.e-learning-expo.com/</a>',
                'thumb' => '20180320-elearning-expo/thumb-elearning-expo.png',
                'cover' => '20180320-elearning-expo/cover-elearning-expo.png',
                'keywords' => 'salon elearning expo',
                'created_at' => '2018-06-29 08:43:09',
                'updated_at' => '2018-06-29 13:08:07',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Startup For Kids à Saclay',
                'slug' => 'startup-for-kids-saclay',
                'date' => '20180528',
                'reference' => '180528-startup-4-kids',
                'event_date' => '28/05/2018',
                'description' => 'Un programme chargé lors de cet évènement, avec conférences et ateliers, afin d\'échanger sur des sujets au cœur de l’actualité comme par exemple l’intelligence artificielle, ou encore la révolution du numérique et son impact dans l\'éducation.',
                'short' => 'Un programme chargé lors de cet évènement, avec conférences et ateliers, afin d\'échanger sur des sujets au cœur de l’actualité comme par exemple l’intelligence artificielle, ou encore la révolution du numérique et son impact dans l\'éducation.
                <a href="http://startupforkids.fr/paris-saclay-pro/">http://startupforkids.fr/paris-saclay-pro/</a>',
                'detailed' => 'Un programme chargé lors de cet évènement, avec conférences et ateliers, afin d\'échanger sur des sujets au cœur de l’actualité comme par exemple l’intelligence artificielle, ou encore la révolution du numérique et son impact dans l\'éducation.
                <a href="http://startupforkids.fr/paris-saclay-pro/">http://startupforkids.fr/paris-saclay-pro/</a>',
                'thumb' => 'startup-for-kids-saclay.jpg',
                'cover' => 'startup-for-kids-saclay.jpg',
                'keywords' => 'startupforkids',
                'created_at' => '2018-06-29 12:44:09',
                'updated_at' => '2018-06-29 12:44:44',
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Le Festival du Digital à Rueil Malmaison : F"AI"tes du numérique !!!',
                'slug' => 'le-festival-du-digital-rueil-malmaison-f-ai-tes-du-num-rique-',
                'date' => '20180602',
                'reference' => '180602-rueil-digital',
                'event_date' => '02/06/2018',
                'description' => 'Cette journée du festival du numérique de Rueil Malmaison, dont nous sommes partenaires, était très riche avec notamment un Hackathon 4 Kids, un Mini FabLab et une table ronde : "Quand l’humanité crée l’Intelligence Artificielle !" pour imaginer ce qui pourrait être demain notre vie avec l\'IA.',
                'short' => 'Cette journée du festival du numérique de Rueil Malmaison, dont nous sommes partenaires, était très riche avec notamment un Hackathon 4 Kids, un Mini FabLab et une table ronde : "Quand l’humanité crée l’Intelligence Artificielle !" pour imaginer ce qui pourrait être demain notre vie avec l\'IA.
                <br>
                <a href="https://www.faitesdunumerique.fr/">https://www.faitesdunumerique.fr/</a>',
                'detailed' => 'Cette journée du festival du numérique de Rueil Malmaison, dont nous sommes partenaires, était très riche avec notamment un Hackathon 4 Kids, un Mini FabLab et une table ronde : "Quand l’humanité crée l’Intelligence Artificielle !" pour imaginer ce qui pourrait être demain notre vie avec l\'IA.
                <br>
                <a href="https://www.faitesdunumerique.fr/">https://www.faitesdunumerique.fr/</a>


                <br>

                <div class="row text-center">
                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/article-presse-rueildigital.jpg\'  alt=\'Article de presse RueilDigital\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/smarteo-marty.jpg\' alt=\'Le Robot Marty - Smarteo\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/rueil-smarteo-stand.jpg\' alt=\'Smarteo au Festival du Digital\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/rueil-artificial-intelligence-conf.jpg\' alt=\'Table ronde Intelligence Artificielle\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/rueil-workshop.jpg\' alt=\'Atelier de Robotique\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/rueil-festival-digital.jpg\' alt=\'Festival du Digital à Rueil Malmaison\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/rueil-hackathon.jpg\' alt=\'Hackathon de Robotique pour les enfants\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/rueildigital/rueil-hackathon-2.jpg\' alt=\'Hackathon de Robotique pour les enfants : remise des prix\'/>
                </div>
                </div>',
                'thumb' => '20180602-rueil-festival-digital\thumb-rueil-festival.jpg',
                'cover' => '20180602-rueil-festival-digital\cover-rueil-festival.jpg',
                'keywords' => 'IA, HackatonForKids',
                'created_at' => '2018-06-29 12:47:36',
                'updated_at' => '2018-09-19 14:19:39',
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Smarteo participe à la conférence « L’entrepreneuriat à l’Ecole » à Sceaux',
                'slug' => 'smarteo-participe-la-conf-rence-l-entrepreneuriat-l-ecole-sceaux',
                'date' => '20180614',
                'reference' => '180614-entrep-ecole',
                'event_date' => '14/06/2018',
                'description' => 'Conférence portée par la communauté Up-Campus à Sceaux et la ville de Sceaux. Chaque année en France, le décrochage scolaire concerne environ 140 000 jeunes de 16 à 25 ans. Une des solutions consiste à développer l’esprit d’entreprise et les compétences nécessaires à la réalisation de projets chez les jeunes générations.',
                'short' => 'Nous étions présents à cette conférence, portée par la communauté <a href="https://up-sceaux.org/">Up-Campus à Sceaux</a> et la ville de Sceaux. Chaque année en France, le décrochage scolaire concerne environ 140 000 jeunes de 16 à 25 ans. Une des solutions consiste à développer l’esprit d’entreprise et les compétences nécessaires à la réalisation de projets chez les jeunes générations.
                <br>
                <a href="https://up-campus.org/evenements/570">https://up-campus.org/evenements/570</a>',
                'detailed' => 'Nous étions présents à cette conférence, portée par la communauté <a href="https://up-sceaux.org/">Up-Campus à Sceaux</a> et la ville de Sceaux. Chaque année en France, le décrochage scolaire concerne environ 140 000 jeunes de 16 à 25 ans. Une des solutions consiste à développer l’esprit d’entreprise et les compétences nécessaires à la réalisation de projets chez les jeunes générations.
                <br>
                <a href="https://up-campus.org/evenements/570">https://up-campus.org/evenements/570</a>

                <br>

                <div class="row text-center">
                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/entrepecole/EntrepEcole.jpg\'  alt=\'Conférence l entrepreneuriat à l école\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/entrepecole/EntrepEcole2.jpg\' alt=\'Conférence l entrepreneuriat à l école\'/>
                </div>
                </div>',
                'thumb' => '20180614-entrep-ecole/thumb-entrepreneuriat-ecole.jpg',
                'cover' => '20180614-entrep-ecole/cover-entrepreneuriat-ecole.jpg',
                'keywords' => 'entrepreneuriat, education',
                'created_at' => '2018-06-29 12:54:45',
                'updated_at' => '2018-09-19 14:29:31',
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Smarteo était au rendez-vous annuel de l\'innovation du 15 au 17 juin #VIVATECH',
                'slug' => 'smarteo-tait-au-rendez-vous-annuel-de-l-innovation-du-15-au-17-juin-vivatech',
                'date' => '20180524',
                'reference' => '180524-vivatech',
                'event_date' => 'Du 24/05/2018 au 26/05/2018',
                'description' => 'Nous étions présents aux salon Vivatech, événement exceptionnel dédié à l\'innovation. Les robots étaient à l\'honneur, de plus en plus sophistiqués, et de plus en plus intelligents. Nous avons aussi et surtout parlé d\'éducation : comment préparer nos enfants à la nouvelle ère digitale ? Et comment promouvoir une culture digitale qui favorise la créativité et l\'épanouissement de nos enfants?',
                'short' => 'Nous étions présents aux salon Vivatech, événement exceptionnel dédié à l\'innovation. Les robots étaient à l\'honneur, de plus en plus sophistiqués, et de plus en plus intelligents. Nous avons aussi et surtout parlé d\'éducation : comment préparer nos enfants à la nouvelle ère digitale ? Et comment promouvoir une culture digitale qui favorise la créativité et l\'épanouissement de nos enfants?
                <br>
                <a href="https://vivatechnology.com/" class="">https://vivatechnology.com/</a>',
                'detailed' => 'Nous étions présents aux salon Vivatech, événement exceptionnel dédié à l\'innovation. 
                <br>
                Les robots étaient à l\'honneur, de plus en plus sophistiqués, et de plus en plus intelligents. 
                Nous avons aussi et surtout parlé d\'éducation : comment préparer nos enfants à la nouvelle ère digitale ? 
                Et comment promouvoir une culture digitale qui favorise la créativité et l\'épanouissement de nos enfants?
                <br>
                <a href="https://vivatechnology.com/">https://vivatechnology.com/</a>

                <br>

                <div class="row text-center">
                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-0.jpg\'  alt=\'Participation de Smarteo à vivatech\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-1.jpg\' alt=\'Participation de Smarteo à vivatech Atistes et Robots\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-2.jpg\' alt=\'Participation de Smarteo à vivatech Maria Montessori\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-3.jpg\' alt=\'Participation de Smarteo à vivatech Conférence Education\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-4.jpg\' alt=\'Participation de Smarteo à vivatech Robot humanoide 1\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-5.jpg\' alt=\'Participation de Smarteo à vivatech Robot humanoide 2\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-6.jpg\' alt=\'Participation de Smarteo à vivatech Robot humanoide Espace\'/>
                </div>

                <div class="col-md-6 pt-3">
                <img class=\'img-fluid w-100\' src=\'https://www.smarteo.co/img/actualites/20180524-vivatech/vivatech-7.jpg\' alt=\'Participation de Smarteo à vivatech Robot humanoide 3\'/>
                </div>
                </div>',
                'thumb' => '20180524-vivatech\thumb-vivatech.jpg',
                'cover' => '20180524-vivatech\cover-vivatech.jpg',
                'keywords' => 'vivatech, robots, innovation',
                'created_at' => '2018-06-29 13:00:28',
                'updated_at' => '2018-09-20 06:13:41',
            ),
6 => 
array (
    'id' => 7,
    'title' => 'Smarteo sera présent au salon KidExpo du 30/10/2018 au 04/11/2018 à Paris Porte de Versailles',
    'slug' => 'smarteo-sera-pr-sent-au-salon-kidexpo-du-30-10-2018-au-04-11-2018-paris-porte-de-versailles',
    'date' => '20181030',
    'reference' => '181030-kidexpo',
    'event_date' => 'Du 30/10/2018 au 04/11/2018',
    'description' => 'Nous vous donnons rdv au salon KIDEXPO à Paris du 30/10/2018 au 04/11/2018 pour présenter nos derniers kits de robotique éducative. Intégrés à l\'espace d\'innovation "Le Lab By KidExpo", nous animerons également un atelier de robotique pour enfants.',
    'short' => 'Nous serons présents au Salon KIDEXPO à Paris du 30/10/2018 au 04/11/2018, pour présenter nos derniers kits de robotique éducative. Nous serons intégrés à <b>"Le Lab by Kidexpo"</b>, un espace 2.0 dédié à la présentation de projets innovants du secteur de la famille, et nous animerons également un atelier de robotique pour enfants : 
    <br/>
    <a href="http://www.kidexpo.com/site/FR/Visiter/Liste_exposants/Zoom_Exposant,C67501,I67502,Zoom-87d57528361443a3ab117104184fd609,SType-LETTRE,Lettre-S.htm?KM_Session=43e5427b407669c73816db0b46e89d5f">SMARTEO SERA PRESENT A KIDEXPO</a>',
    'detailed' => 'Nous serons présents au Salon KIDEXPO à Paris du 30/10/2018 au 04/11/2018, pour présenter nos derniers kits de robotique éducative. Nous serons intégrés à <b>"Le Lab by Kidexpo"</b>, un espace 2.0 dédié à la présentation de projets innovants du secteur de la famille, et nous animerons également un atelier de robotique pour enfants : 
    <br/>
    <a href="http://www.kidexpo.com/site/FR/Visiter/Liste_exposants/Zoom_Exposant,C67501,I67502,Zoom-87d57528361443a3ab117104184fd609,SType-LETTRE,Lettre-S.htm?KM_Session=43e5427b407669c73816db0b46e89d5f">SMARTEO SERA PRESENT A KIDEXPO</a>

    <h3>
    Venez voter pour nous. Le projet lauréat reçoit le Prix de l\'Innovation by Kidexpo. 
    </h3>

    <h2 class="mt-3">Animation : Le robot explorateur de l\'espace</h2>
    <p class="lead">Pour explorer une planète lointaine, les humains ont décidé d’envoyer un robot équipé de capteurs. Ce robot est commandé à distance, il doit exécuter un parcours défini par les enfants, contourner les obstacles, et réaliser un certain nombre d’autres défis. Cette animation sera l’occasion pour les enfants pour découvrir quelques notions clé de la robotique et de la programmation. En fonction de l’âge des enfants, une initiation à la programmation sera faite en utilisant une application dédiée (pour les plus jeunes, 7 à 9 ans) ou en utilisant un langage de codage par blocks (similaire à Scratch) pour les plus âgés. Les enfants seront initiés aux structures logiques de base (conditions, boucles) et les utiliseront pour réaliser leurs défis.</p>',
    'thumb' => '20181030-kidexpo/thumb-kidexpo.jpg',
    'cover' => '20181030-kidexpo/cover-kidexpo.jpg',
    'keywords' => 'education, kidexpo,robotique éducative, robot, ateliers, programmation, enfants, scratch, mbot, marty, ezrobot',
    'created_at' => '2018-09-19 14:44:53',
    'updated_at' => '2018-09-20 05:09:12',
),
7 => 
array (
    'id' => 8,
    'title' => 'MakerFaire Paris à la Cité des Sciences : Nous serons aussi au rendez-vous !',
    'slug' => 'makerfaire-paris-la-cit-des-sciences-nous-serons-aussi-au-rendez-vous-',
    'date' => '20181101',
    'reference' => '181101-makerfaire',
    'event_date' => 'Du 23/11/2018 au 25/11/2018',
    'description' => 'Maker Faire est à la fois une fête de la science, une foire populaire et un événement de référence pour l’innovation. Il regroupe stands de démonstration, ateliers, spectacles et conférences autour des thèmes de la créativité, de la fabrication, des cultures Do It Yourself et maker.',
    'short' => 'Maker Faire est à la fois une fête de la science, une foire populaire et un événement de référence pour l’innovation. Totalement unique, ce concept regroupe stands de démonstration, ateliers de découverte, spectacles et conférences autour des thèmes de la créativité, de la fabrication, des cultures Do It Yourself et maker. 
    <br/>
    <a href="https://paris.makerfaire.com/">https://paris.makerfaire.com/</a>',
    'detailed' => 'Maker Faire est à la fois une fête de la science, une foire populaire et un événement de référence pour l’innovation. Totalement unique, ce concept regroupe stands de démonstration, ateliers de découverte, spectacles et conférences autour des thèmes de la créativité, de la fabrication, des cultures Do It Yourself et maker. 
    <br/>
    <a href="https://paris.makerfaire.com/">https://paris.makerfaire.com/</a>',
    'thumb' => '20181124-makerfaire/thumb-makerfaire.jpg',
    'cover' => '20181124-makerfaire/makerfaire.jpg',
    'keywords' => 'education, makerfaire,diy,robotique éducative, robot, ateliers, programmation, enfants, scratch, mbot, marty, ezrobot',
    'created_at' => '2018-09-20 05:22:09',
    'updated_at' => '2018-11-28 21:03:59',
),
8 => 
array (
    'id' => 9,
    'title' => 'Retour en images - StartupForKids 2018 à l\'école 42',
    'slug' => 'retour-en-images-startupforkids-2018-l-cole-42',
    'date' => '20181124',
    'reference' => '181124-s4k18',
    'event_date' => 'Du 24/11/2018 au 26/11/2018',
    'description' => 'Cette édition de StartupForKids dans les locaux de l’école 42 était très riche et variée. Pendant 3 jours, nous étions heureux d’animer des ateliers d’initiation au code et à la robotique et de partager l’enthousiasme des enfants et des grands pour les robots.',
    'short' => 'Cette édition de StartupForKids dans les locaux de l’école 42 était très riche et variée. Pendant 3 jours, nous étions heureux d’animer des ateliers d’initiation au code et à la robotique et de partager l’enthousiasme des enfants et des grands pour les robots.',
    'detailed' => 'L’édition de StartupForKids dans les locaux de l’école 42 était très spéciale et nous avons partagé l’enthousiasme des jeunes et des parents pour les robots. 
    Lundi dernier était dédié aux écoles. Pas moins que 12 groupes scolaires d’élèves du CP à la CM1 se sont succédé avec leurs enseignants pour découvrir, tester, et coder avec nos robots.
    <br/>


    <div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
    <li data-target="#carousel" data-slide-to="0" class="active"></li>
    <li data-target="#carousel" data-slide-to="1" ></li>
    <li data-target="#carousel" data-slide-to="2"></li>
    <li data-target="#carousel" data-slide-to="3"></li>
    <li data-target="#carousel" data-slide-to="4"></li>
    <li data-target="#carousel" data-slide-to="5"></li>
    <li data-target="#carousel" data-slide-to="6"></li>
    <li data-target="#carousel" data-slide-to="7"></li>
    </ol>
    <div class="carousel-inner">
    <div class="carousel-item active">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/1.jpg" alt="Photo S4k18 1">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/2.jpg" alt="Photo S4k18 2">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/3.jpg" alt="Photo S4k18 3">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/4.jpg" alt="Photo S4k18 4">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/5.jpg" alt="Photo S4k18 5">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/6.jpg" alt="Photo S4k18 6">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/7.jpg" alt="Photo S4k18 7">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="https://www.smarteo.co/img/actualites/20181124-s4k-ecole42/8.jpg" alt="Photo S4k18 8">
    </div>
    </div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Précédent</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Suivant</span>
    </a>
    </div>
    <br/>',
    'thumb' => '20181124-s4k-ecole42/thumb-s4k18.jpg',
    'cover' => '20181124-s4k-ecole42/cover-s4k18.jpg',
    'keywords' => 'education, makerfaire,diy,robotique éducative, robot, ateliers, programmation, enfants, scratch, mbot, marty, ezrobot,s4k18,startupforkids',
    'created_at' => '2018-11-28 17:15:43',
    'updated_at' => '2018-11-28 20:19:32',
),
9 => 
array (
    'id' => 10,
    'title' => 'Retour en images - MakerFaire - Le grand festival des makers à la Cité des Sciences',
    'slug' => 'retour-en-images-makerfaire-le-grand-festival-des-makers-la-cit-des-sciences',
    'date' => '20181123',
    'reference' => '181123-makerfaire18',
    'event_date' => 'Du 23/11/2018 au 25/11/2018',
    'description' => 'Plus qu\'un grand festival, MakerFaire est l\'EVENEMENT phare des Makers et de l\'innovation. Nous avons eu l\'énorme plaisir cette année de faire partie des entreprises innovantes qui sont venues présenter leurs idées.
    Enfants et grands ont adoré nos robots et nos tapis de jeu et se sont lancés au codage ludique en s\'amusant.',
    'short' => 'Plus qu\'un grand festival, MakerFaire est l\'EVENEMENT phare des Makers et de l\'innovation. Nous avons eu l\'énorme plaisir cette année de faire partie des entreprises innovantes qui sont venues présenter leurs idées.',
    'detailed' => 'Plus qu\'un grand festival, MakerFaire est l\'EVENEMENT phare des Makers et de l\'innovation. Nous avons eu l\'énorme plaisir cette année de faire partie des entreprises innovantes qui sont venues présenter leurs idées.
    Enfants et grands ont adoré nos robots et nos tapis de jeu et se sont lancés au codage ludique en s\'amusant.
    <br/>


    <div id="carousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
    <li data-target="#carousel" data-slide-to="0" class="active"></li>
    <li data-target="#carousel" data-slide-to="1" ></li>
    <li data-target="#carousel" data-slide-to="2"></li>
    <li data-target="#carousel" data-slide-to="3"></li>
    <li data-target="#carousel" data-slide-to="4"></li>
    <li data-target="#carousel" data-slide-to="5"></li>
    <li data-target="#carousel" data-slide-to="6"></li>
    <li data-target="#carousel" data-slide-to="7"></li>
    </ol>
    <div class="carousel-inner">
    <div class="carousel-item active">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/2.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/3.jpg" alt="Third slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/4.jpg" alt="Third slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/5.jpg" alt="Third slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/6.jpg" alt="Third slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/7.jpg" alt="Third slide">
    </div>
    <div class="carousel-item">
    <img class="d-block w-100" src="/img/actualites/makerfaire18/8.jpg" alt="Third slide">
    </div>
    </div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Précédent</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Suivant</span>
    </a>
    </div>
    <br/>',
    'thumb' => '20181124-makerfaire/thumb-mkfr18.jpg',
    'cover' => '20181124-makerfaire/cover-mkfr18.jpg',
    'keywords' => 'education, makerfaire,diy,robotique éducative, robot, ateliers, programmation, enfants, scratch, mbot, marty, ezrobot,s4k18,startupforkids',
    'created_at' => '2018-11-28 21:00:56',
    'updated_at' => '2018-12-30 11:31:12',
),
10 => 
array (
    'id' => 11,
    'title' => 'BETT SHOW 2019 : La rencontre internationale des EdTech à Londres',
    'slug' => 'bett-show-2019-la-rencontre-internationale-des-edtech-londres',
    'date' => '20190125',
    'reference' => '190125-betshow19',
    'event_date' => 'Du 23/01/2019 au 26/01/2019',
    'description' => 'Plus qu\'un grand festival, MakerFaire est l\'EVENEMENT phare des Makers et de l\'innovation. Nous avons eu l\'énorme plaisir cette année de faire partie des entreprises innovantes qui sont venues présenter leurs idées.
    Enfants et grands ont adoré nos robots et nos tapis de jeu et se sont lancés au codage ludique en s\'amusant.',
    'short' => 'Plus qu\'un grand festival, MakerFaire est l\'EVENEMENT phare des Makers et de l\'innovation. Nous avons eu l\'énorme plaisir cette année de faire partie des entreprises innovantes qui sont venues présenter leurs idées.',
    'detailed' => '<div class=\'container\'><div class=\'row\'><div class=\'col-md-12 text-center\'>
    <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fsmarteo.co%2Fposts%2F2164620390534525&width=500" width="500" height="718" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

    <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fsmarteo.co%2Fvideos%2F403367113735943%2F&show_text=1&width=560" width="560" height="407" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>

    <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fsmarteo.co%2Fvideos%2F231604794389010%2F&show_text=1&width=560" width="560" height="407" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe></div></div></div>',
    'thumb' => '20190123-bett\thumb-bett-show.jpg',
    'cover' => '20190123-bett\bett-show-banner.png',
    'keywords' => 'bett,diy,robotique éducative, robot, ateliers, programmation, enfants, scratch, mbot, marty, ezrobot,s4k18,startupforkids',
    'created_at' => '2019-01-30 13:05:14',
    'updated_at' => '2019-01-30 13:17:21',
),
));


}
}
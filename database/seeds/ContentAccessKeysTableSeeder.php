<?php

use Illuminate\Database\Seeder;

class ContentAccessKeysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('content_access_keys')->insert([[
            'title' 		=> 'kit mbot',
            'value' 		=> 'axb33c',
            'startdate' 	=> '2018-07-01',
            'maxdate' 		=> '2018-07-31',
            'contentids' 	=> '1;2;3'
        ]
    	]);
    }
}

<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('subjects')->insert([
        [
            'title' 		=> 'J\'ai besoin d\'aide pour commencer avec mBot',
            'slug' 			=> 'j-ai-besoin-d-aide-pour-commencer-avec-mbot',
            'keywords' 		=> 'kew1,kew2,kew3',
            'tags'          => 'tag1,tag2,tag3',     
            'status' 	    => 'live',
            'thumb'         => 'mbot.jpg',
            'details'		=> '{"rows" : [
            {
                "type":"html",
                "value": "Vous pouvez commencer par les contenus suivants :"
                },{
                    "type":"list_contents",
                    "value": [
                    "compil-ressources-mbot",
                    "mbot-cleaner"
                    ]
                    },{
                        "type":"form_contact",
                        "value":"form_contact"                                        
                    }
                ]}'
            
        ],[
                'title'         => 'J\'ai besoin d\'aide pour commencer avec mBot',
                'slug'          => 'j-ai-besoin-d-aide-pour-commencer-avec-mbot-1',
                'keywords'      => 'kew1,kew2,kew3',
                'tags'          => 'tag1,tag2,tag3',     
                'status'        => 'live',
                'thumb'         => 'mbot-cleaner.jpg',
                'details'       => '{"rows" : [
                {
                    "type":"html",
                    "value": "Vous pouvez commencer par les <b>contenus suivants</b> :"
                    },{
                        "type":"list_contents",
                        "value": [
                        "compil-ressources-mbot",
                        "mbot-cleaner"
                        ]
                        },{
                            "type":"form_contact",
                            "value":"form_contact"                                        
                        }
                    ]}'
        ]
        ]);
    }
}

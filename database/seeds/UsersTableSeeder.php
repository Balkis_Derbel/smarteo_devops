<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([[
            'name' => 'admino',
            'email' => 'a@dm.ino',
            'password' => bcrypt('123456'),
            'role' => 'admin',
            'confirmed' => 1
        ],[
            'name' => 'userino',
            'email' => 'u@sr.ino',
            'password' => bcrypt('123456'),
            'role' => 'guest',
            'confirmed' => 0
        ]
    	]);
    }
}

<?php

use Illuminate\Database\Seeder;

class UserContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_contents')->insert([[
            'userid' 	=> '1',
            'contentid' => '1',
            'keyid' 	=> '1',
            'maxdate' 	=> '2018-07-31'
        ]
        ,
        [
            'userid' 	=> '1',
            'contentid' => '2',
            'keyid' 	=> '1',
            'maxdate' 	=> '2018-07-31'
        ]
        ,
        [
            'userid'    => '1',
            'contentid' => '3',
            'keyid'     => '1',
            'maxdate'   => '2018-07-31'
        ]
        ,
        [
            'userid'    => '1',
            'contentid' => '4',
            'keyid'     => '1',
            'maxdate'   => '2018-07-31'
        ]
        ,
        [
            'userid'    => '1',
            'contentid' => '5',
            'keyid'     => '1',
            'maxdate'   => '2018-07-31'
        ]
    	]);
    }
}

<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('orders')->insert([
    		[
                'user_id' 			=> 1,
                'product_reference'	=> 'kit-mbot',
                'order_type'		=>'pay_once',
                'amount'      		=> 1500,
                'status'			=>'archived'
            ]
            ,
            [
                'user_id' 			=> 1,
                'product_reference'	=> 'kit-dash',
                'order_type'		=>'subscribe',
                'amount'      		=> 1500,
                'status'			=>'created'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'payment_success'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'subscription_success'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_delivered'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
            ,
            [
                'user_id'           => 1,
                'product_reference' => 'kit-dash',
                'order_type'        =>'subscribe',
                'amount'            => 1500,
                'status'            =>'product_sent'
            ]
        ]);
    }
}

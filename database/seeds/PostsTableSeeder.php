<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$posts = array([
    		'date'  		=> '20180209',
    		'title' 		=> 'TOTEM#EdTech',
    		'reference' 	=> 'post-1',
    		'thumb' 		=> 'fresque_edtech.jpg',
    		'keywords' 		=> 'these are keywords',
    		'ownerid' 		=> '1',
    		
    	],[
    		'date'  		=> '20180209',
    		'title' 		=> 'TOTEM#EdTech 2',
    		'reference' 	=> 'post-2',
    		'thumb' 		=> 'fresque_edtech.jpg',
    		'keywords' 		=> 'these are keywords',
    		'ownerid' 		=> '1',
    	]);

    	foreach($posts as $item)
    	{
    		\DB::table('posts')->insert([[
    			'title' 		=> $item['title'],
    			'url' 			=> preg_replace("![^a-z0-9]+!i", "-", $item['title']),
    			'date' 			=> $item['date'],
    			'reference' 	=> $item['reference'],
    			'thumb' 			=> $item['thumb'],
    			'keywords' 		=> $item['keywords'],
    			'ownerid' 		=> $item['ownerid']
    		]]);


/*            $table->integer('ownerid');
            $table->string('author',250)->default('');
            $table->string('author_thumb',250)->default('');*/

        }

    }
}

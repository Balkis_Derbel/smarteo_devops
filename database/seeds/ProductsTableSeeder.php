<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            [
                'reference'=>'kit-mbot',
                'type'=>'product',
                'title'=>'mBot : le robot voiture à construire et à programmer',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'kit-robotique-mbot-le-robot-voiture-a-construire-et-a-programmer-avec-scratch',
                'description'=>'Mbot est une voiture robot dont on peut programmer le trajet et le comportement. C\'est l\'outil idéal pour initier les enfants au codage. L\'enfant commence par assembler son robot ensuite il le programme en utilisant un langage par blocks similaire à  Scratch, ou encore en Arduino C.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>3,
                'selling_price'=>'99.9',
                'subscr_amount'=>'6',
                'status'=>'live',
                'tags'=>'mbot'
            ],[
                'reference'=>'kit-dash',
                'type'=>'product',
                'title'=>'DASH : le premier véritable ami robot d\'un enfant',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'dash-le-premier-veritable-ami-robot-dun-enfant',
                'description'=>'Vous cherchez un nouvel outil pédagogique pour enseigner la robotique et la programmation aux enfants ? Pour développer leur curiosité pour les nouvelles technologies ? Le robot Dash de MakeWonder est exactement ce qu\'il vous faut !',
                'thumb'=>'thumb.jpg',
                'nb_images'=>3,
                'selling_price'=>'181.5',
                'subscr_amount'=>'9',
                'status'=>'live',
                'tags'=>'dash'
            ],[
                'reference'=>'kit-dash-dot',
                'type'=>'product',
                'title'=>'Ensemble de robots wonderworkshop Dash et Dot',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'ensemble-robots-wonderworkshop-dash-et-dot',
                'description'=>'Dash et Dot suscitent la créativité chez les enfants de tous âges et les incitent à  s\'amuser et à  acquérir de nouvelles compétences grâce à  la robotique. Les enfants peuvent imaginer de nouvelles aventures tout en apprenant à  coder à  tout niveau.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>3,
                'selling_price'=>'181.5',
                'subscr_amount'=>'9',
                'status'=>'hidden',
                'tags'=>'dash,dot'                
            ],[
                'reference'=>'kit-matatalab',
                'type'=>'product',
                'title'=>'MATATALAB : le nouveau robot de codage sans écrans',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-le-nouveau-robot-de-codage-sans-ecrans',
                'description'=>'Apprenez à  coder sans tablette ni ordinateur ! Matatalab est un nouveau robot de codage pratique destiné aux enfants âgés de 4 à  9 ans et qui se base sur un concept original pour apprendre à  commander un robot-voiture en toute simplicité.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'144',
                'subscr_amount'=>'9',
                'status'=>'live',
                'tags'=>'matatalab'                
            ],[
                'reference'=>'kit-matatalab-artist',
                'type'=>'product',
                'title'=>'Matatalab : Le Artist Addon',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-artist-addon',
                'description'=>'Poussez les limites de ce que vous pouvez faire avec Matatalab. En utilisant cet ensemble de commandes supplémentaires, vous pouvez coder le robot pour effectuer des déplacements plus complexes et pour dessiner différentes formes géométriques.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>2,
                'selling_price'=>'29.9',
                'subscr_amount'=>'2.4',
                'status'=>'live',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-matatalab-musician',
                'type'=>'product',
                'title'=>'Matatalab : Le Musician Addon',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-musician-addon',
                'description'=>'Cet ensemble de commandes étend les possibilités de jeu avec Matatalab en permettant de programmer de la musique. Et si vous codiez une danse sur les rythmes du morceau que vous aurez choisi ou inventé ?',
                'thumb'=>'thumb.jpg',
                'nb_images'=>2,
                'selling_price'=>'49.9',
                'subscr_amount'=>'3.6',
                'status'=>'live',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-matatalab-mat-fairytale',
                'type'=>'product',
                'title'=>'Matatalab : La carte de jeu \'Contes de Fées\'',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-carte-contes-de-fees',
                'description'=>'Une foràªt magique, une princesse somnambule, un lutin voleur et un dragon qui veille sur son trésor. Le tapis Conte de fée vous propose de jolies histoires qui font travailler l\'imagination.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'28',
                'subscr_amount'=>'2.4',
                'status'=>'live',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-matatalab-mat-space',
                'type'=>'product',
                'title'=>'Matatalab : La carte de jeu \'Système Solaire\'',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-carte-systeme-solaire',
                'description'=>'Qui n\'a jamais ràªvé de visiter entre les planà¨tes de notre systà¨me solaire ? Embarquez dans la fusée Matatalab! Une bonne occasion pour apprendre quelques notions d\'astrophysique dà¨s le plus jeune âge.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'28',
                'subscr_amount'=>'2.4',
                'status'=>'live',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-matatalab-mat-pirats',
                'type'=>'product',
                'title'=>'Matatalab : La carte de jeu \'L\'Ile Aux Pirates\'',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-carte-tresor-perdu-pirates',
                'description'=>'La carte au trésor vous a amené sur une île perdue et pleine de dangers. Il faudra suivre les instructions avec précision si vous voulez trouver le coffre des pirates.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'28',
                'subscr_amount'=>'2.4',
                'status'=>'live',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-matatalab-mat-healthy-eating',
                'type'=>'product',
                'title'=>'Matatalab : La carte de jeu \'Alimentation Equilibrée\'',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-carte-alimentation-equilibree',
                'description'=>'Grâce à  ce tapis, les enfants vont à  apprendre à  gérer une alimentation saine et équilibrée. Ils peuvent suivre des menus tout faits ou composer leur propre menu.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'28',
                'subscr_amount'=>'2.4',
                'status'=>'live',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-matatalab-mat-preschool',
                'type'=>'product',
                'title'=>'Matatalab : La carte de jeu \'Chiffres et des formes\'',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'matatalab-carte-chiffres-et-formes',
                'description'=>'Matatalab : La carte de jeu "Chiffres et des formes"',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'28',
                'subscr_amount'=>'4.5',
                'status'=>'hidden',
                'tags'=>'matatalab'
            ],[
                'reference'=>'kit-ezrobot',
                'type'=>'product',
                'title'=>'EZrobot le robot humanoide',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'ezrobot-le-robot-humanoide',
                'description'=>'Apprenez-en plus sur la technologie avec les robots Clip\'n\'Play! Ces robots possà¨dent les capacités de vision, la reconnaissance vocale, les animations et l\'intelligence artificielle. Il peuvent àªtre programmés en utilisant Blockly, RoboScratch, .NET et plus encore!',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'450',
                'subscr_amount'=>'24',
                'status'=>'live',
                'tags'=>'ezrobot'
            ],[
                'reference'=>'micro-bit-go',
                'type'=>'product',
                'title'=>'BBC Micro : Bits Go',
                'subtitle'=>'A partir de 8 ans',
                'slug'=>'micro-bit',
                'description'=>'La carte BBC micro:bit est une carte micro-contrôleur qui a été conçue — notamment par la BBC — dans un objectif pédagogique. Elle se programme à l\'aide de différentes interfaces et différents langages, permettant à des élèves de tous niveaux d’aborder la robotique et d’interagir avec le monde réel.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>3,
                'selling_price'=>'24.99',
                'subscr_amount'=>'0',
                'status'=>'live',
                'tags'=>'micro:bit'
            ],[
                'reference'=>'ozobot-bit-2-0',
                'type'=>'product',
                'title'=>'Ozobot Bit 2.0',
                'subtitle'=>'A partir de 8 ans',
                'slug'=>'robot-ozobot-bit-2-0',
                'description'=>'Ozobot est un très petit robot qui roule et qui possède sous sa base des capteurs lui permettant de prendre des informations de couleurs.
                    Il obéit à des instructions codées par des successions de couleurs. On peut ainsi programmer ce robot simplement en dessinant des lignes de couleur sur du papier.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>2,
                'selling_price'=>'59.5',
                'subscr_amount'=>'6.0',
                'status'=>'live',
                'tags'=>''                
            ],[                
                'reference'=>'ozobot-evo',
                'type'=>'product',
                'title'=>'Ozobot Evo',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'robot-ozobot-evo',
                'description'=>'Ozobot est un très petit robot qui roule et qui possède sous sa base des capteurs lui permettant de prendre des informations de couleurs.
                    Il obéit à des instructions codées par des successions de couleurs. On peut ainsi programmer ce robot simplement en dessinant des lignes de couleur sur du papier.',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'119.0',
                'subscr_amount'=>'8.0',
                'status'=>'live',                
                'tags'=>''
            ],[
            /*    'reference'=>'robot-sphero',
                'type'=>'product',
                'title'=>'Le robot Sphero',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'robot-sphero',
                'description'=>'[description à ajouter ici]',
                'thumb'=>'thumb.jpg',
                'nb_images'=>1,
                'selling_price'=>'450',
                'subscr_amount'=>'0',
                'status'=>'live',
            ],[   */             
                'reference'=>'smarteo-test',
                'type'=>'product',
                'title'=>'Smarteo Test',
                'subtitle'=>'A partir de 6 ans',
                'slug'=>'smarteo-test',
                'description'=>'TEST',
                'thumb'=>'NULL',
                'nb_images'=>3,
                'selling_price'=>'1',
                'subscr_amount'=>'1',
                'status'=>'hidden',
                'tags'=>''
            ]
        ]);
}
}

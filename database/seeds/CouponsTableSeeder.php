<?php

use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('coupons')->insert([[
            'email'         => 'ALL_USERS',
            'coupon'        => 'OFFRE_HIVER',
            'token_rent'    => 'offre-hiver',
            'token_sell'    => '',
            'discount_rent' =>  15.0,
            'discount_sell' =>  0.0, 
            'status'        =>  'live'
        ]
        ]);
    }
}

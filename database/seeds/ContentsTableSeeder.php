<?php

use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('contents')->insert([[
            'reference' 	=> 'generic-content',
            'title' 		=> 'Generic Content',
            'description'      => '',
            'slug' 			=> 'generic-content',
            'keywords' 		=> 'kew1,kew2,kew3',
            'tags'          => '',     
            'tags_1'        => 'micro:bit,codage,initiation',                               
            'thumb' 		=> 'overview.jpg',
            'integration'   => 'local',
            'parameters'    => '',
            'access_type' 	=> 'public',
            'status' 		=> 'inprogress'
        ]
        ,
        [
            'reference' 	=> 'kit_mbot_0',
            'title' 		=> 'Initiation Kit MBOT',
            'description'      => '',
            'slug' 			=> 'initiation-kit-mbot',
            'keywords' 		=> 'kit,mbot',
            'tags'          => '',  
            'tags_1'        => 'micro:bit,codage,initiation',                                  
            'thumb' 		=> 'mbot.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type' 	=> 'restricted'/*restricted*/,
            'status' 		=> 'inprogress'
        ]
        ,
        [
            'reference'     => 'premiers-pas-avec-ez-robot',
            'title'         => 'Premiers pas avec EZRobot',
            'description'   => '',
            'slug'          => 'premiers-pas-avec-ez-robot',
            'keywords'      => 'EZRobot',
            'tags'          => 'ezrobot, initiation',
            'tags_1'        => 'présentation, fonctions de base',                        
            'thumb'         => 'ezrobot.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'compil-ressources-mbot',
            'title'         => 'Compilation de ressources pour bien commencer avec le robot mBot',
            'description'   => 'Pour bien commencer avec le robot mBot',
            'slug'           => 'compilation-de-ressources-pour-bien-commencer-avec-le-robot-mbot',
            'keywords'      => '',
            'tags'          => 'mbot,robotique,codage',
            'tags_1'        => 'tutoriels,makeblocks,capteurs',                        
            'thumb'         => 'mbot.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'makewonder-ideas',
            'title'         => 'Découvrez ce que vous pouvez faire avec le robot DASH',
            'description'   => '',
            'slug'           => 'idees-activites-robot-dash-makewonder',
            'keywords'      => '',
            'tags'          => 'robot,DASH',
            'tags_1'        => 'idees,créativité,makeblocks',                        
            'thumb'         => 'makewonder.jpg',
            'integration'   => 'href',
            'parameters'    => 'url>https://www.makewonder.com/play/ideas/',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'micro-bit-guide',
            'title'         => 'Commencez votre aventure micro:bit avec MakeCode',
            'description'   => '',
            'slug'           => 'commencez-votre-aventure-micro-bit-avec-make-code',
            'keywords'      => '',            
            'tags'          => 'micro:bit,codage,initiation',
            'tags_1'        => 'makeCode',            
            'thumb'         => 'micro-bit-guide.jpg',
            'integration'   => 'href',
            'parameters'    => 'url>https://microbit.org/fr/guide/',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'micro-python-guide',
            'title'         => 'Commencez votre aventure micro:bit avec MicroPython',
            'description'   => '',
            'slug'           => 'commencez-votre-aventure-micro-bit-avec-micro-python',
            'keywords'      => '',            
            'tags'          => 'micro:bit,codage,initiation',
            'tags_1'        => 'python',            
            'thumb'         => 'micro-python-guide.jpg',
            'integration'   => 'href',
            'parameters'    => 'url>https://microbit.org/fr/guide/python/',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'robot-dash-first-steps',
            'title'         => 'Ressources périscolaires : Dash, Dot et leurs applications',
            'description'   => '',
            'slug'           => 'ressources-periscolaires-dash-dot-applications',
            'keywords'      => '',            
            'tags'          => 'robot, DASH, codage,initiation',
            'tags_1'        => 'makeWonder,Blockly',            
            'thumb'         => 'robot-dash.jpg',
            'integration'   => 'href',
            'parameters'    => 'url>https://recitpresco.qc.ca/fr/pages/dash-dot',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'mb-game-1',
            'title'         => 'BBC Micro:Bit - Pierre Feuille Ciseaux',
            'description'   => 'Développer un pierre feuille ciseaux',
            'slug'           => 'mb-1',
            'keywords'      => '',            
            'tags'          => '',
            'tags_1'        => 'micro:bit,codage,initiation',                        
            'thumb'         => 'compil-ressources-mbot.png',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'inprogress'
        ]
        ,
        [
            'reference'     => 'mbot-avancer-reculer',
            'title'         => 'Faire avancer et reculer le mbot',
            'description'   => '',
            'slug'          => 'faire-avancer-reculer-mbot',
            'keywords'      => 'mbot, tutoriels',            
            'tags'          => 'mbot, programmation',
            'tags_1'        => 'mblock',                        
            'thumb'         => 'mbot-0.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'mbot-tourner',
            'title'         => 'Faire tourner le robot mbot',
            'description'   => '',
            'slug'          => 'faire-tourner-mbot',
            'keywords'      => 'mbot, tutoriels',            
            'tags'          => 'mbot, programmation',
            'tags_1'        => 'mblock',                        
            'thumb'         => 'mbot-0.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'mbot-cleaner',
            'title'         => 'mBot : le robot nettoyeur',
            'description'   => '',
            'slug'          => 'installer un robot mBot',
            'keywords'      => 'mbot, tutoriels',            
            'tags'          => 'mbot, programmation',
            'tags_1'        => 'mblock',                        
            'thumb'         => 'mbot-cleaner.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'mbot-installation',
            'title'         => 'Premiers pas avec le robot mBot',
            'description'   => '',
            'slug'          => 'introduction-robot-mbot',
            'keywords'      => 'mbot, installation',            
            'tags'          => 'mbot, installation',
            'tags_1'        => 'mblock',                        
            'thumb'         => 'mbot-0.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready'
        ]
        ,
        [
            'reference'     => 'dash-installation',
            'title'         => 'Premiers pas avec le robot Dash',
            'description'   => '',
            'slug'          => 'introduction au robot Dash',
            'keywords'      => 'dash, tutoriels',            
            'tags'          => '',
            'tags_1'        => '',                        
            'thumb'         => 'robot-dash.jpg',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready' 
        ]
        ,
        [
            'reference'     => 'dash-abeille',
            'title'         => 'DASH et la danse des abeilles',
            'description'   => '',
            'slug'          => 'dash-et-la-danse-des-abeilles',
            'keywords'      => 'dash, tutoriels',            
            'tags'          => 'tutoriel,DASH,initiation',
            'tags_1'        => 'Blockify',                        
            'thumb'         => 'dash-abeille.png',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready' 
        ]
        ,
        [
            'reference'     => 'microbit-installation',
            'title'         => 'Introduction à la carte microbit',
            'description'   => '',
            'slug'          => 'introduction-carte-microbit',
            'keywords'      => 'microbit, tutoriels',            
            'tags'          => '',
            'tags_1'        => '',                        
            'thumb'         => 'microbit.png',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready' 
        ]
        ,
        [
            'reference'     => 'microbit-radio',
            'title'         => 'Programmer une carte microbit',
            'description'   => '',
            'slug'          => 'programmer une carte microbit',
            'keywords'      => 'microbit, tutoriels',            
            'tags'          => '',
            'tags_1'        => '',                        
            'thumb'         => 'microbit.png',
            'integration'   => 'local',
            'parameters'    => '',            
            'access_type'   => 'public',
            'status'        => 'ready' 
        ]
    ]);
    }
}

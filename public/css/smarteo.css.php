<?php

ini_set("zlib.output_compression", "Off");
ob_start("ob_gzhandler");
ob_start("compress");

// seconds, minutes, hours, days
$expires = 60*60*1;

header('Content-type: text/css');
header("Pragma: public");
header("Cache-Control: maxage=".$expires);
header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');

function compress($buffer) {
    /* remove comments */
    $bbuffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    /* remove tabs, spaces, newlines, etc. */
    $bbuffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
    return $buffer;
}

/* Bootstrap core CSS */
include("../vendor/bootstrap-4.0.0/css/bootstrap.min.css");
include("../vendor/bootstrap-4.0.0/css/bootstrap-grid.css");
include("../vendor/simple-line-icons/css/simple-line-icons.css");

include("../vendor/shards/shards.css");

/* Custom styles */          
include("smarteo.css");

/* Styles for webchat */
include("webchat.css");

/* MailChimp */
include("mailchimp.css");

ob_end_flush();

?>
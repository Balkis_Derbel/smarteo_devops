mysqldump: [Warning] Using a password on the command line interface can be insecure.
-- MySQL dump 10.13  Distrib 5.7.24, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: smrt
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact_requests`
--

DROP TABLE IF EXISTS `contact_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_requests`
--

LOCK TABLES `contact_requests` WRITE;
/*!40000 ALTER TABLE `contact_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_access_keys`
--

DROP TABLE IF EXISTS `content_access_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_access_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `startdate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `maxdate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contentids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_access_keys`
--

LOCK TABLES `content_access_keys` WRITE;
/*!40000 ALTER TABLE `content_access_keys` DISABLE KEYS */;
INSERT INTO `content_access_keys` VALUES (1,'kit mbot','axb33c','2018-07-01','2018-07-31','1;2;3',NULL,NULL);
/*!40000 ALTER TABLE `content_access_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_type` enum('public','restricted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'restricted',
  `status` enum('ready','inprogress','deleted') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inprogress',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1,'generic-content','Generic Content','','generic-content','kew1,kew2,kew3',NULL,'overview.jpg','public','inprogress',NULL,NULL),(2,'kit_mbot_0','Initiation Kit MBOT','','initiation-kit-mbot','kit,mbot',NULL,'robot-mbot-initiation.png','public','inprogress',NULL,NULL),(3,'premiers-pas-avec-ez-robot','Premiers pas avec EZRobot','','premiers-pas-avec-ez-robot','EZRobot',NULL,'kit-ezrobot.png','public','inprogress',NULL,NULL),(4,'compil-ressources-mbot','Compilation de ressources','Pour bien commencer avec le robot mBot','compilation-de-ressources-pour-bien-commencer-avec-le-robot-mbot','',NULL,'compil-ressources-mbot.png','public','inprogress',NULL,NULL);
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `coupon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `token_rent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `token_sell` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `discount_rent` double NOT NULL DEFAULT '0',
  `discount_sell` double NOT NULL DEFAULT '0',
  `kit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` enum('live','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
INSERT INTO `coupons` VALUES (1,'ALL_USERS','OFFRE_HIVER','offre-hiver','',15,0,'','live',NULL,NULL);
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djtts`
--

DROP TABLE IF EXISTS `djtts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djtts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_kids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djtts`
--

LOCK TABLES `djtts` WRITE;
/*!40000 ALTER TABLE `djtts` DISABLE KEYS */;
/*!40000 ALTER TABLE `djtts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giveaways`
--

DROP TABLE IF EXISTS `giveaways`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giveaways` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `usertoken` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `invitertoken` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `newsletter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giveaways`
--

LOCK TABLES `giveaways` WRITE;
/*!40000 ALTER TABLE `giveaways` DISABLE KEYS */;
/*!40000 ALTER TABLE `giveaways` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kits`
--

DROP TABLE IF EXISTS `kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `price_rent` double NOT NULL DEFAULT '0',
  `price_sell` double NOT NULL DEFAULT '0',
  `status` enum('live','hidden') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'hidden',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kits`
--

LOCK TABLES `kits` WRITE;
/*!40000 ALTER TABLE `kits` DISABLE KEYS */;
INSERT INTO `kits` VALUES (1,'kit-mbot','kit-robotique-mbot-le-robot-voiture-a-construire-et-a-programmer-avec-scratch','kit-robotique-mbot-le-robot-voiture-a-construire-et-a-programmer-avec-scratch',5.4,99.9,'live',NULL,NULL),(2,'kit-dash','dash-le-premier-veritable-ami-robot-dun-enfant','dash-le-premier-veritable-ami-robot-dun-enfant',9,181.5,'live',NULL,NULL),(3,'kit-dash-dot','ensemble-robots-wonderworkshop-dash-et-dot','ensemble-robots-wonderworkshop-dash-et-dot',9,181.5,'live',NULL,NULL),(4,'kit-matatalab','matatalab-le-nouveau-robot-de-codage-sans-ecrans','matatalab-le-nouveau-robot-de-codage-sans-ecrans',7.5,116,'live',NULL,NULL),(5,'kit-matatalab-artist','matatalab-artist-addon','matatalab-artist-addon',7.5,116,'live',NULL,NULL),(6,'kit-matatalab-musician','matatalab-musician-addon','matatalab-musician-addon',7.5,116,'live',NULL,NULL),(7,'kit-matatalab-mat-fairytale','Matatalab : La carte de jeu \"Contes de Fées\"','matatalab-carte-contes-de-fees',4.5,28,'live',NULL,NULL),(8,'kit-matatalab-mat-space','Matatalab : La carte de jeu \"Système Solaire\"','matatalab-carte-systeme-solaire',4.5,28,'live',NULL,NULL),(9,'kit-matatalab-mat-pirats','Matatalab : La carte de jeu \"A la chasse du trésor perdu des pirates\"','matatalab-carte-tresor-perdu-pirates',4.5,28,'live',NULL,NULL),(10,'kit-matatalab-mat-healthy-eating','Matatalab : La carte de jeu \"Alimentation Equilibrée\"','matatalab-carte-alimentation-equilibree',4.5,28,'live',NULL,NULL),(11,'kit-matatalab-mat-preschool','Matatalab : La carte de jeu \"Chiffres et des formes\"','matatalab-carte-chiffres-et-formes',4.5,28,'live',NULL,NULL),(12,'kit-ezrobot','ezrobot-le-robot-humanoide','ezrobot-le-robot-humanoide',24,450,'live',NULL,NULL);
/*!40000 ALTER TABLE `kits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_uri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remote_addr` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bot` tinyint(1) NOT NULL DEFAULT '0',
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-21 15:09:40'),(2,'127.0.0.1','http://localhost:83/smrt-2/public','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-21 15:28:13'),(3,'127.0.0.1','http://localhost:83/smrt-2/public','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'',0,'','2019-06-22 08:38:12'),(4,'127.0.0.1','http://localhost:83/smrt-2/public/login','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'',0,'','2019-06-22 08:38:31'),(5,'127.0.0.1','http://localhost:83/smrt-2/public/login','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'',0,'','2019-06-22 08:38:42'),(6,'127.0.0.1','http://localhost:83/smrt-2/public/mon-espace-personnel','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:38:46'),(7,'127.0.0.1','http://localhost:83/smrt-2/public','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:38:53'),(8,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:39:03'),(9,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/create','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:39:09'),(10,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/create','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:40:13'),(11,'127.0.0.1','http://localhost:83/smrt-2/public/admin','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:40:52'),(12,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:40:58'),(13,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:41:08'),(14,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:42:21'),(15,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:54:42'),(16,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:54:47'),(17,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:55:14'),(18,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:55:15'),(19,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:57:12'),(20,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:57:20'),(21,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:57:21'),(22,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 08:57:28'),(23,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:05:46'),(24,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:05:49'),(25,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:05:50'),(26,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:10:20'),(27,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:12:32'),(28,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:12:41'),(29,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:12:41'),(30,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:12:50'),(31,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:17:35'),(32,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:17:39'),(33,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:17:40'),(34,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:17:47'),(35,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:19:03'),(36,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:19:05'),(37,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:19:06'),(38,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:19:18'),(39,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:19:51'),(40,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:20:05'),(41,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:20:05'),(42,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:20:13'),(43,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:25:21'),(44,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:25:25'),(45,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:25:25'),(46,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:25:32'),(47,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:26:20'),(48,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:26:24'),(49,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:26:25'),(50,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:26:30'),(51,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:48:41'),(52,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:48:50'),(53,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:49:55'),(54,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:50:01'),(55,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:50:01'),(56,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:50:09'),(57,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:51:52'),(58,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:51:55'),(59,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:51:55'),(60,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:55:10'),(61,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:55:14'),(62,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:55:14'),(63,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:55:22'),(64,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:56:44'),(65,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:56:44'),(66,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:59:51'),(67,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:59:55'),(68,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 09:59:56'),(69,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:00:07'),(70,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:14:57'),(71,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:15:00'),(72,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:15:00'),(73,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:15:08'),(74,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:40:26'),(75,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:40:29'),(76,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:40:29'),(77,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:40:33'),(78,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:42:48'),(79,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:42:50'),(80,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:42:51'),(81,'127.0.0.1','http://localhost:83/smrt-2/public/admin/logs?l=eyJpdiI6IlRsVjlUMitBNHVtMHY0eGwrSkdURmc9PSIsInZhbHVlIjoiOWxkZTZhclpoblFGcXIxY204TTlEYXVreFlNSFVzZ0dvXC9Gb2JPSXV3R2M9IiwibWFjIjoiMDlmYWJkNTAyYTEzNzk4NzRiMzc2YzQxY2RiYWRjODIzMjIxMjRkOWE2NTE3M2NjMTdlZjRlNTJjMGFhZTBjYSJ9','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:42:55'),(82,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup','GET','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:50:14'),(83,'127.0.0.1','http://localhost:83/smrt-2/public/admin/backup/export_db','POST','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',0,'admino',1,'','2019-06-22 10:50:18');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_06_18_124740_create_logs_table',1),(4,'2018_06_18_125435_create_contact_requests_table',1),(5,'2018_06_26_213154_create_news_table',1),(6,'2018_07_06_092013_create_contents_table',1),(7,'2018_07_06_092650_create_kits_table',1),(8,'2018_07_06_145556_create_user_contents_table',1),(9,'2018_07_06_151225_create_user_infos_table',1),(10,'2018_07_08_093345_create_content_access_keys_table',1),(11,'2018_10_15_124813_create_posts_table',1),(12,'2018_11_08_113756_create_orders_table',1),(13,'2018_11_08_113850_create_order_status_table',1),(14,'2018_11_11_151052_create_coupons_table',1),(15,'2019_01_17_103947_create_giveaways_table',1),(16,'2019_03_26_113429_create_djtts_table',1),(17,'2019_04_28_223722_add_confirmation',1),(18,'2019_05_20_134744_create_partner_discounts_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `event_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `short` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `detailed` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `image` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `keywords` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `news_title_unique` (`title`),
  UNIQUE KEY `news_url_unique` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'TOTEM#EdTech','TOTEM-EdTech','20180209','180209-totem-edtech','09/02/2018','A l\'ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L\'individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.','A l\'ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L\'individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.\r\n            <br>\r\n            <a href=\"https://blog.lehub.bpifrance.fr/edtech-retour-images/\">https://blog.lehub.bpifrance.fr/edtech-retour-images/</a>','A l\'ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L\'individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.\r\n            <br>\r\n            <a href=\"https://blog.lehub.bpifrance.fr/edtech-retour-images/\">https://blog.lehub.bpifrance.fr/edtech-retour-images/</a>','fresque_edtech.jpg','these are keywords',NULL,NULL),(2,'TOTEM#EdTech 2','TOTEM-EdTech-2','20180209','180209-totem-edtech-2','09/02/2018','A l\'ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L\'individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.','A l\'ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L\'individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.\r\n            <br>\r\n            <a href=\"https://blog.lehub.bpifrance.fr/edtech-retour-images/\">https://blog.lehub.bpifrance.fr/edtech-retour-images/</a>','A l\'ère de l’économie de la connaissance, la technologie a un rôle clé à jouer pour dynamiser l’éducation et la formation et mettre entre les mains de leurs acteurs des solutions efficaces. L\'individualisation de la formation, le développement de soft skills ou encore l’employabilité sont quelques objectifs que servent les EdTech.\r\n            <br>\r\n            <a href=\"https://blog.lehub.bpifrance.fr/edtech-retour-images/\">https://blog.lehub.bpifrance.fr/edtech-retour-images/</a>','fresque_edtech.jpg','these are keywords',NULL,NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '99',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kit_reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `kit_label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `kit_price` int(11) NOT NULL DEFAULT '0',
  `latest_status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner_discounts`
--

DROP TABLE IF EXISTS `partner_discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner_discounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `discount_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `discount` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner_discounts`
--

LOCK TABLES `partner_discounts` WRITE;
/*!40000 ALTER TABLE `partner_discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `partner_discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `date` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `detailed` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `thumb` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `keywords` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ownerid` int(11) NOT NULL,
  `author` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `author_thumb` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_url_unique` (`url`),
  UNIQUE KEY `posts_reference_unique` (`reference`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'TOTEM#EdTech','TOTEM-EdTech','20180209','post-1','','fresque_edtech.jpg','these are keywords',1,'','',NULL,NULL),(2,'TOTEM#EdTech 2','TOTEM-EdTech-2','20180209','post-2','','fresque_edtech.jpg','these are keywords',1,'','',NULL,NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_contents`
--

DROP TABLE IF EXISTS `user_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `contentid` int(11) NOT NULL,
  `keyid` int(11) NOT NULL,
  `maxdate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_contents`
--

LOCK TABLES `user_contents` WRITE;
/*!40000 ALTER TABLE `user_contents` DISABLE KEYS */;
INSERT INTO `user_contents` VALUES (1,1,1,1,'2018-07-31',NULL,NULL),(2,1,2,1,'2018-07-31',NULL,NULL),(3,1,3,1,'2018-07-31',NULL,NULL),(4,1,4,1,'2018-07-31',NULL,NULL),(5,1,5,1,'2018-07-31',NULL,NULL);
/*!40000 ALTER TABLE `user_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_infos`
--

DROP TABLE IF EXISTS `user_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_infos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_infos`
--

LOCK TABLES `user_infos` WRITE;
/*!40000 ALTER TABLE `user_infos` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `role` enum('guest','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'guest',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admino','a@dm.ino','$2y$10$hyoWDZrGjhxDtqkbH/ntVuNxgNSbY/9Ex9/n8ozEi/1.Obd1Mvbu6','','','admin',NULL,NULL,NULL,1,NULL),(2,'userino','u@sr.ino','$2y$10$qmQu4gt6T8BtKwD12fuUh.wUCyp9mPQjNf5SQZYZM6yBV2/FM.G2i','','','guest',NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-22 12:50:19

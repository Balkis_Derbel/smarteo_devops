var slideIndex = 1;
var currentIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
	showDivs(slideIndex += n);
}

function currentDiv(n) {
	showDivs(slideIndex = n);
}

function showDivs(n) {
	var i;
	var x = document.getElementsByClassName("mySlides");
	var dots = document.getElementsByClassName("dots");

	transition=" ";
	if(slideIndex < currentIndex) {transition="w3-animate-left";}
	if(slideIndex > currentIndex) {transition="w3-animate-right";}

	if (n > x.length) {slideIndex = 1;}    
	if (n < 1) {slideIndex = x.length;}

	for (i = 0; i < x.length; i++) {
		x[i].style.display = "none";  
	}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" w3-red", "");
	}

	x[slideIndex-1].style.display = "block";  
	x[slideIndex-1].classList.remove('w3-animate-left');
	x[slideIndex-1].classList.remove('w3-animate-right');
	x[slideIndex-1].classList.add(transition);

	dots[slideIndex-1].className += " w3-red";

	document.getElementById("nextSlide").style.visibility="visible";
	document.getElementById("prevSlide").style.visibility="visible";

	if (slideIndex == 1) {document.getElementById("prevSlide").style.visibility="hidden";}
	if (slideIndex == x.length) {document.getElementById("nextSlide").style.visibility="hidden";}

	currentIndex = slideIndex;

	$("html, body").animate({ scrollTop: 0 }, "slow");
}
@extends('layouts.default')

@section('title','Challenge WLRCxSMARTEO : Le Voyage de Dash dans le Temps')

@section('description','Smarteo permet d\'apprendre en jouant, ou plutôt de jouer en apprenant. Coder un robot devient un jeu facile et amusant. Plusieurs modèles sont proposés, pour tous les âges, en vente ou en mode location')

@section('content')

<!-- Parents -->



<section class="bg-light">
	<div class="container">
		<div class="row pt-5">


			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\01.jpg')}}" alt="First slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\02.jpg')}}" alt="Second slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\03.jpg')}}" alt="Third slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\04.jpg')}}" alt="Fourth slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\05.jpg')}}" alt="Fifth slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\06.jpg')}}" alt="Sixth slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\07.jpg')}}" alt="Seventh slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\08.jpg')}}" alt="Eigth slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="{{URL::to('img\djtt\challenge-wlrc-smarteo\09.jpg')}}" alt="Nineth slide">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				<ol class="carousel-indicators" style="padding:5px;border-radius:5px;background:#2d90ad;">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
				</ol>
			</div>

		</div>


		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<h1 class="bd-title text-warning font-weight-bold">		
					Recevez par email le lien de téléchargement de la documentation complète du Challenge
				</h1>
			</div>
		</div>

		<div class="row pt-3 pb-5">
			<div class="col-md-3"></div>
			<div class="col-sm-6">


				{!! Form::open(['url' => 'challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps\en-savoir-plus']) !!}
				<div class="form box">

					<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
						{!! Form::text('name', null, ['class' => 'form-control w-100 name', 'placeholder' => 'Prénom, Nom']) !!}
						{!! $errors->first('name', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Email']) !!}
						{!! $errors->first('email', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('school') ? 'has-error' : '' !!}">
						{!! Form::text('school', null, ['class' => 'form-control mail', 'placeholder' => 'Institution']) !!}
						{!! $errors->first('school', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div style="text-align:center;">
						{{ Form::hidden('route', '/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/en-savoir-plus') }}
						{!! Form::submit('Envoyer', ['class' => 'btn btn-info btn-lg']) !!}
					</div>
				</div>
				{!! Form::close() !!}

			</div>
		</div>

	</div>


</section>





@stop
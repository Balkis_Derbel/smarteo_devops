@extends('layouts.default')


@section('content')

<section class="bg-light">

  <div class="container container-fluid pt-5 pb-5" style="width:100%;">


<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>        
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{url('img/djtt/winners/1.jpg')}}" alt="1">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{url('img/djtt/winners/2.jpg')}}" alt="2">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{url('img/djtt/winners/3.jpg')}}" alt="3">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{url('img/djtt/winners/4.jpg')}}" alt="4">
    </div>
        <div class="carousel-item">
      <img class="d-block w-100" src="{{url('img/djtt/winners/5.jpg')}}" alt="5">
    </div>
      </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

  </div>
</section>

@stop
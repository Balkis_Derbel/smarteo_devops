@extends('layouts.default')

@section('title','Conditions générales')


@section('content')

<section class="bg-light">

    <div class="masthead faq text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="description col-xl-9 mx-auto">
                    <h1>
                        Conditions générales
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container text-left">

        <div class="page-content">


            <div class="row">
                <div class="col-md-12 text-justify">
                    <h1 class="mt-3">Conditions de participation</h1>
                    <p class="lead">Ce challenge est ouvert à toute personne majeure résidant en France métropolitaine, à l'exclusion des membres du personnel de la Société Organisatrice. 
                        En cas de litige, un justificatif d’identité pourra être demandé.
                    La participation implique l’acceptation sans réserve du Règlement. </p>
                    
                    <h1 class="mt-3">Modalités</h1>
                    <p class="lead">Pour participer les candidats doivent remplir le formulaire prévu à cet effet à l'adresse : 
                        <a href='https://www.smarteo.co/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps'>
                           https://www.smarteo.co/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps
                       </a>
                   </p> 


                   <h1 class="mt-3">Consultation et acceptation du règlement</h1>
                   <p class="lead">
                    Le simple fait de participer entraîne l'acceptation entière et sans réserve du présent règlement.
                    La Société Organisatrice se réserve le droit, à tout moment et sans préavis ni obligation de justifier sa décision, d’écourter, de prolonger, de reporter ou d’annuler le challenge  ainsi que de modifier tout ou partie des conditions d’accès et/ou des modalités de mise en œuvre, sans que sa responsabilité ne puisse être engagée ni qu’aucune indemnité ne puisse lui être réclamée de ce fait.
                </p>

                <h1 class="mt-3">Conditions de participation</h1>
                <p class="lead">
                    L’Organisateur se réserve le droit de refuser toute inscription qu’il jugerait contraire ou sans lien avec le public et les objectifs visés par le challenge.
                    <br/>
                    <br/>
                    L’Organisateur se réserve le droit de demander à tout moment aux participants de présenter leur pièce d’identité. De plus, il se réserve le droit d’exclure du hackathon SMART BOT CAMP© toute personne troublant son déroulement.
                </p>


                <h1 class="mt-3">Outils, matériels, équipements informatiques et infrastructures techniques</h1>
                <p class="lead">
                    La liste du matériel et des équipements (notamment informatique, logiciel) dont devra être équipé le participant est indiquée sur le Site internet et dans la documentation du Challenge. 
                </p>


                <h1 class="mt-3">Engagements des participants </h1>
                <p class="lead">
                    Toute personne participant au Challenge s’engage à répondre au formulaire d’inscription en fournissant des informations sincères.
                    Les participants devront respecter scrupuleusement les règles de sécurité.
                    <br/><br/>
                    EN cas de prêt de matériel, les participants sont responsables du matériel qui leur est confié et dont ils conservent la garde, des éventuels dommages qui pourraient survenir sur le matériel et logiciels dans le cadre du Challenge.
                </p>

                <h1 class="mt-3">Données personnelles</h1>
                <p class="lead">
                    Les participants autorisent par avance du seul fait de leur participation, que les Organisateurs utilisent librement à des fins publicitaires ou promotionnelles quel qu’en soit le support, toutes les informations nominatives communiquées pour le compte de ceux-ci et sur tous supports.
                    Les données à caractère personnel recueillies vous concernant sont obligatoires et nécessaires pour le traitement de votre participation au jeu. Elles sont destinées aux Organisateurs, ou à des sous-traitants et/ou des prestataires pour des besoins de gestion.
                    <br>
                    <br>
                    Conformément à la réglementation en vigueur, les informations collectées sont destinées exclusivement aux Organisateurs et elles ne seront ni vendues, ni cédées à des tiers, de quelque manière que ce soit.
                    Chaque Participant au Jeu reconnaît avoir pris connaissance de l'intégralité des stipulations du Règlement et l'avoir par conséquent accepté en toute connaissance de cause.
                    <br>
                    <br>
                    Conformément à la loi « Informatique et Libertés » du 06/01/1978, vous disposez d'un droit d'accès, de rectification et de suppression de ces données ainsi que d’un droit de vous opposer à ce que ces données fassent l'objet d'un traitement en nous contactant par courrier à l’adresse suivante SMARTEO – 24 avenue Georges Clémenceau – 92330 SCEAUX ou par e-mail à l’adresse contact@smarteo.co.
                </p>





                <h1 class="mt-3">Accessibilité du règlement</h1>
                <p class="lead">
                    Le Règlement peut être consulté sur le site www.smarteo.co ou pourra être adressé par email à toute personne en faisant la demande.

                </p>





                <h1 class="mt-3">Loi applicable et opposabilité du règlement</h1>
                <p class="lead">
                    Le Règlement est soumis au droit français.
                    Le fait de s’inscrire et de participer au Challenge implique l’acceptation sans réserve du Règlement.


                </div>
            </div>
        </div>
    </div>
</section>

@stop
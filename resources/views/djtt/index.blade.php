@extends('layouts.default')

@section('title','Challenge WONDERWORKSHOPxSMARTEO : Un magnifique voyage dans le temps')

@section('description','Smarteo permet d\'apprendre en jouant, ou plutôt de jouer en apprenant. Coder un robot devient un jeu facile et amusant.')

@section('image', URL::to('img/djtt/masthead-wlrc.png'))

@section('content')

<!-- Parents -->

<section class="djtt bg-light">	

	<div class="masthead djtt text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<img src="{{URL::to('img/djtt/wlrc-logo.png')}}" alt="logo-wlrc" style="width:120px;margin-bottom:15px;" />
					<h2> Challenge SMARTEO/WONDERWORKSHOP : </h2>
					<h1>'Un magnifique voyage dans le temps'</h1>
					<a href="{{URL('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/equipe-gagnante')}}" class="btn btn-info">Découvrir le projet gagnant</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-light">
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<h1 class="bd-title text-warning font-weight-bold">	
					Smarteo et MakeWonder s'associent pour proposer ce challenge pour imaginer et créer le magnifique voyage du robot 'Dash' dans le temps
				</h1>


				<p class="lead text-justify">
					Ce Challenge est destiné aux enfants en école élémentaire, à partir du CE2. Il est réalisé en classe, avec l’enseignant, sous la forme d’un projet qui combine les aspects techniques, créatifs et interpersonnels. Il permet aux enfants de stimuler leur créativité et d’innover.
				</p>
			</div>

			<div class="col-md-12 text-center">
				<a href={{url('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/en-savoir-plus')}}
				class='btn btn-success btn-lg'>Je veux en savoir plus sur ce challenge</a>
			</div>

			<div class="col-md-12 text-center pt-3">
				<a href="https://www.makewonder.com/classroom/robotics-competition/"
				class='btn btn-warning btn-lg'>En savoir plus sur Wonder League Robotics Competition : WLRC</a>
			</div>


			<div class="col-md-12 pt-3">
				<p class="lead text-justify">
					La société Wonder Workshop suscite la créativité chez les enfants de tous âges et les incite à s'amuser en apprenant de nouvelles compétences avec la robotique. Les robots conçus par Wonder Workshop mettent le pouvoir du jeu entre leurs mains afin qu'ils puissent imaginer de nouvelles aventures tout en apprenant à coder, quel que soit leur niveau.
				</p>
			</div>

			<div class="col-md-12 text-center">

				<ul class="horizontal-slide">
					<li class="col-md-2"><img src="{{URL::to('img/djtt/image1.jpg')}}"/></li>
					<li class="col-md-2"><img src="{{URL::to('img/djtt/image2.png')}}"/></li>
					<li class="col-md-2"><img src="{{URL::to('img/djtt/image3.jpg')}}"/></li>
					<li class="col-md-2"><img src="{{URL::to('img/djtt/image4.jpg')}}"/></li>
					<li class="col-md-2"><img src="{{URL::to('img/djtt/image5.jpg')}}"/></li>
					<li class="col-md-2"><img src="{{URL::to('img/djtt/image6.jpg')}}"/></li>
				</ul>
			</div>
		</div>

<!--
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<h1 class="bd-title text-warning font-weight-bold">		
					Inscrivez maintenant votre équipe et relevez le challenge :
				</h1>
			</div>
		</div>


		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<h1 class="bd-title text-warning font-weight-bold">		
					Inscrivez maintenant votre équipe et relevez le challenge :
				</h1>
			</div>
		</div>

		<div class="row pt-3 pb-5">
			<div class="col-md-3"></div>
			<div class="col-sm-6">


				{!! Form::open(['url' => 'challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps']) !!}
				<div class="form box">

					<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
						{!! Form::text('name', null, ['class' => 'form-control w-100 name', 'placeholder' => 'Prénom, Nom']) !!}
						{!! $errors->first('name', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Email']) !!}
						{!! $errors->first('email', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">
						{!! Form::text('phone', null, ['class' => 'form-control mail', 'placeholder' => 'Numéro de téléphone']) !!}
						{!! $errors->first('phone', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('school') ? 'has-error' : '' !!}">
						{!! Form::text('school', null, ['class' => 'form-control mail', 'placeholder' => 'Institution']) !!}
						{!! $errors->first('school', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('projectname') ? 'has-error' : '' !!}">
						{!! Form::text('projectname', null, ['class' => 'form-control mail', 'placeholder' => 'Nom du projet']) !!}
						<small class="help-block text-info">Veuillez choisir un nom pour votre projet. Vous pourrez le changer ultérieurement.</small>
						{!! $errors->first('projectname', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('nb_kids') ? 'has-error' : '' !!}">
						{!! Form::text('nb_kids', null, ['class' => 'form-control mail', 'placeholder' => 'Nombre d\'enfants']) !!}
						{!! $errors->first('nb_kids', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('age') ? 'has-error' : '' !!}">
						{!! Form::text('age', null, ['class' => 'form-control mail', 'placeholder' => 'Moyenne d\'âge']) !!}
						{!! $errors->first('age', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('cb_leg') ? 'has-error' : '' !!}">
						{{ Form::checkbox('cbcond',null,null, array('id'=>'cbcond')) }}
						J'ai consulté <span class="text-info"> <a href={{url('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/conditions-generales')}}>les conditions générales de participation</a></span>
					</div>

					<div style="text-align:center;">
						{{ Form::hidden('route', '/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps') }}
						{!! Form::submit('Participer', ['class' => 'btn btn-info btn-lg']) !!}
					</div>
				</div>
				{!! Form::close() !!}

			</div>
		</div>
-->
	</div>


</section>





@stop
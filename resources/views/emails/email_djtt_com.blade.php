<!DOCTYPE html>

<html lang="fr">

<head>
  <meta charset="utf-8">
</head>

<body>

  <p>
    Chère Madame, Cher Monsieur,
    <br/><br/>
    J’ai le plaisir de vous contacter pour vous proposer de participer au Challenge de Robotique WonderWorkshop/Smarteo ‘Un Magnifique Voyage Dans Le Temps’, destiné aux enfants de 8 à 11 ans.
    <br/><br/>
    Smarteo est une startup française qui développe depuis 2018 des activités éducatives et créatives utilisant des robots programmables. Notre partenaire Wonder Workshop crée des robots éducatifs et organise depuis 4 ans la compétition internationale WLRC (63 pays participants en 2018).
    <br/><br/>
    Ce défi est destiné aux élèves des classes de CE2, CM1 et CM2 et utilise le robot éducatif ‘Dash’. Chaque équipe participante pourra créer un scénario et une maquette mettant en scène le robot dans une thématique historique de son choix, qui peut être liée aux sujets d'Histoire au programme de l'année.     <br/>
    Je vous prie de trouver tous les détails sur le site : <br/>
    https://www.smarteo.co/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps
    <br/><br/>
    L’objectif est de proposer aux enfants une expérience créative et stimulante. La partie ‘robotique’ peut se faire sur une demi-journée sous la forme d’une activité.
    <br/><br/>
    Je me tiens à votre disposition si vous souhaitez échanger pour avoir plus de détails sur les modalités pratiques pour la création de la maquette ou pour avoir les robots programmables. N’hésitez pas à m’appeler au 01 84 19 64 56, ou à nous indiquer l’heure idéale pour vous joindre : <br/>

    https://calendly.com/smarteo-co/entretien-telephonique
    <br/><br/>
    Vous souhaitant une bonne journée, et dans l’attente de votre retour,
    <br/><br/>
    Bien cordialement
    <br/><br/>
    Abdelmoughith Feki
    Fondateur<br/>
    +33 1 84 19 64 56<br/>
    +33 6 68 59 73 66<br/>
    www.smarteo.co - www.smartbotcamp.fr<br/>

  </p>
</body>

</html>
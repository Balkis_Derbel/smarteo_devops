<!DOCTYPE html>

<html lang="fr">

<head>

  <meta charset="utf-8">

</head>

<body style="font-family: Arial, sans-serif;padding-top: 20px;font-size:16px;">

  <div style="width:80%; margin:auto;border:1px solid #ccc;border-radius:15px;padding:15px;">

    <div style='width:100%; text-align:center;'>
      <img src="https://www.smarteo.co/img/logo-mascotte.png"/>
      <h1>Votre robot est en route !</h1>
    </div>

    <p>Bonjour {{ $user_name }},</p>
    <p> Nous vous remercions pour votre commande sur notre site www.smarteo.co et avons le plaisir de vous confirmer que votre commande <b>a été envoyée</b>.</p>

    <div style='width:100%; text-align:center;'>
      <h3>Vous pouvez suivre votre commande sur le lien suivant :</h3>

      <h2>
        <a href='https://www.laposte.fr/particulier/outils/suivre-vos-envois'>https://www.laposte.fr/particulier/outils/suivre-vos-envois</a>
      </h2>
      <h3> en indiquant votre code de suivi : </h3>


      <h1>{{ $colissimo_code }}</h1>
    </div>

    <div>
      <p style="margin-top:40px;">Une question ? Un commentaire ? Rendez-vous ici : 
        <a href='https://www.smarteo.co/contact'>https://www.smarteo.co/contact</a>
      </p>
    </div>


    <div style="margin-top:60px 0;width:100%;text-align:justify;color:#888;font-size:14px;">

      Conformément à la loi Informatique et Libertés du 6 janvier 1978, vous disposez d’un droit d’accès, de rectification et d’opposition aux données personnelles vous concernant. Il vous suffit de nous écrire à contact@smarteo.co ou par courrier à Smarteo SAS - 24 avenue Geroges Clemenceau - 92330 Sceaux. Plus d’informations sur la protection des données personnelles ici : <a href='https://www.smarteo.co/mentions-legales'>https://www.smarteo.co/mentions-legales</a>.
    </div>

    <div style="width:100%; text-align:center;margin-top:20px;">
      © 2018 Smarteo
    </div>



  </div>

</body>

</html>
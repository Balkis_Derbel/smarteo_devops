<!DOCTYPE html>

<html lang="fr">

<head>

  <meta charset="utf-8">

</head>

<body>

  <h2>Nous vous remercions pour votre intérêt pour le Challenge WonderWorkshop/Smarteo :</h2>

  <div style="width:100%;text-align:center;">
    <h1>Un Magnifique Voyage Dans Le Temps</h1>
  </div>

  <p>Nous vous prions de trouver ci-dessous le lien de téléchargement pour la documentation de cette compétition :</p>

  <div style="width:100%;text-align:center;">
    <a href="https://drive.google.com/file/d/1UQFmp6Dni09sU26g9_3uJVr96cXVPjIx">
      https://drive.google.com/file/d/1UQFmp6Dni09sU26g9_3uJVr96cXVPjIx
    </a>
  </div>

  <p>A très bientôt !</p>
  <p>L'équipe SMARTEO</p>
</body>

</html>
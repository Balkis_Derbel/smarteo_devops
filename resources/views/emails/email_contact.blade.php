<!DOCTYPE html>

<html lang="fr">

  <head>

    <meta charset="utf-8">

  </head>

  <body>

    <h2>Prise de contact sur mon beau site</h2>

    <p>Réception d'une prise de contact avec les éléments suivants :</p>

    <ul>

      <li><strong>Nom</strong> : {{ $data['name'] }}</li>

      <li><strong>Email</strong> : {{ $data['email'] }}</li>

      <li><strong>Numéro</strong> : {{ $data['phone'] }}</li>

      <li><strong>Message</strong> : {{ $data['textmsg'] }}</li>

      <li><strong>IP</strong> : {{ $data['ip'] }}</li>

    </ul>

  </body>

</html>
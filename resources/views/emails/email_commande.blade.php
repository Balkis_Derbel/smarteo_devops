<!DOCTYPE html>

<html lang="fr">

<head>

  <meta charset="utf-8">

</head>

<body>


  <div style="width:100%;">
    <div style="width:100%;text-align:center;font-weight: bold;">
      <h3>SMARTEO SAS</h3>
      <h4>24 Avenue Georges Clemenceau, 92330 Sceaux<br/>
        service-commercial@smarteo.co<br/>
        01.84.19.64.56
      </h4>
    </div>

    <div style="width:100%;text-align: center; margin-top: 50px;">
      <h1>Confirmation de votre commande</h1>
      <p>Pour : <br/>
        {{$order['name']}}<br/>
        {{$order['email']}}<br/>
      </p>
    </div>

    <div style="margin-top:50px;">
      <table cellspacing="0">
        <thead style="background-color: #eeeeee; border: none;">
          <tr>
            <th width="150px" height="35px" style="margin: 5px">Date</th>
            <th width="400px">Description</th>
            <th width="150px">Montant</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td height="45px">{{$order['date']}}</td>
            <td>{{$order['description']}} <br/>
              {{$order['options']}}
            </td>
            <td>
              {{$order['price']}} EUR TTC
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div style="width:100%;text-align: center; margin-top: 50px;margin-bottom:50px;">
      <h4>
        Vous pouvez consulter le détail de vos commandes et paiements dans votre espace personnel :
      </h4>
      <a href="https://www.smarteo.co/mon-espace-personnel">https://www.smarteo.co/mon-espace-personnel</a>
    </div>

  </div>

</body>

</html>
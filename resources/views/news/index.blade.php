@extends('layouts.default')

@section('title','Actualités - SMARTEO - Apprendre en s\'amusant avec des robots')

@section('description','Actualités et évènements de Smarteo. Nos participations aux évènements et conférences autours de l\'apprentissage de la robotique et de la programmation.')

@section('content')

<!-- actualités -->

<section class="bg-blue">

	<div class="container text-left pt-5 pb-5">
		@foreach($data as $item)
		@include('news/short', $item)
		@endforeach
	</div>
</section>

@stop
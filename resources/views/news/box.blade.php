<div class="col-md-6 p-5">
	<div class="card w-100 h-100 bg-white p-2 cursor-pointer smart-hover grow">
		<div class="w-100 h-100" onclick="location.href='{{url('actualites/'.$item['slug'])}}'">
			<img class="card-img-top" src="{{url('img/actualites/'.$item['thumb'])}}" alt="{{$item['reference']}}" />
			<div class="card-body">
				<span class="lead badge badge-success">{{$item['event_date']}}</span>
				<h4 class="card-title">{{$item['title']}}</h4>
			</div>
		</div>
	</div>
</div>
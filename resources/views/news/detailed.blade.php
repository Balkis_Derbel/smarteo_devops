@extends('layouts.default')

@section('title','Actualités - '.$data['title'])

@section('description',$data['description'])

@section('keywords',$data['keywords'])

@section('image', URL::to('img/actualites/'.$data['thumb']))

@section('content')

<section class="bg-clouds text-center">
    <div class="container pt-5 pb-5">

        <div class="row bg-clouds">

            <div class="col-md-10 offset-md-1">
                <img src="{{url('img/actualites/'.$data['cover'])}}" class="mw-100" alt="{{$data['reference']}}" />

                <div class="display-4 text-info font-weight-bold m-3">{{$data['title']}}</div>
                <div class="display-5"><span class="badge badge-info badge-lg rounded-0">{{$data['event_date']}}</span></diav>

                <div class="pt-3" style="text-align:justify;">
                    <p class="font-weight-light">
                        {!!$data['detailed']!!}
                    </p>
                </div>

                <div class="display-5 pt3">
                    <a href="{{url('actualites')}}" class="button btn-orange rounded">Voir toutes les actualités</a>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="bg-green text-center">
    <div class="container">
        <div class="row">
                @foreach($alldata as $item)
                @include('news/box', $item)
                @endforeach

        </div>

    </div>

</section>

@stop
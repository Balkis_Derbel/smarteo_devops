
<div class="row mt-5 p-5 bg-white cursor-pointer smart-hover grow" onclick="location.href='{{url('actualites/'.$item['slug'])}}'">
    <div class="col-md-5 order-md-1 pb-1">
        <img src="{{url('img/actualites/'.$item['thumb'])}}" class="w-100 rounded-circle" alt="{{$item['reference']}}" />
    </div>
    <div class="col-md-7 order-md-2">
        <span class="lead badge badge-success">{{$item['event_date']}}</span>
        <h4 style="color:#1289A7;font-weight:bold;margin: 10px 0;">{{$item['title']}}</h4>
        <p class="font-weight-light" style="text-align:justify;">
            {!!$item['short']!!}
        </p>
    </div>
</div>
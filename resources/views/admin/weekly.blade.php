@extends('layouts.default')

@section('title','ADMIN')

@section('css')
<style>
	.pagination > li > a, .pager > li > a {
		margin : 0 10px 0 0;
	}
	.dataTables_wrapper .row {
		width:100%;
		margin:10px;
	}
	thead input {
		width: 100%;
	}
</style>
@stop

@section('scripts')
<script src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js'></script>
<script>
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#filtredtable thead tr').clone(true).appendTo( '#filtredtable thead' );
    $('#filtredtable thead tr:eq(1) th').each( function (i) {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );

    	$( 'input', this ).on( 'keyup change', function () {
    		if ( table.column(i).search() !== this.value ) {
    			table
    			.column(i)
    			.search( this.value )
    			.draw();
    		}
    	} );
    } );

    var table = $('#filtredtable').DataTable( {
    	orderCellsTop: true,
    	fixedHeader: true
    } );
} );
</script>
@stop


@section('content')

<?php $page_title = 'ADMIN / Weekly Report';?>
@include('admin/header')

<section class="bg-light ptb-2">
	<div class="container">

		<table id = "filtredtable" class="table table-bordered w-100">
			<thead>
				<tr>
					<th>remote_addr</th>
					<th>week</th>
					<th>count</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $item)
				<tr>
					<td>{{$item->remote_addr}}</td>
					<td>{{$item->week_nb}}</td>
					<td>{{$item->cn}}</td>
				</tr>
				@endforeach
				<tfoot>
					<tr>
						<th>remote_addr</th>
						<th>week</th>
						<th>count</th>
					</tr>
				</tfoot>
			</table>

		</div>
	</section>


	@stop
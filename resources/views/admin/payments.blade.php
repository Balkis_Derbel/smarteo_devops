@extends('layouts.admin')

@section('title','ADMIN/PRODUCTS')

@section('content')


<?php $page_title = 'ADMIN / PAYMENTS';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Création ou MAJ</h2>
			</div>
		</div>

		{!! Form::open(['url' => 'admin/payments']) !!}
		<div class="row form">
			<div class="col-md-12">
				<div class="row" style="width:100%;display:inline-block;text-align:center;">
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>				
				</div>
			</div>

			<div class="col-md-12">
				<table class="table-sm w-100">
					<tr>
						<td class="w-30">{!! Form::label('ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td class="w-70">{!! Form::text('id',  $selected->id, ['class' => 'form-control']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('User ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('user_id', $selected->user_id, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Order ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('order_id', $selected->order_id, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Product Ref',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('product_ref', $selected->product_ref, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Amount',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('amount', $selected->amount, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

				</table>

			</div>
			<div class="col-md-12 text-center">
				{!! Form::submit('Envoyer !', ['class' => 'btn', 'name' => 'submitbutton']) !!}
			</div>
		</div>
		{!! Form::close() !!}

	</div>

	<hr/>

	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Données</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table id = "filtredtable" class="table table-bordered table-sm table-responsive">
					<thead>
						<th>ID</th>
						<th>User ID</th>
						<th>Order ID</th>
						<th>Product Ref</th>					
						<th>Amount</th>
						<th>Stripe Customer ID</th>
						<th>Stripe Transaction ID</th>
						<th>Created At</th>
					</thead>
					@foreach($data as $row)
					<tr>
						<td><a href="{{url('admin/payments/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
						<td>{{$row->user_id}}</td>
						<td>{{$row->order_id}}</td>
						<td>{{$row->product_ref}}</td>
						<td>{{$row->amount}}</td>
						<td>{{$row->stripe_customer_id}}</td>
						<td>{{$row->stripe_transaction_id}}</td>
						<td>{{$row->created_at}}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>

</section>

@stop



@extends('layouts.default')

@section('title','ADMIN/Users')

@section('css')
<style>
	.pagination > li > a, .pager > li > a {
		margin : 0 10px 0 0;
	}
	.dataTables_wrapper .row {
		width:100%;
		margin:10px;
	}
	thead input {
		width: 100%;
	}
</style>
@stop

@section('scripts')
<script src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js'></script>
<script>
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#filtredtable thead tr').clone(true).appendTo( '#filtredtable thead' );
    $('#filtredtable thead tr:eq(1) th').each( function (i) {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );

    	$( 'input', this ).on( 'keyup change', function () {
    		if ( table.column(i).search() !== this.value ) {
    			table
    			.column(i)
    			.search( this.value )
    			.draw();
    		}
    	} );
    } );

    var table = $('#filtredtable').DataTable( {
    	orderCellsTop: true,
    	fixedHeader: true
    } );
} );
</script>
@stop

@section('content')

<?php $page_title = 'ADMIN / Users';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container mt-3">
		<h1>Active user : {{$currentuser}}</h1>

		<div class="row">
			<table id = "filtredtable" class="table table-bordered w-100">
				<thead>
					<th>ID</th>
					<th>Name</th>
					<th>EMail</th>
					<th>Role</th>
					<th>Created_at</th>
					<th>Confirmed</th>
					<th>Action</th>					
				</thead>
				@foreach($data as $row)
				<tr>
					<td>{{$row->id}}</td>	
					<td>{{$row->name}}</td>	
					<td>{{$row->email}}</td>	
					<td>{{$row->role}}</td>	
					<td>{{$row->created_at}}</td>	
					<td>{{$row->confirmed}}</td>	
					<td>
						<a href="impersonate/{{$row->id}}" data-toggle='tooltip' data-placement='top' title='Impersonate' class='icon-style'><i class='fa fa-user-secret fa-lg'></i></a>

						<a href="users/mail/{{$row->id}}" data-toggle='tooltip' data-placement='top' title='Email' class='icon-style'><i class='fa fa-envelope-square fa-lg'></i></a>

						<a href="users/logs/{{$row->id}}" data-toggle='tooltip' data-placement='top' title='Email' class='icon-style'><i class='fa fa-google-wallet fa-lg'></i></a>
					</td>

				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
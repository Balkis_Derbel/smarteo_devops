@extends('layouts.admin')

@section('scripts')
<script>
  $(document).ready(function() {
    $('#table-interest').DataTable();
    $('#table-payment').DataTable();    
    $('#table-service-ongoing').DataTable();   
    $('#table-service-completed').DataTable();   
    $('#table-archived').DataTable();    
  } );
</script>
@stop

@section('content')

<?php $page_title = 'ADMIN / DASHBOARD / ORDERS';?>
@include('admin/header')



<div class="container mt-3">
  <div class="row">
    <div class="col-xs-12 ">
      <nav>
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" data-toggle="tab" href="#nav-interest" role="tab" aria-selected="true">
            <span class="badge badge-pill badge-primary">{{count($interest)}}</span>
            Interest
          </a>
          <a class="nav-item nav-link" data-toggle="tab" href="#nav-payment" role="tab" aria-selected="false">
            <span class="badge badge-pill badge-danger">{{count($payment)}}</span>
            Payment
          </a>
          <a class="nav-item nav-link" data-toggle="tab" href="#nav-service-ongoing" role="tab" aria-selected="false">
            <span class="badge badge-pill badge-warning">{{count($service_ongoing)}}</span>
            Service Ongoing
          </a>
          <a class="nav-item nav-link" data-toggle="tab" href="#nav-service-completed" role="tab" aria-selected="false">
            <span class="badge badge-pill badge-warning">{{count($service_completed)}}</span>
            Service Completed
          </a>
          <a class="nav-item nav-link" data-toggle="tab" href="#nav-archived" role="tab" aria-selected="false">
            <span class="badge badge-pill badge-secondary">{{count($archived)}}</span>
            Archived
          </a>
        </div>
      </nav>


      <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

        <div class="tab-pane fade show active" id="nav-interest" role="tabpanel" aria-labelledby="nav-home-tab">

          <!-- INTEREST -->
          <div class="row">
            <div class="col-md-12">

              <table id="table-interest" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <th>ID</th>
                  <th>Product Reference</th>
                  <th>Amount</th>         
                  <th>Order Type</th>
                  <th>Created At</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Status</th>           

                </thead>
                @foreach($interest as $row)
                <tr>
                  <td><a href="{{url('admin/orders/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
                  <td>{{$row->product_reference}}</td>
                  <td>{{$row->amount}}</td>
                  <td>{{$row->order_type}}</td>
                  <td>{{$row->created_at}}</td>
                  <td>{{$row->firstname}}</td>
                  <td>{{$row->lastname}}</td>
                  <td>{{$row->email}}</td>                 
                  <td>{{$row->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>

        </div>


        <div class="tab-pane fade" id="nav-payment" role="tabpanel">

          <!-- PAYMENT -->
          <div class="row">
            <div class="col-md-12">
              <table id="table-payment" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <th>ID</th>
                  <th>Product Reference</th>
                  <th>Amount</th>         
                  <th>Order Type</th>
                  <th>Created At</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Status</th>           

                </thead>
                @foreach($payment as $row)
                <tr>
                  <td><a href="{{url('admin/orders/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
                  <td>{{$row->product_reference}}</td>
                  <td>{{$row->amount}}</td>
                  <td>{{$row->order_type}}</td>
                  <td>{{$row->created_at}}</td>
                  <td>{{$row->firstname}}</td>
                  <td>{{$row->lastname}}</td>
                  <td>{{$row->email}}</td>                 
                  <td>{{$row->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>


        <div class="tab-pane fade" id="nav-service-ongoing" role="tabpanel" aria-labelledby="nav-about-tab">


          <!-- SERVICE ONGOING -->
          <div class="row">
            <div class="col-md-12">              
              <table id="table-service-ongoing" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <th>ID</th>
                  <th>Product Reference</th>
                  <th>Amount</th>         
                  <th>Order Type</th>
                  <th>Created At</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Status</th>           

                </thead>
                @foreach($service_ongoing as $row)
                <tr>                  
                  <td><a href="{{url('admin/orders/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
                  <td>{{$row->product_reference}}</td>
                  <td>{{$row->amount}}</td>
                  <td>{{$row->order_type}}</td>
                  <td>{{$row->created_at}}</td>
                  <td>{{$row->firstname}}</td>
                  <td>{{$row->lastname}}</td>
                  <td>{{$row->email}}</td>                 
                  <td>{{$row->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>


        </div>

        <div class="tab-pane fade" id="nav-service-completed" role="tabpanel">

          <!-- SERVICE END -->
          <div class="row">
            <div class="col-md-12">
              <table id="table-service-completed" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <th>ID</th>
                  <th>Product Reference</th>
                  <th>Amount</th>         
                  <th>Order Type</th>
                  <th>Created At</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Status</th>           

                </thead>
                @foreach($service_completed as $row)
                <tr>
                  <td><a href="{{url('admin/orders/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>                  
                  <td>{{$row->product_reference}}</td>
                  <td>{{$row->amount}}</td>
                  <td>{{$row->order_type}}</td>
                  <td>{{$row->created_at}}</td>
                  <td>{{$row->firstname}}</td>
                  <td>{{$row->lastname}}</td>
                  <td>{{$row->email}}</td>                 
                  <td>{{$row->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>


        </div>

        <div class="tab-pane fade" id="nav-archived" role="tabpanel">

          <!-- ARCHIVED -->
          <div class="row">
            <div class="col-md-12">
              <table id="table-archived" class="table table-striped table-bordered" style="width:100%">                
                <thead>
                  <th>ID</th>
                  <th>Product Reference</th>
                  <th>Amount</th>         
                  <th>Order Type</th>
                  <th>Created At</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Status</th>           

                </thead>
                @foreach($archived as $row)
                <tr>
                  <td><a href="{{url('admin/orders/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>                  
                  <td>{{$row->product_reference}}</td>
                  <td>{{$row->amount}}</td>
                  <td>{{$row->order_type}}</td>
                  <td>{{$row->created_at}}</td>
                  <td>{{$row->firstname}}</td>
                  <td>{{$row->lastname}}</td>
                  <td>{{$row->email}}</td>                 
                  <td>{{$row->status}}</td>
                </tr>
                @endforeach
              </table>
            </div>
          </div>


        </div>
      </div>

    </div>
  </div>
</div>
@stop
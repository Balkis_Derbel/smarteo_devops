@extends('layouts.default')

@section('title','ADMIN/Contents')

@section('content')


<?php $page_title = 'ADMIN / Contents';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">
		<h1>MAJ des prix des Contenus</h1>

		{!! Form::open(['url' => 'admin/contents']) !!}
		<div class="row form">

			<div class="row" style="width:100%;display:inline-block;text-align:center;">
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>
			</div>

			<table style="width:100%">
				<tr>
					<td>{!! Form::label('ID') !!}</td>
					<td>{!! Form::text('id',  $selected->id, ['class' => 'form-control', 'placeholder' => 'ID']) !!}</td>
				</tr>
				<tr>
					<td>{!! Form::label('Reference') !!}</td>
					<td>{!! Form::text('reference', $selected->reference, ['class' => 'form-control mt-2', 'placeholder' => 'Reference']) !!}</td>
				</tr>
				<tr>
					<td>{!! Form::label('Titre') !!}</td>
					<td>{!! Form::text('title', $selected->title, ['class' => 'form-control mt-2', 'placeholder' => 'lien pour la location (utilisé pour les liens de paiement)']) !!}</td>
				</tr>
				<tr>
					<td>{!! Form::label('URL (aut.)') !!}</td>
					<td>{!! Form::text('url', $selected->url, ['class' => 'form-control mt-2', 'placeholder' => 'lien pour l\'achat (utilisé pour les liens de paiement)']) !!}</td>
				</tr>
				<tr>
					<td>{!! Form::label('Prix d\'achat') !!}</td>
					<td>{!! Form::text('price_rent', $selected->price_rent, ['class' => 'form-control mt-2', 'placeholder' => 'Prix de location pour une semaine']) !!}</td>
				</tr>
				<tr>
					<td>{!! Form::label('Prix de vente') !!}</td>
					<td>{!! Form::text('price_sell', $selected->price_sell, ['class' => 'form-control mt-2', 'placeholder' => 'Prix de vente']) !!}</td>
				</tr>
				<tr>
					<td>{!! Form::label('Statut : live / hidden') !!}</td>
					<td>{!! Form::text('status', $selected->status, ['class' => 'form-control mt-2', 'placeholder' => 'Statut : live / hidden']) !!}</td>
				</tr>
			</table>
			{!! Form::submit('Envoyer !', ['class' => 'btn btn-info mt-2', 'name' => 'submitbutton']) !!}

		</div>
		{!! Form::close() !!}

	</div>

	<div class="container mt-3">
		<h1>Result</h1>

		<div class="row">
			<table class="table table-bordered table-sm">
				<thead>
					<th>ID</th>
					<th>Reference</th>
					<th>Title</th>
					<th>URL</th>					
					<th>Price-Rent</th>
					<th>Price-Sell</th>
					<th>Status</th>

				</thead>
				@foreach($data as $row)
				<tr>
					<td><a href="{{url('admin/kits/'.$row->id)}}">{{$row->id}}</a></td>
					<td>{{$row->reference}}</td>
					<td>{{$row->title}}</td>
					<td>{{$row->url}}</td>
					<td>{{$row->price_rent}}</td>
					<td>{{$row->price_sell}}</td>
					<td>{{$row->status}}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
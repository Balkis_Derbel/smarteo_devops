@extends('layouts.admin')

@section('title','ADMIN/News')

@section('content')

<?php $page_title = 'ADMIN / News';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container">

		<div class="row cursor-pointer">
			<div class="col-md-12" onclick="toggle_form_update()">
				<h2 class="text-white bg-secondary text-center p-1">Création ou MAJ</h2>
			</div>
		</div>

		{!! Form::open(['url' => 'admin/news']) !!}
		<div class="row form" id="form_update">
			<div class="col-md-12">
				<h4><a href="{{url('actualites/'.$selected->url)}}">{{url('actualites/'.$selected->url)}}</a></h4>
			</div>

			<div class="col-md-12">

				<div class="w-100 text-center">
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>
				</div>
			</div>

			<div class="col-md-12">
				<table class='w-100'>
					<tr>
						<td>{!! Form::label('id','ID', ['class' => ''])!!}</td>
						<td>{!! Form::text('id',  $selected->id, ['class' => 'form-control', 'placeholder' => 'ID']) !!}</td>
					</tr>
					<tr>
						<td>	{!! Form::label('Titre')!!}</td>
						<td>	{!! Form::text('title', $selected->title, ['class' => 'form-control', 'placeholder' => 'Titre']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('SLUG (aut.)')!!}</td>
						<td>			{!! Form::text('slug', $selected->slug, ['class' => 'form-control', 'placeholder' => 'URL (laisser vide pour générer automatiquement)']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Date')!!}</td>
						<td>{!! Form::text('date', $selected->date, ['class' => 'form-control', 'placeholder' => 'Date']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Reference')!!}</td>
						<td>{!! Form::text('reference', $selected->reference, ['class' => 'form-control', 'placeholder' => 'Reference']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Date de l\'évènement')!!}</td>
						<td>{!! Form::text('event_date', $selected->event_date, ['class' => 'form-control', 'placeholder' => 'Date de l\'évènement']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Description')!!}</td>
						<td>{!! Form::textarea ('description', $selected->description, ['class' => 'form-control mt-2', 'rows'  => 3,  'placeholder' => 'Description']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Version courte')!!}</td>
						<td>{!! Form::textarea ('short', $selected->short, ['class' => 'form-control mt-2', 'rows'  => 3,  'placeholder' => 'Version courte']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Version détaillée')!!}</td>
						<td>{!! Form::textarea ('detailed', $selected->detailed, ['class' => 'form-control mt-2', 'rows'  => 3,  'placeholder' => 'Version détaillée']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Thumb')!!}</td>
						<td>{!! Form::text('thumb', $selected->thumb, ['class' => 'form-control mt-2', 'placeholder' => 'Image Thumb']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Cover')!!}</td>
						<td>{!! Form::text('cover', $selected->cover, ['class' => 'form-control mt-2', 'placeholder' => 'Image Cover']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Keywords')!!}</td>
						<td>{!! Form::text('keywords', $selected->keywords, ['class' => 'form-control mt-2', 'placeholder' => 'Keywords']) !!}</td>
					</tr>
				</table>
			</div>

			<div class="col-md-12 text-center">
				{!! Form::submit('Envoyer !', ['class' => 'btn btn-lg rounded btn-info mt-2']) !!}
			</div>

		</div>
		{!! Form::close() !!}

	</div>


	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Données</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table id = "filtredtable" class="table table-bordered table-sm table-responsive">
					<thead>

						<th>ID</th>
						<th>Title</th>
						<th>Url</th>
						<th>Date</th>
						<th>Reference</th>

					</thead>
					@foreach($data as $row)
					<tr>
						<td><a href="{{url('admin/news/'.$row->id)}}"><span class="badge badge-info">{{$row->id}}</span>
							<img src="{{URL::to('img/actualites/'.$row->thumb)}}" style="width:120px;"/>
						</a></td>
						<td>{{$row->title}}</td>
						<td>{{$row->slug}}</td>
						<td>{{$row->date}}</td>
						<td>{{$row->reference}}</td>										
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>
</section>

@stop
@extends('layouts.default')

@section('title','ADMIN')

@section('content')


<?php $page_title = 'ADMIN / SQL Queries';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">

		{!! Form::open(['url' => 'admin/queries']) !!}
		<div class="row form">

			<!-- predefined queries -->
			<div class="col-md-12 radio">
				<label>
					<input name="query_id" type="radio" class="ace" value="0" {{$query_id === '0' ? 'checked':''}}/>
					<span class="lbl"> Use custom query </span>
				</label>
			</div>

			<div class="col-md-12 radio">
				<label>
					<input name="query_id" type="radio" class="ace" value="1" {{$query_id === '1' ? 'checked':''}}/>
					<span class="lbl"> select id, name, email, role, created_at, confirmed from users </span>
				</label>
			</div>

			<div class="col-md-12 radio">
				<label>
					<input name="query_id" type="radio" class="ace" value="2" {{$query_id === '3' ? 'checked':''}}/>
					<span class="lbl"> select * from contact_requests </span>
				</label>
			</div>

			<div class="col-md-12 radio">
				<label>
					<input name="query_id" type="radio" class="ace" value="3" {{$query_id === '3' ? 'checked':''}}/>
					<span class="lbl"> select * from logs where created_at > '<?=date("Y-m-d")?>' order by created_at desc  </span>
				</label>
			</div>

			<div class="col-md-12 radio">
				<label>
					<input name="query_id" type="radio" class="ace" value="4" {{$query_id === '4' ? 'checked':''}}/>
					<span class="lbl"> select remote_addr, count(remote_addr) as cn from logs where created_at> '<?=date("Y-m-d")?>' group by (remote_addr) order by cn desc  </span>
				</label>
			</div>


			{!! Form::textarea ('sql_query', $sql_query, ['class' => 'form-control mt-2', 'rows'  => 3,  'placeholder' => 'Custom query']) !!}


			{{ Form::hidden('route', '/home') }}
			{!! Form::submit('Envoyer !', ['class' => 'btn btn-info mt-2']) !!}

		</div>
		{!! Form::close() !!}

	</div>

	<div class="container mt-3">
		<h1>Result</h1>
		<h2>{{$sql_query}}</h2>

		<div class="row">
			<table class="table table-bordered table-sm">
				<thead>
					@foreach($cols as $col_name)
					<th>
						{{$col_name}}
					</th>
					@endforeach

				</thead>
				@foreach($data as $row)
				<tr>
					@foreach($row as $col)
					<td>{{ $col }}</td>
					@endforeach
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
@extends('layouts.default')

@section('title','ADMIN')

@section('content')

<section class="bg-light">

    <div class="masthead admin text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="description col-xl-9 mx-auto">
                    <div>
                        Espace ADMIN
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container ptb-4">

        <div class="row pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">DASHBOARD</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row justify-content-md-center pb-3">

            <div class="col-md-2 p-1">
                <a href="{{url('admin/dashboard/orders')}}" class="w-100 btn btn-lg rounded">   
                    Orders
                </a>
            </div>


            <div class="col-md-2 p-1">
                <a href="{{url('admin/weekly')}}" class="w-100 btn btn-lg rounded">Weekly Report</a> 
            </div>

        </div>

        <div class="row pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">General Database Reports and Metrics</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row justify-content-md-center">

            <div class="col-md-2 p-1">
                <a href="{{url('admin/queries')}}" class="w-100 btn btn-lg rounded">   
                    Queries 
                </a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/logs')}}" class="w-100 btn btn-lg rounded">Logs</a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/showdata')}}" class="w-100 btn btn-lg rounded btn-danger">Show Data</a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/weekly')}}" class="w-100 btn btn-lg rounded">Weekly Report</a> 
            </div>

        </div>

        <div class="row pt-3 pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">Data Elements</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row justify-content-md-center">
            <div class="col-md-2 p-1">
                <a href="{{url('admin/users')}}" class="w-100 btn btn-lg rounded">Users</a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/news')}}" class="w-100 btn btn-lg rounded">News</a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/contents')}}" class="w-100 btn btn-lg rounded">Contenus</a> 
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/subjects')}}" class="w-100 btn btn-lg rounded">Sujets</a> 
            </div>

        </div>

        <div class="row pt-3 pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">Coupons and Partner Programs</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row  justify-content-md-center">
            <div class="col-md-2 p-1">
                <a href="{{url('admin/coupons')}}" class="w-100 btn btn-lg rounded">Coupons</a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/discounts')}}" class="w-100 btn btn-lg rounded">Discounts</a>               
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/giveaway')}}" class="w-100 btn btn-lg rounded">Giveaway</a>                        
            </div>
        </div>

        <div class="row pt-3 pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">Subscriptions to Events</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row  justify-content-md-center">
            <div class="col-md-2 p-1">
                <a href="{{url('admin/djtt')}}" class="w-100 btn btn-lg rounded">Djtt</a>    

            </div>
        </div>

        <div class="row pt-3 pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">Shop</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row  justify-content-md-center">
            <div class="col-md-2 p-1">
                <a href="{{url('admin/products')}}" class="w-100 btn btn-lg rounded">Products</a>    
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/orders')}}" class="w-100 btn btn-lg rounded">Orders</a>    
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/payments')}}" class="w-100 btn btn-lg rounded">Payments</a>    
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/parcel-tracking')}}" class="w-100 btn btn-lg rounded">Parcel Tracking</a>
            </div>

        </div>

        <div class="row pt-3 pb-3">
            <div class="col"><hr></div>
            <div class="col-auto"><h3 class="text-info">UTILS</h3></div>
            <div class="col"><hr></div>
        </div>

        <div class="row  justify-content-md-center">
            <div class="col-md-2 p-1">
                <a href="{{url('admin/backup')}}" class="w-100 btn btn-lg rounded">Backup DB</a>
            </div>

            <div class="col-md-2 p-1">
                <a href="{{url('admin/cheat-sheet')}}" class="w-100 btn btn-lg rounded bg-info">Cheat Sheet</a>
            </div>

        </div>

    </div>

</section>

@stop
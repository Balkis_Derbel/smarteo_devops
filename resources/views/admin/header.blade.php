<section class="masthead admin text-white text-center">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="description col-xl-9 mx-auto">
                <a href="{{url('admin')}}" class="btn btn-info">ADMIN</a>
            
                <div>
                    <?=$page_title?>
                </div>
            </div>
        </div>
    </div>
</section>
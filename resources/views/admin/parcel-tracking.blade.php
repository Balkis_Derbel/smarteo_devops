@extends('layouts.admin')

@section('title','ADMIN/PARCEL-TRACKING')

@section('content')


<?php $page_title = 'ADMIN / PARCEL-TRACKING';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">
		<div class="row cursor-pointer" onclick="toggle_form_update()">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Création ou MAJ</h2>
			</div>
		</div>

		{!! Form::open(['url' => 'admin/parcel-tracking']) !!}
		<div class="row form" id="form_update">
			<div class="col-md-12">
				<div class="row" style="width:100%;display:inline-block;text-align:center;">
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>	
				</div>
			</div>

			<div class="col-md-12">
				<table class="table-sm w-100">

					<tr>
						<td>{!! Form::label('ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('id', $selected->id, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Order ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('orderid', $selected->orderid, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('First Name',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('firstname', $selected->firstname, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Last Name',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('lastname', $selected->lastname, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Phone',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('phone', $selected->phone, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Email',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('email', $selected->email, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Address',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('address', $selected->address, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('City',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('city', $selected->city, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('ZipCode',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('zipcode', $selected->zipcode, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					
					<tr>
						<td>{!! Form::label('Status',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('status', array(
							'prepared'	=> 'prepared',
							'sent'		=> 'sent',
							'delivered' => 'delivered',
							'sent_back' => 'sent_back',
							'received' 	=> 'received',
							'archived'	=> 'archived'
							),$selected->status, ['class' => 'form-control mt-2']) !!}</td>
						</tr>

						<tr>
							<td>{!! Form::label('Transporter',null, ['class' => 'w-100 text-right']) !!}</td>
							<td>{!! Form::text('transporter', $selected->transporter, ['class' => 'form-control mt-2']) !!}</td>
						</tr>

						<tr>
							<td>{!! Form::label('Tracking Id',null, ['class' => 'w-100 text-right']) !!}</td>
							<td>{!! Form::text('tracking_id', $selected->tracking_id, ['class' => 'form-control mt-2']) !!}</td>
						</tr>

						<tr>
							<td>{!! Form::label('Date Sent',null, ['class' => 'w-100 text-right']) !!}</td>
							<td>{!! Form::text('date_sent', $selected->date_sent, ['class' => 'form-control mt-2']) !!}</td>
						</tr>

						<tr>
							<td>{!! Form::label('Date Delivered',null, ['class' => 'w-100 text-right']) !!}</td>
							<td>{!! Form::text('date_delivered', $selected->date_delivered, ['class' => 'form-control mt-2']) !!}</td>
						</tr>

						<tr>
							<td>{!! Form::label('Comment',null, ['class' => 'w-100 text-right']) !!}</td>
							<td>{!! Form::text('comment', $selected->comment, ['class' => 'form-control mt-2']) !!}</td>
						</tr>
					</table>

				</div>
				<div class="col-md-12 text-center">
					{!! Form::submit('Envoyer !', ['class' => 'btn', 'name' => 'submitbutton']) !!}
				</div>
			</div>
			{!! Form::close() !!}

		</div>

		<hr/>

		<div class="container mt-3">
			<div class="row">
				<div class="col-md-12">
					<h2 class="text-white bg-secondary text-center p-1">Données</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<table id = "filtredtable" class="table table-bordered table-sm table-responsive"  style="font-size:0.9rem;">
						<thead class='text-white bg-info'>
							<th>ID</th>
							<th>Order ID</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Address</th>
							<th>Zipcode</th>
							<th>City</th>
							<th>Status</th>						
							<th>Transporter</th>
							<th>Tracking_id</th>
							<th>Date Sent</th>
							<th>Date Delivered</th>	
							<th>Comment</th>						
							<th>Action</th>
						</thead>
						@foreach($data as $row)
						<tr>
							<td><a href="{{url('admin/parcel-tracking/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
							<td>{{$row->order_id}}</td>
							<td>{{$row->firstname}}</td>
							<td>{{$row->lastname}}</td>
							<td>{{$row->email}}</td>                 
							<td>{{$row->phone}}</td>
							<td>{{$row->address}}</td>
							<td>{{$row->zipcode}}</td>
							<td>{{$row->city}}</td>
							<td>{{$row->status}}</td>
							<td>{{$row->transporter}}</td>
							<td>{{$row->tracking_id}}</td>
							<td><span class="badge badge-info">{{$row->date_sent}}</span></td>
							<td><span class="badge badge-success">{{$row->date_delivered}}</span></td>							
							<td>{{$row->comment}}</td>
							<td>
								<a href="{{url('admin/colissimo_tracking/'.$row->tracking_id)}}" data-toggle='tooltip' data-placement='top' title='Track' class='icon-style'><i class='fa fa-google-wallet fa-lg'></i></a>
							</td>


						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>

	</section>

	@stop



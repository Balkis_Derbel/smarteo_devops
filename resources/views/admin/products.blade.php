@extends('layouts.admin')

@section('title','ADMIN/PRODUCTS')

@section('content')


<?php $page_title = 'ADMIN / PRODUCTS';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">
		<div class="row cursor-pointer" onclick="toggle_form_update()">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Création ou MAJ</h2>
			</div>
		</div>

		{!! Form::open(['url' => 'admin/products']) !!}
		<div class="row form" id="form_update">
			<div class="col-md-12">
				<div class="row" style="width:100%;display:inline-block;text-align:center;">
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>
					<label class="btn btn-danger" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="create_update_in_stripe"> Update in STRIPE</label>
					<label class="btn btn-info" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="view_product"> Voir la page du produit</label>				
				</div>
			</div>

			<div class="col-md-12">
				<table class="table-sm w-100">
					<tr>
						<td class="w-30">{!! Form::label('ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td class="w-70">{!! Form::text('id',  $selected->id, ['class' => 'form-control']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Reference',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('reference', $selected->reference, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Type',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('type', ['none'=>'none','product'=>'product','workshop'=>'workshop','pricing_plan'=>'pricing_plan'], $selected->type, ['class' => 'form-control mt-2'])!!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Title',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('title', $selected->title, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('SubTitle',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('subtitle', $selected->subtitle, ['class' => 'form-control mt-2']) !!}</td>
					</tr>					
					<tr>
						<td>{!! Form::label('Slug (aut.)',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('slug', $selected->slug, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Description',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::textarea('description', $selected->description, ['class' => 'form-control mt-2', 'rows'=>'3']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Thumb',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('thumb', $selected->thumb, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('NB Images',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('nb_images', $selected->nb_images, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Featured',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('featured', $selected->featured, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Tags',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('tags', $selected->tags, ['class' => 'form-control mt-2 w-100', 'data-role' => 'tagsinput']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Selling Price',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('selling_price', $selected->selling_price, ['class' => 'form-control mt-2', 'placeholder' => 'Prix de location pour une semaine']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Subscription Amount',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('subscr_amount', $selected->subscr_amount, ['class' => 'form-control mt-2', 'placeholder' => 'Prix de vente']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Subscription Payment Frequency',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('subscr_frequency', ['none'=>'none','weekly'=>'weekly','monthly'=>'monthly','yearly'=>'yearly'], $selected->subscr_frequency, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Stripe Product Ref',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('stripe_product_ref', $selected->stripe_product_ref, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Stripe Subscription Plan',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('stripe_subscr_plan', $selected->stripe_subscr_plan, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Inventory',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('inventory', $selected->inventory, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Status',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('status', array('live'=>'live','hidden'=>'hidden'),$selected->status, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
				</table>

			</div>
			<div class="col-md-12 text-center">
				{!! Form::submit('Envoyer !', ['class' => 'btn btn-lg rounded btn-info', 'name' => 'submitbutton']) !!}
			</div>
		</div>
		{!! Form::close() !!}

	</div>

	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Données</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table id = "filtredtable" class="table table-bordered table-sm table-responsive">
					<thead>
						<th>ID</th>
						<th>Reference</th>
						<th>Type</th>
						<th>Title</th>					
						<th>Slug</th>
						<th>Description</th>
						<th>Thumb</th>
						<th>Featured</th>
						<th>Tags</th>
						<th>Selling Price</th>
						<th>Subscription Amount</th>
						<th>Subscr Frequency</th>
						<th>Stripe Prod Ref</th>
						<th>Stripe Subs Plan</th>
						<th>Status</th>						

					</thead>
					@foreach($data as $row)
					<tr>
						<td><a href="{{url('admin/products/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
						<td>{{$row->reference}}</td>
						<td>{{$row->type}}</td>
						<td>{{$row->title}}</td>
						<td>{{$row->slug}}</td>
						<td>{{$row->description}}</td>
						<td>{{$row->thumb}}</td>
						<td>{{$row->featured}}</td>
						<td>{{$row->tags}}</td>
						<td>{{$row->selling_price}}</td>
						<td>{{$row->subscr_amount}}</td>
						<td>{{$row->subscr_frequency}}</td>                 
						<td>{{$row->stripe_product_ref}}</td>
						<td>{{$row->stripe_subscr_plan}}</td>
						<td>{{$row->status}}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>

</section>

@stop



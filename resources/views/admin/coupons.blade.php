@extends('layouts.default')

@section('title','ADMIN/Coupons')

@section('content')

<?php $page_title = 'ADMIN / Coupons';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container">
		<h1>Création ou MAJ des coupons de réduction</h1>

		{!! Form::open(['url' => 'admin/coupons']) !!}
		<div class="row form">

			<div class="row" style="width:100%;display:inline-block;text-align:center;">
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>
			</div>

			{!! Form::text('id',  $selected->id, ['class' => 'form-control', 'placeholder' => 'Id']) !!}

			{!! Form::text('email', $selected->email, ['class' => 'form-control mt-2', 'placeholder' => 'Email de l\'utilisateur' ]) !!}

			{!! Form::text('coupon', $selected->coupon, ['class' => 'form-control mt-2', 'placeholder' => 'Coupon']) !!}

			{!! Form::text('token_rent', $selected->token_rent, ['class' => 'form-control mt-2', 'placeholder' => 'Token pour la location']) !!}

			{!! Form::text('token_sell', $selected->token_sell, ['class' => 'form-control mt-2', 'placeholder' => 'Token pour la vente']) !!}

			{!! Form::text('discount_rent', $selected->discount_rent, ['class' => 'form-control mt-2', 'placeholder' => 'Discount pour la location (exemple : 0.5. price *= (1-discount/100))']) !!}

			{!! Form::text('discount_sell', $selected->discount_sell, ['class' => 'form-control mt-2', 'placeholder' => 'Discount pour la vente (exemple : 0.5. price *= (1-discount/100))']) !!}

			{!! Form::text('kit', $selected->kit, ['class' => 'form-control mt-2', 'placeholder' => 'Kit concerné']) !!}

			{!! Form::text('status', $selected->status, ['class' => 'form-control mt-2', 'placeholder' => 'Statut (live / inactive)']) !!}


			{!! Form::submit('Envoyer !', ['class' => 'btn btn-info mt-2']) !!}
		</div>
		{!! Form::close() !!}

	</div>

	<div class="container mt-3">
		<h1>Result</h1>

		<div class="row">
			<table class="table table-bordered table-sm">
				<thead>
					<th>Id</th>
					<th>Email</th>
					<th>Coupon</th>
					<th>Token_rent</th>
					<th>Token_sell</th>
					<th>Discount_rent</th>
					<th>Discount_sell</th>					
					<th>Kit</th>
					<th>Status</th>	
				</thead>
				@foreach($data as $row)
				<tr>
					<td>{{$row->id}}</td>
					<td>{{$row->email}}</td>
					<td>{{$row->coupon}}</td>
					<td>{{$row->token_rent}}</td>
					<td>{{$row->token_sell}}</td>
					<td>{{$row->discount_rent}}</td>	
					<td>{{$row->discount_sell}}</td>											
					<td>{{$row->kit}}</td>	
					<td>{{$row->status}}</td>	
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
@extends('layouts.default')

@section('title','ADMIN/Contents')

@section('css')
<style>
    .pagination > li > a, .pager > li > a {
        margin : 0 10px 0 0;
    }
    .dataTables_wrapper .row {
        width:100%;
        margin:10px;
    }
    thead input {
        width: 100%;
    }
</style>
@stop

@section('scripts')
<script src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js'></script>
<script>
    $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#filtredtable thead tr').clone(true).appendTo( '#filtredtable thead' );
    $('#filtredtable thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                .column(i)
                .search( this.value )
                .draw();
            }
        } );
    } );

    var table = $('#filtredtable').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );
} );
</script>
@stop

@section('content')


<?php $page_title = 'ADMIN / Contents';?>
@include('admin/header')


<section class="bg-light ptb-2">

    <div class="container admin">
        <div class="row">
            <div class="col-md-12">
                <h1><strong>Liste des contenus</strong>
                    <a href="{{url('admin/contents/update')}}" class="btn btn-success btn-lg">Ajouter</a>
            </div>

            <div class="col-md-12 overflow-auto">
                <table id="filtredtable" class="table table-striped table-bordered w-100">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Référence</th>
                            <th>Thumb</th>                        
                            <th>Titre</th>
                            <th>Description</th>
                            <th>Slug</th>
                            <th>Keywords</th>
                            <th>Tags</th>
                            <th>Tags_1</th>                            
                            <th>Type</th>
                            <th>Integration</th>
                            <th>Paramètre</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($contents as $content)

                        <tr>
                            <td>{{ $content->id }}</td>
                            <td>{{ $content->reference }}</td>
                            <td>
                                {{ $content->thumb }}
                                <img src="{{url('contents/thumbs/'.$content->thumb)}}"
                                style="width:100px;"/>
                            </td>  
                            <td>{{ $content->title }}</td>
                            <td>{{ $content->description }}</td>
                            <td>{{ $content->slug }}</td>
                            <td>{{ $content->keywords }}</td>
                            <td>{{ $content->tags }}</td> 
                            <td>{{ $content->tags_1 }}</td>        
                            <td>{{ $content->access_type }}</td> 
                            <td>{{ $content->integration }}</td>
                            <td>{{ $content->parameters }} </td>
                            <td>{{ $content->status }}</td>
                            <td width=300>
                                <a class="btn btn-default" href="{{url('admin/contents/view/'.$content->id)}}">Voir</a>
                                <a class="btn btn-info" href="{{url('admin/contents/update/'.$content->id)}}">Modifier</a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@stop

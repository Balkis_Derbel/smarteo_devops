@extends('layouts.admin')

@section('title','ADMIN/Contents')

@section('scripts')

<script type="text/javascript">

  function extract_metatags()
  {
    $('#content-title').val('');
    $('#content-description').val('');
    $('#content-keywords').val('');
    $('#content-slug').val('');
    $('#content-thumb').val('');
    $('#content-parameters').val('');

    getMetaTags();
    $("html, body").animate({ scrollTop: 0 }, "slow");
  };

  function getMetaTags() 
  {
    var name = $('#target_url').val();
    var url = '{{url('admin/contents/crawle_url')}}';

    $.ajax({
      url 	: url,
      data 	: {
       search : name
     }
   }).done(function (data) 
   {
    params = JSON.parse(data); 
    $('#content-ref').val(params['reference']);
    $('#content-title').val(params['title']);
    $('#content-description').val(params['description']);
    $('#content-keywords').val(params['keywords']);
    $('#content-thumb').val(params['image']);
    $('#content-parameters').val(params['parameters']);
    $('#content-slug').val(params['slug']);
    $('#content-integration').val(params['integration']);
    alert('Opération finie avec succès :)');
  }).fail(function () 
  {
    alert('Crawle failed :(');
  });
}

</script>

@stop


@section('content')


<?php $page_title = 'ADMIN / Contents';?>
@include('admin/header')


<section class="bg-light ptb-2">

  <div class="container admin mb-3">
    <div class="row">
      <div class="col-md-12 p-3 text-center text-white bg-info">
        Editer un contenu
      </div>

      <div class="col-md-12 mt-3 text-center">
        <span class="btn btn-info rounded" onclick='toggle_form_update();' id='show_hide_url'>
          Ajouter à partir d'un URL
        </span>
        <div class="w-100 bg-dark p-3 m-3 rounded" style="display:none;" id="form_update">
          <input type="text" class="w-100 form-control" id="target_url" />    
          <span class="btn btn-info mt-3" onclick='extract_metatags();'>
            Générer les métas
          </span>
        </div>

        <div class="col-md-12">
          {!! Form::open(['url' => 'admin/contents/update']) !!}

          <div class="form-group">
            {{ Form::label('reference', 'Référence :') }}
            {{ Form::text('reference',$content->reference, ['class'=>'form-control', 'id' => 'content-ref']) }}
          </div>

          <div class="form-group">
            {{ Form::label('title', 'Titre :') }}
            {{ Form::text('title',$content->title, ['class'=>'form-control', 'id' => 'content-title']) }}
          </div>

          <div class="form-group">
            {{ Form::label('description', 'Description :') }} 
            {{ Form::textarea('description',$content->description, ['class'=>'form-control', 'rows'=>'3', 'id'=>'content-description']) }}
          </div>

          <div class="form-group">
            {{ Form::label('slug', 'Slug :') }} 
            {{ Form::text('slug',$content->slug, ['class'=>'form-control', 'id' => 'content-slug']) }}
            <small class="form-text text-muted">Keep empty to generate automatically</small>
          </div>

          <div class="form-group">
            {{ Form::label('keywords', 'Keywords :') }}
            {{ Form::text('keywords',$content->keywords, ['class'=>'form-control', 'id' => 'content-keywords']) }}
          </div>

          <div class="form-group">
            {{ Form::label('tags', 'Tag :') }} 
            {{ Form::text('tags',$content->tags, ['class'=>'form-control']) }}
          </div>

          <div class="form-group">
            {{ Form::label('tags_1', 'Tag 1:') }} 
            {{ Form::text('tags_1',$content->tags_1, ['class'=>'form-control']) }}
          </div>

          <div class="form-group">
            {{ Form::label('thumb', 'Thumb :') }}
            {{ Form::text('thumb',$content->thumb, ['class'=>'form-control', 'id' => 'content-thumb']) }}
            <small class="form-text text-muted">files should be copied in ~/contents/thumbs/</small>
          </div>

          <div class="form-group">
            {{ Form::label('access_type', 'Access Type :') }}
            {{ Form::select('access_type', ['public'=>'Public' , 'restricted'=>'Restricted'], $content->access_type, ['class'=>'form-control']) }}
          </div>

          <div class="form-group">
            {{ Form::label('integration', 'Integration :') }}
            {{ Form::select('integration', ['iframe'=>'iframe','href'=>'href','local'=>'local'],$content->integration, ['class'=>'form-control', 'id' => 'content-integration']) }}
          </div>

          <div class="form-group">
            {{ Form::label('parameters', 'Paramètres :') }}
            {{ Form::text('parameters', $content->parameters, ['class' => 'form-control', 'id' => 'content-parameters']) }}
          </div>

          <div class="form-group">
            {{ Form::label('status', 'Status :') }}
            {{ Form::select('status', ['inprogress'=>'InProgress','ready'=>'Ready','deleted'=> 'Deleted'], $content->status, ['class'=>'form-control']) }}
          </div>

          <div class="col-md-12 text-center">

            <a class="btn btn-danger" href="{{url('admin/contents/delete/'.$content->id)}}"> Suppr.</a>
            <a class="btn btn-success" href="{{url('admin/contents')}}"> Retour</a>

            {{ Form::hidden('id', $content->id) }}
            {!! Form::submit('Enregistrer', ['class' => 'btn btn-info']) !!}
          </div>

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</section>
@stop


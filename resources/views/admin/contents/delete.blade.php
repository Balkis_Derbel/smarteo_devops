@extends('layouts.default')

@section('title','ADMIN/Contents')

@section('content')


<?php $page_title = 'ADMIN / Contents';?>
@include('admin/header')


<section class="bg-light ptb-2">
  <div class="container admin">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1><strong>Suppression du cours</strong></h1>
      </div>
      <div class="col-md-12 text-center">
        <p class="alert alert-warning"> Etes vous sûr de vouloir supprimer le cours ?</p>
        {!! Form::open(['url' => 'admin/contents/delete/'.$content->id]) !!}
        <div class="form-actions">
          {{ Form::submit('Oui', ['class' => 'btn btn-warning']) }}
          {{ Form::close() }}
          <a class="btn btn-default" href="{{url('admin/contents')}}">Non</a>
        </div>
      </div>
    </div>
  </div>
</section>
@stop



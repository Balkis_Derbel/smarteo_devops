@extends('layouts.default')

@section('title','ADMIN/Discounts')

@section('content')

<?php $page_title = 'ADMIN / Discounts';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container">
		<h1>Création ou MAJ des codes de réduction partenaire</h1>

		{!! Form::open(['url' => 'admin/discounts']) !!}
		<div class="row form">

			<div class="row" style="width:100%;display:inline-block;text-align:center;">
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
				<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>
			</div>

			{!! Form::text('id',  $selected->id, ['class' => 'form-control', 'placeholder' => 'Id']) !!}

			{!! Form::text('userid', $selected->userid, ['class' => 'form-control mt-2', 'placeholder' => 'ID de l\'utilisateur' ]) !!}

			{!! Form::text('discount_code', $selected->discount_code, ['class' => 'form-control mt-2', 'placeholder' => 'Code Partenaire']) !!}

			{!! Form::text('discount', $selected->discount, ['class' => 'form-control mt-2', 'placeholder' => 'Réduction']) !!}


			{!! Form::submit('Envoyer !', ['class' => 'btn btn-info mt-2']) !!}
		</div>
		{!! Form::close() !!}

	</div>

	<div class="container mt-3">
		<h1>Result</h1>

		<div class="row">
			<table class="table table-bordered table-sm">
				<thead>
					<th>Id</th>
					<th>UserId</th>
					<th>Discount_code</th>
					<th>Discount</th>					
				</thead>
				@foreach($data as $row)
				<tr>
					<td>{{$row->id}}</td>
					<td>{{$row->userid}}</td>
					<td>{{$row->discount_code}}</td>											
					<td>{{$row->discount}}</td>	
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
@extends('layouts.admin')
@section('title','ADMIN/CHEAT-SHEET')

@section('content')
<?php $page_title = 'ADMIN / CheatSheet';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container">
		<div class="row">

			<div class="col-md-4 bg-white-smoke display-6 text-info" style="height:150px;">
				color-white-smoke
			</div>

			<div class="col-md-4 bg-medium display-6 text-info" style="height:150px;">
				color-gray-light
			</div>

			<div class="col-md-4 bg-dark display-6 text-white" style="height:150px;">
				color-gray-dark
			</div>

			<div class="col-md-4 bg-blue display-6 text-white" style="height:150px;">
				color-blue
			</div>

			<div class="col-md-4 bg-green display-6 text-white" style="height:150px;">
				color-green
			</div>

			<div class="col-md-4 bg-orange display-6 text-white" style="height:150px;">
				color-orange
			</div>

			<div class="col-md-4 bg-purple display-6 text-white" style="height:150px;">
				color-purple
			</div>

			<div class="col-md-4 bg-yellow display-6 text-white" style="height:150px;">
				color-yellow
			</div>

		</div>

	</div>
</section>

@stop



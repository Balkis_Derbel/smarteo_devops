@extends('layouts.admin')

@section('title','ADMIN/SUBJECTS')

@section('content')


<?php $page_title = 'ADMIN / SUBJECTS';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">
		<div class="row cursor-pointer" onclick="toggle_form_update()">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Création ou MAJ</h2>
			</div>
		</div>

		{!! Form::open(['url' => 'admin/subjects']) !!}
		<div class="row form" id="form_update">
			<div class="col-md-12">
				<div class="row" style="width:100%;display:inline-block;text-align:center;">
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>
					<label class="btn btn-info" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="view_product"> Voir la page du correspondante</label>				
				</div>
			</div>

			<div class="col-md-12">
				<table class="table-sm w-100">
					<tr>

						<td class="w-30">{!! Form::label('ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td class="w-70">{!! Form::text('id',  $selected->id, ['class' => 'form-control']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Title',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('title', $selected->title, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Slug (aut.)',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('slug', $selected->slug, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Keywords',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('keywords', $selected->keywords, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Tags',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('tags', $selected->tags, ['class' => 'form-control mt-2 w-100', 'data-role' => 'tagsinput']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Thumb',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('thumb', $selected->thumb, ['class' => 'form-control mt-2']) !!}
							<small class="form-text text-muted">files should be copied in ~/contents/thumbs/</small>
						</td>
					</tr>

					<tr>
						<td>{!! Form::label('Status',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('status', array('live'=>'live','hidden'=>'hidden'),$selected->status, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Détails',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::textarea ('details', $selected->details, ['class' => 'form-control mt-2', 'rows'  => 5,  'placeholder' => 'Détails (JSON)']) !!}</td>
					</tr>
				</table>

			</div>
			<div class="col-md-12 text-center">
				{!! Form::submit('Envoyer !', ['class' => 'btn btn-lg rounded btn-info', 'name' => 'submitbutton']) !!}
			</div>
		</div>
		{!! Form::close() !!}

	</div>

	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Données</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table id = "filtredtable" class="table table-bordered table-sm table-responsive">
					<thead>
						<th>ID</th>
						<th>Title</th>					
						<th>Slug</th>
						<th>Keywords</th>
						<th>Tags</th>
						<th>Thumb</th>
						<th>Status</th>
						<th>Details</th>
					</thead>
					@foreach($data as $row)
					<tr>
						<td><a href="{{url('admin/subjects/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
						<td>{{$row->title}}</td>
						<td>{{$row->slug}}</td>
						<td>{{$row->keywords}}</td>
						<td>{{$row->tags}}</td>						
						<td>{{$row->thumb}}</td>
						<td>{{$row->status}}</td>
						<td>{{$row->details}}</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>

</section>

@stop



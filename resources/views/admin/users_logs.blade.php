@extends('layouts.default')

@section('title','ADMIN')

@section('css')
<style>
	.pagination > li > a, .pager > li > a {
		margin : 0 10px 0 0;
	}
	.dataTables_wrapper .row {
		width:100%;
		margin:10px;
	}
	thead input {
		width: 100%;
	}
</style>
@stop

@section('scripts')
<script src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js'></script>
<script>
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#filtredtable thead tr').clone(true).appendTo( '#filtredtable thead' );
    $('#filtredtable thead tr:eq(1) th').each( function (i) {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );

    	$( 'input', this ).on( 'keyup change', function () {
    		if ( table.column(i).search() !== this.value ) {
    			table
    			.column(i)
    			.search( this.value )
    			.draw();
    		}
    	} );
    } );

    var table = $('#filtredtable').DataTable( {
    	orderCellsTop: true,
    	fixedHeader: true
    } );
} );
</script>
@stop

@section('content')


<?php $page_title = 'ADMIN / Users LOGS';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container mt-3">

		<div class="row">
			<table id = "filtredtable" class="table table-bordered w-100">
				<thead>
					@foreach($cols as $col_name)
					<th>
						{{$col_name}}
					</th>
					@endforeach

				</thead>
				@foreach($data as $row)
				<tr>
					@foreach($row as $col)
					<td>{{ $col }}</td>
					@endforeach
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
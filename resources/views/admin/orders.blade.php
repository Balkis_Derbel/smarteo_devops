@extends('layouts.admin')

@section('title','ADMIN/PRODUCTS')

@section('content')


<?php $page_title = 'ADMIN / ORDERS';?>
@include('admin/header')


<section class="bg-light ptb-2">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Création ou MAJ</h2>
			</div>
		</div>

		{!! Form::open(['url' => 'admin/orders']) !!}
		<div class="row form">
			<div class="col-md-12">
				<div class="row" style="width:100%;display:inline-block;text-align:center;">
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-a" value="create"> Create</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-b" value="update"> Update</label>
					<label class="btn" style="margin:5px;"><input type="radio" name="action" id="radio-d" value="delete"> Delete</label>	
				</div>
			</div>

			<div class="col-md-12">
				<table class="table-sm w-100">
					<tr>
						<td class="w-30">{!! Form::label('ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td class="w-70">{!! Form::text('id',  $selected->id, ['class' => 'form-control']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('User ID',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('user_id', $selected->user_id, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Product Reference',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('product_reference', $selected->product_reference, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Amount',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('amount', $selected->amount, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Discount Code',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('discount_code', $selected->discount_code, ['class' => 'form-control mt-2']) !!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('Order Type',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('order_type', ['pay_once'=>'pay_once','subscribe'=>'subscribe'], $selected->order_type, ['class' => 'form-control mt-2'])!!}</td>
					</tr>

					<tr>
						<td>{!! Form::label('First Name',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('firstname', $selected->firstname, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Last Name',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('lastname', $selected->lastname, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Phone',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('phone', $selected->phone, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Email',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('email', $selected->email, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Address',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('address', $selected->address, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('City',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('city', $selected->city, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('ZipCode',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('zipcode', $selected->zipcode, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
										<tr>
						<td>{!! Form::label('Options',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('options', $selected->options, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
										<tr>
						<td>{!! Form::label('Other_Infos',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::text('other_infos', $selected->other_infos, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
					<tr>
						<td>{!! Form::label('Status',null, ['class' => 'w-100 text-right']) !!}</td>
						<td>{!! Form::select('status', array(
							'created'				=>'created',
							'checkout'				=>'checkout',
							'payment_success'		=>'payment_success',
							'subscription_success'	=>'subscription_success',
							'product_sent'			=>'product_sent',
							'product_delivered'		=>'product_delivered',
							'workshop_confirmed'	=>'workshop_confirmed',
							'subscription_ongoing'	=>'subscription_ongoing',
							'subscription_finished'	=>'subscription_finished',
							'workshop_completed'	=>'workshop_completed',
							'processed'				=>'processed',
							'archived'				=>'archived'								
							),$selected->status, ['class' => 'form-control mt-2']) !!}</td>
					</tr>
				</table>

			</div>
			<div class="col-md-12 text-center">
				{!! Form::submit('Envoyer !', ['class' => 'btn', 'name' => 'submitbutton']) !!}
			</div>
		</div>
		{!! Form::close() !!}

	</div>

	<hr/>

	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<h2 class="text-white bg-secondary text-center p-1">Données</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table id = "filtredtable" class="table table-bordered table-sm table-responsive">
					<thead>
						<th>ID</th>
						<th>User ID</th>
						<th>Product Reference</th>
						<th>Amount</th>					
						<th>Discount Code</th>
						<th>Order Type</th>
						<th>Created At</th>
						<th>Request URI</th>
						<th>Unique Key</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Address</th>
						<th>Zipcode</th>
						<th>City</th>
						<th>Status</th>						

					</thead>
					@foreach($data as $row)
					<tr>
						<td><a href="{{url('admin/orders/'.$row->id)}}" class="badge badge-info">{{$row->id}}</a></td>
						<td>{{$row->user_id}}</td>
						<td>{{$row->product_reference}}</td>
						<td>{{$row->amount}}</td>
						<td>{{$row->discount_code}}</td>
						<td>{{$row->order_type}}</td>
						<td>{{$row->created_at}}</td>
						<td>{{$row->request_uri}}</td>
						<td>{{$row->unique_key}}</td>
						<td>{{$row->firstname}}</td>
						<td>{{$row->lastname}}</td>
						<td>{{$row->email}}</td>                 
						<td>{{$row->phone}}</td>
						<td>{{$row->address}}</td>
						<td>{{$row->zipcode}}</td>
						<td>{{$row->city}}</td>
						<td>{{$row->status}}</td>


					</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>

</section>

@stop



@extends('layouts.default')

@section('title','ADMIN/Users')

@section('content')

<?php $page_title = 'ADMIN / Users';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container mt-3">
		<h1> Send Email to selected user (contact@smarteo.co is automatically in cc) </h1>

		<div class="row">
			<div class="col-md-12">

				{!! Form::open(['url' => 'admin/users/mail_order_sent']) !!}

				<div class="form-group">
					<label for="sel1">Select list:</label>
					<select class="form-control" id="sel1" name="email_type">
						<option>1_package_sent</option>
						<option>2_package_delivered</option>
						<option>3_end_commitment_reminder</option>
						<option>4_package_returned</option>
					</select>
				</div>

				<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
					{!! Form::text('email', $user_email, ['class' => 'form-control', 'placeholder' => 'email']) !!}
				</div>

				<div class="form-group">
					{!! Form::text('name', $user_name, ['class' => 'form-control', 'placeholder' => 'user name']) !!}
				</div>

				<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
					{!! Form::text('colissimo_code', NULL, ['class' => 'form-control', 'placeholder' => 'code de suivi']) !!}
				</div>


				{{ Form::hidden('route', '/home') }}

				{!! Form::submit('Envoyer !', ['class' => 'btn btn-info mt-2', 'name' => 'submitbutton']) !!}

				{!! Form::submit('Prévisualiser', ['class' => 'btn btn-info mt-2', 'name' => 'submitbutton', 'style' => 'margin-left:10px;']) !!}

				{!! Form::close() !!}

			</div>

		</div>
	</div>

</section>

@stop
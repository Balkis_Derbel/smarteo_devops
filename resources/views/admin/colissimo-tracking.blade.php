@extends('layouts.default')

@section('title','ADMIN/ColissimoTracking')

@section('scripts')

@section('content')

<?php $page_title = 'ADMIN / ColissimoTracking';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container">
		<h1>Suivi de colis</h1>

		<div class="row">
			<div class="col-md-12">
				
				<table id = "filtredtable" class="table table-bordered table-sm table-responsive">
					<thead class="text-white bg-info">
						<th class="w-30 text-center p-3">Date</th>
						<th class="w-70 p-3">Evenement</th>
					</thead>
					<?php if($data != NULL){?>
						@foreach($data as $row)
						<tr>

							<td class="text-center p-3"><?php echo(array_key_exists('date',$row) ? 
									date('Y-m-d H:i:s', strtotime($row['date'])):''); ?></td>
							<td class="p-3"><?php echo(array_key_exists('label',$row) ? $row['label']:''); ?></td>
						</tr>
						@endforeach
					<?php } ?>
				</table>
			</div>

		</div>
	</div>

</section>

@stop
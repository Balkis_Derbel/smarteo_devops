@extends('layouts.default')

@section('title','ADMIN/Djtt')

@section('content')

<?php $page_title = 'ADMIN / Djtt';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container mt-3">
		<div class="row">
			<table class="table table-bordered table-sm">
				<thead>

					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>School</th>
					<th>ProjectName</th>					
					<th>NB_Kids</th>
					<th>Age</th>
					<th>Created_At</th>					
				</thead>
				@foreach($data as $row)
				<tr>
					<td>{{$row->id}}</td>	
					<td>{{$row->name}}</td>
					<td>{{$row->email}}</td>	
					<td>{{$row->phone}}</td>	
					<td>{{$row->school}}</td>
					<td>{{$row->projectname}}</td>
					<td>{{$row->nb_kids}}</td>											
					<td>{{$row->age}}</td>											
					<td>{{$row->created_at}}</td>		
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
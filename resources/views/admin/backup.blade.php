@extends('layouts.default')

@section('title','ADMIN/Backup')

@section('content')


<?php $page_title = 'ADMIN / Backup';?>
@include('admin/header')

<section class="bg-light ptb-2">

    <div class="container">



        {!! Form::open(['url' => 'admin/backup/create']) !!}
        {!! Form::submit('Créer un backup de l application', ['class' => 'btn btn-info mb-3']) !!}
        {!! Form::close() !!}

        {!! Form::open(['url' => 'admin/backup/export_db']) !!}
        {!! Form::submit('Créer un backup de la base de données', ['class' => 'btn btn-info mb-3']) !!}
        {!! Form::close() !!}

        <h3>Sauvegardes existantes:</h3>
        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Dossier</th>
                    <th>Date</th>
                    <th>Nom</th>
                    <th class="text-right">Taille</th>
                    <th class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($backups as $k => $b)
              <tr>
                <th scope="row">{{ $k+1 }}</th>
                <td>{{ $b['disk'] }}</td>
                <td>{{ \Carbon\Carbon::createFromTimeStamp($b['last_modified'])->formatLocalized('%d %B %Y, %H:%M') }}</td>
                <td>{{ $b['file_name']}}</td>
                <td class="text-right">{{ round((int)$b['file_size']/1048576, 2).' MB' }}</td>
                <td class="text-right">
                    <a class="btn btn-xs btn-danger" data-button-type="delete" 
                    href="{{ url('admin/backup/delete/'.$b['file_name']) }}">
                    <i class="fa fa-trash-o"></i> Supprimer</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
</section>
@endsection

@section('after_scripts')
<!-- Ladda Buttons (loading buttons) -->
<script src="{{ asset('vendor/backpack/ladda/spin.js') }}"></script>
<script src="{{ asset('vendor/backpack/ladda/ladda.js') }}"></script>

<script>
  jQuery(document).ready(function($) {

    // capture the Create new backup button
    $("#create-new-backup-button").click(function(e) {
        e.preventDefault();
        var create_backup_url = $(this).attr('href');
        // Create a new instance of ladda for the specified button
        var l = Ladda.create( document.querySelector( '#create-new-backup-button' ) );

        // Start loading
        l.start();

        // Will display a progress bar for 10% of the button width
        l.setProgress( 0.3 );

        setTimeout(function(){ l.setProgress( 0.6 ); }, 2000);

        // do the backup through ajax
        $.ajax({
            url: create_backup_url,
            type: 'PUT',
            success: function(result) {
                l.setProgress( 0.9 );
                    // Show an alert with the result
                    if (result.indexOf('failed') >= 0) {
                        new PNotify({
                            title: "{{ trans('backpack::backup.create_warning_title') }}",
                            text: "{{ trans('backpack::backup.create_warning_message') }}",
                            type: "warning"
                        });
                    }
                    else
                    {
                        new PNotify({
                            title: "{{ trans('backpack::backup.create_confirmation_title') }}",
                            text: "{{ trans('backpack::backup.create_confirmation_message') }}",
                            type: "success"
                        });
                    }

                    // Stop loading
                    l.setProgress( 1 );
                    l.stop();

                    // refresh the page to show the new file
                    setTimeout(function(){ location.reload(); }, 3000);
                },
                error: function(result) {
                    l.setProgress( 0.9 );
                    // Show an alert with the result
                    new PNotify({
                        title: "{{ trans('backpack::backup.create_error_title') }}",
                        text: "{{ trans('backpack::backup.create_error_message') }}",
                        type: "warning"
                    });
                    // Stop loading
                    l.stop();
                }
            });
    });

    // capture the delete button
    $("[data-button-type=delete]").click(function(e) {
        e.preventDefault();
        var delete_button = $(this);
        var delete_url = $(this).attr('href');

        if (confirm("{{ trans('backpack::backup.delete_confirm') }}") == true) {
            $.ajax({
                url: delete_url,
                type: 'DELETE',
                success: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "{{ trans('backpack::backup.delete_confirmation_title') }}",
                        text: "{{ trans('backpack::backup.delete_confirmation_message') }}",
                        type: "success"
                    });
                    // delete the row from the table
                    delete_button.parentsUntil('tr').parent().remove();
                },
                error: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "{{ trans('backpack::backup.delete_error_title') }}",
                        text: "{{ trans('backpack::backup.delete_error_message') }}",
                        type: "warning"
                    });
                }
            });
        } else {
            new PNotify({
                title: "{{ trans('backpack::backup.delete_cancel_title') }}",
                text: "{{ trans('backpack::backup.delete_cancel_message') }}",
                type: "info"
            });
        }
    });

});
</script>
@endsection

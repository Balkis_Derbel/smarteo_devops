@extends('layouts.default')

@section('title','ADMIN/GiveAway')

@section('content')

<?php $page_title = 'ADMIN / GiveAway';?>
@include('admin/header')

<section class="bg-light ptb-2">

	<div class="container mt-3">
		<div class="row">
			<table class="table table-bordered table-sm">
				<thead>

					<th>ID</th>
					<th>FirstName</th>
					<th>LastName</th>
					<th>EMail</th>
					<th>UserToken</th>
					<th>InviterToken</th>					
					<th>Newsletter</th>
					<th>Created_At</th>					
				</thead>
				@foreach($data as $row)
				<tr>
					<td>{{$row->id}}</td>	
					<td>{{$row->firstname}}</td>
					<td>{{$row->lastname}}</td>	
					<td>{{$row->email}}</td>	
					<td>{{$row->usertoken}}</td>
					<td>{{$row->invitertoken}}</td>
					<td>{{$row->newsletter}}</td>											
					<td>{{$row->created_at}}</td>		
				</tr>
				@endforeach
			</table>
		</div>
	</div>

</section>

@stop
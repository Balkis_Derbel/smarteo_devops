<div class="container">

    <div class="row">
        <h3 id="intro-tuto">
            Dans cet atelier, nous allons essayer de créer un programme pour notre robot capable de déplacer et nettoyer des déchets. En exploitant le capteur infra-rouge, le robot va se mouvoir dans une zone à nettoyer et pousser tous les déchets vers le bord de la zone. <br />

            Le robot sera autonome, et doit être calibré pour pouvoir nettoyer toute la piste. <br  />
        
            Nous souhaitons aussi qu’il puisse détecter les obstacles, de façon à ce qu’il n’entre pas en collision avec d’autres robots nettoyeurs si il y en a.</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">1- Faire avancer le robot</h1>
        </div>
        <div class="col-md-7">
            Commençons par un premier programme simple, où le robot va <span style="color:blue;font-weight:bold">avancer</span>  jusqu’à atteindre le bord du terrain.  La logique est la suivante : <br /><br />
        <ul>
            <li>Le robot avance en surveillant le signal détecté par le capteur infra-rouge;</li>
            <li>Toutes les 1/10 secondes, il vérifie s’il est toujours à l’intérieur de la piste;</li>
            <li>Si c’est le cas, il continue d’avancer. Sinon il arrête le mouvement.</li>
        </ul>
        </div>
        <div class="col-md-5">
           <a href="{{ url('contents/mbot-cleaner/prog-1.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-cleaner/prog-1.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">2- Parcourir la piste</h1>
        </div>
        <div class="col-md-7">
            Nous sommes maintenant prêts pour la suite. Essayons de changer le programme précédent pour que, une fois arrivé au bord de la piste, le robot change de direction. <br />

            En option, on peut modifier l’apparence du robot pour qu’il par exemple change de couleur et émette un signal sonore lors d'un changement de direction. <br />
        
            Le programme peut ressembler à l’image suivante :
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-cleaner/prog-2.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-cleaner/prog-2.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">3- Réaction aléatoire</h1>
        </div>
        <div class="col-md-7">
            Dans le programme précédent, le robot tourne toujours <span id="hard">à droite</span> pour changer de direction. <br />

            Il serait plus intéressant de faire en sorte qu’il pivote à droite <span id="hard">OU</span> à gauche. On va le programmer pour choisir le sens de rotation de manière aléatoire. <br />
                
            Pour cela nous allons créer une variable, à laquelle on va attribuer une valeur aléatoire comprise entre 0 et 1 :
                <ul>
                    <li>Si le tirage au sort retourne <span id="hard">1</span> : le robot pivote vers la droite,</li>
                    <li><span id="medium">SINON</span>, il tourne vers la gauche.</li>
                </ul>
        </div>
        
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-cleaner/prog-3.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-cleaner/prog-3.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">4- Blocs personnalisés</h1>
        </div>
        <div class="col-md-7">
            On va maintenant simplifier ce programme pour qu’il devienne plus lisible. Dans makeCode, il est possible de créer des <span style="color:violet;font-weight:bold;">blocs personnalisés</span>. On peut donc écrire notre programme de la manière suivante :
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-cleaner/prog-4.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-cleaner/prog-4.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">5- Détection d'obstacles</h1>
        </div>
        <div class="col-md-7">
            Pour compléter le tout, il ne nous reste plus qu'à ajouter la dernière brique manquante pour que notre robot détecte les obstacles. <br />

            Le capteur à ultrasons va renvoyer <span id="hard">une distance</span> qui sépare le robot de l’objet en face de lui. Dans cet exemple, le robot va <span style="color:violet;font-weight:bold">REAGIR</span> si cette distance est inférieure à 10 cm.
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-cleaner/prog-5.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-cleaner/prog-5.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>
    <hr />
    <div class="row">
        <h3 id="intro-tuto">
            A partir de ce programme, le robot mBot est capable de se déplacer dans une zone définie en se repositionnant d'une manière aléatoire, tout en évitant des obstacles, le rendant ainsi autonome pour se débarasser de chaque déchet. Pas mal non ?</h3>
    </div> 
</div>


{{----------------------------------------------------------------------------------
<div class="row">
    <h1></h1>
        <div class="col-md-7">

        </div>
        <div class="col-md-5">

        </div>
</div> 
--}}



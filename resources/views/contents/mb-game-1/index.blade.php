@extends('layouts.default')

@section('title','Contenus et tutoriels')

@section('css')

<style>
	.masthead.content {
		background: url('../contents/'.{{$item['reference']}}.'/bg-masthead.jpg') no-repeat center center;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		padding-top: 6rem;
		padding-bottom: 6rem;
	}

	.masthead .description h1{
		font-weight:400;
	}

</style>

@stop


@section('content')

<!-- Enseignant -->

<section class="bg-light">
	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<h1>
						Défi : Développer un jeu 'Pierre Feuille Ciseaux' avec la carte <span class="focus">BBC Micro:Bit</span>
					</h1>
					<div class="display-5">
						<span class="badge badge-pill badge-primary">micro:bit</span>
						<span class="badge badge-pill badge-primary">makecode</span>	
					</div>
					<a href="{{url('contenus-et-tutoriels')}}" class="btn btn-info display-5 mt-3">Voir tous les défis</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-light lead py-3">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				L'objectif de cet exercice est de programmer le fameux jeu 'Pierre Feuille Ciseaux' à l'aide de la carte Mico:Bit. Tout dont vous aurez besoin est : 

				<ol>
					<li>Votre carte micro:bit,</li> 
					<li>Un PC connecté à internet.</li>
				</ol>

				Nous allons réaliser ce défi en utilisant le langage de programmation par blocks 'MakeCode'.
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="smrt-panel smrt-info">

					<h4>Vous avez déjà une connaissance de make:code ? </h4>
					<p class="lead">Vous souhaitez peut être redécouvrir les défis plus simples ? Que vous pourrez trouver ici :</p>


				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<p class="lead">La carte micro:bit inclut un accélérateur. qui permet de détecter 'carte secouée'
				On veut que la carte mette à jour l'affichage à chaque fois qu'on bascule.</p>


				<div class="display-5 badge badge-success">** Première étape :</div> 
				<p class="lead">Nous allons commencer par générer un nombre aléatoire de 1 à 3 quand la carte est secouée.</p>

				<div class="display-5 badge badge-success"> ** Deuxième étape :</div> 
				<p class="lead">if then : affichage</p>

				<a href="/" class="btn btn-info">Programme complet à télécharger : ici :</a>

				<h2>Pour aller plus loins :</h2>

				<p class="lead">Le programme ci dessus peut être rendu plus beau en créant des fonctions</p>

				<ol>
					<li>1. créer une fonction 'pfs'</li>
					<li>2. si besculement : on appelle pfs</li>
				</ol>

				<a href="/" class="btn btn-success">Questions ? N'hésitez pas à les poser à la communauté ici :</a>

				<h1>Aller plus loins et découvrir d'autres activités ici :</h1>

			</div>
		</div>
	</section>



	@stop
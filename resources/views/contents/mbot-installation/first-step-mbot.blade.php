<div class="container">

    <div class="row">
        <h3 id="intro-tuto">
           Vous venez d'obtenir votre permier robot mbot et vous aimeriez pouvoir vous en servir ? Pas de panique, ce tutoriel vous guidera pas à pas dans l'installation et l'utilisation de votre robot.</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">1- Présentation du robot mbot</h1>
        </div>
        <div class="col-md-7">
            <p>Tout d'abord, il est important de présenter le mbot. C'est un robot-véhicule programmable avec le logiciel <span id="blue">mBlock</span>, basé sur Scratch.</p> 

            <p>Il peut détecter des obstacles, suivre une ligne, émettre des sons et des signaux lumineux, recevoir des ordres d’une télécommande ou encore communiquer par un canal infrarouge avec un autre robot.</p>

            <p>Il est intéressant d’inspecter de près les différents composants et d’identifier les pièces clés du robot :
            <ul>
                <li>Les capteurs : Le suiveur de ligne et le capteur de distance ultrason,</li>
                <li>Les moteurs,</li>
                <li>Le contrôleur mCore, avec ses principales parties.</li>
            </ul></p>

            <p>Les capteurs sont connectés avec des cables à un des blocs numérotés (1/2/3/4), qui sont appelés "Port". Ces numéros sont importants, nous allons nous en servir quand nous commencerons à exploiter les capteurs.</p>
        </div>
        <div class="col-md-5">
           <a href="{{ url('contents/mbot-installation/mbot-1.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-installation/mbot-1.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">2- L'application mBlock</h1>
        </div>
        <div class="col-md-7">
            <p>Pour commencer avec mBot, il faut impérativement installer le logiciel mBlock qui est une extension de Scratch sur votre ordinateur, de manière à pouvoir programmer votre robot plus tard. Vous pouvez télécharger l'application<a href="{{ url('https://www.mblock.cc/en-us/download') }}" id="links-tuto"> en cliquant ici</a>.</p> 
            
            <p>Une fois l'installation faite, lancez mBlock.</p>
            
            <p>Vous voila sur l'interface du logiciel ! Pour bien commencer, on va selectionner le bon appareil. Pour cela il faut aller dans l'onglet "appareil", cliquer sur "ajouter", puis sélectionner le périphérique correspondant à votre robot, soit "mbot", et cliquer sur "Ok" (Pensez à supprimer le robot "codey" qui est présent par défaut !).</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-installation/mblock-2.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-installation/mblock-2.png') }}" alt="" class="img-tuto"></a>
        </div>
        <div class="row">
            <div class="col-md-7">
            <p>Ensuite, on va connecter le câble USB à votre robot et le mettre le en marche. On clique enfin sur le bouton "Connecter", ce qui va faire apparaitre Le bouton "Exporter". C’est ce bouton qui va vous permettre d’exporter votre programme à votre robot mBot !</p>

            <p>Une fois le périphérique connecté à l'ordinateur, Le programme va pouvoir être écrit sur l'application, puis <span id="blue">exporté</span> vers le contrôleur du robot. Pour cela il faut activer le mode <span id="blue">"Téléchargement"</span> ou <span id="blue">"Téléverser"</span> (le bouton en question est traduit ou non selon la version de mblock que vous possédez). Le résultat final est présenté sur l'image :</p>

            <p></p>
            </div>
            <div class="col-md-5">
                <a href="{{ url('contents/mbot-installation/mblock-3.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-installation/mblock-3.png') }}" alt="" class="img-tuto"></a>
            </div>
        </div>
        
    </div>

    <div class="row">
        <h3 id="intro-tuto">
            Après avoir suivi ce tutoriel, vous êtes maintenant prêt à programmer votre mbot ! Vous pouvez consulter nos différents programmes afin de bien assimiler les bases du code, et créer ainsi votre propre programme. N'hésitez pas à partager vos créations, qui pourrons être publiées sur notre site !</h3>
    </div> 
</div>
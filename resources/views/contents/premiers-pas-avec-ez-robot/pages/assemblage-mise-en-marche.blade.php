@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')


<style>
.masthead.content {
	background: url("../../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	/*background: #ccc;*/
	padding-top: 3rem;
	padding-bottom: 3rem;
}
.masthead .description {
	padding: 20px;
}
.masthead .description h1{
	font-weight:400;
}
.mySlides {display:none}
</style>

@stop


@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<a href="{{url('contenus-et-tutoriels/'.$item['slug'])}}" class='button btn-info btn-lg btn-block'>{{$item['title']}}</a>
					<h1>
						Assemblage et Mise en Marche
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="lead">Les étapes de l'assemblage sont disponibles sur le site EZ-Robot :</p>

				<a class="text-info" href = "http://www.ez-robot.com/Tutorials/Lesson/61?courseId=1">
					http://www.ez-robot.com/Tutorials/Lesson/61?courseId=1
				</a>
				<p class="lead mt-3">
				On reprend ici les principales étapes :</p>
			</div>

			<div class="col-md-8 offset-md-2">
				<div class="mySlides">
					<h1>Etape 1 : Connecter les bras du robot</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-1.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 2 : Connecter la tête</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-2.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 3 : Connecter le bras gauche</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-3.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 4 : Connecter l'avant bras gauche</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-4.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 5 : Connecter la main gauche</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-5.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 6 : Connecter le bras droit</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-6.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 7 : Connecter l'avant bras droit</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-7.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 8 : Connecter la main droite</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-8.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 9 : Connecter la jambe gauche</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-9.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 10 : Connecter le pied gauche</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-10.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 11 : Connecter la jambe droite</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-11.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>Etape 12 : Connecter le pied droit</h1>
					<img src="{{url('contents/'.$item['slug'].'/step-12.jpg')}}" style="width:100%">
				</div>

			</div>

			<div class="col-md-12 text-center pt-2">
				<button id="prevSlide" class="w3-button w3-light-grey" onclick="plusDivs(-1)">❮ Précédent</button>
				<button id="nextSlide" class="w3-button w3-light-grey" onclick="plusDivs(1)">Suivant ❯</button>
			</div>

			<div class="col-md-12 text-center pt-2">
				<button class="w3-button demo" onclick="currentDiv(1)">1</button> 
				<button class="w3-button demo" onclick="currentDiv(2)">2</button> 
				<button class="w3-button demo" onclick="currentDiv(3)">3</button> 
				<button class="w3-button demo" onclick="currentDiv(4)">4</button> 
				<button class="w3-button demo" onclick="currentDiv(5)">5</button> 
				<button class="w3-button demo" onclick="currentDiv(6)">6</button> 
				<button class="w3-button demo" onclick="currentDiv(7)">7</button> 
				<button class="w3-button demo" onclick="currentDiv(8)">8</button> 
				<button class="w3-button demo" onclick="currentDiv(9)">9</button> 
				<button class="w3-button demo" onclick="currentDiv(10)">10</button> 
				<button class="w3-button demo" onclick="currentDiv(11)">11</button> 
				<button class="w3-button demo" onclick="currentDiv(12)">12</button>
			</div>


		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
	var slideIndex = 1;
	showDivs(slideIndex);

	function plusDivs(n) {
		showDivs(slideIndex += n);
	}

	function currentDiv(n) {
		showDivs(slideIndex = n);
	}

	function showDivs(n) {
		var i;
		var x = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("demo");
		if (n > x.length) {slideIndex = 1}    
			if (n < 1) {slideIndex = x.length}
				for (i = 0; i < x.length; i++) {
					x[i].style.display = "none";  
				}
				for (i = 0; i < dots.length; i++) {
					dots[i].className = dots[i].className.replace(" w3-red", "");
				}
				x[slideIndex-1].style.display = "block";  
				dots[slideIndex-1].className += " w3-red";

				//window.scrollTo(0,0);
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		</script>

		@stop
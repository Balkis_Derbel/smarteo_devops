@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')


<style>
.masthead.content {
	background: url("../../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	/*background: #ccc;*/
	padding-top: 3rem;
	padding-bottom: 3rem;
}
.masthead .description {
	padding: 20px;
}
.masthead .description h1{
	font-weight:400;
}
.mySlides {display:none}
</style>

@stop


@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<a href="{{url('contenus-et-tutoriels/'.$item['slug'])}}" class='button btn-info btn-lg btn-block'>{{$item['title']}}</a>
					<h1>
						Ressources Additionnelles
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-md-6 offset-3 text-center">
				<img src="{{url('contents/'.$item['slug'].'/build_0.jpg')}}" style="width:100%">
			</div>

			<div class="col-md-12 pt-3 pb-3 text-center">

				<a href="http://www.ez-robot.com/Tutorials/UserTutorials/">
					<h1>
						http://www.ez-robot.com/Tutorials/UserTutorials/ 
					</h1>
				</a>
			</div>
		</div>
	</div>

</section>
@stop
@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')


<style>
.masthead.content {
	background: url("../../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	/*background: #ccc;*/
	padding-top: 3rem;
	padding-bottom: 3rem;
}
.masthead .description {
	padding: 20px;
}
.masthead .description h1{
	font-weight:400;
}
.mySlides {display:none}
</style>

@stop


@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<a href="{{url('contenus-et-tutoriels/'.$item['slug'])}}" class='button btn-info btn-lg btn-block'>{{$item['title']}}</a>
					<h1>
						Application mobile EZ-Builder 
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="lead">Pour avoir un aperçu sur les les possibilités du EZ-Robot, le plus simple est de commencer par utiliser l'application mobile 'EZ-Builder' :</p>

				<a class="text-info" href = "https://www.ez-robot.com/EZ-Builder/mobile">
					https://www.ez-robot.com/EZ-Builder/mobile
				</a>
			</div>

			<div class="col-md-12 mt-3 mb-3">

				<img src="{{url('contents/'.$item['slug'].'/robot-app.jpg')}}" style="width:100%">

			</div>

		</div>
	</div>
</section>
@stop
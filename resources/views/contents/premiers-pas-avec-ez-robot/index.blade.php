@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')

<style>
.masthead.content {
	background: url("../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	padding-top: 6rem;
	padding-bottom: 6rem;
}

.masthead .description h1{
	font-weight:400;
}

.section-link{
	background:#078292;
	width:96%;
	margin:2%;
	padding:20px;
}

.section-link .title{
	font-size:24px;
	height:60px;
	color:#fff;
	font-weight:300;
}

</style>

@stop


@section('content')

<!-- sections -->

@php ($sections = [
[
'title'	=>'Assemblage et mise en marche',
'url'	=>'assemblage-mise-en-marche'
],[
'title'	=>'Premières commandes avec l\'application mobile',
'url'	=>'commandes-application-mobile'
],[
'title'	=>'Ressources additionnelles sur le site EZRobot',
'url'	=>'ressources-additionnelles'
]])

<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<h1>
						{{$item['title']}}
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container container-fluid pt-5 pb-5" style="width:100%;">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="lead">
					Le Robot Humanoïde Revolution JD EZ-Robot est un robot humanoïde entièrement fonctionnel construit à partir d'EZ-Bits. Il est doté de 16 degrés de liberté et équipé de 16 servos robustes à engrenages métalliques. La tête de ce robot contient une caméra permettant le suivi par la vision avec différents modes, notamment la couleur, le mouvement, les glyphes, les codes QR et les visages. Les yeux de ce robot sont équipés de 18 LED (9 par œil). JD est équipé d'applications qui lui permettent de marcher immédiatement.
				</p>
			</div>
		</div>

		<div class="row">

			@foreach($sections as $section)
			<div class="col-md-6">
				@include('contents/'.$item['reference'].'/item',['section'=>$section, 'content_url'=>$item['slug']])
			</div>
			@endforeach


		</div>

	</section>

	@stop
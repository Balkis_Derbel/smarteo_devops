@extends('layouts.default')

@section('title','Compilation de ressources pour bien commencer avec le robot mbot')

@section('description','mBot est un robot voiture sur 2 roues (et une roulette pour l\'équilibre) que l\'on pourra construire, démonter, remonter... Mbot peut être programmé avec le logiciel Mblock, qui est en fait le logiciel Scratch auquel a été ajouté en extension un ensemble de blocs supplémentaires.')

@section('keywords','MBot','MBlock','programmer')

@section('image', URL::to('contents/'.$item['reference'].'/'.$item['thumb']))

@section('css')

<style>
.masthead.content {
	background: url('../contents/'.{{$item['reference']}}.'/bg-masthead.jpg') no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	padding-top: 6rem;
	padding-bottom: 6rem;
}

.masthead .description h1{
	font-weight:400;
}

</style>

@stop


@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<h1>
						{{$item['title']}}
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container container-fluid pt-5 pb-5" style="width:100%;">
		<div class="row">
			<div class="col-md-12 text-justify">
				<p class="lead">
					Ça y est, vous venez de recevoir votre robot voiture mBot. Ce robot est très populaire dans les milieux scolaires et parascolaires et il est déjà utilisé dans plusieurs écoles et collèges pour initier à des activités de robotique et de programmation. 
				</p>

				<p class="lead">
					Plusieurs exemples d'activités, tutoriels et vidéos peuvent être trouvés sur internet. On a rassemblé pour vous une sélection des meilleures ressources pour vous aider à faire les premiers pas avec mBot.
				</p>

				<p class="lead">
					Vous avez des commentaires ? Ou avez-vous peut être détecté une ressource intéressante que vous souhaitez partager avec les autres utilisateurs ? Cette collection se veut collaborative : n’hésitez pas à nous indiquer toute autre ressource qui mérite d’être ajoutée <a href="{{url('contenus-et-tutoriels/'.$item['slug'].'/suggestions')}}" class="text-info">[C'est ici]</a>.
				</p>
			</div>
		</div>

		<div class="row mt-3">
			<div class="col-md-12 text-center">
				<p class="lead p-3 text-light bg-info">
					Nous vous conseillons, pour commencer, de consulter les vidéos-tutoriels suivants pour découvrir mBot en action et voir ses possibilités:
				</p>
			</div>

			<ul class="list text-center">

				<li>
					<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=q_MfPk_ko8k'">
						<div class='title'>Présentation du mBot</div>
						<img src="{{url('contents/'.$item['reference'].'/prez-mbot.jpg')}}">
					</div>
				</li>

				<li>
					<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=Vr-VC8Su1UY'">
						<div class='title'>Makeblock mBot Soccer Game</div>
						<img src="{{url('contents/'.$item['reference'].'/mbot-soccer.jpg')}}">
					</div>
				</li>

				<li>
					<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=g325Tp5dvbo&t=3s'">
						<div class='title'>La vidéo suivante contient un excellent tutoriel pour le montage et la mise en marche du mBot</div>
						<img src="{{url('contents/'.$item['reference'].'/prez-tuto-mbot.jpg')}}">
					</div>
				</li>
			</ul>
		</div>


		<div class="row mt-3">
			<div class="col-md-12 text-center">
				<p class="lead p-3 text-light bg-info">
					Des ressources intéressantes peuvent être trouvées sur l'excellent blog <a href='http://www.fredtechnocollege.org'><b>fredtechnocollege</b></a> :
				</p>
			</div>

			<ul class="list text-center">

				<li>
					<div class="smart-link" onclick="location.href='http://www.fredtechnocollege.org/le-robot-mbot-de-makebloc/les-capteurs-et-actionneurs-de-mbot-en-video/'">
						<div class='title'>La vidéo suivante présente tous les capteurs et actionneurs de mBot… :</div>
						<img src="{{url('contents/'.$item['reference'].'/fred-1.jpg')}}">
					</div>
				</li>

				<li>
					<div class="smart-link" onclick="location.href='http://www.fredtechnocollege.org/le-robot-mbot-de-makebloc/sinitier-a-la-programmation-de-mbot/'">
						<div class='title'>Vous trouverez ici un cours complet et des tutoriels pour écrire vos premières lignes de code :</div>
						<img src="{{url('contents/'.$item['reference'].'/fred-2.jpg')}}">
					</div>
				</li>

				<li>
					<div class="smart-link" onclick="location.href='http://www.fredtechnocollege.org/le-robot-mbot-de-makebloc/suivi-de-ligne-avec-mbot/'">
						<div class='title'>Et encore ici, avec le suiveur de ligne :</div>
						<img src="{{url('contents/'.$item['reference'].'/fred-3.jpg')}}">
					</div>
				</li>

				<li>
					<div class="smart-link" onclick="location.href='http://www.fredtechnocollege.org/tout-mblock-et-ses-extensions-en-un-seul-poster/'">
						<div class='title'>Enfin, pour ne rien oublier, l'équipe du blog <b>fredtechnocollege</b> a partagé ce poster que vous pourrez télécharger :</div>
						<img src="{{url('contents/'.$item['reference'].'/fred-4.jpg')}}">
					</div>
				</li>

			</div>



			<div class="row mt-3">
				<div class="col-md-12 text-center">
					<p class="lead p-3 text-light bg-info">
						Pour bien démarrer vos activités, nous vous conseillons ces excellents tutoriels, partagés sur la chaine youtube <b>technopek</b> (Profs de techno au collège "La forêt" de Saint Genix sur Guiers) :
					</p>
				</div>

				<ul class="list text-center">

					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=nQGjWcXBlHE&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 1 : Faire avancer et reculer un mBot (Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-1.jpg')}}">
						</div>
					</li>


					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=Ij8YPbnwmuw&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 2 : Faire avancer et tourner un mBot (Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-2.jpg')}}">
						</div>
					</li>

					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=6WYRky7s-eM&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 3 : Faire avancer et tourner un mBot (Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-3.jpg')}}">
						</div>
					</li>

					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=8nDeV2QKvUA&index=7&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 4 : Utilisation de la matrice Led sur un Mbot (Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-4.jpg')}}">
						</div>
					</li>

					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=Wy1rG0_8aWg&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 5 : Utilisation du capteur ultrason sur un Mbot (Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-5.jpg')}}">
						</div>
					</li>

					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=HUtyW-4ySvM&index=5&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 6 : Utilisation du suiveur de ligne sur un Mbot ( Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-6.jpg')}}">
						</div>
					</li>

					<li>
						<div class="smart-link" onclick="location.href='https://www.youtube.com/watch?v=NEl6DwFTtuM&index=4&list=UUMlrAbHc21QZ9b4n10R6Bgg'">
							<div class='title'>Tuto 7 : Utilisation de variables sur un Mbot (Mblock)</div>
							<img src="{{url('contents/'.$item['reference'].'/tuto-7.jpg')}}">
						</div>
					</li>

				</div>



				<div class="row mt-3">
					<div class="col-md-12 text-center">
						<p class="lead p-3 text-light bg-info">
							Plusieurs autres ressources très intéressantes sont partagées sur la page consacrée à mBot du site internet du réseau Canopé :
						</p>
					</div>

					<ul class="list text-center" style="width:100%;">
						<li>
							<div class="smart-link" onclick="location.href='http://www.reseau-canope.fr/atelier-yvelines/spip.php?article1308'">
								<img src="{{url('contents/'.$item['reference'].'/reseau-canope.jpg')}}" style="height:150px;">
							</div>
						</li>
					</ul>

				</div>


				<div class="row mt-3">
					<div class="col-md-12 text-center">
						<p class="lead p-3 text-light bg-info">
							Des cours et TP complets peuvent aussi être trouvés sur les sites des différentes académies, comme par exemple :
						</p>
					</div>

					<ul class="list">

						<li>
							<div class="smart-link" onclick="location.href='http://sti.ac-bordeaux.fr/techno/coder/mbot/'">
								<div class='title'>Blog Académique des Sciences Industrielles pour l’Ingénieur (SII) - Académie Bordeaux</div>
								<img src="{{url('contents/'.$item['reference'].'/ac-bordeaux.jpg')}}">
							</div>
						</li>

						<li>
							<div class="smart-link" onclick="location.href='http://technologie.ac-creteil.fr/spip.php?article252'">
								<div class='title'>Page 'SII-Technologie collège' - Académie de Créteil</div>
								<img src="{{url('contents/'.$item['reference'].'/ac-creteil.jpg')}}">
							</div>
						</li>

						<li>
							<div class="smart-link" onclick="location.href='http://blogpeda.ac-poitiers.fr/technologie/2000/01/01/ressources-pour-le-robot-mbot/'">
								<div class='title'>Blog du collège 'Collège Joachim du Bellay de Loudun' - Académie de Poitiers</div>
								<img src="{{url('contents/'.$item['reference'].'/ac-poitiers.jpg')}}">
							</div>
						</li>
					</ul>


				</div>
			</div>

		</section>

		@stop
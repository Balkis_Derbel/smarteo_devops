@extends('layouts.default')

@section('title','Compilation de ressources pour bien commencer avec le robot mbot')

@section('css')

<style>
.masthead.content {
	background: url('../contents/'.{{$item['reference']}}.'/bg-masthead.jpg') no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	padding-top: 6rem;
	padding-bottom: 6rem;
}

.masthead .description h1{
	font-weight:400;
}

</style>

@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<h1>
						{{$item['title']}}
					</h1>
					<h2>
						{{$item['description']}}
					</h1>
					<h3><a href=''><a href="{{url('contenus-et-tutoriels/'.$item['slug'])}}" class="text-info">[Retour]</a></a></h3>
				</div>
			</div>
		</div>
	</div>

	<div class="container container-fluid pt-5 pb-5" style="width:100%;">
		<div class="row">
			<div class="col-md-12 text-justify">
				<p class="lead">
					Vous avez des commentaires ? Ou avez-vous peut être détecté une ressource intéressante que vous souhaitez partager avec les autres utilisateurs ? Veuillez nous indiquer vos remarques ou suggestions et nous serons très heureux d'y répondre :
				</p>
			</div>


			<div class="col-md-12">

				{!! Form::open(['url' => 'contentFeedbackForm']) !!}
				<div class="content-suggest-box">

					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">

						{!! Form::text('nom', null, ['class' => 'form-control name', 'placeholder' => 'Votre nom']) !!}
						{!! $errors->first('nom', '<small class="help-block">:message</small>') !!}

					</div>

					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">

						{!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Votre email']) !!}
						{!! $errors->first('email', '<small class="help-block">:message</small>') !!}

					</div>

					<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">

						{!! Form::textarea ('texte', null, ['class' => 'form-control', 'placeholder' => 'Votre message']) !!}
						{!! $errors->first('texte', '<small class="help-block">:message</small>') !!}

					</div>

					{{ Form::hidden('route', '/contenus-et-tutoriels/'.$item['slug']) }}
					{{ Form::hidden('content_ref', $item['reference']) }}

					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}

					<!--<div class="g-recaptcha" data-sitekey="<?=env('RECAPTCHA_PUBLIC')?>"></div>-->
				</div>
				{!! Form::close() !!}
			</div>
		</div>

	</div>

</section>

@stop
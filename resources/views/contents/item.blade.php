<?php
	$x = 'content';
	if($item->integration == 'href') $x = 'url'; 
	
	$y = $item->thumb;
	
	if(!filter_var($y, FILTER_VALIDATE_URL))
	{
		$y = 'contents/thumbs/'.$y; 
    } 
?>
<div class="card smart-content" onclick="location.href='{{url('contenus-et-tutoriels/'.$item->slug)}}'">
	<div class="card-img-top">
		<img src="{{URL::to($y)}}" alt="{{$item->slug}}" style="min-height:100%" />
	</div>

	<div class="card-comment">
		<div class="float-left">
			<img src="{{URL::to('img/ico-'.$x.'.png')}}" alt="ico_{{$x}}" style="width:40px;margin-left:7px;" />
		</div>
		<div>
			<div class="w-100 text-center">
				<?php
				$tags = explode(",", $item->tags);
				foreach($tags as $t) {?>
					<span class="badge badge-success"><?=$t?></span>
				<?php } ?>
			</div>
			<div class="w-100">
				<?php
				$tags = explode(",", $item->tags_1);
				foreach($tags as $t) {?>
					<span class="badge badge-secondary"><?=$t?></span>
				<?php } ?>
			</div> 
		</div>
	</div>

	<div class='card-title text-white bg-info' style='display:table;'>
		<div style="vertical-align:middle;display:table-cell;height:100%">
			<h3 class="display-6">{{$item->title}}</h3>
		</div>
	</div>
</div>
@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')


<style>
.masthead.content {
	background: url("../../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	/*background: #ccc;*/
	padding-top: 3rem;
	padding-bottom: 3rem;
}
.masthead .description {
	padding: 20px;
}
.masthead .description h1{
	font-weight:400;
}
.mySlides {display:none}
</style>

@stop


@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<a href="{{url('contenus-et-tutoriels/'.$item['url'])}}" class='button btn-info btn-lg btn-block'>{{$item['title']}}</a>
					<h1>
						Section Title
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="lead">Texte générique xxx uuu xxx jjjdceaz  fre dez adez adez af ez fer fer frze </p>
			</div>

			<div class="col-md-8 offset-md-2">
				<div class="mySlides">
					<h1>Etape 1 : Page 1</h1>
					<img src="{{url('contents/'.$item['url'].'/img1.jpg')}}" style="width:100%">
					<div class="w3-display-bottomright w3-large w3-container w3-padding-16 w3-black">
						Image 1
					</div>
				</div>

				<div class="mySlides">
					<h1>Etape 2 : Page 2</h1>
					<img src="{{url('contents/'.$item['url'].'/img2.jpg')}}" style="width:100%">
					<div class="w3-display-bottomright w3-large w3-container w3-padding-16 w3-black">
						Image 2
					</div>
				</div>

				<div class="mySlides">
					<h1>Etape 3 : Page 3</h1>
					<img src="{{url('contents/'.$item['url'].'/img3.jpg')}}" style="width:100%">
					<div class="w3-display-bottomright w3-large w3-container w3-padding-16 w3-black">
						Image 3
					</div>
				</div>

				<div class="mySlides">
					<h1>Etape 4 : Page 4</h1>
					<img src="{{url('contents/'.$item['url'].'/img4.jpg')}}" style="width:100%">
					<div class="w3-display-bottomright w3-large w3-container w3-padding-16 w3-black">
						Image 4
					</div>
				</div>

			</div>

			<div class="col-md-12 text-center pt-2">
				<button id="prevSlide" class="w3-button w3-display-left w3-black" onclick="plusDivs(-1)">&#10094;</button>
				<button id="nextSlide" class="w3-button w3-display-right w3-black" onclick="plusDivs(1)">&#10095;</button>
			</div>

			<div class="col-md-12 text-center pt-2">
				<button class="w3-button dots" onclick="currentDiv(1)">1</button> 
				<button class="w3-button dots" onclick="currentDiv(2)">2</button> 
				<button class="w3-button dots" onclick="currentDiv(3)">3</button> 
				<button class="w3-button dots" onclick="currentDiv(4)">4</button> 
			</div>


		</div>
	</div>
</div>

</section>
@stop

@section('scripts')
<script type="text/javascript" src="{{url('contents/common/slides.js')}}"></script>
@stop
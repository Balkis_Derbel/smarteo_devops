<div class="smart-content">
	<img src="{{URL::to('contents/kit_mbot_0/robot-mbot-initiation.png')}}" alt="" />
	<div class='title'>#TITRE</div>
	<div class='desc'>
		<p class="font-weight-light">{{$item['title']}}</p>
	</div>
	<a href="{{url('contenus-et-tutoriels/initiation-kit-mbot/'.$item['url'])}}" class="button">Ouvrir</a>
</div>
@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')

<style>
.masthead.content {
	background: url("../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	padding-top: 6rem;
	padding-bottom: 6rem;
}
</style>

@stop


@section('content')

<!-- sections -->

@php ($sections = [
[
'title'	=>'section 1',
'url'	=>'section-1'
],[
'title'	=>'section 2',
'url'	=>'section-2'
]])

<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<h1>
						TITRE
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container container-fluid pt-5 pb-5" style="width:100%;">

		<div id="kits" class="row text-center" style="display:inline-block;">


			<ul class="list">
				@foreach($sections as $item)
				<li>
					@include('contents/kit_mbot_0/item', $item)
				</li>
				@endforeach

			</ul>

		</div>

	</div>

</section>

@stop
@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('css')

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.mySlides {display:none}
</style>

<style>
.masthead.content {
	background: url("../../contents/kit_mbot_0/bg-masthead-contents.jpg") no-repeat center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	/*background: #ccc;*/
	padding-top: 3rem;
	padding-bottom: 3rem;
}
.masthead .description {
	padding: 20px;
}

</style>

@stop


@section('content')


<section class="bg-light">

	<div class="masthead content text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">
					<a href="{{url('contenus-et-tutoriels/initiation-kit-mbot')}}" class='button btn-info btn-lg btn-block'>titre de contenu</a>
					<h1>
						TITRE
					</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-md-12">
				<div class="mySlides">
					<h1>balblabla</h1>
					<img src="{{url('contents/kit_mbot_0/bg-masthead-contents.jpg')}}" style="width:100%">
				</div>

				<div class="mySlides">
					<h1>
						Titre de la section
					</h1>

					<h2>
						Sous-titre
					</h2>

					<p>
						texte texte texte
					</p>

				</div>

				<img class="mySlides" src="{{url('contents/kit_mbot_0/bg-masthead-contents.jpg')}}" style="width:100%">
				<img class="mySlides" src="{{url('contents/kit_mbot_0/bg-masthead-contents.jpg')}}" style="width:100%">
			</div>

			<div class="col-md-12 text-center pt-2">
				<button class="w3-button w3-light-grey" onclick="plusDivs(-1)">❮ Prev</button>
				<button class="w3-button w3-light-grey" onclick="plusDivs(1)">Next ❯</button>
			</div>

			<div class="col-md-12 text-center pt-2">
				<button class="w3-button demo" onclick="currentDiv(1)">1</button> 
				<button class="w3-button demo" onclick="currentDiv(2)">2</button> 
				<button class="w3-button demo" onclick="currentDiv(3)">3</button> 
				<button class="w3-button demo" onclick="currentDiv(4)">4</button> 
			</div>


		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
	var slideIndex = 1;
	showDivs(slideIndex);

	function plusDivs(n) {
		showDivs(slideIndex += n);
	}

	function currentDiv(n) {
		showDivs(slideIndex = n);
	}

	function showDivs(n) {
		var i;
		var x = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("demo");
		if (n > x.length) {slideIndex = 1}    
			if (n < 1) {slideIndex = x.length}
				for (i = 0; i < x.length; i++) {
					x[i].style.display = "none";  
				}
				for (i = 0; i < dots.length; i++) {
					dots[i].className = dots[i].className.replace(" w3-red", "");
				}
				x[slideIndex-1].style.display = "block";  
				dots[slideIndex-1].className += " w3-red";

				//window.scrollTo(0,0);
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		</script>

		@stop
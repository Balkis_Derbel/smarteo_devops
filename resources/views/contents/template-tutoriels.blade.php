<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tutoriels</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <style>
            body{
                background-color: #f3f4fa;
            }
            .container{
                margin: 0 auto;
            }
            h1, .mbot{
                color: black;
                margin: 5%;
                text-align: center;
                padding-bottom: 10px;
                text-shadow: 1px 1px 2px #4c8ed9;
            }
            h1, .dash{
                color: black;
                margin: 5%;
                text-align: center;
                padding-bottom: 10px;
                text-shadow: 1px 1px 2px #40ce4e;
            }
            h1, .bandereau-microbit{
                color: black;
                margin: 5%;
                text-align: center;
                padding-bottom: 10px;
                text-shadow: 1px 1px 2px #f2bd12;
            }

            h3{
                margin: 10px;
                padding: 10px;
                font-style: italic;
            }
            .row{
                margin: 10px;
                text-align: center;
            }
            .row, p{
                font-size: 18px;

            }
            .bandereau-mbot{
                margin: 0 auto;
                background-color: #4c8ed9;
                color: gold;
                padding: 10px;
                border: 5px solid darkslateblue;
                border-radius: 5px;
            }
            
            .bandereau-dash{
                margin: 0 auto;
                background-color: #40ce4e;
                color: gold;
                padding: 10px;
                border: 5px solid greenyellow;
                border-radius: 5px;
            }
            .bandereau-microbit{
                margin: 0 auto;
                background-color: #f2bd12;
                color: gold;
                padding: 10px;
                border: 5px solid orange;
                border-radius: 5px;
            }
            p, .bandereau{
                text-shadow: 1px 1px black;
            }
            span, .easy{
                color:green;
                font-weight:bold;
            }
            span, .medium{
                color:orange;
                font-weight:bold;
            }
            span, .hard{
                color:red;
                font-weight:bold;
            }
            .card{
                background-color: #ffffff;
                margin: 10px; 
                padding: 10px;
                border-radius: 5% 5%;
                transition: transform .5s;
            }
            .card:hover{
                background-color: gold;
                transform: scale(1.05);
                box-shadow: 0px 0px 5px 1px black;
            }
            .card-img-top{
                margin: 0 auto;
                padding: 10px;
            }
            .card-text{
                text-align: center;
            }
            
            .img-tuto{
                width: 100%;
                margin: 0;
            }
            .btn{
                margin:5px;
                width: 50%;
            }
            
  </style>
</head>
<body>
    <main class="section">
        <div class="container">
            @yield('contents')
        </div>
    </main>
    
</body>
</html>
<div class="container">

    <div class="row">
        <h3 id="intro-tuto">Dans ce tutoriel, vous allez apprendre à programmer votre robot pour qu'il ce déplace de manière autonome.</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">1- Faire avancer le mbot</h1>
        </div>
        <div class="col-md-7">
            <p>On va commencer par faire avancer le robot. Pour cela, on va selectionner dans <span id="blue">Action</span> la commande <span id="blue">avancer à "50%" de puissance pendant "1 seconde"</span>.</p>
            
            <p>En traduisant ce programme, j'indique à mon robot qu'au démarrage (en mode "ON") : 
                <ul>
                    <li>il avance tout droit à la moitié de sa vitesse maximale (50% de puissance),</li>
                    <li>sur une distance qui dure 1 seconde. Le robot ne parcours pas une distance prédéfini, mais plutôt un temps (en seconde) prédéfini.</li>
                </ul></p>

            <p>Maintenant, pourquoi avoir mis l'événement <span id="orange">Lorsque le mbot(mcore) démarre?</span> 
            Sans cette instruction, le robot n'interprétera pas le code écrit sur mblock. Si on oublie d'utiliser cet événement, le robot ne fera rien du tout. il est donc impératif de toujours l'implémenter !</p>
        </div>
        <div class="col-md-5">
           <a href="{{ url('contents/mbot-avancer-reculer/avancer-1.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-avancer-reculer/avancer-1.jpg') }}" alt="" class="img-tuto"></a>
        </div>
        <div class="col-md-7">
            <p>Notre robot avance, mais pas de manière continue. En effet, il interprète exactement ce qu'on lui écrit ! On lui a indiqué d'avancer pendant 1 seconde, ce qu'il a fait. Maintenant on veut qu'il avance sans jamais s'arrêter. Ca tombe bien, il existe dans la partie <span id="orange">Contrôle</span> le bloc <span id="orange">pour toujours</span> qu'on va faire glisser juste en dessous du bloc <span id="orange">démarrer</span>, et introduire l'action <span id="blue">avancer</span> dans ce nouveau bloc. A partir de ce programme, le robot est devenu capable d'avancer tout droit sans jamais s'arrêter, tant qu'il sera allumé (en mode "ON"). N'hésitez pas à modifier la puissance du robot pour le faire avancer rapidement ou lentement et de faire plusieurs test !</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-avancer-reculer/avancer-2.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-avancer-reculer/avancer-2.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">2- Faire reculer le robot</h1>
        </div>
        <div class="col-md-7">
            <p>Le raisonnement reste le même, on va juste remplacer le bloc <span id="blue">avancer</span> par <span id="blue">reculer</span> et observer ce qu'il ce passe.</p>
            
            <p>Le robot va reculer en continue a partir du code téléverser.</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-avancer-reculer/reculer.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-avancer-reculer/reculer.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">3- Faire avancer et reculer le robot</h1>
        </div>
        <div class="col-md-7">
            <p>On a vu comment faire avancer, puis comment faire reculer le robot. Maintenant, on va modifier notre code afin de faire avancer et reculer le robot.</p>
            
            <p>Pour cela, on a juste à ajouter les deux blocs d'action <span id="blue">avancer</span> et <span id="blue">reculer</span> dans le bloc <span id="orange">pour toujours</span> et transmettre le code au robot ! On peut ajouter entre chaque action un bloc <span id="orange">attendre 1 seconde</span> dans la partie contrôle pour marquer un temps d'arrêt d'une seconde entre chaque action.</p>
            <p>Si on traduit le programme, mon robot va pour toujours:
                <ul>
                    <li>avancer pendant 1 seconde à 50% de sa vitesse maximale,</li>
                    <li>attendre 1 seconde à l'arrêt,</li>
                    <li>reculer pendant 1 seconde à 50% de sa vitesse maximale,</li>
                    <li>attendre de nouveau 1 seconde à l'arrêt,</li>
                    <li>répéter tout depuis le début</li>
                </ul>
            
            </p> Vous pouvez bien évidemment modifier la vitesse, le temps d'éxécution de chaque action, ou bien retirer les blocs d'arrêt selon vos envies, de tester et retester différentes combinaisons.</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-avancer-reculer/avancer-reculer.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-avancer-reculer/avancer-reculer.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <h3 id="intro-tuto">
            Grâce à ce tutoriel, vous savez maintenant faire avancer et reculer votre robot de manière autonome, de manière continue ou non. N'hésitez pas à partager vos créations, qui pourrons être publiées sur notre site !</h3>
    </div> 
</div>
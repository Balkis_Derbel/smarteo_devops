<div class="col-md-12">
	<ul class="list">
		@foreach($data as $item)
		<li>
			<?php $it = (array)$item; ?>
			@include('contents/item',$it)
		</li>
		@endforeach
	</ul>

	<div style="display:inline-block">
		{{ $data->links() }}
	</div>
</div>
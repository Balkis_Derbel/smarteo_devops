<div class="container">
    <div class="col-md-12">
        <h3 id="intro-tuto">
            Vous venez de faire l'acquisition d'une carte micro:bit ? Voici quelques informations utiles et essentielles pour pouvoir comprendre son focntionnement et débuter avec.
            </h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="microbit">1- Présentation de la carte micro:Bit</h1>
        </div>
        <div class="col-md-7">
            La carte micro-bit est une petite carte électronique programmable. Véritable micro-ordinateur, cette carte dispose de plusieurs specificités techniques comme :
            <ul>
                <li>25 LEDs programmables</li>
                <li>1 port de connexion</li>
                <li>2 boutons programmables</li>
                <li>Des capteurs de lumière et températures</li>
                <li>Des capteurs de mouvements (accéléromètre et boussole)</li>
                <li>Une communication sans fil via Radio et bluetooth</li>
                <li>Une interface USB</li>
            </ul>   
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/microbit-installation/microbit-details.png') }}" title="Cliquer ici pour agrandir"><img src="{{ url('contents/microbit-installation/microbit-details.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-12">
            <h1 id="microbit">2- Application et programmation</h1>
        </div>    
        <div class="col-md-7">

            <p>Pour pouvoir programmer votre carte micro:bit, il faut utiliser un éditeur de code spécifique. L'éditeur de base proposé par Microsoft est <span id="blue">MakeCode</span> (voir image), que vous pouvez retrouver sur leur site <a href="{{ url('https://microbit.org/fr/guide/quick/') }}" id="links-tuto">en cliquant ici</a>, où tout est très bien détaillé pour bien débuter avec la carte, de l'installation à l'utilisation.</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/microbit-installation/makecode.png') }}" title="Cliquer ici pour agrandir"><img src="{{ url('contents/microbit-installation/makecode.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div>
    <hr /> 
    <div class="col-md-12">
        <h3 id="intro-tuto">
            Vous voilà prêt à utiliser votre carte micro:bit ! N'hésitez pas à consulter nos différents contenus, à réaliser plusieurs projets, et à nous les partager !</h3>
    </div>
</div>




{{-- 
<div class="row">
    <h1></h1>
        <div class="col-md-7">

        </div>
        <div class="col-md-5">

        </div>
</div> 
--}}
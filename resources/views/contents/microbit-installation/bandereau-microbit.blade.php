
<div id="container">
      <div class="row" id="bandereau-microbit">
        
        <div class="col-md-10">
          <h1 id="microbit">Premiers pas avec la carte micro:bit</h1>
        </div>
        <div class="col-md-2">
          <img src="{{ url('img/robot-smarteo.png') }}" alt="" class="img-bandereau">
        </div>
           <div class="text-center col-md-6">
             <p id="tuto">Temps estimé : 20 minutes</p> 
           </div>
           <div class="text-center col-md-6">
             <p id="tuto">Difficulté: <span id="easy">Facile</span></p> 
           </div>  
      </div>
    </div>
    </div>   
  </div>
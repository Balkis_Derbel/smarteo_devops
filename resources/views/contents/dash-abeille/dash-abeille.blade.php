<div class="container">
    <div class="col-md-12">
        <h3 id="intro-tuto">
            Savez vous que des scientifiques ont inventé des modèles de
            robots pour communiquer avec les abeilles ? <br />

            «Les abeilles communiquent les lieux de nourriture nouvellement
            trouvés aux nids d’abeilles via une série de mouvements qu’ils
            exécutent dans l’obscurité de la ruche» <br />

            Pour mieux communiquer avec les abeilles, les chercheurs ont
            inventé des robots qui imiter cette danse des abeille. L’objectif
            de cet atelier sera donc de programmer le robot DASH pour reproduire ces
            danses.</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="dash">1- Premier programme</h1>
        </div>   
            <div class="col-md-7">
                Nous allons tout d'abord créer un premier programme pour indiquer à notre robot de se déplacer en dessinant un carré. Le programme peut s’écrire comme sur l'image.
            </div>
            <div class="col-md-5">
                <a href="{{ url('contents/dash-abeille/img-2-bis.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-abeille/img-2-bis.png') }}" alt="" class="img-tuto"></a>
            </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            Essayons maintenant de lui faire éxécuter un trajet circulaire. Pour cela, il suffit d’actionner les deux roues à des vitesses différentes : si la roue droite tourne plus vite que la roue gauche, le robot avance en tournant à gauche, et réciproquement.
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/dash-abeille/img-3-bis.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-abeille/img-3-bis.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="dash">2- Danse circulaire</h1>
        </div>
            <div class="col-md-7">
                Pour indiquer la présence de nourriture à faible distance, l’abeille retrace des cercles horizontaux en tournant dans le sens des aiguilles d’une montre et inversement.
            </div>
            <div class="col-md-5">
                <a href="{{ url('contents/dash-abeille/mouvement_cercle.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-abeille/mouvement_cercle.png') }}" alt="" class="img-tuto"></a>
            </div>
    </div>

    <div class="row">
        <div class="col-md-7">
        Nous allons imiter nos chères abeilles en essayant de commander le robot Dash afin qu'il éxécute des mouvements similaires. Pour faire cela, on va lui indiquer comment faire pour tracer un cercle. <br />

        Essayons de faire cela en déclarant des variables pour les vitesses de rotation droite et gauche. Ces vitesses doivent être calibrées pour avoir le cercle le plus parfait.
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/dash-abeille/img-7.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-abeille/img-7.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            Pour exécuter cette danse circulaire, l’abeille va tourner successivement à droite puis à gauche, en faisant un demi-tour entre ces deux mouvements. <br />

            Pour simplifier le programme, essayons de créer des instructions spécifiques pour les demi-cercles droite et gauche. Le programme final peut se représenter comme sur l'image. <br />
        
            Nous pouvons enrichir ce programme, en jouant par exemple sur les effets de lumière et de son lorsque le robot effectue son mouvement.
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/dash-abeille/img-6.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-abeille/img-6.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="dash">3- La danse en huit</h1>
        </div>
            <div class="col-md-7">
                Ce mouvement est utilisé par les abeilles pour indiquer la direction de la nourriture. Les abeilles se déplacent en dessinant un <span id="hard">8</span>, à une vitesse qui indique la distance qu’il faut parcourir pour y arriver. <br>

                Programmons maintenant ce déplacement, en calibrant les fonctions nécessaires pour exécuter les deux grands demi-cercles, ainsi que le mouvement oscillatoire dans l’axe du <span id="hard">8</span>.
            </div>
            <div class="col-md-5">
                <a href="{{ url('contents/dash-abeille/mouvement_huit.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-abeille/mouvement_huit.png') }}" alt="" class="img-tuto"></a>
            </div>
    </div> 
    <hr />
    <div class="row">
        <div class="col-md-12">
            <h3 id="intro-tuto">
                Ca y est, votre robot Dash est maintenant capable de reproduire les mêmes danses de nos chères abeilles, et ainsi communiquer avec elles !
            </h3>

        </div>
    </div>
</div>

{{-----------------------------------------------------
    <div class="row">
    <h1></h1>
    <div class="col-md-7">

</div>
<div class="col-md-5">

</div>
</div> 
--}}





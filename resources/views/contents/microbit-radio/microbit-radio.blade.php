<div class="container">
    <div class="col-md-12">
        <h3 id="intro-tuto">
            Les cartes micro:bit sont dotées d’un émetteur radio, permettant ainsi une communication à distance entre deux cartes différentes. <br />
            Dans cet atelier, nous allons vous proposer un programme afin d'exploiter cet émetteur et ainsi envoyer un message radio via les cartes. <br />
            Pour cela, il est recommandé d'utiliser au minimum deux cartes micro:Bit pour que ce programme soit fonctionnel sous vos yeux !</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="microbit">1- Premier programme</h1>
        </div>
        <div class="col-md-7">
            <p>Pour découvrir la carte, commençons par écrire un premier programme simple qui va afficher un carré plein au démarrage.</p>

            <p>Exploitons ensuite les autres fonctionnalités de la carte pour indiquer comment changer l’affichage sur la carte en fonction de l’action de l’utilisateur en exploitant les différentes fonctionnalités de la carte(boutons, accéléromètre).</p>

            <p>N'hésitez pas à créer des motifs personnalisés !</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/microbit-radio/img-2.jpg') }}" title="Cliquer ici pour agrandir"><img src="{{ url('contents/microbit-radio/img-2.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-12">
            <h1 id="microbit">2- Message radio</h1>
        </div>    
        <div class="col-md-7">
            <p>Maintenant que vous maîtrisez les bases, passons à un code exploitant l'émetteur radio. On va écrire un programme identique à l’exemple sur l'image, permettant ainsi d'afficher le message envoyé par la carte distante.</p>

            <p>Notez bien que nous avons défini un <span id="pink">groupe</span> d’émission radio, qui joue le rôle d’une <span id="pink">fréquence d’émission</span>. Pour communiquer, les deux cartes doivent donc être sur le même groupe !</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/microbit-radio/img-3.jpg') }}" title="Cliquer ici pour agrandir"><img src="{{ url('contents/microbit-radio/img-3.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>
    <hr /> 
    <div class="col-md-12">
        <h3 id="intro-tuto">
            Maintenant que vous savez utiliser l'émetteur radio, n'hésitez pas à envoyer différents messages à vos ami(e)s qui possèdent une carte micro:Bit !</h3>
    </div>
</div>
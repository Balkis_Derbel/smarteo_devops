<div class="container">
    <div class="row">
        <h3 id="intro-tuto">
           Vous venez d'obtenir votre permier robot DASH et vous aimeriez pouvoir vous en servir ? Pas de panique, ce tutoriel vous guidera pas à pas dans l'installation et l'utilisation de votre robot.</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="dash">1- Présentation du robot Dash</h1>
        </div>   
        <div class="col-md-7">
            <p>Le robot véhicule Dash est équipé de deux moteurs permettant de contrôler les déplacements. Il est aussi possible de contrôler les mouvements de la tête horisontalement et verticalement.</p>

            <p>Ce robot possède aussi un ensemble de capteurs (capteur de voix, capteur d’obstacle) permettant de créer des intéractions avec son environnement extérieur.</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/dash-installation/dash.png') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-installation/dash.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="dash">2- L'application Blockly</h1>
        </div>
        <div class="col-md-7">
            <p>La programmation se fair avec le logiciel <span id="blue">Blockly</span> que vous pouvez télécharger sur votre tablette. Une fois le logiciel prêt à l'emploi, celui-ci permet de faire de la programmation <span id="easy">"par blocks"</span>. L’interface de l’outil est comme sur l'image:
                <ul>
                <li>A gauche, les différentes instructions sont regroupées en fonction de leur type.</li>
                <li>A droite, la zone ou l'on fait glisser ces instructions qu'on associera par block.</li>
            </ul></p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('img/dash-installation/img-1.PNG') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/dash-installation/img-1.png') }}" alt="" class="img-tuto"></a>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12">
            <h3 id="intro-tuto">
                Vous êtes maintenant prêt pour piloter votre robot Dash ! Vous pouvez consulter nos différents programmes afin de bien assimiler les bases de l'application, et créer ainsi votre propre programme. N'hésitez pas à partager vos créations, qui pourrons être publiées sur notre site !
            </h3>

        </div>
    </div>
</div>






<div class="container">

    <div class="row">
        <h3 id="intro-tuto">Ce tutoriel va nous apprendre comment faire tourner le robot mbot à partir du logiciel mBlock.</h3>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">1- Choisir un sens de rotation</h1>
        </div>
        <div class="col-md-7">
            <p>On va commencer par développer un programme très simple:
                <ul>
                    <li><span id="orange">Lorsque le mbot démarre</span>,</li>
                    <li><span id="blue">tourner à droite (ou gauche) à 50% pendant 1 seconde.</span></li>
                </ul>
            </p>
            
            <p>Ici, lorsque le robot est allumé, celui-ci va tourner sur sa droite à 50% de sa vitesse max, le tout sur une durée de 1 seconde. Après celà, le robot s'arrête. il faut donc l'éteindre puis le rallumer ( mode "OFF" puis "ON") pour que le robot réitère les instructions du programme crée.</p>
        </div>
        <div class="col-md-5">
           <a href="{{ url('contents/mbot-tourner/tourner-1.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-tourner/tourner-1.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>
        
        <div class="row">
            <div class="col-md-12">
            <h1 id="mbot">2- Faire tourner en rond</h1>
        </div>
        <div class="col-md-7">
            <p>Il est possible de faire tourner le robot en rond sans arrêt. Pour cela, il suffit juste de rajouter dans la partie <span id="orange">contrôle</span> le bloc <span id="orange">pour toujours</span>.</p>
            
            <p>On peut modifier sa vitesse de rotation en modifiant le % de puissance (plus c'est élevé, plus il tournera vite). Il n'est cependant pas nécessaire de modifier le temps, puisqu'on répète l'action à l'infini avec le bloc <span id="orange">pour toujours</span>.</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-tourner/tourner-2.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-tourner/tourner-2.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h1 id="mbot">3- Faire tourner puis avancer</h1>
        </div>
        <div class="col-md-7">
            <p>Maintenant que l'on sait faire tourner le robot en sélectionnant un sens de rotation, on va pouvoir ajouter une nouvelle action qui s'éxécutera avant ou après la rotation. On va rajouter par exemple l'action <span id="blue">avancer</span> dans notre programme.</p>

            <p>En téléversant les instructions au robot, celui va:
                <ul>
                    <li><span id="blue">tourner</span> (à gauche ou à droite) pendant 0.5 seconde,</li>
                    <li><span id="blue">avancer</span> tout droit pendant 1 seconde,</li>
                    <li>recommencer à nouveau ces deux actions continuellement.</li>
                </ul>
            </p>
            <p>On peut ajouter des <span id="orange"> contrôles d'attente</span> comme sur l'image pour bien observer l'éxécution de chaque actions, mais si on préfère voir le robot agir en continue sans s'arrêter, alors ce n'est pas la peine d'en ajouter. On peut très bien aussi faire <span id="blue">reculer</span> le robot puis le faire tourner, libre à vous de completer votre programme !</p>
        </div>
        <div class="col-md-5">
            <a href="{{ url('contents/mbot-tourner/tourner-avancer.jpg') }}" title="cliquer ici pour agrandir"><img src="{{ url('contents/mbot-tourner/tourner-avancer.jpg') }}" alt="" class="img-tuto"></a>
        </div>
    </div>

    <div class="row">
        <h3 id="intro-tuto">
            Ce tutoriel permet d'exploiter de nouvelles actions, et de les combiner avec d'autre, rendant ainsi le robot plus autonome, tout en explorant un peu plus les alentours.  N'hésitez pas à partager vos créations, qui pourrons être publiées sur notre site !</h3>
    </div> 
</div>
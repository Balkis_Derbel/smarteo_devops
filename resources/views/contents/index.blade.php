@extends('layouts.default')

@section('title','Contenus et Tutoriels - SMARTEO')

@section('scripts')

<script type="text/javascript">

	$(function() {
		$('body').on('click', '.pagination a', function(e) {
			e.preventDefault();

			$('#load a').css('color', '#dfecf6');

			var url = '{{url('contenus-et-tutoriels')}}' + $(this).attr('href');  
			getContents(url);
			window.history.pushState("", "", url);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});

		$(".search_button").click(function(e) {
			e.preventDefault();
			var url = '{{url('contenus-et-tutoriels')}}';
			getContents(url);
			window.history.pushState("", "", url);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});

		function getContents(url) {
			var name = $('#search_box').val();

			$.ajax({
				url : url,
				data 	: {
					search : name
				}
			}).done(function (data) {
				$('#content_list').empty().html(data);
			}).fail(function () {
				alert('Une erreur est survenue ...');
			});
		}
	});

</script>

@stop


@section('content')

<!-- kits -->

@include('contents/contents-header')

<section class="bg-clouds">

	<div class="container container-fluid pt-5 pb-5">

		<div class="row">
			<div class="col-md-12">
				<form method="get" action="" autocomplete="on" class="search_box">
					<input type="text" id="search_box" class="from-control" placeholder=""/>
					<input type="submit" class="search_button" style="z-index:5;"/>
				</form>
			</div>
		</div>

		<div class="row text-center d-inline-block w-100" id="content_list">
			@include('contents/items-list')
		</div>
	</div>

</section>

@stop
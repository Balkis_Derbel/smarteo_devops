@extends('layouts.default')

@section('title','Qui sommes nous - SMARTEO - Les kits robotiques pour apprendre à l\'ère du digital')

@section('description','Smarteo vise à déveloper les compétences numériques des enfants à travers la robotique et la programmation. Nous offrons des kits prêts à l\'emploi en mode \'location\' qui incluent les appareils nécessaires (robots, composants) ainsi que des ressources pédagogiques et des tutoriels, pour organiser des activités riches et créatives.')

@section('content')

<section class="bg-light">

    @include('includes.masthead', ['which' => "engagements", 'title' =>"Qui sommes nous"])

    <div class="container pt-5 pb-5">
        <div class="row text-justify">

            <div class="col md-12">
                <h2 class='lead'>Les nouvelles technologies changent rapidement le monde dans lequel nous vivons. Selon une étude récente*, 85% des métiers de 2030 n'existent pas encore. Les nouveaux métiers seront radicalement impactés par les nouvelles innovations technologiques qui font désormais partie de notre quotidien.</p><p>*source : étude 2017 de Dell et le think tank L’institut du futur</h2>


                <p class='lead'>
                Smarteo vise à déveloper les compétences numériques des enfants à travers <strong class='text-info b'>la robotique et la programmation</strong>. Nous offrons des kits prêts à l'emploi qui incluent les appareils nécessaires (robots, composants) ainsi que des ressources pédagogiques et des tutoriels, pour organiser des activités riches et créatives. Nos kits peuvent être souscrits en mode <strong class='text-info b'>location et paiement à l’usage</strong> afin de les rendre accessibles au plus grand nombre d’apprenants.</p>

                <p class='lead'>
                    Plusieurs oranismes nous soutiennent déjà, et nous travaillons main dans la main avec les établissements scolaires et les enseignants afin de concevoir les kits les plus pertinents.
                </p>

            </div>
        </div>

        <div class="row text-center pt-5">
            <div class="col-md-12">
                <h1>Ils nous soutiennent</h1>

                <a href="http://www.theschoolab.com/"><img src="{{URL::to('img/logos/logo-schoolab.jpg')}}"  style="width:260px;margin:10px;" alt="logo_schoolab"/></a>

                <a href="http://www.observatoire-edtech.com/"><img src="{{URL::to('img/logos/logo_obs_edtech.png')}}"  style="width:220px;margin:10px;" alt="logo_observatoire_edtech"/></a>

                <a href="https://hdsi.asso.fr/actualites-hdsi/773-sceaux-un-territoire-a-fort-potentiel"><img src="{{URL::to('img/logos/logo_sceaux_valley.png')}}" style="width:280px;margin:10px;" alt="logo_sceaux_valley"/></a>
<br/>
                <a href="http://www.bge-parif.com/"><img src="{{URL::to('img/logos/logo_bge_parif.png')}}"  style="width:160px;margin:10px;" alt="logo_bge_parif"/></a>

                <a href="http://www.valleesud.fr/"><img src="{{URL::to('img/logos/logo_vallee_sud_grand_paris.png')}}" style="width:180px;margin:10px;" alt="logo_vallee_sud_grand_paris"/></a>

            </div>
        </div>

    </div>


    <div class="container pt-5 pb-5">
        <div class="row">

            <div class="col-md-12 pb-3">
                <a href="{{url('/kits')}}">
                    <div class="button-img img-5">
                        <h2>Voir les kits</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{url('/actualites')}}">
                    <div class="button-img img-1">
                        <h2>Consulter nos dernières actualités</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{url('nos-engagements')}}">
                    <div class="button-img img-2">
                        <h2>En savoir plus sur nos engagements</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{url('surveys')}}">
                    <div class="button-img img-3">
                        <h2>Participez à nos enquêtes exclusives</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{url('faq')}}">
                    <div class="button-img img-4">
                        <h2>Consulter nos questions fréquentes</h2>
                    </div>
                </a>
            </div>
        </div>
    </div>

</section>
@stop
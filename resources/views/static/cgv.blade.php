@extends('layouts.default')

@section('title','Mentions légales')


@section('content')

<section class="bg-light">

    @include('includes.masthead', ['which' => "faq", 'title' =>"Conditions Générales de Vente"])

    <div class="container text-left">

        <div class="page-content">


            <div class="row">
                <div class="col-md-12 text-justify">

                <p class="text-info">Les présentes conditions générales de vente sont valables à partir du 19 septembre 2018. </p>

                <h1 class="mt-3">Préambule</h1>
                Le site est la propriété de la société SMARTEO en sa totalité, ainsi que l'ensemble des droits y afférents. Toute reproduction, intégrale ou partielle, est systématiquement soumise à l'autorisation des propriétaires. Toutefois, les liaisons du type hypertextes vers le site sont autorisées sans demandes spécifiques.

                <h1 class="mt-3">1. Acceptation des conditions</h1>
                Le client reconnaît avoir pris connaissance, au moment de la passation de commande, des conditions particulières de vente énoncées sur cet écran et déclare expressément les accepter sans réserve. Les présentes conditions générales de vente régissent les relations contractuelles entre SMARTEO et son client, les deux parties les acceptant sans réserve. Ces conditions générales de vente prévaudront sur toutes autres conditions figurant dans tout autre document, sauf dérogation préalable, expresse et écrite.

                <h1 class="mt-3">2. Commande</h1>
                Les systèmes d'enregistrement automatique sont considérés comme valant preuve, de la nature, du contenu et de la date de la commande. SMARTEO confirme l'acceptation de sa commande au client à l'adresse mail que celui-ci aura communiqué. La vente ne sera conclue qu'à compter de la confirmation de la commande. SMARTEO se réserve le droit d'annuler toute commande d'un client avec lequel existerait un litige relatif au paiement d'une commande antérieure. Les informations énoncées par l'acheteur, lors de la prise de commande engagent celui-ci : en cas d'erreur dans le libellé des coordonnées du destinataire, le vendeur ne saurait être tenu responsable de l'impossibilité dans laquelle il pourrait être de livrer le produit.

                <h1 class="mt-3">3. Livraison</h1>
                Après confirmation de commande, SMARTEO s'engage à livrer à son transporteur, toutes les références commandées par l'acheteur. Ce transporteur s'engage lui par contrat avec SMARTEO à livrer la commande à l'adresse de l'acheteur fournie par SMARTEO. 
                Les clients ou les destinataires des produits s'interdisent toute revente partielle ou totale des produits. SMARTEO se dégage de fait de toute responsabilité juridique si l'acquittement des taxes n'était pas effectué par le client. 
                La livraison est effectuée dans le créneau horaire prévu avec le client par la remise directe du produit au destinataire annoncé, soit en cas d'absence, à une autre personne habilitée par le client. Conformément au code de la consommation, le client peut dénoncer le contrat en cas de dépassement de la date de livraison excédant 7 jours et non dû à un cas de force majeure. 
                Dans le cas d'un produit technique, le client veillera particulièrement à vérifier le bon fonctionnement de l'appareil livré, à lire la notice d'emploi qui lui est fournie. En cas de défauts apparents, l'acheteur bénéficie du droit de retour dans les conditions prévues dans ce document.
                Sont considérés comme cas de force majeure déchargeant le vendeur de son obligation de livrer, la guerre, l'émeute, l'incendie, les grèves, les accidents et l'impossibilité d'être approvisionné.
                Conformément au code de la consommation, SMARTEO est responsable du bon acheminement des produits et d'une manière générale responsable de la bonne exécution du contrat. Ainsi, sauf en cas de force majeure, le client n'aura pas à démontrer la faute d'autres intervenants en cas de mauvaise exécution du contrat.
                Il est conseillé au client de systématiquement vérifier le colis à l'arrivée et à refuser tout colis endommagé.
                Le client dispose d'un délai de 48 heures pour faire d'éventuelles réserves auprès du transporteur en cas de manquant ou de dégradation.
                Pour des raisons de disponibilité, une commande peut être livrée en plusieurs fois au client. Le client ne règle alors qu'une seule livraison. Si le client souhaite 2 lieux de livraison, il passe 2 commandes, avec les frais de livraison liés.

                <h1 class="mt-3">4. Rétractation</h1>
                Vous avez 14 jours (à compter de la réception des articles) pour vous faire une opinion. En cas d'échange ou remboursement, le client doit renvoyer le(les) article(s) neuf(s) dans son(leurs) emballage d'origine, intact, accompagné de tous les accessoires éventuels, notices d'emploi et documentations à l'adresse suivante :
                SMARTEO - 24 Avenue Georges Clemenceau - 92330 Sceaux - FRANCE
                En cas d'exercice du droit de rétractation, SMARTEO est tenu au remboursement des sommes versées par le client, sans frais, à l'exception des frais de retour. Le remboursement est dû dans un délai maximum de 14 jours. Dans le cas où le client aura choisi pour l'expédition initiale des produits un mode de livraison express, SMARTEO ne remboursera que les frais d'expédition standard, c'est à dire correspondant au mode économique.

                <h1 class="mt-3">5. Prix</h1>
                Le prix est exprimé en euros toutes taxes comprises.
                Le prix indiqué dans la confirmation de commande est le prix définitif, exprimé toutes taxes comprises et incluant la TVA pour la France et les pays de la communauté Européenne. Ce prix comprend le prix des produits, les frais de manutention, d'emballage et de conservation des produits, les frais de transport et de mise en service.

                <h1 class="mt-3">6. Paiement</h1>
                Le prix facturé au client est le prix indiqué sur la confirmation de commande adressée par SMARTEO.
                Le prix des produits est payable au comptant le jour de la commande effective.
                Le paiement s'effectue par carte bancaire.
                Le système de paiement par carte bancaire vous permet de régler via une passerelle de paiement STRIPE dans un environnement sécurisé. Votre règlement s'effectue directement à une banque dans un environnement sécurisé sans passer par le serveur de la boutique, garantie d'autant plus importante que vos informations de règlement et en particulier vos codes de carte bancaire ne sont à aucun moment porté à la connaissance de SMARTEO, la transaction s'effectuant en direct avec la banque qui nous informe seulement du succès ou de l'échec du règlement.
                La commande validée par le client ne sera considérée effective que lorsque les centres de paiement bancaire concernés auront donné leur accord. En cas de refus des dits centres, la commande sera automatiquement annulée et le client prévenu par courrier électronique. Par ailleurs, SMARTEO se réserve le droit de refuser toute commande d'un client avec lequel existerait un litige.

                <h1 class="mt-3">7. Litiges</h1>
                Le présent contrat est soumis au droit français. SMARTEO ne peut être tenu pour responsable des dommages de toute nature, tant matériels qu'immatériels ou corporels, qui pourraient résulter d'un mauvais fonctionnement ou de la mauvaise utilisation des produits commercialisés. Il en est de même pour les éventuelles modifications des produits résultant des fabricants. En cas de difficultés dans l'application du présent contrat, l'acheteur a la possibilité, avant toute action en justice, de rechercher une solution amiable notamment avec l'aide : d'une association professionnelle de la branche, d'une association de consommateurs ou de tout autre conseil de son choix. Il est rappelé que la recherche de la solution amiable n'interrompt pas le " bref délai " de la garantie légale, ni la durée de la garantie contractuelle. Il est rappelé qu'en règle générale et sous réserve de l'appréciation des Tribunaux, le respect des dispositions du présent contrat relatives à la garantie contractuelle suppose que l'acheteur honore ses engagements financiers envers le vendeur. Les réclamations ou contestations seront toujours reçues avec bienveillance attentive, la bonne foi étant toujours présumée chez celui qui prend la peine d'exposer ses situations. En cas de litige, le client s'adressera en priorité à SMARTEO pour obtenir une solution amiable.

                <h1 class="mt-3">8. Garantie</h1>
                En toute hypothèse, le client bénéficie de la garantie légale d'éviction et des vices cachés (Art.1625 et suivants du Code Civil). A la condition que l'acheteur fasse la preuve du défaut caché, le vendeur doit légalement en réparer toutes les conséquences (art.1641 et suivants du code civil) ; si l'acheteur s'adresse aux tribunaux, il doit le faire dans un " bref délai " à compter de la découverte du défaut caché (art.1648 du code civil). Vous pouvez contacter le service clients par mail à contact@smarteo.co.

                <h1 class="mt-3">10. Informations légales</h1>
                Le renseignement des informations nominatives collectées aux fins de la vente à distance est obligatoire, ces informations étant indispensables pour le traitement et l'acheminement des commandes, l'établissement des factures et contrats de garantie. Le défaut de renseignement entraîne la non validation de la commande. Conformément à la loi " Informatique et Libertés ", le traitement des informations nominatives relatives aux clients a fait l'objet d'une déclaration auprès de la Commission Nationale de l'Informatique et des Libertés (CNIL). Le client dispose (article 34 de la loi du 6 janvier 1978) d'un droit d'accès, de modification, de rectification et de suppression des données qui le concernent, qu'il peut exercer auprès de SMARTEO.


            </div>
        </div>
    </div>
</div>

</section>

@stop
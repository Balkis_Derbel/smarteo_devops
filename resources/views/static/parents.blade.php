@extends('layouts.default')

@section('title','Parents - SMARTEO - Apprendre en s\'amusant avec des robots')

@section('description','Avec Smarteo vous avez un grand choix de kits éducatifs que vous pouvez utiliser pour offrir des expériences originales
	à vos enfants. Nos kits incluent des équipements complets et prêts à l\'emploi, ainsi que des tutoriels pour vous aider à accompagner votre enfant dans son aventure créative.')

@section('content')

<!-- Parents -->

<section class="parents bg-light">	

	<div class="masthead parents text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">

					<h1 style="font-size:24px;line-height:35px;">			
						Savez-vous que selon une récente étude <span style="font-size:40px;color:#f6b93b;font-weight:bold;vertical-align:middle;">85%</span> des emplois de 2030 n'existent pas encore ?
					</h1>

					<h1 style="font-size:40px;color:#f6b93b;font-weight:bold;line-height:40px;margin:30px 0 0;">			
						Vos enfants sont t-ils prêts pour la révolution digitale ?			
					</h1>

				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row text-center d-inline-block">
			<div class="col-md-10 col-lg-8 col-xl-12 mx-auto">

				<h2 style="font-size:30px;color:#60a3bc;line-height:30px;margin:30px 0 0;">
					On t'ils déjà eu l'occasion de <span style="font-weight:bold">programmer un robot</span> ?
				</h2>
				<h2 style="font-size:30px;color:#60a3bc;line-height:30px;margin:10px 0 40px;">
					On t'ils déjà <span style="font-weight:bold">conçu et imprimé leur première maquette 3D</span> ?
				</h2>


				<h4 style="margin: 30px 0;text-align:justify;">
					Avec Smarteo vous avez un grand choix de kits éducatifs que vous pouvez utiliser pour offrir des expériences originales
					à vos enfants. Nos kits incluent des équipements complets et prêts à l'emploi, ainsi que des tutoriels pour vous aider à accompagner votre enfant dans son aventure créative.
				</h4>

				<h4 style="margin: 30px 0;text-align:justify;">
					Les kits sont proposés en mode <span style="font-weight:bold;">paiement à l'usage</span>. Vous ne payez donc que pour votre durée réelle d'utilisation. Et cela vous permet de varier les activités que vous pourrez offrir à votre enfant.
				</h4>
			</div>
		</div>

		<div class="row features-icons-1" style="text-align:center;">
			<div class="col-lg-4">
				<div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
					<div class="features-icons-icon">
						<span style="font-size: 300%; color: #C4E538;">&#9312;</span>
						<img src="{{url('img/ico-modulable.png')}}" alt="chosissez"/> 
					</div>
					<h4 class="lead mb-0">Vous choisissez avec votre enfant parmis notre sélection de kits, en fonction de ses intérêts et de son âge.</h4>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="features-icons-item mx-auto mb-0 mb-lg-3">
					<div class="features-icons-icon">
						<span style="font-size: 300%; color: #C4E538;">&#9313;</span>
						<img src="{{url('img/ico-build.png')}}" alt="recevez" /> 
					</div>
					<h4 class="lead mb-0">Vous recevez vos kits chez vous. Ils sont complets et prêts à l'emploi, tutoriels inclus.</h4>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="features-icons-item mx-auto mb-0 mb-lg-3">
					<div class="features-icons-icon">
						<span style="font-size: 300%; color: #C4E538;">&#9314;</span>
						<img src="{{url('img/ico-simple.png')}}" alt="retournez" /> 
					</div>
					<h4 class="lead mb-0">Vous vous êtes bien amusés ? Vous nous renvoyez les kits et pourrez repartir sur une nouvelle aventure.</h4>
				</div>
			</div>

			<div class="explore-btn" style="margin-top:60px;">
				<a href="{{url('/#smart-kits')}}" > Explorer les kits</a>
			</div>

		</div>
	</div>
</section>



<section class="contact" id="contact">
	<div class="bg"></div>
	<div class="bg1"></div>
	<div class="container-fluid p-0">
		<div class="row no-gutters">

			<div class="col-lg-6 order-lg-1">
				<div class="contact-text">
					<h1> Besoin de plus de détails ? </h1>
					<h4> N'hésitez pas à nous contacter pour une démonstration gratuite de nos kits, et pour définir la formule la plus adaptée à vos besoins. </h4>
				</div>
			</div>

			<div class="col-lg-6 order-lg-2">	
				



				{!! Form::open(['url' => 'contactForm']) !!}
				<div class="row form" style="margin: 2rem 3rem;">
					<div class="box">

						<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">

							{!! Form::text('nom', null, ['class' => 'form-control name', 'placeholder' => 'Votre nom']) !!}
							{!! $errors->first('nom', '<small class="help-block">:message</small>') !!}

						</div>

						<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">

							{!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Votre email']) !!}
							{!! $errors->first('email', '<small class="help-block">:message</small>') !!}

						</div>

						<div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">

							{!! Form::text('phone', null, ['class' => 'form-control phone', 'placeholder' => 'Votre numéro']) !!}
							{!! $errors->first('phone', '<small class="help-block">:message</small>') !!}

						</div>

						<div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">

							{!! Form::textarea ('texte', null, ['class' => 'form-control', 'placeholder' => 'Votre message']) !!}
							{!! $errors->first('texte', '<small class="help-block">:message</small>') !!}

						</div>
						
						{{ Form::hidden('route', '/parents') }}

						{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
					</div>
				</div>
				{!! Form::close() !!}

			</div>
		</div>

	</div>

</section>

@stop
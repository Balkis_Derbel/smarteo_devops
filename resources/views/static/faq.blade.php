@extends('layouts.default')

@section('title','FAQ')

@section('description','Questions fréquentes, Quand commence et finit la période de location des kits robotiques, Livraison et retours')

@section('content')


<section class="bg-light">

  @include('includes.masthead', ['which' => "faq", 'title' =>"Questions Fréquentes"])

  <div class="container text-left">

    <div class="page-content">


      <div class="row">
        <div class="col-md-12">
          <!-- PAGE CONTENT BEGINS -->

          <div class="tabbable">

            <div class="tab-content no-border padding-24">
              <div id="faq-tab-1" class="tab-pane" style="display:block;">
                <h3 class='text-info'>
                  <i class="icon-ok bigger-110"></i> Politique de location
                </h3>

                <div class="space-8"></div>

                <div id="faq-list-1" class="panel-group accordion-style1 accordion-style2">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-1-1" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Quand commence et finit la période de location des kits ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-1-1">
                      <div class="panel-body">
                        La période de location commence à la date de livraison de votre commande. Le kit doit être renvoyé avant la date de fin de location (cachet de poste faisant foi). Si la date de fin de location correspond à un weekend ou à un jour férié, le kit peut être renvoyé le jour ouvrable suivant.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-1-2" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Puis-je annuler ma commande ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-1-2">
                      <div class="panel-body">
                        L'annulation est autorisée dans les 24 heures. Il vous suffit d’envoyer un email au service client (contact@smarteo.co) en indiquant votre numéro de commande.
                        <br> L'annulation de commande n'est acceptable que dans les 24 heures suivant la commande. En cas d'urgence ou de tout autre événement justifiant une annulation, veuillez contacter le service client (contact@smarteo.co).
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-1-3" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Et si je veux prolonger la période de location ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-1-3">
                      <div class="panel-body">
                        Il est possible de prolonger la période de location en envoyant un email au service clients (contact@smarteo.co) avec votre numéro de commande et la période d'extension souhaitée. Dès réception de votre demande, nous vous enverrons une facture séparée par email. La période de location sera automatiquement prolongée dès le paiement de la nouvelle facture. Veuillez noter que le paiement doit être effectué avant la date d'échéance du retour d'origine pour éviter les frais de retard.
                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-1-4" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Dois-je payer un dépôt de garantie ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-1-4">
                      <div class="panel-body">
                        Aucun dépôt n'est requis pour la plupart des kits. Pour certains produits, vous devrez peut-être payer un dépôt. Si le kit est retourné en retard ou pas du tout, des frais associés seront ajoutés au mode de paiement utilisé pour la location.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-1-5" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Que se passe-t-il si je perds ou si je casse des pièces ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-1-5">
                      <div class="panel-body">
                        Il n'y a pas de frais pour l'usure normale ou pour les petites pièces facilement remplaçables. Cependant, pour les composants principaux (par exemple, les contrôleurs), nous facturons des frais de remplacement en fonction du prix de la pièce. Si les dommages résultent d'une utilisation inappropriée, d'un dégât d'eau, d'une casse traumatique (chute, projection, etc.), des frais équivalents au prix de vente du kit seront facturés.
                      </div>
                    </div>
                  </div>


                </div>
              </div>



              <div id="faq-tab-2" class="tab-pane mt-3" style="display:block;">
                <h3 class='text-info'>
                  Livraison et retours
                </h3>

                <div class="space-8"></div>

                <div id="faq-list-2" class="panel-group accordion-style1 accordion-style2">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-2-1" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle collapsed">
                      Comment retourner un kit ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-2-1">
                      <div class="panel-body">
                        Utilisez l'étiquette de retour prépayée incluse dans le paquet. Lorsque le produit arrive, assurez-vous qu'il comprend une étiquette de retour. Au retour, placez toutes les pièces démontées dans la boîte du produit. Placez ensuite la boîte de produit dans la boîte Smarteo et scellez avec du ruban adhésif. Collez l'étiquette de retour sur la boîte. Déposez-le au bureau de poste.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-2-2" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle collapsed">
                      Que faire si je perds l'étiquette de retour ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-2-2">
                      <div class="panel-body">
                        Veuillez contacter le service clientèle (contact@smarteo.co). Nous vous enverrons un lien pour imprimer une nouvelle étiquette de retour.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-2-3" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle collapsed">
                      Quel est le coût d’expédition ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-2-3">
                      <div class="panel-body">
                        Les frais de port (y compris les frais de retour) sont gratuits, quelle que soit la région en France métropolitaine. Des frais additionnels peuvent s’ajouter pour des envois à l’étranger.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-2-4" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle collapsed">
                      Combien de temps dure généralement la livraison ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-2-4">
                      <div class="panel-body">
                        L'expédition prend habituellement 5-6 jours ouvrables à partir du jour où vous passez votre commande.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-2-5" data-parent="#faq-list-1" data-toggle="collapse" class="accordion-toggle collapsed">
                      Que faire en cas de pièces manquantes ou défectueuses dans l'envoi que je reçois ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-2-5">
                      <div class="panel-body">

                        Tous les kits sont inspectés de près avant l'expédition, mais s'il y a des pièces manquantes ou défectueuses, veuillez contacter le service client (contact@smarteo.com). Vous pouvez choisir de renvoyer le kit ou demander une pièce de rechange. Si vous voulez retourner, nous vous rembourserons votre paiement à 100% une fois que nous aurons reçu le kit. Si vous choisissez d'obtenir une pièce de rechange, nous vous l'enverrons le plus rapidement possible et la période de location ne commencera pas avant la réception de la pièce de rechange. Lorsque vous retournez ce produit, veuillez inclure les pièces manquantes ou défectueuses (c.-à-d., Ne pas jeter les pièces défectueuses). Sinon, nous pouvons facturer le prix de la pièce.
                      </div>
                    </div>
                  </div>
                </div>
              </div>



              <div id="faq-tab-3" class="tab-pane mt-3" style="display:block;">
                <h3 class='text-info'>
                  Autres questions
                </h3>

                <div class="space-8"></div>

                <div id="faq-list-3" class="panel-group accordion-style1 accordion-style2">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-3-1" data-parent="#faq-list-3" data-toggle="collapse" class="accordion-toggle collapsed">
                      Les kits de location peuvent-ils être achetés?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-3-1">
                      <div class="panel-body">
                        Si vous souhaitez garder le kit, vous pouvez envoyer un email au service clients (contact@smarteo.co) avec votre numéro de commande, afin de calculer le supplément à payer.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-3-2" data-parent="#faq-list-3" data-toggle="collapse" class="accordion-toggle collapsed">
                      Puis-je acheter une carte-cadeau pour Smarteo ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-3-2">
                      <div class="panel-body">
                        Smarteo n'offre pas encore d'achat par carte-cadeau. Cependant, nous fournirons des options de cartes-cadeaux dans un proche avenir.
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <a href="#faq-3-3" data-parent="#faq-list-3" data-toggle="collapse" class="accordion-toggle collapsed">
                      Comment puis-je obtenir de l'aide ?</a>
                    </div>

                    <div class="panel-collapse collapse" id="faq-3-3">
                      <div class="panel-body">
                        Pour obtenir de l'aide, veuillez nous contacter par email (contact@smarteo.co). Généralement, dans les 24 heures ouvrables, notre équipe examinera et répondra à votre question.
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>

          <!-- PAGE CONTENT ENDS -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </div>
</section>

@stop
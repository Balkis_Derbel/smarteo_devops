@extends('layouts.default')

@section('title','Enseignants - SMARTEO - Les kits robotiques pour apprendre à l\'ère du digital')

@section('description','Vous êtes enseignant ? Offrez des expériences pédagogiques créatives et digitales à vos élèves. Nos kits incluent des équipements prêts à l\'emploi pour apprendre la programmation, la robotique et l\'art digital. Ils sont fournis avec des tutoriels et des ressources pour aider à concevoir les ateliers.')

@section('content')

<!-- Enseignant -->

<section class="bg-light">

    <div class="masthead enseignant text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="description col-xl-9 mx-auto">
                    <h1>
                        Vous êtes enseignant ? Offrez des expériences pédagogiques <span class="focus">riches</span>, <span class="focus">créatives</span> et <span class="focus">digitales</span> à vos élèves.
                    </h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row" style="text-align:center;display:inline-block;">
            <div class="col-md-10 col-lg-8 col-xl-12 mx-auto">


                <h2 style="margin: 30px 0;text-align:justify;">
                    Avec Smarteo vous avez un grand choix de kits éducatifs que vous pouvez utiliser pour concevoir des expériences originales à vos élèves. Nos kits incluent des équipements prêts à l'emploi pour apprendre la programmation, la robotique et l'art digital. Ils sont fournis avec des tutoriels et des ressources pour aider à concevoir les ateliers.
                </h2>
            </div>
        </div>

        <div class="row features-icons-1" style="text-align:center;">
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                    <div class="features-icons-icon">
                        <span style="font-size: 300%; color: #C4E538;">&#9312;</span>
                        <img src="{{url('img/ico-plan.png')}}" alt="planifier" />
                    </div>
                    <h4 class="lead mb-0">Planifiez vos ateliers. On peut vous donner un coup de main et fournir ressources et tutoriels.</h4>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon">
                        <span style="font-size: 300%; color: #C4E538;">&#9313;</span>
                        <img src="{{url('img/ico-build.png')}}" alt="recevez" />
                    </div>
                    <h4 class="lead mb-0">Recevez vos équipements prêts à l'emploi et assurez votre cours.</h4>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                    <div class="features-icons-icon">
                        <span style="font-size: 300%; color: #C4E538;">&#9314;</span>
                        <img src="{{url('img/ico-simple.png')}}" alt="retournez" />
                    </div>
                    <h4 class="lead mb-0">On récupère les équipements pour entretien et réparation.</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="explore-btn">
                <a href="{{url('/#smart-kits')}}"> Explorer les kits</a>
            </div>
        </div>
    </div>
</section>



<section class="contact" id="contact">
    <div class="bg"></div>
    <div class="bg1"></div>
    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-lg-6 order-lg-1">
                <div class="contact-text">
                    <h1> Besoin de plus de détails ? </h1>
                    <h4> N'hésitez pas à nous contacter pour une démonstration gratuite de nos kits, et pour définir la formule la plus adaptée à vos besoins. </h4>
                </div>
            </div>

            <div class="col-lg-6 order-lg-2">


                {!! Form::open(['url' => 'contactForm']) !!}
                <div class="row form" style="margin: 2rem 3rem;">
                    <div class="box">

                        <div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">

                            {!! Form::text('nom', null, ['class' => 'form-control name', 'placeholder' => 'Votre nom']) !!}
                            {!! $errors->first('nom', '<small class="help-block">:message</small>') !!}

                        </div>

                        <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">

                            {!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Votre email']) !!}
                            {!! $errors->first('email', '<small class="help-block">:message</small>') !!}

                        </div>

                        <div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">

                            {!! Form::text('phone', null, ['class' => 'form-control phone', 'placeholder' => 'Votre numéro']) !!}
                            {!! $errors->first('phone', '<small class="help-block">:message</small>') !!}

                        </div>

                        <div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">

                            {!! Form::textarea ('texte', null, ['class' => 'form-control', 'placeholder' => 'Votre message']) !!}
                            {!! $errors->first('texte', '<small class="help-block">:message</small>') !!}

                        </div>
                        
                        {{ Form::hidden('route', '/parents') }}

                        {!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>

    </div>

</section>

@stop
@extends('layouts.default')

@section('title','Nos engagements')

@section('description','Offrir une éducation de qualité à la culture digitale, ouverte et accessible à tous, Préparer nos enfants aux compétences du 21éme siècle, Rester ancrées à nos valeurs, basées sur le partage, le respect de l\'environnement et le développement durable')


@section('content')

<section class="bg-light">

    @include('includes.masthead', ['which' => "engagements", 'title' =>"Nos Engagements"])

    <div class="container pt-5 pb-5">
        <div class="row text-center" style="padding: 1rem 0;">
            <div class="row" style="width:100%;padding:10px;margin-top:40px;">
                <div class="col-md-5 order-md-1">
                    <img src="{{url('img/kids-1.jpg')}}" style="width:100%;border-radius:15px;margin-bottom:10px;" alt="engagement 1"/>
                </div>
                <div class="col-md-7 order-md-2 my-auto showcase-text">
                    <h5 style="color:#1289A7;width:100%;">#1 : </h5>
                    <h4 style="color:#1289A7;font-weight:bold;margin: 10px 0;">Offrir une éducation de qualité à la culture digitale, ouverte et accessible à tous</h4>
                    <p class="font-weight-light">
                        Nous voulons donner les moyens à tous les enfants pour développer leur culture digitale. Les expériences éducatives axées sur le digital doivent être simples à mettre en place dans touts les établissements scolaires, parascolaires, centres culturels, et aussi dans le cadre associatif. Elles doivent être facilement accessibles à tous les enfants, dans les meilleures conditions.


                    </p>
                </div>
            </div>

            <div class="row" style="width:100%;margin-top:50px;padding:10px;">
                <div class="col-md-5 order-md-1">
                    <img src="{{url('img/ecolo2.jpg')}}" style="width:100%;border-radius:15px;margin-bottom:10px;" alt="engagement 2"/>
                </div>
                <div class="col-md-7 order-md-2 my-auto showcase-text;">
                    <h5 style="color:#1289A7;width:100%;">#2 : </h5>
                    <h4 style="color:#1289A7;font-weight:bold;margin: 10px 0;">Préparer nos enfants aux compétences du 21éme siècle</h4>
                    <p class="font-weight-light">
                        Plusieurs référentiels ont été élaborés pour définir l'ensemble de savoirs et de savoir-faire qui seront nécessaires pour préparer nos enfants aux besoins de la société de demain. Ces compétences sont axées sur la collaboration, la communication, la maitrise des technologies de l’information et des communications, le développement des habiletés sociales et culturelles, ainsi que le développement d'une pensée créative, critique et capable d'appréhender et résoudre les problèmes.
                    </p>
                </div>
            </div>

            <div class="row" style="width:100%;margin-top:50px;padding:10px;">
                <div class="col-md-5 order-md-1">
                    <img src="{{url('img/ecolo3.jpg')}}" style="width:100%;border-radius:15px;margin-bottom:10px;" alt="engagement 3"/>
                </div>
                <div class="col-md-7 order-md-2 my-auto showcase-text">
                    <h5 style="color:#1289A7;width:100%;">#3 : </h5>
                    <h4 style="color:#1289A7;font-weight:bold;margin: 10px 0;">Rester ancrées à nos valeurs, basées sur le partage, le respect de l'environnement et le développement durable</h4>
                    <p class="font-weight-light">
                        Ces valeurs font partie de l'ADN de Smarteo&reg;, et on oeuvre pour un modèle basé sur le partage des expériences, partage des ressources, et une utilisation éco-responsable du digital.
                    </p>
                </div>
            </div>


        </div>
    </div>
</section>
@stop
@extends('layouts.default')

@section('title','Formules')

@section('description','formules, abonnement, pay as-you-go')

@section('content')

<section class="bg-light">

	@include('includes.masthead', ['which' => "faq", 'title' =>"Mentions Légales"])

	<div class="container">

		<div class="row">
			<div class="col-md-12">

				<div class="card mt-4 mb-4 box-shadow border-info">
					<div class="card-header bg-info text-center border-0">
						<h4 class="my-0 font-weight-bold">Découverte</h4>
						
						<button type="button" class="btn btn-lg btn-primary rounded mt-3">Créez votre compte gratuit</button>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 text-center">
				<div class="card mb-4 box-shadow border-warning">
					<div class="card-header bg-warning border-0">
						<h4 class="my-0 font-weight-bold text-white">Pay as-you-go</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">Sans engagement</h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>10 users included</li>
							<li>2 GB of storage</li>
							<li>Email support</li>
							<li>Help center access</li>
						</ul>
						<button type="button" class="btn btn-lg btn-warning rounded border-0">Commencer à en profitez</button>
					</div>
				</div>
			</div>

			<div class="col-md-4 text-center">
				<div class="card mb-4 box-shadow border-success">
					<div class="card-header bg-success  border-0">
						<h4 class="my-0 font-weight-bold text-white">Easy</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">39.9 EUR <small class="text-muted">/ mois</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>10 users included</li>
							<li>2 GB of storage</li>
							<li>Email support</li>
							<li>Help center access</li>
						</ul>
						<button type="button" class="btn btn-lg btn-success rounded border-0">Souscrire à cet abonnement</button>
					</div>
				</div>
			</div>

			<div class="col-md-4 text-center">
				<div class="card mb-4 box-shadow border-dark">
					<div class="card-header bg-dark  border-0">
						<h4 class="my-0 font-weight-bold text-white">Platinium</h4>
					</div>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">359.9 EUR <small class="text-muted">/ an</small></h1>
						<ul class="list-unstyled mt-3 mb-4">
							<li>10 users included</li>
							<li>2 GB of storage</li>
							<li>Email support</li>
							<li>Help center access</li>
						</ul>
						<button type="button" class="btn btn-lg btn-grey rounded border-0">Souscrire à cet abonnement</button>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">

				<div class="card mb-4 box-shadow border-info">
					<div class="card-header bg-info text-center">
						<h4 class="my-0 font-weight-bold text-white">Vous voulez adopter le robot ? </h4>

						<p class="mt-3">Vous voulez l'acheter ? Ou le garder après la fin de votre période de souscription ? </p>
						<p class="font-weight-bold">Le prix prend en compte votre durée d’utilisation passée.</p>

						<button type="button" class="btn btn-lg btn-primary rounded mt-3">Consulter les conditions générales de vente</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
@stop
<!doctype html>
<html lang="fr">
<head>

	@include('includes.head')

	<style>
		.pagination > li > a, .pager > li > a {
			margin : 0 10px 0 0;
		}
		.dataTables_wrapper .row {
			width:100%;
			margin:10px;
		}
		thead input {
			width: 100%;
		}
	</style>


	<style>
		/* bootstrap-tagsinput.css file - add in local */

		.bootstrap-tagsinput {
			background-color: #fff;
			border: 1px solid #ccc;
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			display: inline-block;
			padding: 4px 6px;
			color: #555;
			vertical-align: middle;
			border-radius: 4px;
			max-width: 100%;
			line-height: 22px;
			cursor: text;
		}
		.bootstrap-tagsinput input {
			border: none;
			box-shadow: none;
			outline: none;
			background-color: transparent;
			padding: 0 6px;
			margin: 0;
			width: auto;
			max-width: inherit;
		}
		.bootstrap-tagsinput.form-control input::-moz-placeholder {
			color: #777;
			opacity: 1;
		}
		.bootstrap-tagsinput.form-control input:-ms-input-placeholder {
			color: #777;
		}
		.bootstrap-tagsinput.form-control input::-webkit-input-placeholder {
			color: #777;
		}
		.bootstrap-tagsinput input:focus {
			border: none;
			box-shadow: none;
		}
		.bootstrap-tagsinput .tag {
			line-height: 32px;
			color: white;
			background:#bababa; border-radius:5px;padding:5px;
		}
		.bootstrap-tagsinput .tag [data-role="remove"] {
			margin-left: 8px;
			cursor: pointer;
			background:#888;
		}
		.bootstrap-tagsinput .tag [data-role="remove"]:after {
			content: "x";
			padding: 0px 2px;
		}
		.bootstrap-tagsinput .tag [data-role="remove"]:hover {
			box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
		}
		.bootstrap-tagsinput .tag [data-role="remove"]:hover:active {
			box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
		}

	</style>
	@yield('css')
	@yield('script')

</head>

<body>

	<header class="container">
		@include('includes.header')
	</header>

	<div id="main">

		<div class="flash-message">
			@include('flash::message')
		</div>

		@yield('content')
		@include('includes.popupnsl')
	</div>

	@if(strpos($_SERVER['REQUEST_URI'], 'surveys')==false 
	&& strpos($_SERVER['REQUEST_URI'], 'animateurs')==false )

	<footer>
		@include('includes.footer')


		<!-- Bootstrap core JavaScript -->
		<script src="{{url('/vendor/jquery/jquery.min.js')}}"></script>
		<script src="{{url('/vendor/bootstrap-4.0.0/js/bootstrap.min.js')}}"></script>

		<!-- Plugin JavaScript -->
		<script src="{{url('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
		<script src="{{url('/vendor/scrollreveal/scrollreveal.min.js')}}"></script>

		<!-- Custom scripts for this template -->
		<script src="{{url('/js/creative.js')}}"></script>

		<!-- Google sign-in -->
		<script src="https://apis.google.com/js/platform.js" async defer></script>

		
		<script>
			$('div.flash-message').delay(5000).fadeOut(500);
		</script>

		<script src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
		<script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js'></script>
		
		<script>
			$(document).ready(function() {
				$('#filtredtable thead tr').clone(true).appendTo( '#filtredtable thead' );
				$('#filtredtable thead tr:eq(1) th').each( function (i) {
					var title = $(this).text();
					$(this).html( '<input type="text" placeholder="Search '+title+'" />' );

					$( 'input', this ).on( 'keyup change', function () {
						if ( table.column(i).search() !== this.value ) {
							table
							.column(i)
							.search( this.value )
							.draw();
						}
					} );
				} );

				var table = $('#filtredtable').DataTable( {
					orderCellsTop: true,
					fixedHeader: true
				} );
			} );
		</script>

		<script src='https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.min.js'></script>

		<script type="text/javascript">

			function show_form_update()
			{
				document.getElementById("form_update").style.display="block";
			}

			function hide_form_update()
			{
				document.getElementById("form_update").style.display="none";
			}

			function toggle_form_update() {
				var x = document.getElementById("form_update");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			}

			$(document).ready(function()
			{
				<?php
				if(isset($selected)){
					if($selected->id != 0) {
					?>
					show_form_update();
					<?php } else { ?>
					hide_form_update();
					<?php }} ?>
			});

		</script>

		@yield('scripts')
	</footer>

	@endif

</body>
</html>

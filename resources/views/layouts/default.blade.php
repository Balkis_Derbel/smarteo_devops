<!doctype html>
<html lang="fr">
<head>

	@include('includes.head')
	@yield('css')
	@yield('script')

</head>

<body>

	<header>
		@include('includes.header')
	</header>

	<div id="main">

		<div class="flash-message">
			@include('flash::message')
		</div>

		@yield('content')
		@include('includes.popupnsl')
	</div>

	

	<footer>
		@if(!isset($hide_footer))
		@include('includes.footer')
		@endif



		<!-- Bootstrap core JavaScript -->
		<script src="{{url('/vendor/jquery/jquery.min.js')}}"></script>
		<script src="{{url('/vendor/bootstrap-4.0.0/js/bootstrap.min.js')}}"></script>

		<!-- Plugin JavaScript -->
		<script src="{{url('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
		<script src="{{url('/vendor/scrollreveal/scrollreveal.min.js')}}"></script>

		<!-- Custom scripts for this template -->
		<script src="{{url('/js/creative.js')}}"></script>

		<!-- Google sign-in -->
		<script src="https://apis.google.com/js/platform.js" async defer></script>

		<!-- Newsletter -->
		<script>
			$(document).ready(function() {

				var AuthUser = "{{{ (Auth::user()) ? Auth::user() : null }}}";
				if(AuthUser != null && AuthUser != "") return;

    			var delay = 300; // milliseconds
    			var cookie_expire = 0.0005; // days

    var cookie = localStorage.getItem("list-builder");
    if(cookie == undefined || cookie == null) {
    	cookie = 0;
    }

    if(((new Date()).getTime() - cookie) / (1000 * 60 * 60 * 24) > cookie_expire) {
    	$("#list-builder").delay(delay).fadeIn("fast", () => {
    		$("#popup-box").fadeIn("fast", () => {});
    	});

    	$("button[name=subscribe]").click(() => {
    		$.ajax({
    			type: "POST",
    			url: $("#popup-form").attr("action"),
    			data: $("#popup-form").serialize(),
    			success: (data) => {
    				$("#popup-box-content").html("<p style='text-align: center'>Merci pour votre souscription à notre newsletter!</p>");
    			}
    		});
    	});

    	$("#popup-close").click(() => {
    		$("#list-builder, #popup-box").hide();
    		localStorage.setItem("list-builder", (new Date()).getTime());
    	});
    }
});
</script>


<script>
	$('div.flash-message').delay(5000).fadeOut(500);
</script>

<script>
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("mainNavBar").style.padding = "0px 10px";
    document.getElementById("mainNavBar").style.opacity = "1";
  } else {
    document.getElementById("mainNavBar").style.padding = "25px 10px";
    document.getElementById("mainNavBar").style.opacity = "0.6";
  }
}
</script>

@yield('scripts')
</footer>

</body>
</html>

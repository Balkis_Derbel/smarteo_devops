@extends('layouts.default')

@section('title','Nos enquêtes pour les parents et les acteurs de l\'enseignement')

@section('description','Participez à notre enquête sur le développement d\'une culture digitale pour nos enfants')


@section('content')

<!-- Surveys -->

<div class="masthead surveys text-white text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="description col-xl-12 mx-auto">
        <div>
          Educatec-Educatice : <span class="description1">Le Salon Professionnel de l'Education. 21, 22 & 23 NOV. 2018</span>
        </div>
      </div>
    </div>
  </div>
</div>

<iframe class="airtable-embed" src="https://airtable.com/embed/shrn2JlMevxgnU0a8?backgroundColor=cyan" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>
    
@stop


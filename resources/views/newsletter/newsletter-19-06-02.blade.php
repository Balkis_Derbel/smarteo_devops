<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr><tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_0" id="Layout_0">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width:590px;">
                        <table width="100%" cellpadding="0" border="0" height="38" cellspacing="0">
                            <tbody><tr>
                                <td valign="top" height="38">
                                    <img width="20" height="38" style="display:block; max-height:38px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_1" id="Layout_1">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">
                                    <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0">
                                        <tbody><tr>
                                            <td valign="top" align="center">
                                                <table cellpadding="0" border="0" align="center" cellspacing="0" class="logo-img-center"> 
                                                    <tbody><tr>
                                                        <td valign="middle" align="center" style="line-height: 1px;">
                                                            <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block; " cellspacing="0" cellpadding="0" border="0"><div><a style="text-decoration:none;" target="_blank" href="https://www.smarteo.co"><img width="491" vspace="0" hspace="0" border="0" alt="" style="float: left;max-width:491px;" class="rnb-logo-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76d4be940be7bc47491370.png"></a></div>
                                                            </div></td>
                                                    </tr>
                                                </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_" id="Layout_"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="font-size:20px;"><span style="color:#2E91AE;"><strong>Nous sommes heureux de partager avec vous les dernières actualités de notre beau projet</strong></span></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_22" id="Layout_22"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><font color="#ffffff"><span style="font-size: 22px;"><b>NOUVEAUX LOCAUX<br>
NOUVEL ESPACE DE CREAIVITE</b></span></font></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_8">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:'Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><font color="#2e91ae"><span style="font-size: 18px;">Nos nouveaux locaux, situés à Sceaux, sont presque prêts. Nous comptons en faire aussi un vrai espace de créativité. Nous y avons déjà installé imprimantes 3D, robots, et PCs, et&nbsp;nous vous enverrons les premières photos très prochainement.&nbsp;</span></font><span style="text-align: center; background-color: transparent;">&nbsp;</span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_14" id="Layout_14">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="0" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://goo.gl/maps/ZBBfgbyCnT87G6FH9">Comment venir</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_25" id="Layout_25"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><font color="#ffffff"><span style="font-size: 22px;"><b>STAGES D'ETE</b></span></font></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_31">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;">
<div><span style="color:#2E91AE;"><span style="font-size:18px;">Les vacances de l’été sont le moment idéal pour se lancer dans des activités créatives et apprendre en s’amusant. Nous sommes heureux de lancer nos premiers stages d'été pour initier les enfants et ados&nbsp;</span></span><span style="font-size: 18px; color: rgb(46, 145, 174); background-color: transparent;">à la programmation informatique, la robotique, l’impression 3D et aussi l’IA.</span></div>

<div>&nbsp;</div>

<div><span style="font-size: 18px; color: rgb(46, 145, 174); background-color: transparent;">Ces stages sont organisés avec une méthodologie d’apprentisssage ‘par projets'. Partant d’un thème de départ, les participants construisent leur projet en utilisant les différents outils qui sont mis à leur disposition. Les nouvelles notions sont apprises au fil de l’avancement du projet et sont directement mises en application.</span></div>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_38" id="Layout_38">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:5px; width:590;;max-width:590px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img ng-if="col.img.source != 'url'" border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 5px; " src="http://img.mailinblue.com/2245632/images/rnb/original/5cf4cdee15d43a0a6f67c51e.jpg"></div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_32" id="Layout_32">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://www.smarteoclub.fr/ateliers-ete-2019">Informations et inscriptions</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_35" id="Layout_35"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><font color="#ffffff"><span style="font-size: 22px;"><b>LES ROBOTS FONT LEUR CINEMA</b></span></font></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_34">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><span style="color:#2E91AE;"><span style="font-size:18px;">Cet atelier créatif et pluridisciplinaire allie des aspects artistiques et techniques. Il est proposé conjointement par Smarteo et WIP Production et s’inscrit dans le programme OFF de Futur.e.s Festival 2019</span></span><br>
&nbsp;</div>

<div style="text-align: justify;">
<div><span style="color:#2E91AE;"><span style="font-size:18px;">Les participants apprendront les bases de la robotique et de la programmation informatique. Ils découvriront comment organiser la narration d’un petit film et s’initieront aux principes de prises de vue. Ils travailleront en équipe pour produire leurs réalisations, et auront l’opportunité de développer leur culture digitale et cinématographique, tout en s’amusant.</span></span></div>

<div>&nbsp;</div>

<div><span style="color:#2E91AE;"><span style="font-size:18px;">Cet atelier se déroule dans les studios de montage de WIP Production situés à Massy : LE LOMA.</span></span></div>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_12" id="Layout_12">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:590px !important;border-top:0px None #9c9c9c;border-right:0px None #9c9c9c;border-bottom:0px None #9c9c9c;border-left:0px None #9c9c9c;border-collapse: separate;border-radius: 0px;">
                                                    <div><img ng-if="col.img.source != 'url'" border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://img.mailinblue.com/2245632/images/rnb/original/5cf4c93635a38cbb232b0440.png"></div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_11">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><font color="#2e91ae"><span style="font-size: 18px;">Le nombre de places est limité. Informations et inscriptions ici :</span></font></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_28" id="Layout_28">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="0" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://www.eventbrite.fr/e/billets-les-robots-font-leur-cinema-62482159822">Informations et inscriptions</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_26" id="Layout_26"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><font color="#ffffff"><span style="font-size: 20px;"><b>NOUVEAUX ROBOTS ET NOUVELLES ACTIVITES</b></span></font></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_41">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><font color="#2e91ae"><span style="font-size: 18px;">Notre catalogue continue à s'enrichir. Nos tapis de jeu pour matatalab sont maintenant disponibles : Découvrez tous les nouveaux produits sur le site web www.smarteo.co.</span></font></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_39" id="Layout_39">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:5px; width:580;;max-width:580px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img ng-if="col.img.source != 'url'" border="0" hspace="0" vspace="0" width="580" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 5px; " src="http://img.mailinblue.com/2245632/images/rnb/original/5cf4cb6a15d43a02e0544f5b.jpg"></div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_36" id="Layout_36">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://www.smarteo.co/robots-educatifs">Robots Educatifs et Activités</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso 15]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso 15]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_15" id="Layout_15">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="max-width: 100%; min-width: 100%; table-layout: fixed; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding: 20px;">
                            <tbody><tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>

                                            <td class="rnb-force-col img-block-center" valign="top" width="220" style="padding-right: 20px;">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-2-noborder-onright" width="220">


                                                    <tbody><tr>
                                                        <td width="100%" style="line-height: 1px;" class="img-block-center" valign="top" align="left">
                                                            <div style="border-top:0px none #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img alt="" border="0" hspace="0" vspace="0" width="220" style="vertical-align:top; float: left; width:220px;max-width:884px !important; " class="rnb-col-2-img-side-xs" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76d9f205775dbd9479c635.jpg"></div><div style="clear:both;"></div></div></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td><td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="350" align="left" class="rnb-last-col-2">

                                                    <tbody><tr>
                                                        <td style="font-size:24px; font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:left;">
                                                            <span style="color:#3c4858; "><span style="color:#FFFFFF;"><strong><span style="font-size:18px;">Apprendre en s'amusant avec de vrais robots</span></strong></span></span></td>
                                                    </tr><tr>
                                                        <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td class="rnb-mbl-float-none" style="font-size:14px; font-family:Arial,Helvetica,sans-serif;color:#3c4858;float:right;width:350px; line-height: 21px;"><span style="color:#FFFFFF;">Samerteo propose des dispositifs complets pour l'apprentissage du code et de la robotique avec une approche basée sur la pratique et le jeu.</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="left" cellspacing="0" class="rnb-btn-col-content" style="border-collapse: separate;">

                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#fff" align="center" height="39" style="font-size:16px; font-family:Arial,Helvetica,sans-serif; text-align:center; color:#2E91AE; font-weight:normal; padding-left:14px; padding-right:14px; background-color:#fff; border-radius:0px;border-top:0px None #9c9c9c;border-right:0px None #9c9c9c;border-bottom:0px None #9c9c9c;border-left:0px None #9c9c9c;border-collapse: separate;">
                                                                        <span style="color:#2E91AE; font-weight:normal;">
                                                                        <a style="text-decoration:none; color:#2E91AE; font-weight:normal;" target="_blank" href="https://www.smarteo.co">En savoir plus &gt;</a>
                                                                        
                                                                       </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>

                                                    </tr></tbody></table>
                                                </td>

                                            </tr></tbody></table></td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso 15]>
                </td>
                <![endif]-->

                <!--[if mso 15]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_18" id="Layout_18"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="color:#2E91AE;"><span style="font-size:24px;">N'oubliez pas de nous suivre sur les réseaux sociaux pour avoir nos updates en avant-première : </span></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_" id="Layout_">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#fff" style="min-width:590px; background-color: #fff;">
                            <table width="590" class="rnb-container" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#fff" style="background-color: rgb(255, 255, 255);">
                                <tbody><tr>
                                    <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="rnb-container-padding" style="font-size: 14px; font-family: Arial,Helvetica,sans-serif; color: #888888;" align="center">

                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                            <tbody><tr>
                                                <td class="rnb-force-col rnb-social-width2" valign="top" style="mso-padding-alt: 0 20px 0 20px; padding-right: 28px; padding-left: 28px;">

                                                    <table border="0" valign="top" cellspacing="0" cellpadding="0" width="533" align="center" class="rnb-last-col-2">

                                                        <tbody><tr>
                                                            <td valign="top">
                                                                <table cellpadding="0" border="0" cellspacing="0" class="rnb-social-align2" align="center">
                                                                    <tbody><tr>
                                                                        <td valign="middle" style="line-height: 0px;" class="rnb-text-center" ng-init="width=setSocialIconsBlockWidth(item)" width="533" align="center">
                                                                            <!--[if mso]>
                                                                            <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                            <![endif]-->

                                                                                <div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.facebook.com/smarteo.co/"><img alt="Facebook" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_fb.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://twitter.com/smarteo_co"><img alt="Twitter" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_tw.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.linkedin.com/company/smarteo-co"><img alt="LinkedIn" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_in.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.instagram.com/smarteo.co/"><img alt="Instagram" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_ig.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.youtube.com/channel/UCGl2bP1iFIf-g3lvF6jpoXw"><img alt="Youtube" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_yt.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><!--[if mso]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>
                                                    </td>
                                            </tr>
                                        </tbody></table></td>
                                </tr>
                                <tr>
                                    <td height="10" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr></tbody></table>
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_19" id="Layout_19"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="color:#2E91AE;"><span style="font-size:24px;">A très bientôt :)</span></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                            <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                                <tbody><tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                        <div>© 2019 Smarteo</div>
                                    </td></tr>
                                <tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                
            </div></td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>
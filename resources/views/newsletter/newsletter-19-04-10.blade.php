<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_0" id="Layout_0">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width:590px;">
                        <table width="100%" cellpadding="0" border="0" height="38" cellspacing="0">
                            <tbody><tr>
                                <td valign="top" height="38">
                                    <img width="20" height="38" style="display:block; max-height:38px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_1" id="Layout_1">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">
                                    <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0">
                                        <tbody><tr>
                                            <td valign="top" align="center">
                                                <table cellpadding="0" border="0" align="center" cellspacing="0" class="logo-img-center"> 
                                                    <tbody><tr>
                                                        <td valign="middle" align="center" style="line-height: 0px;">
                                                            <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block; " cellspacing="0" cellpadding="0" border="0"><div><img width="491" vspace="0" hspace="0" border="0" alt="Smarteo" style="float: left;max-width:491px;display:block;" class="rnb-logo-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76d4be940be7bc47491370.png"></div></div></td>
                                                    </tr>
                                                </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_" id="Layout_"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="font-size:20px;"><span style="color:#2E91AE;"><strong>Les semaines passées ont été riches en rencontres et en événements et c'est grâce à vous !</strong></span></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_22" id="Layout_22"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="font-size:20px;"><font color="#ffffff"><b><span style="font-size:22px;">①&nbsp;</span>La deuxième édition du SmartBotCamp®</b></font></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_8">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:'Arial',Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><span style="color:#2E91AE;"><span style="font-size:18px;">Cette édition a rassemblé le 17/03 dernier une soixantaine d'enfants âgés de 8 à 14 ans, qui ont collaboré, réfléchi, imaginé et construit leurs maquettes robotisées sur le thème :</span></span></div>

<div style="text-align: justify;">&nbsp;</div>

<div style="text-align: center;"><span style="font-size:24px;"><span style="color:#FAB900;"><strong>'Robots et Nouvelles Formes d'Intelligence'</strong></span></span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_29" id="Layout_29">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="max-width: 100%; min-width: 100%; table-layout: fixed; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" width="170" valign="top" style="padding-right: 20px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5cae18e0b6e71b8276464444.jpg" style="vertical-align: top; max-width: 1200px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td><td class="rnb-force-col" width="170" valign="top" style="padding-right: 20px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5cae196a5c4c2283471bd06f.jpg" style="vertical-align: top; max-width: 1200px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td><td class="rnb-force-col" width="170" valign="top" style="padding-right: 0px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-last-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5cae18e080fcb2890f43d5fe.jpg" style="vertical-align: top; max-width: 1200px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td></tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="0" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_27">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div><span style="color:#2E91AE;"><span style="font-size:18px;">Les réalisations ont été extraordinaires. Retrouvez les moments forts de cet évènement sur le site SmartBotCamp.fr.</span></span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_14" id="Layout_14">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="http://www.smartbotcamp.fr/massy">Retour sur le SmartBotCamp</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_25" id="Layout_25"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="font-size:20px;"><font color="#ffffff"><b><span style="font-size:22px;">②&nbsp;</span>Deuxième édition de StartupForKids à Paris-Saclay</b></font></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_31">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><span style="color:#2E91AE;"><span style="font-size:18px;">Plus que 5000 visiteurs sont venus découvrir les dernières innovations présentées par une trentaine de startups. Nos robots ont fait le bonheur des grands et des petits.</span></span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="font-size:1px; line-height:0px">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_30" id="Layout_30">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="max-width: 100%; min-width: 100%; table-layout: fixed; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" width="170" valign="top" style="padding-right: 20px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5cae1cf280fcb292f75d91dd.jpg" style="vertical-align: top; max-width: 1200px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td><td class="rnb-force-col" width="170" valign="top" style="padding-right: 20px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5cae1cf15c4c228ae30964ab.jpg" style="vertical-align: top; max-width: 1200px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td><td class="rnb-force-col" width="170" valign="top" style="padding-right: 0px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-last-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5cae1cf380fcb292eb592828.jpg" style="vertical-align: top; max-width: 1200px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td></tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="0" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_32" id="Layout_32">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://www.smarteo.co/actualites">Retrouvez tous les événements ICI</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_35" id="Layout_35"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="font-size:20px;"><font color="#ffffff"><b><span style="font-size:22px;">③&nbsp;</span>Le challenge 'WonderWorkshop/Smarteo' :<br>
<span style="font-size:28px;">Un maginfique voyage dans le temps</span></b></font></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_34">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><span style="color:#2E91AE;"><span style="font-size:18px;">Et bien sur : nous vous gardons le meilleur pour la fin :) Nous avons le plaisir d’annoncer le lancement du Challenge de Robotique WonderWorkshop/Smarteo, destiné aux enfants de 9 à 14 ans, sur le thème du voyage dans le temps.</span></span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_12" id="Layout_12">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:1200px !important;border-top:0px None #9c9c9c;border-right:0px None #9c9c9c;border-bottom:0px None #9c9c9c;border-left:0px None #9c9c9c;border-collapse: separate;border-radius: 0px;">
                                                    <div><img ng-if="col.img.source != 'url'" border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://img.mailinblue.com/2245632/images/rnb/original/5cae18edf46e8d840b6bc6bb.png"></div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_33">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><span style="color:#2E91AE;"><span style="font-size:18px;">En visite au Musée du Louvre, le robot s’est retrouvé dans une machine à remonter dans le temps. A vous d’imaginer le voyage, concevoir une maquette, et aider le robot à explorer son environnement pour retourner au présent !</span></span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_28" id="Layout_28">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://www.smarteo.co/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps">Inscrivez-vous sans tarder et tentez de gagner ce challenge 🏆</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_26" id="Layout_26"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="height: 0px; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><font color="#ffffff"><span style="font-size: 20px;"><b>Nous vous avons promis une surprise :<br>
Bénéficiez de 15% de remise pour vos prochaines locations de robots éducatifs</b></span></font></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
            
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_11">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div style="text-align: justify;"><span style="color:#2E91AE;"><span style="font-size:18px;">Bénéficiez de cette remise exceptionnelle pour profiter d'un robot aux prochaines vacances scolaires, en utilisant le code :</span></span></div>

<div style="text-align: justify;">&nbsp;</div>

<div style="text-align: center;"><span style="font-size:24px;"><span style="color:#FAB900;"><strong>OFFRE_PRINTEMPS</strong></span></span></div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_37" id="Layout_37">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="max-width: 100%; min-width: 100%; table-layout: fixed; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" width="170" valign="top" style="padding-right: 20px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76dc8b6c02a0c28a2d4a65.jpg" style="vertical-align: top; max-width: 900px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td><td class="rnb-force-col" width="170" valign="top" style="padding-right: 20px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76dc9005775dc1840e0ce6.png" style="vertical-align: top; max-width: 900px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td><td class="rnb-force-col" width="170" valign="top" style="padding-right: 0px;">
                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-last-col-3" width="170">
                                                    <tbody><tr>
                                                        <td width="100%" class="img-block-center" valign="top" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="100%" valign="top" align="left" class="img-block-center">
                                                                        <table style="display: inline-block;" cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody><tr>
                                                                                <td>
                                                                                    <div style="border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img border="0" width="170" hspace="0" vspace="0" alt="" class="rnb-col-3-img" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76dc8a6c02a0c09d6de750.jpg" style="vertical-align: top; max-width: 900px; float: left;"></div><div style="clear:both;"></div>
                                                                                    </div>
                                                                            </td>
                                                                            </tr>
                                                                        </tbody></table>

                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table></td>
                                                    </tr><tr>
                                                        <td height="10" class="col_td_gap" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;">
                                                            <div></div>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>

                                                </td></tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="0" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_36" id="Layout_36">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="mso-button-block rnb-container" style="background-color: rgb(255, 255, 255); border-radius: 0px; padding-left: 20px; padding-right: 20px; border-collapse: separate;">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>
                                            <td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="550" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="center" cellspacing="0" class="rnb-btn-col-content" style="margin:auto; border-collapse: separate;">
                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#FAB900" align="center" height="40" style="font-size:18px; font-family:Arial,Helvetica,sans-serif; color:#ffffff; font-weight:normal; padding-left:20px; padding-right:20px; vertical-align: middle; background-color:#FAB900;border-radius:4px;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;">
                                                                        <span style="color:#ffffff; font-weight:normal;">
                                                                                <a style="text-decoration:none; color:#ffffff; font-weight:normal;" target="_blank" href="https://www.smarteo.co/robots-educatifs?code=OFFRE_PRINTEMPS">Je veux en bénéficier maintenant</a>
                                                                            </span>
                                                                    </td>
                                                                </tr></tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                                </td>
                                        </tr>
                                    </tbody></table></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso 15]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso 15]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_15" id="Layout_15">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#2E91AE" style="max-width: 100%; min-width: 100%; table-layout: fixed; background-color: rgb(46, 145, 174); border-radius: 0px; border-collapse: separate; padding: 20px;">
                            <tbody><tr>
                                <td valign="top" class="rnb-container-padding" align="left">

                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                        <tbody><tr>

                                            <td class="rnb-force-col img-block-center" valign="top" width="220" style="padding-right: 20px;">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" align="left" class="rnb-col-2-noborder-onright" width="220">


                                                    <tbody><tr>
                                                        <td width="100%" style="line-height: 0px;" class="img-block-center" valign="top" align="left">
                                                            <div style="border-top:0px none #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;display:inline-block;"><div><img alt="" border="0" hspace="0" vspace="0" width="220" style="vertical-align:top; float: left; width:220px;max-width:884px !important; " class="rnb-col-2-img-side-xs" src="http://img.mailinblue.com/2245632/images/rnb/original/5c76d9f205775dbd9479c635.jpg"></div><div style="clear:both;"></div></div></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td><td class="rnb-force-col" valign="top">

                                                <table border="0" valign="top" cellspacing="0" cellpadding="0" width="350" align="left" class="rnb-last-col-2">

                                                    <tbody><tr>
                                                        <td style="font-size:24px; font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:left;">
                                                            <span style="color:#3c4858; "><span style="color:#FFFFFF;"><strong><span style="font-size:18px;">Apprendre en s'amusant avec de vrais robots</span></strong></span></span></td>
                                                    </tr><tr>
                                                        <td height="10" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td class="rnb-mbl-float-none" style="font-size:14px; font-family:Arial,Helvetica,sans-serif;color:#3c4858;float:right;width:350px; line-height: 21px;"><span style="color:#FFFFFF;">Samerteo propose des dispositifs complets pour l'apprentissage du code et de la robotique avec une approche basée sur la pratique et le jeu.</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                                    </tr><tr>
                                                        <td valign="top">
                                                            <table cellpadding="0" border="0" align="left" cellspacing="0" class="rnb-btn-col-content" style="border-collapse: separate;">

                                                                <tbody><tr>
                                                                    <td width="auto" valign="middle" bgcolor="#fff" align="center" height="39" style="font-size:16px; font-family:Arial,Helvetica,sans-serif; text-align:center; color:#2E91AE; font-weight:normal; padding-left:14px; padding-right:14px; background-color:#fff; border-radius:0px;border-top:0px None #9c9c9c;border-right:0px None #9c9c9c;border-bottom:0px None #9c9c9c;border-left:0px None #9c9c9c;border-collapse: separate;">
                                                                        <span style="color:#2E91AE; font-weight:normal;">
                                                                        <a style="text-decoration:none; color:#2E91AE; font-weight:normal;" target="_blank" href="https://www.smarteo.co">En savoir plus &gt;</a>
                                                                        
                                                                       </span>
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>

                                                    </tr></tbody></table>
                                                </td>

                                            </tr></tbody></table></td>
                            </tr>
                        </tbody></table>

                    </td>
                </tr>
            </tbody></table>
            <!--[if mso 15]>
                </td>
                <![endif]-->

                <!--[if mso 15]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_18" id="Layout_18"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="color:#2E91AE;"><span style="font-size:24px;">N'oubliez pas de nous suivre sur les réseaux sociaux pour avoir nos updates en avant-première : </span></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_" id="Layout_">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top" bgcolor="#fff" style="min-width:590px; background-color: #fff;">
                            <table width="590" class="rnb-container" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#fff" style="background-color: rgb(255, 255, 255);">
                                <tbody><tr>
                                    <td height="10" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="rnb-container-padding" style="font-size: 14px; font-family: Arial,Helvetica,sans-serif; color: #888888;" align="center">

                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                            <tbody><tr>
                                                <td class="rnb-force-col rnb-social-width2" valign="top" style="mso-padding-alt: 0 20px 0 20px; padding-right: 28px; padding-left: 28px;">

                                                    <table border="0" valign="top" cellspacing="0" cellpadding="0" width="533" align="center" class="rnb-last-col-2">

                                                        <tbody><tr>
                                                            <td valign="top">
                                                                <table cellpadding="0" border="0" cellspacing="0" class="rnb-social-align2" align="center">
                                                                    <tbody><tr>
                                                                        <td valign="middle" style="line-height: 0px;" class="rnb-text-center" ng-init="width=setSocialIconsBlockWidth(item)" width="533" align="center">
                                                                            <!--[if mso]>
                                                                            <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                            <![endif]-->

                                                                                <div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.facebook.com/smarteo.co/"><img alt="Facebook" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_fb.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://twitter.com/smarteo_co"><img alt="Twitter" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_tw.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.linkedin.com/company/smarteo-co"><img alt="LinkedIn" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_in.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.instagram.com/smarteo.co/"><img alt="Instagram" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_ig.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><div class="rnb-social-center" style="display: inline-block;">
                                                                                <!--[if mso]>
                                                                                <td align="center" valign="top">
                                                                                <![endif]-->
                                                                                    <table align="left" style="float:left; display: inline-block" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td style="padding:0px 5px 5px 0px; mso-padding-alt: 0px 4px 5px 0px;" align="left">
                                                                                <span style="color:#ffffff; font-weight:normal;">
                                                                                    <a target="_blank" href="https://www.youtube.com/channel/UCGl2bP1iFIf-g3lvF6jpoXw"><img alt="Youtube" border="0" hspace="0" vspace="0" style="vertical-align:top;" target="_blank" src="http://img.mailinblue.com/new_images/rnb/theme1/rnb_ico_yt.png"></a></span>
                                                                                            </td></tr></tbody></table>
                                                                                <!--[if mso]>
                                                                                </td>
                                                                                <![endif]-->
                                                                                </div><!--[if mso]>
                                                                            </tr>
                                                                            </table>
                                                                            <![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>
                                                    </td>
                                            </tr>
                                        </tbody></table></td>
                                </tr>
                                <tr>
                                    <td height="10" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr></tbody></table>
                
            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            
                <table width="100%" cellpadding="0" border="0" cellspacing="0" name="Layout_19" id="Layout_19"><tbody><tr>
                    <td align="center" valign="top"><table border="0" width="100%" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="height: 0px; background-color: rgb(255, 255, 255); border-radius: 0px; border-collapse: separate; padding-left: 20px; padding-right: 20px;"><tbody><tr>
                                <td class="rnb-container-padding" style="font-size: px;font-family: ; color: ;">

                                    <table border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container" align="center" style="margin:auto;">
                                        <tbody><tr>

                                            <td class="rnb-force-col" align="center">

                                                <table border="0" cellspacing="0" cellpadding="0" align="center" class="rnb-col-1">

                                                    <tbody><tr>
                                                        <td height="10"></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="font-family:Arial,Helvetica,sans-serif; color:#3c4858; text-align:center;">

                                                            <span style="color:#3c4858;"><span style="color:#2E91AE;"><span style="font-size:24px;">A très bientôt :)</span></span></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    </tbody></table>
                                                </td></tr>
                                    </tbody></table></td>
                            </tr>

                        </tbody></table>

                    </td>
                </tr>

            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                
                <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                            <table width="100%" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                                <tbody><tr>
                                    <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                        <div>© 2019 Smarteo</div>
                                    </td></tr>
                                <tr>
                                    <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
                
            </div></td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>
<div class="smart-link news" onclick="location.href='{{url('newsletter/'.$item['url'])}}'">
    <div class='date'>
        {{$item['date']}}
    </div>
    <div class='title'>
        <h5>{{$item['title']}}</h5>
    </div>
</div>
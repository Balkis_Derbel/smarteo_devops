@extends('layouts.default')

@section('title','Newsletter - SMARTEO - '.$selected['title'])


@php $hide_footer = true @endphp
@php $ref = $selected['ref'] @endphp

@section('css')
@stop

@section('content')

<section class="bg-light text-center">
  <div class="container pt-5 pb-5">

    <div class="row">

      <div class="col-md-8 mb-5">
        
        @include('newsletter/'.$ref, [$ref])

      </div>

      <div class="col-md-4">
        <a href="{{url('newsletter')}}" class="nounderline"><h2 class="w-100 bg-info p-3"> Dernières newsletters</h2></a>

        @foreach($data as $item)
        @include('newsletter/box', $item)
        @endforeach

      </div>
    </div>

  </div>

</section>


@stop
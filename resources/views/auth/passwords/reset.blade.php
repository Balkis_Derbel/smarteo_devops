@extends('layouts.default')

@section('content')
<section id="account">
    <div class="bg"></div>
    <div class="bg1"></div>
    
    <div class="container-fluid p-0">
        <div class="row no-gutters">

           <div class="col-md-8"  style="margin:auto;">
            <div class="account-text">
                <h1> Réinitialisation du mot de passe</h1>
                <h4> Veuillez saisir votre nouveau mot de passe </h4>
            </div>
        </div>


        <div class="col-md-8" style="margin:auto;">

            <form method="POST" class="row form" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
                @csrf

                <div class="box">

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group row">

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mail" name="email" placeholder="Email" value="{{ $email ?? old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                    </div>

                    <div class="form-group row">

                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} password" name="password" placeholder="Mot de passe" required>

                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                    </div>

                    <div class="form-group row">

                            <input id="password-confirm" type="password" class="form-control password" name="password_confirmation" placeholder="Confirmation" required>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Réinitialiser
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

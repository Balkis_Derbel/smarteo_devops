@extends('layouts.default')

@section('content')

<section id="account">
    <div class="bg"></div>
    <div class="bg1"></div>
    
    <div class="container-fluid p-0">
        <div class="row no-gutters">

         <div class="col-md-8"  style="margin:auto;">
            <div class="account-text">
                <h1> Mot de passe oublié</h1>
                <h4> Veuillez entrer votre adresse email pour ré-initialiser votre mot de passe </h4>
            </div>
        </div>


        <div class="col-md-8" style="margin:auto;">   

            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif

            <form method="POST" class="row form" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                @csrf

                <div class="box">
                    <div class="form-group">

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mail" placeholder="Email" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Envoyer') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</section>
@endsection


@extends('layouts.default')

@section('content')
<section id="account" style="margin-top:46px;">
    <div class="bg"></div>
    <div class="bg1"></div>

    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-md-8"  style="margin:auto;">
                <div class="account-text">
                    <h2> Créer votre compte</h2>
                    <h5> Vous avez déjà un compte ? </h5>

                    <div class="explore-btn m-0">
                        <a href="{{url('login')}}" class="display-5 button btn-orange p-3 m-2 rounded" >Accéder à votre compte</a>
                    </div>
                </div>
            </div>

            <div class="col-md-8 social-login">   
                <a href="{{ url('/redirect/facebook') }}">
                    <img src="{{ URL::to('/img/ico-social-fb.png') }}" alt = "ico-fb"/>
                </a>


                <a href="{{ url('/redirect/linkedin') }}">
                    <img src="{{ URL::to('/img/ico-social-linkedin.png') }}" alt = "ico-linkedin"/>
                </a>
                
                <!--<a href="{{ url('/redirect/google') }}">
                    <img src="{{ URL::to('/img/ico-social-google.png') }}" alt = "ico-google"/>
                </a>-->

            </div>
            
            <div class="col-md-8" style="margin:auto;">
                <div class="panel panel-default">

                    <div class="panel-body">
                        @if (session('confirmation-success'))
                        <div class="alert alert-success">
                            {{ session('confirmation-success') }}
                        </div>
                        @else
                        <form role="form" class="row form" method="POST" action="{{ url('/register') }}">
                            {{ csrf_field() }}

                            <div class="box">

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                    <input id="name" type="text" class="form-control name" name="name" placeholder="Nom d'utilisateur" value="{{ old('name') }}" required>

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                    <input id="email" type="email" class="form-control mail" name="email" placeholder="Email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <input id="password" type="password" class="form-control password" name="password" placeholder="Mot de passe" required>

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <input id="password-confirm" type="password" class="form-control password" name="password_confirmation" placeholder="Confirmation" required>
                                </div>
                            </div>


                            <div class="w-100 text-center mt-4">
                                <button type="submit" class="display-6 btn btn-lg btn-primary p-3 m-3 rounded">
                                    Créer mon compte
                                </button>
                            </div>

                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

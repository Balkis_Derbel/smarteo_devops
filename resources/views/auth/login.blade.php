@extends('layouts.default')

@section('content')
<section id="account" style="margin-top:46px;">
    <div class="bg"></div>
    <div class="bg1"></div>

    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-md-8"  style="margin:auto;">
                <div class="account-text">
                    <h2> Connexion à votre compte</h2>
                    <h5> Vous n'êtes pas encore inscrit ? </h5>

                    <div class="explore-btn m-0">
                        <a href="{{url('register')}}" class="display-6 button btn-orange p-3 m-2 rounded">Créer votre compte maintenant</a>
                    </div>
                </div>
            </div>

            <div class="col-md-8 social-login">   
                <a href="{{ url('/redirect/facebook') }}">
                    <img src="{{ URL::to('/img/ico-social-fb.png') }}" alt = "ico-fb"/>
                </a>


                <a href="{{ url('/redirect/linkedin') }}">
                    <img src="{{ URL::to('/img/ico-social-linkedin.png') }}" alt = "ico-linkedin"/>
                </a>

                <!--<a href="{{ url('/redirect/google') }}">
                    <img src="{{ URL::to('/img/ico-social-google.png') }}" alt = "ico-google"/>
                </a>-->

            </div>

            <div class="col-md-8" style="margin:auto;">   
                <div class="panel panel-default">

                    <div class="panel-body">
                        @if (session('confirmation-success'))
                        <div class="alert alert-success">
                            {{ session('confirmation-success') }}
                        </div>
                        @endif
                        @if (session('confirmation-danger'))
                        <div class="alert alert-danger">
                            {!! session('confirmation-danger') !!}
                        </div>
                        @endif

                        <form role="form" class="row form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                            
                            <div class="box">

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">


                                    <input id="email" type="email" class="form-control mail" name="email" placeholder="Email" value="{{ old('email') }}" required >

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                                    <input id="password" type="password" class="form-control password" name="password" placeholder="Mot de passe" required>

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Se rappeler de moi
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="w-100 text-center">
                                <button type="submit" class="display-6 btn btn-lg btn-primary p-3 m-3 rounded">
                                    Accéder à votre compte
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Mot de passe oublié ?
                                </a>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

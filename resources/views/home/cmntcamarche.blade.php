<!-- Comment ça marche -->
<section class="how-it-works bg-orange ptb-3">

    <div class="container p-2">

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card display-4 text-secondary bg-white rounded-circle p-4">Comment ça marche
                </div>
            </div>
        </div>


        <div class="row text-center d-inline-block mt-3 pt-3">

            <a href="{{url('robots-educatifs')}}" class="text-decoration-none">
                <div class="col-md-4 hiw-box cursor-pointer smart-hover swing">
                    <div class='number text-orange'>&#9312;</div>
                    <img src='img/ico-modulable.png' alt="step-1"/>
                    <div class="display-6 text-secondary"> Je choisis <span class="text-orange">facilement</span> mon robot éducatif.<br></div>
                </div>
            </a>

            <a href="{{url('contenus-et-tutoriels')}}" class="text-decoration-none">
                <div class="col-md-4 hiw-box cursor-pointer smart-hover swing">
                    <div class='number text-orange'>&#9313;</div>
                    <img src='img/ico-creatif.png' alt="step-2"/>
                    <div class='display-6 text-secondary'> Je le reçois et je profite d'expériences <span class="text-orange">riches</span> et <span class="text-orange">créatives</span>. </div>
                </div>
            </a>

            <a href="{{url('faq')}}" class="text-decoration-none">
                <div class="col-md-4 hiw-box cursor-pointer smart-hover swing">
                    <div class='number text-orange'>&#9314;</div>
                    <img src='img/ico-simple.png' alt="step-3" />
                    <div class='display-6 text-secondary'> Je peux le renvoyer et <span class="text-orange">ne paye que pour mon utilisation</span>.</div>
                </div>
            </a>
        </div>
    </div>
</section>

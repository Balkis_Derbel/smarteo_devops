<!-- Produits -->
<section class="bg-light text-center ptb-3">
    <div class="container">

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="display-4 bg-yellow text-white rounded-circle p-4">Ce qu'ils en ont pensé</div>
            </div>

            <div class="row text-center d-inline-block pt-3 mt-3">
                <div class="col-md-4">
                    Alexandre était intéressé/excité à l'idée d'utiliser le robot. Il était très heureux, il a beaucoup rigolé. Après, je dois dire qu'il n'utilise jamais la tablette donc il n'était pas très à l'aise pour utiliser l'application qui va avec Dash. C'est pour ça que je préfère garder le robot, attendre 9 mois et refaire les activités. Je suis sur qu'il sera plus à l'aise et autonome.
                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

    </div>
</section>

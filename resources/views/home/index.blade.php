@extends('layouts.default')

@section('content')

@include('home/masthead')
@include('home/description')
@include('home/whyus')
@include('home/produits',['kits'=>$kits])
@include('home/cmntcamarche')
@include('home/dernieres-actualites')
<!--include('home/feedbacks')-->
@include('home/partners')
@include('home/additional-links')


@stop
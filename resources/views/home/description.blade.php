<section class="bg-light text-center pt-5 pb-5">
    <div class="container pt-3">
        <div class="row">

            <div class="col-md-7 text-center">
                <div class="display-4 text-warning font-weight-bold">Le codage c'est fun !</div>
                <h1 class="display-5">Smarteo propose des expériences originales pour développer la créativité et apprendre en s’amusant.</h1>
                <h2 class="lead">Les enfants apprennent à commander des robots éducatifs pour jouer et réaliser des défis adaptés à leur âge. Il n’y a pas de limite à leur imagination : ils créent en manipulant des objets intelligents et développent les compétences du futur.</h2>

                <div class="explore-btn" id="viewall-btn">
                    <a href="robots-educatifs" class="button btn-orange">Découvrir nos activités robotiques</a>
                </div>

            </div>

            <div class="col-md-5 text-center pt-3">
                <img class="w-100 fancy-display border-orange" src="{{ url('img/kids-1.jpg') }}" alt="description"/>
            </div>
        </div>
    </div>
</section>
<section class="bg-light text-center" style="padding-bottom:4em;">
	<div class="container pt-4">
		<div class="row">
			<div class="col-md-6 cursor-pointer smart-hover grow">
				<a href="{{url('/actualites')}}">
					<div class="button-img img-1">
						<h2>Consulter nos dernières actualités</h2>
					</div>
				</a>
			</div>
			<div class="col-md-6 cursor-pointer smart-hover grow">
				<a href="{{url('nos-engagements')}}">
					<div class="button-img img-2">
						<h2>En savoir plus sur nos engagements</h2>
					</div>
				</a>
			</div>
			<div class="col-md-6 cursor-pointer smart-hover grow">
				<a href="{{url('surveys')}}">
					<div class="button-img img-3">
						<h2>Participez à nos enquêtes exclusives</h2>
					</div>
				</a>
			</div>
			<div class="col-md-6 cursor-pointer smart-hover grow">
				<a href="{{url('faq')}}">
					<div class="button-img img-4">
						<h2>Consulter nos questions fréquentes</h2>
					</div>
				</a>
			</div>
		</div>
	</div>

</section>
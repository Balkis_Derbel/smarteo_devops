<!-- actualités -->
<section class="bg-clouds pt-4">

    <div class="container text-center">

        <div class="row">
            <div class="col-md-12">
                <h1 class="card text-white bg-green p-5 rounded">
                    Nos dernières actualités
                </h1>
            </div>
        </div>

        <div class="row text-center" style="display:inline-block;">

            @foreach($latestnews as $item)
            @include('news/short', $item)
            @endforeach

        </div>

        <div class="row w-100 text-center mt-5" style="display:inline-block;">
            <div class="explore-btn" id="viewall-btn">
                <a href="actualites" class="button">Voir toutes les actualités</a>
            </div>
        </div>
    </div>
</section>
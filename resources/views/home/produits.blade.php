<!-- Produits -->
<section class="bg-light text-center ptb-3">
    <div class="container">

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card display-4 bg-yellow text-white rounded-circle p-4">Découvrez nos robots</div>
            </div>

            <div class="row text-center d-inline-block pt-3 mt-3">
                <div class="col-md-12">
                    <ul class="list">

                        @foreach($kits as $item)
                        <li>
                            @include('kits/card',$item)
                        </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>

        <div class="row w-100 d-inline-block text-center mt-4">
            <div class="explore-btn">
                <a href="robots-educatifs" class="button btn-yellow">Voir tous les robots</a>
            </div>
        </div>
    </div>
</section>

<section class="bg-green text-center ptb-3">
    <div class="container">

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card display-4 text-secondary bg-white rounded-circle p-4">Pourquoi choisir Smarteo ?</div>
            </div>
        </div>

        <div class="row pt-3 mt-3 text-white">

            <div class="col-md-4 text-center">

                <img class="w-100 rounded-circle" src="{{ url('img/illustration/smarteo-activity-mbot-2.jpg') }}" alt="">

                <div class="card-content mt-3">
                    <h3 class="mb-3">Simple</h3>                         
                    <p>Nous allons vous montrer que le codage est simple. Commencez à manipuler et à commander votre premier robot et à réaliser vos défis personnels !</p>
                </div>
            </div>

            <div class="col-md-4 text-center">

                <img class="w-100 rounded-circle" src="{{ url('img/illustration/smarteo-smbc-3.jpg') }}" alt="">

                <div class="card-content mt-3">
                    <h3 class="mb-3">Ludique</h3>                         
                    <p>Apprendre en s'amusant, ou plutôt s'amuser en apprenant. Nous vous aidons à passer de bons moments fun et ludiques en développant des compétences uniques.</p>     
                </div>
            </div>

            <div class="col-md-4 text-center">

                <img class="w-100 rounded-circle" src="{{ url('img/illustration/smarteo-activity-mbot-1.jpg') }}" alt="">

                <div class="card-content mt-3">
                    <h3 class="mb-3">Créatif</h3>                        
                    <p>Imaginez vos scénarios et libérez votre créativité ! Inventez des maquettes personnelles  et fabriquez vos propres programmes avec des robots.</p>                               
                </div>
            </div>   
        </div>
    </div>
</section>

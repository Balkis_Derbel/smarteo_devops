<section class="bg-blue text-center ptb-3">
    <div class="container">

        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card display-4 text-secondary bg-white rounded-circle p-4">Ils nous font confiance</div>
            </div>
        </div>

        <div class="row pt-3 mt-3 text-white">

            <div class="col-md-12 text-center" id="list-partners">

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='https://theschoolab.com/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-schoolab.png')}}" alt="schoolab"/>
                </div>

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='https://theschoolab.com/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-edfab.png')}}" alt="edfab"/>
                </div>

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='https://www.bpifrance.fr/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-bpifrance.png')}}" alt="bpifrance"/>
                </div>

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='https://les-savanturiers.cri-paris.org/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-savanturiers.png')}}" alt="savanturiers"/>
                </div>

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='https://www.nokia.com/fr_int/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-nokia-cie.jpg')}}" alt="nokia-cie"/>
                </div>

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='https://paris.makerfaire.com/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-makerfaire.png')}}" alt="meker-faire"/>
                </div>

                <div class="card bg-light cursor-pointer smart-hover grow w-25 d-inline-block m-2" onclick="location.href='http://firstlegoleaguefrance.fr/les-rencontres/meudon/'">
                    <img class="card-img" src="{{URL::to('img\network\logo-into-orbit.jpg')}}" alt="into-orbit-meudon"/>
                </div>                    


            </div>
        </div>
    </section>

<div class="text-center">
    <img src="{{URL::to('img/blog/post-1/adopter-robot.jpg')}}" style="display:inline-block;height:300px;width:100%;" alt="kit-marty" />

    <h1 class="col-md-12 center-block mt-3">
        Je veux adopter un robot
    </h1>
</div>


<div class="well well-sm">
    [Ce post est inspiré d’une discussion que j’ai eu avec un enfant âgé de 6 ans, qui se projette déjà dans ce que sera sa vie dans quelques années]
</div>

<figure class="figure">
  <img src="..." class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
  <figcaption class="figure-caption text-right">A caption for the above image.</figcaption>
</figure>

<div class="text-justify">
    <p class="lead mt-3"> 
        &#8226; Tu veux adopter un robot ? <br/>
        &#8226; Ah bon ? Et pourquoi pas un chien ? Ou tout autre animal de companie ? <br/>
        &#8226; Par ce qu’il est gentil, dis-tu ? Et qu’il t’aimera de tout son cœur ? <br/>
        &#8226; Et est-ce que tu penses qu’un jour ce robot pourra avoir des sentiments pour toi ? Qu’il sera fidèle et qu’il te défendra en cas de besoin ? <br/>
        &#8226; Est-ce que tu penses que son amitié sera sincère ?<br/>
        &#8226; Et si oui, pourra t’il un jour se retourner contre toi et se montrer méchant ?
    </p>
    <p class="mt-3">
        Maintenant revenons à l’essentiel et dis moi : mais qu’est ce qu’un robot exactement ? Je veux dire : dans le monde d’aujourd’hui. Je ne parle pas des robots imaginaires, que tu peux voir dans tes livres de bandes dessinées. Ni des robots de la Science Fiction comme ceux imaginés par le célèbre auteur Isaac Asimov, ou mis en scène par (…) (même si on a parfois le sentiment que ces innovations technologiques deviennent de plus en plus accessibles à la connaissance humaine).

        Sais tu que le terme ‘Robot’ est apparu pour la première fois dans une pièce de théatre en 1920 (*) ? Le mot a été créé à partir du mot tchèque « robota » qui signifie « travail, besogne, corvée ».

        Tu l’as compris, un robot est une machine, qui peut exécuter plusieurs tâches qui lui sont confiées par l’homme. Ces machines n’ont pas forcément une forme humaine. Elles ont simplement la forme optimale qui leur permet d’exécuter la mission pour laquelle elles ont été conçues.

        Par exemple, les robots sont ajourd’hui utilisés dans des grandes usines, pour effectuer des taches difficiles ou répétitives. On commence aussi à voir des robots agriculteurs, ou encore des robots chirurgiens. Il faut dire qu’une machine peut être beacoup plus précise qu’un humain. Ces machines peuvent aussi avoir une forme d’intelligence, qu’on appelle l’Intelligence Artificielle, qui leur permet de traiter rapidement une grande quantité d’informations afin de faire des déductions et être plus efficaces.

        Mais pour que les robots puissent exécuter leurs taches, il faut déjà les concevoir de façon à ce qu’ils puissent le faire. Et il faut ensuite savoir les commander. 

        Sais tu comment il faut faire pour commander un robot ? Eh ben il faut le programmer. Il faut lui écrire ce qu’il doit faire exactement, comment il doit raisonner et réagir, et encore comment il peut apprendre de ses erreurs. La programmation informatique est la façon de commander ces machines en décrivant les règles qui régissent leur fonctionnement.

        Alors tu me dis que tu aimes bien manipuler des robots ? Il est peut être temps que je commence à te montrer comment on fait pour dire à un robot comment il doit se déplacer ? Et on commencera par une application sur tablette, très simple, tu vas voir que c’est amsant. Plus tard, si tu le souhaites, je pourrai te montrer des languages de programmation qui s’appellent ‘Scratch’ et ‘mBlock’. Et on passera ensuite à des languages encore plus sophistiqués.

        Mais avant de commencer, je te laisse donc choisir un nom pour notre robot. Tu me dis ‘Smartoni’ ? Excellent choix ! Nous sommes donc prêts pour commencer à jouer.

        * de  l'auteur tchèque Karel Čapek

         


    </p>
</div>
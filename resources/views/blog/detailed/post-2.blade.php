@extends('layouts.default')

@section('title','EzRobot: le robot humanoide - SMARTEO - Apprendre en s\'amusant avec des robots')

@section('description','Apprenez-en plus sur la technologie avec les robots Clip\'n\'Play! Tous les robots EZ incluent le logiciel EZ-B v4 et EZ-Builder pour les capacités de vision, la reconnaissance vocale, les animations et l\'intelligence artificielle. Il peuvent être programmés en utilisant Blockly, RoboScratch, .NET et plus encore.')

@section('keywords','EZRobot')

@section('image', URL::to('img/kits/ezrobot.png'))

@section('content')

<section class="bg-light">

    <div class="container" style="padding-top:6rem;">
        <div class="row text-justify">
            <div class="col-md-12">
                <div style="text-align:center;">
                    <img src="{{URL::to('img/kits/ezrobot.png')}}" style="display:inline-block;max-height:300px;max-width:100%;" alt="kit-ezrobot" />
                    <h1 class="col-md-12 center-block mt-3">
                        L’Intelligence Artificielle finira-t-elle par prendre le dessus sur l’Intelligence Humaine ?
                    </h1>
                </div>

                <p class="lead mt-3">
                    Apprenez-en plus sur la technologie avec les robots Clip'n'Play!
                </p>
                <p class="lead mt-1">
                    Tous les robots EZ incluent le logiciel EZ-B v4 et EZ-Builder pour les capacités de vision, la reconnaissance vocale, les animations et l'intelligence artificielle. Il peuvent être programmés en utilisant Blockly, RoboScratch, .NET et plus encore! Les possibilités de ce robot sont très larges et il peut convenir à un jeune comme à un adulte intéressé d'explorer les possibilités de la robotique.
                </p>

                <h2 class="col-md-12 col-md-offset-2 mt-3">
                    EzRobot pour l'éducation:
                </h2>

                <p class="lead mt-3">
                    EZ-Robot a reconnu l'importance de l'éducation des robots à la maison, en classe et au travail. Il existe une commaunauté active et plusieurs tutoriels peuvent être trouvés sur site du fabriquant:
                    
                    <a href="https://www.ez-robot.com/Tutorials/">https://www.ez-robot.com/Tutorials/</a>
                </p>
                

                <p class="lead mt-1">
                    Le kit Smarteo inclut l'accès à des ressources additionnelles en ligne qui aideront l'utilisateur à avoir une prise en main rapide du robot et le guideront à réussir tous les paramétrages nécessaires pour commencer à programmer. 
                </p>
            </div>
        </div>
        @include('kits/detailed/order')
    </div>
</section>

@stop
@extends('layouts.default')

@section('title','Apprendre la programmation informatique : est-ce vraiment nécessaire ?')

@section('description','')

@section('keywords','')

@section('image', URL::to('img/kits/dash.png'))

@section('content')


<section class="bg-light">

    <div class="container" style="padding-top:6rem;">
        <div class="row text-justify">
            <div class="col-md-12">
                <div class="text-center">
                    <img src="{{URL::to('img/kits/dash.png')}}" style="display:inline-block;max-height:300px;max-width:100%;" alt="kit-dash" />
                    <h1 class="col-md-12 center-block mt-3">
                        Apprendre la programmation informatique : est-ce vraiment nécessaire ?
                    </h1>
                </div>

                [Une discussion imaginaire entre un parent et un enfant, similaire à beacoup d’autres discussions improbables que j’ai eues avec des enfants débordant d’imagination]

                -   Et quand tu sera grand, est ce que tu aimerais apprendre à conduire une voiture ?
                -   Oui, absolument,
                -   Ceci dit, peut être que d’ici là, personne n’aura plus besoin d’apprendre à conduire une voiture. La science a déjà inventé les véhicules autonomes, qui s’autoconduisent eux-mêmes.
                -   Ah bon ? Et les trains aussi vont pouvoir se conduire eux-mêmes ? Et les avions aussi ? 
                -   Oui, et même les bateaux et les sous-marins. 
                -   Et les navettes spatiales ?
                -   Tu sais ? Les humains ont déjà envoyé un robot sur la planète Mars, et qui s’est débrouillé seul. Ils ont aussi envoyé une navette spatiale qui s’appelle Rosetta avec son robot Philae, qui s’est posé il y’a 4 ans sur un comète lointain, et qui s’est aussi débrouillé seul.
                -   Mais alors, qu’est-ce que je vais conduire, moi ?
                -   Tu pourra peut-être commander ces machines qui s’auto-conduisent ?


                Il n’y a désormais plus aucun doute que le monde dans lequel nous vivons a déjà pris le tournant de la 4éme révolution industrielle. Les robots performants et l’intelligence artificielle ont quitté depuis des lustres le domaine de la science-fiction. Ils sont aujourd’hui présents dans nos usines, nos fermes, nos hopitaux et aussi dans notre vie quotidienne. Et ils continuent à prendre une place de plus en plus importante dans notre quotidien.

                Alors, à la question ‘Faut-il apprendre la programmation informatique ?’ : Il y’a 150 ans, cette question aurait été équivalente à ‘Faut’il apprendre à lire et à écrire’. Les spécialistes commencent à parler d’« illectronisme », un néologisme qui transpose le concept d’ « illettrisme », pour traduire la nécessite d’apprendre à coder.

                Ce sujet n’est pas banal. Selon une étude récente, 85% des métiers de 2030 n’existent pas encore, et il y’a un grand risque qu’une partie des enfants d’aujourd’hui n’aient pas les compétences nécessaires pour être employables dans 15 ans. Serons-nous face à une nouvelle forme accentuée de la fracture numérique ? Ou à de nouvelles exclusions ?

                <p class="lead mt-3">
                    Alors OUI, il FAUT commencer à apprendre la programmation dès maintenant. Et aussi la robotique. Non pas pour devenir tous des roboticiens, mais pour continuer à interagir et jouer notre rôle dans la société de demain. 
                </p>



            </div>
        </div>
    </div>

</section>


<section class="bg-light text-center">
    <div class="container pt-5 pb-5">

        <div class="row">

            <div class="col-lg-8">
                <img src="{{url('img/actualites/'.$data['image'])}}" style="width:100%;border-radius:15px;" alt="{{$data['reference']}}" />

                <h1 style="color:#1289A7;font-weight:bold;margin: 10px 0;">Apprendre la programmation informatique : est-ce vraiment nécessaire ?</h1>
                <h4 style="color:#1289A7;">event_date</h4>

                <div class="pt-3" style="text-align:justify;">
                    <p class="font-weight-light">

                    @include('blog/detailed/post-1')

                        
                    </p>
                </div>

                <div class="explore-btn pt3" id="viewall-btn">
                    <a href="{{url('actualites')}}" class="button">Voir toutes les actualités</a>
                </div>

            </div>

            <div class="col-lg-4">
                <a href="{{url('actualites')}}" class="nounderline"><h2 class="w-100 bg-info p-3"> Dernières actualités</h2></a>

                @*foreach($alldata as $item)
                    @include('news/box', $item)
                @endforeach*@

            </div>
        </div>



    </div>

</div>
</section>


@stop
<div class="smart-link news" onclick="location.href='{{url('blog/'.$item['url'])}}'">
    <div class='date'>
        {{$item['published_date']}}
    </div>
    <img src="{{url('img/blog/'.$item['image'])}}" alt="{{$item['title']}}" />
    <div class='title'>
        <h3>{{$item['title']}}</h3>
    </div>
</div>
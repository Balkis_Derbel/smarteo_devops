
<div class="row" style="width:100%;padding:10px;margin-top:40px;">
    <div class="col-md-5 order-md-1">
        <img src="{{url('img/actualites/'.$item['image'])}}" style="width:100%;border-radius:15px;margin-bottom:10px;" alt="{{$item['reference']}}" />
    </div>
    <div class="col-md-7 order-md-2">
        <h5 style="color:#1289A7;">{{$item['published_date']}}</h5>
        <a href="{{url('/blog/'.$item['url'])}}">
            <h4 style="color:#1289A7;font-weight:bold;margin: 10px 0;">{{$item['title']}}</h4>
        </a>
        <p class="font-weight-light" style="text-align:justify;">
            {!!$item['description']!!}
        </p>
    </div>
</div>
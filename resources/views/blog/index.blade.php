@extends('layouts.default')

@section('title','Blog - Apprendre à l\'ère du digital - SMARTEO')

@section('description','Suivez notre Blog pour échanger autour de l\'importance de développer les compétences numériques pour bien se préparer au monde de demain')

@section('content')


<!-- blog -->

<section class="bg-light">

    <div class="masthead news text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="description col-xl-9 mx-auto">
                    <h1>
                        Blog
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container text-left pt-5 pb-5">
        <?php print_r($data); ?>
        @foreach($data as $item)
        @include('blog/short', $item)
        @endforeach

    </div>
</section>

@stop
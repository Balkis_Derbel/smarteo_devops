@extends('layouts.default')

@section('title','Actualités - '.$alldata[$selected]['title'])

@section('description',$alldata[$selected]['description'])

@section('keywords',$alldata[$selected]['keywords'])

@section('image', URL::to('img/blog/'.$alldata[$selected]['image']))

@section('content')

<section class="bg-light text-center">
    <div class="container pt-5 pb-5">

        <div class="row">

            <div class="col-lg-8">

                @include('blog/detailed/'.$alldata[$selected]['reference'])

                <div class="explore-btn pt3" id="viewall-btn">
                    <a href="{{url('actualites')}}" class="button">Voir touts les posts</a>
                </div>

            </div>

            <div class="col-lg-4">
                <a href="{{url('actualites')}}" class="nounderline"><h2 class="w-100 bg-info p-3"> Derniers posts</h2></a>

                @foreach($alldata as $item)
                @include('blog/box', $item)
                @endforeach

            </div>
        </div>



    </div>

</section>

@stop
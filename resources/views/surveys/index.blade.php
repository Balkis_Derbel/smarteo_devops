@extends('layouts.default')

@section('title','Nos enquêtes pour les parents et les acteurs de l\'enseignement')

@section('description','Participez à notre enquête sur le développement d\'une culture digitale pour nos enfants')


@section('content')

<!-- Surveys -->

<div class="masthead surveys text-white text-center">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="description col-xl-12 mx-auto">
        <div>
          Participez à notre enquête sur le développement d'une <span class="description1">culture digitale</span> pour nos enfants
        </div>
        <div>
          <a href="{{url('surveys/parents')}}" class="button b1">Parents</a>
          <a href="{{url('surveys/enseignant')}}" class="button b2">Acteurs de l'Enseignement</a>
        </div>
      </div>
    </div>
  </div>
</div>

@if(strpos($_SERVER['REQUEST_URI'], 'parents')==true)
    @include('surveys/02_parent')
@else
    @include('surveys/01_enseignant')
@endif

    
@stop
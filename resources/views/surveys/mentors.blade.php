@extends('layouts.default')

@section('title','Devenez Animateur / Animatrice d\'Ateliers de Robotique avec SMARTEO')

@section('description','Devenez Animateur / Animatrice d \'Ateliers de Robotique avec SMARTEO')
@section('image','https://www.smarteo.co/img/overview/smarteo-animateurs.png')

@section('content')

<!-- Surveys -->

<div class="masthead mentors text-white text-center">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="description col-xl-12 mx-auto">
				<div>
					Rejoignez notre réseau de <span style="color:#f6b93b;font-weight:bold;">SmartMentors</span> <br> et venez animer des ateliers de <span style="color:#f6b93b;font-weight:bold;">robotique</span> et de <span style="color:#f6b93b;font-weight:bold;">codage</span>
				</div>
			</div>
		</div>
	</div>
</div>

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScLKEX1poZNkz4iWNP5tvv4L4fSNbLtUXZOK1LvVTGGt3zjEg/viewform?embedded=true" width="100%" style='position:absolute; top:550px; left:0px; width:100%; height:100%;z-index:100; 'frameborder="0" marginheight="0" marginwidth="0">Chargement en cours...</iframe>

@stop
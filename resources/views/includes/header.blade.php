<!-- Navigation -->
<nav class="navbar navbar-light fixed-top p-0" id="mainNav">
  <div class="container mw-100 bg-light" id="mainNavBar">
    <a class="navbar-brand js-scroll-trigger bg-light" href="{{url('/#home')}}" style="margin-right:0.5rem;">
      <img src="{{URL::to('img/smarteo.png')}}" class="img-logo" id="mainNavLogo" alt="SMARTEO"/>
    </a>

    <div>
      <span class="img-item">
        <a href="{{url('login')}}"><img src="{{URL::to('img/ico-furby.png')}}" alt="ico-user" /></a>
      </span>

      <span class="img-item">
        <a href="{{url('robots-educatifs')}}"><img src="{{URL::to('img/ico-shopping.png')}}" alt="ico-search"/></a>
      </span>

      <span class="img-item">
        <a href="{{url('faq')}}"><img src="{{URL::to('img/ico-chat.png')}}" alt="ico-help"/></a>
      </span>
    </div>

    <button class="navbar-toggler navbar-toggler-right m-0" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
</div>
  <div class="container mw-100">
    <div class="collapse navbar-collapse bg-light p-3" id="navbarResponsive" style="opacity:1;">
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/qui-sommes-nous')}}">Qui Sommes Nous</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/mon-espace-personnel')}}">
          Espace personnel</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/robots-educatifs')}}">Robots éducatifs</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/contenus-et-tutoriels')}}">Contenus et tutoriels</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/communaute')}}">Communauté</a>
        </li>

        <li class="nav-item nav-focus-1 bg-success">
            <a class="nav-link js-scroll-trigger pl-3 pr-3" href="http://www.smartbotcamp.com">SmartBotCamp</a>
        </li>

        <li class="nav-item nav-focus-1 bg-blue">
            <a class="nav-link js-scroll-trigger pl-3 pr-3" href="https://www.smarteoclub.fr">
            SmarteoClub</a>
        </li>

        <li class="nav-item nav-focus-1 bg-danger">
            <a class="nav-link js-scroll-trigger pl-3 pr-3" href="{{url('/challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps')}}">
            Le Challenge WLRCxSmarteo : 'Un Magnifique Voyage Dans le Temps'</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/actualites')}}">Nos dernières actualités</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/newsletter')}}">Newsletter</a>
        </li>

        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/nos-engagements')}}">Nos Engagements</a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="{{url('/contact')}}">Contact</a>
        </li>

      </ul>
    </div>
  </div>
</nav>
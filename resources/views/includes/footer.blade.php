<section class="bg-dark">
  <div class="container pt-4">
    <div class="row pt-4">
      <div class="col-md-4">

        <ul class="list-unstyled mb-2">
          <li>
            <a class="js-scroll-trigger" href="{{url('/qui-sommes-nous')}}">A propos de Smarteo</a>
          </li>

          <li>
            <a class="js-scroll-trigger" href="{{url('/parents')}}">Pour les parents</a>
          </li>

          <li>
            <a class="js-scroll-trigger" href="{{url('/enseignant')}}">Pour les éducateurs</a>
          </li> 

          <li>
            <a class="js-scroll-trigger" href="{{url('/kits')}}">Nos kits</a>
          </li> 

          <li>
            <a class="js-scroll-trigger" href="http:\\www.smartbotcamp.com">Le SmartBotCamp</a>
          </li> 

        </ul>

      </div>

      <div class="col-md-4">

        <ul class="list-unstyled mb-2">
          <li>
            <a class="js-scroll-trigger" href="{{url('/nos-engagements')}}">Nos engagements</a>
          </li>

          <li>
            <a class="js-scroll-trigger" href="{{url('/cgv')}}">Conditions générales</a>
          </li>

          <li>
            <a class="js-scroll-trigger" href="{{url('/mentions-legales')}}">Mentions légales</a>
          </li>

          <li>
            <a class="js-scroll-trigger" href="{{url('/faq')}}">Questions fréquentes</a>
          </li>

          <li>
            <a class="js-scroll-trigger" href="{{url('/contact')}}">Contact</a>
          </li>

        </ul>
      </div>
      
      <div class="col-md-4">

        <div id="mc_embed_signup">
          <h4 id="soc-title">Restez à l'écoute, abonnez vous à notre newsletter et suivez nous sur les réseaux sociaux</h4>
          <form action="https://smarteo.us18.list-manage.com/subscribe/post?u=beb839abc2a2c8555b7b37928&amp;id=5820aa215d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
              <div class="row">
                <div class="col-md-8">
                  <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Adresse Email" required style="width:100%;">
                  <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_beb839abc2a2c8555b7b37928_5820aa215d" tabindex="-1" value=""></div>
                </div>
                <div class="col-md-4">
                  <input type="submit" value="S'inscrire" name="subscribe" id="mc-embedded-subscribe" class="button" style="width:100%;">
                </div>
              </div>
            </div>
          </form>
        </div>

        <div id="social-icons">
          <a href="https://www.facebook.com/smarteo.co">
            <div class="ico-social ico-facebook"></div>
          </a>

          <a href="https://twitter.com/smarteo_co">
            <div class="ico-social ico-twitter"></div>
          </a>

          <a href="https://www.instagram.com/smarteo.co/">
            <div class="ico-social ico-instagram"></div>
          </a>

          <a href="https://www.linkedin.com/company/smarteo-co/">
            <div class="ico-social ico-linkedin"></div>
          </a>
        </div>
      </div>
      
    </div>

    <div class="row pt-2">
      <div class="col-lg-12 text-center my-auto">
        <img src="{{url('/img/logos/secure-stripe-payment-logo.png')}}" style="width:240px;"/>
      </div>
    </div>

    <div class="row pb-2">
      <div class="col-lg-12 h-100 text-center my-auto">
        <p class="text-muted small mt-4 mb-4 mb-lg-0">&copy; Smarteo 2018. Tous droits réservés.</p>
      </div>

    </div>
  </div>
</section>
<div class="masthead {{$which}} text-white text-center">
    <div class="container">
        <div class="row">
            <div class="description col-xl-12 mx-auto">
                <h1 class="text-white">
                    {{$title}}
                </h1>
            </div>
        </div>
    </div>
</div>
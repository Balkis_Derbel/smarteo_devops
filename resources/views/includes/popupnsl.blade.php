<div id="list-builder" style="z-index:100;"></div>
<div id="popup-box" style="z-index:101;">
    <img src="{{url('img/close.png')}}" id="popup-close" />
    <div id="popup-box-content">
        <div class="display-5 text-white">Abonnez vous à notre newsletter</div>
        <p class="lead">
         Restez au courant des dernières nouveautés et recevez un contenu exclusif en vous abonnant à notre newsletter.
     </p>

     <form action="https://smarteo.us18.list-manage.com/subscribe/post?u=beb839abc2a2c8555b7b37928&amp;id=5820aa215d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
          <div class="row">
            <div class="col-md-8">
              <input type="email" value="" name="EMAIL" class="email p-3 h-100" id="mce-EMAIL" placeholder="Adresse Email" required style="width:100%;">
              <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_beb839abc2a2c8555b7b37928_5820aa215d" tabindex="-1" value=""></div>
          </div>
          <div class="col-md-4">
              <input type="submit" value="S'inscrire" name="subscribe" id="mc-embedded-subscribe" class="lead btn btn-info w-100 p-3 h100">
          </div>
      </div>
  </div>
</form>
</div>
</div>
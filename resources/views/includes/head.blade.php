<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Meta tags -->
<title>@yield('title', config('app.title'))</title>
<meta name="description" content="@yield('description', config('app.description'))"/>
<meta name="keywords" content="@yield('keywords'),{{config('app.keywords')}}"/>
<meta name="copyright" content="{{ config('app.name') }}">
<meta name="author" content="{{ config('app.name') }}"/>
<meta name="application-name" content="{{ config('app.name') }}">
<!--GEO Tags-->
<meta name="DC.title" content="@yield('title', config('app.title'))"/>
<meta name="geo.region" content="GB-HMF"/>
<meta name="geo.placename" content="Paris"/>
<meta name="geo.position" content="48.7828;2.3044"/>
<meta name="ICBM" content="48.7828;2.3044"/>
<!--Facebook Tags-->
<meta property="og:site_name" content="{{ config('app.name') }}">
<meta property="og:type" content="article"/>
<meta property="og:url" content="{{ request()->fullUrl() }}"/>
<meta property="og:title" content="@yield('title', config('app.title'))"/>
<meta property="og:description" content="@yield('description', config('app.description'))"/>
<meta property="og:image" content="@yield('image', config('app.image'))"/>
<meta property="og:locale" content="fr_FR"/>
<!--Twitter Tags-->
<meta name="twitter:card" content="summary"/>
<meta name="twitter:site" content="{{ '@' . config('app.name') }}"/>
<meta name="twitter:title" content="@yield('title', config('app.title'))"/>
<meta name="twitter:description" content="@yield('description', config('app.description'))"/>
<meta name="twitter:image" content="@yield('image', config('app.image'))"/>

<link rel="shortcut icon" type="image/x-icon" href="{{url('img/logo-smarteo.png')}}" />
<link rel="apple-touch-icon" sizes="120x120" href="{{url('img/apple-touch-icon-120x120-precomposed.png')}}" />
<link rel="apple-touch-icon" sizes="120x120" href="{{url('img/apple-touch-icon-152x152-precomposed.png')}}" />

<?php
$path = Request::path();
$url = "https://www.smarteo.co/";
if($path != "/") {$url = $url.$path;}
?>
<link rel="canonical" href="{{$url}}" />
<meta name="google-signin-client_id" content="970435945813-dk77huv382442ff4mh7jp9jjjea1uadh.apps.googleusercontent.com">

<?php 
if(false/*APP_DEBUG*/)
{
  echo '<link href="vendor/bootstrap-4.0.0/css/bootstrap.min.css" rel="stylesheet">';
  echo '<link href="vendor/bootstrap-4.0.0/css/bootstrap-grid.css" rel="stylesheet">';
  echo '<link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">';
  echo '<link href="css/ace.css" rel="stylesheet">';
  echo '<link href="css/smarteo.css" rel="stylesheet">';
  echo '<link href="css/account.css" rel="stylesheet">';
  echo '<link href="css/contents.css" rel="stylesheet">';
}?>

{!! Html::style('vendor/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('vendor/googleapis/font.css') !!}
{!! Html::style('css/smarteo.css.php') !!}

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-42237523-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() {dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-42237523-4');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 998479216 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-998479216"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'AW-998479216');
</script>

<!-- Crisp -->
<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="e384bf3a-76fc-4f87-9441-4ddc27da77ce";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

 {!! NoCaptcha::renderJs() !!}
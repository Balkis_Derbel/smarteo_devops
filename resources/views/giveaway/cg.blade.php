@extends('layouts.default')

@section('title','Conditions générales')


@section('content')

<section class="bg-light">

    <div class="masthead faq text-white text-center">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="description col-xl-9 mx-auto">
                    <h1>
                        Conditions générales
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container text-left">

        <div class="page-content">


            <div class="row">
                <div class="col-md-12 text-justify">
                    <h1 class="mt-3">Conditions de participation</h1>
                    <p class="lead">Ce jeu est ouvert à toute personne majeure résidant en France métropolitaine, à l'exclusion des membres du personnel de la Société Organisatrice. 
                        En cas de litige, un justificatif d’identité pourra être demandé.
                    La participation au jeu implique l’acceptation sans réserve du Règlement. </p>
                    
                    <h1 class="mt-3">Modalités</h1>
                    <p class="lead">Pour participer les candidats doivent remplir le formulaire prévu à cet effet. Une seule participation est autorisée par candidat (même nom et même prénom).</p> 
                    <ul>
                        <li>
                            <p class="lead">Un tirage au sort sera effectué le 14/02/2018 par la Société Organisatrice du jeu pour désigner les 3 gagnants parmi les participants du jeu. </p>
                        </li>

                        <li><p class="lead">La date du tirage au sort pourra être décalée. Les lots seront adressés aux gagnants après avoir été contactés par la Société Organisatrice. </p>
                        </li>
                        <li><p class="lead">Tout lot non réclamé dans le délai de un mois après la date du tirage au sort sera considéré comme abandonné par le(s) gagnant(s) et comme restant propriété de la société organisatrice.</p></li>
                        <li><p class="lead">Toute identification ou participation incomplète, erronée ou illisible, volontairement ou non, ou réalisée sous une autre forme que celle prévue dans le présent Règlement sera considérée comme nulle.</p></li>

                    </ul>                    

                <h1 class="mt-3">Les prix</h1>
                <p class="lead">Les lots sont composés de trois 3 abonnements gratuits d’un mois pour la souscription à des kits robotiques, ou de bons de réduction, d’une valeur unitaire minimale de 24 €, utilisables sur le site https://www.smarteo.co.
                    Toutes précisions complémentaires pour la remise du prix seront données en temps utile au gagnant.
                    Les prix offerts ne peuvent donner lieu, de la part des gagnants, à aucune contestation d'aucune sorte ni à la remise de leur contre-valeur sous quelques formes que ce soit, ni à leur remplacement ou échange pour quelque cause que ce soit.
                    En cas de force majeure, ou si les circonstances l'exigeaient, la Société Organisatrice se réserve le droit de remplacer les lots gagnés en tout ou partie par des lots de valeur équivalente.
                </p>

                <h1 class="mt-3">Modalités de désignation, d’information des Gagnants</h1>
                <p class="lead">
                    Les gagnants seront notifiés par email ou par téléphone dans la semaine suivant le tirage au sort.

                    Aucun changement (de date, de lot…) pour quelque raison que ce soit ne pourra être demandé à la Société Organisatrice. Il est précisé que la Société Organisatrice ne fournira aucune prestation de garantie concernant le lot. En tout état de cause, l’utilisation de la (des) dotation(s) se fera selon les modalités communiquées par la Société Organisatrice.

                </p>

                <h1 class="mt-3">Consultation et acceptation du règlement</h1>
                <p class="lead">
                    Le simple fait de participer entraîne l'acceptation entière et sans réserve du présent règlement.
                    La Société Organisatrice se réserve le droit, à tout moment et sans préavis ni obligation de justifier sa décision, d’écourter, de prolonger, de reporter ou d’annuler le Jeu ainsi que de modifier tout ou partie des conditions d’accès et/ou des modalités de mise en œuvre du Jeu, sans que sa responsabilité ne puisse être engagée ni qu’aucune indemnité ne puisse lui être réclamée de ce fait.
                </p>

                <h1 class="mt-3">Données personnelles</h1>
                <p class="lead">
                    Les participants autorisent par avance du seul fait de leur participation, que les Organisateurs utilisent librement à des fins publicitaires ou promotionnelles quel qu’en soit le support, toutes les informations nominatives communiquées pour le compte de ceux-ci et sur tous supports.
                    Les données à caractère personnel recueillies vous concernant sont obligatoires et nécessaires pour le traitement de votre participation au jeu. Elles sont destinées aux Organisateurs, ou à des sous-traitants et/ou des prestataires pour des besoins de gestion.
                    <br>
                    <br>
                    Conformément à la réglementation en vigueur, les informations collectées sont destinées exclusivement aux Organisateurs et elles ne seront ni vendues, ni cédées à des tiers, de quelque manière que ce soit.
                    Chaque Participant au Jeu reconnaît avoir pris connaissance de l'intégralité des stipulations du Règlement et l'avoir par conséquent accepté en toute connaissance de cause.
                    <br>
                    <br>
                    Conformément à la loi « Informatique et Libertés » du 06/01/1978, vous disposez d'un droit d'accès, de rectification et de suppression de ces données ainsi que d’un droit de vous opposer à ce que ces données fassent l'objet d'un traitement en nous contactant par courrier à l’adresse suivante SMARTEO – 24 avenue Georges Clémenceau – 92330 SCEAUX ou par e-mail à l’adresse contact@smarteo.co.
                </p>


            </div>
        </div>
    </div>
</div>
</section>

@stop
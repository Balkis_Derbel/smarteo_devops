@extends('layouts.default')

@section('title','Jeu concours - Gagnez votre abonnement pour utiliser un de nos robots éducatifs')

@section('description','Smarteo permet d\'apprendre en jouant, ou plutôt de jouer en apprenant. Coder un robot devient un jeu facile et amusant. Plusieurs modèles sont proposés, pour tous les âges, en vente ou en mode location')

@section('content')

<!-- Parents -->

<section class="giveaway bg-light">	

	<div class="masthead giveaway text-white text-center">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="description col-xl-9 mx-auto">

					<h1>			
						Tentez de gagner le gros lot en participant au tirage au sort
					</h1>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-light">
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<h1 class="text-info">	
					Veuillez rentrer vos informations pour participer au jeu :
				</h1>
			</div>
		</div>

		<div class="row" style="padding: 2rem 0;">

			<div class="col-md-3"></div>
			<div class="col-sm-6">


				{!! Form::open(['url' => 'jeu-concours']) !!}
				<div class="form box">

					<div class="form-group {!! $errors->has('firstname') ? 'has-error' : '' !!}">
						{!! Form::text('firstname', null, ['class' => 'form-control w-100 name', 'placeholder' => 'Prénom']) !!}
						{!! $errors->first('firstname', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('lastname') ? 'has-error' : '' !!}">
						{!! Form::text('lastname', null, ['class' => 'form-control name', 'placeholder' => 'Nom']) !!}
						{!! $errors->first('lastname', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
						{!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Email']) !!}
						{!! $errors->first('email', '<small class="help-block text-danger">:message</small>') !!}
					</div>

					<div class="form-group"">

						{{ Form::checkbox('cbnsl',null,null, array('id'=>'cbnsl')) }}
						S'inscrire à la newsletter
					</div>
					<div class="form-group {!! $errors->has('cb_leg') ? 'has-error' : '' !!}">

						{{ Form::checkbox('cbcond',null,null, array('id'=>'cbcond')) }}
						J'ai consulté <span class="text-info"> <a href={{url('jeu-concours/conditions-generales')}}>les conditions générales de participation</a></span>
					</div>

					<div style="text-align:center;">
						{{ Form::hidden('token', $token) }}
						{{ Form::hidden('route', '/jeu-concours') }}
						{!! Form::submit('Participer', ['class' => 'btn btn-info btn-lg']) !!}
					</div>
				</div>
				{!! Form::close() !!}

			</div>
		</div>


		<div class="row text-center">
			<div class="col-md-2"></div>
			<div class="col-md-8">

				<h1 class="text-info">	
					Doublez vos chances de gagner en invitant des amis
				</h1>

				<h2 class="text-warning">Vos chances seront augmentées pour chaque ami invité !</h2>
				<h2 class="text-warning">En invitant 10 amis, vous aurez 10 fois plus de chances de gagner.</h2>

			</div>
		</div>
		<div class="row text-center pt-3">
			<div class="col-md-12">
				<a href="mailto:?to= 
				&bcc=5300217@bcc.hubspot.com 
				&subject=Jeu%20concours%20SMARTEO%20-%20Apprendre%20à%20coder%20avec%20des%20robots
				&body=Salut,%0D%0A %0D%0A Je t'invite à participer au jeu concours SMARTEO pour tenter de gagner des abonnements gratuits à leurs robots éducatifs :) Il suffit de cliquer sur ce lien: %0D%0A	https://www.smarteo.co/jeu-concours/{{$token_0}}
				%0D%0A %0D%0A
				A bientôt !"
				class='btn btn-success btn-lg'>J'invite mes amis</a>
			</div>
		</div> 

	</div>


</section>




@include('home/additional-links')

@stop
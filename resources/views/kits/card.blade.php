<div class="card smart-kit" onclick="location.href='{{url('robots-educatifs/'.$item->slug)}}';">
	<div class="card-img-top">
		<img src="{{URL::to('products/'.$item->reference.'/'.$item->thumb)}}" alt="{{$item->title}}" />
	</div>
	<div class="card-comment">
		{{$item->subtitle}}
	</div>
	<div class="card-title px-1">
		<h3 class="display-6 text-dark font-weight-bold">{{$item->title}}</h3>
	</div>
	<div class="card-price">
		<div class="col-md-12 text-center">
			@if($item->selling_price != 0)
			<span class="text-info font-weight-bold display-6 mr-3"><img src="{{URL::to('img/ico-shop-buy.png')}}" style="height:20px;"/>
				{{$item->selling_price}} &euro;
			</span>
			@endif


			@if($item->subscr_amount != 0)
			<span class="text-info font-weight-bold display-6">
				<img src="{{URL::to('img/ico-shop-subscribe.png')}}" style="height:20px;"/>{{$item->subscr_amount}} &euro; <span class="display-7">/ Sem.</span>
			</span>
			@endif

		</span>
	</div>
</div>
</div>
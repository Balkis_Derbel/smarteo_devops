@extends('layouts.default')

@section('title','Catalogue de robots éducatifs - SMARTEO')

@section('description','Catalogue de robots éducatifs et activités pour apprendre en s\'amusant avec des robots.')


@section('scripts')

<script type="text/javascript">

	$(function() {
		$('body').on('click', '.pagination a', function(e) {
			e.preventDefault();

			$('#load a').css('color', '#dfecf6');

			var url = '{{url('robots-educatifs')}}' + $(this).attr('href');
			getProducts(url);
			window.history.pushState("", "", url);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});

		$(".search_button").click(function(e) {
			e.preventDefault();
			var url = '{{url('robots-educatifs')}}';
			getProducts(url);
			window.history.pushState("", "", url);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});

		function getProducts(url) {
			var name = $('#search_box').val();

			$.ajax({
				url 	: url,
				data 	: {
					search : name
				}
			}).done(function (data) {
				$('#products_list').empty().html(data);  
			}).fail(function () {
				alert('Une erreur est survenue ...');
			});
		}
	});

</script>


@stop


@section('content')


@include('kits/products-header')


<section class="bg-clouds">

	<div class="container container-fluid pt-5 pb-5">

		<div class="row">
			<div class="col-md-12">
				<form method="get" action="" autocomplete="on" class="search_box">
					<input type="text" id="search_box" class="from-control" placeholder=""/>
					<input type="submit" class="search_button" style="z-index:5;"/>
				</form>
			</div>
		</div>

		<div class="row text-center d-inline-block w-100" id="products_list">
			@if (!$data->count())
			@include('kits/products-list-empty')
			@else
			@include('kits/products-list')
			@endif
		</div>

	</div>
	
</section>

@stop
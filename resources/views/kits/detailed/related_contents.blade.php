@if(sizeof($related_contents) !=0)
<div class="section-head" id="smart-kits">Consultez ces contenus et ressources pour apprendre à maitriser ce robot</div>

<section class="bg-clouds">
    <div class="container container-fluid py-3">
        <div class="row">
            <div class="col-md-12 text-center d-inline-block">
                <ul class="list">
                    @foreach($related_contents as $item)
                    <li>
                        @include('contents/item', $item)
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center explore-btn mt-3">
                <a href="{{url('contenus-et-tutoriels')}}" class="button">Voir toutes les ressources</a>
            </div>
        </div>
    </div>
</section>
@endif
<div class="row text-justify pt-5">
    <div class="col-md-1"></div>
    
    @if($kit->subscr_amount_ref != 0)        
    <div class="col-md-5 text-center">
        <div class="box-order">
            <img src="{{URL::to('img/ico-fast.png')}}" alt="abonnement" />
            <h2 class="text-info">
                Testez, expérimentez et ne payez que pour votre utilisation
            </h2>
            <p class="lead mt-3">
                Vous pouvez utiliser ce produit pour 

                @if($kit->subscr_amount != $kit->subscr_amount_ref)
                <span class="badge badge-danger price"><s>{{$kit->subscr_amount_ref}} EUR</s></span>
                @endif

                <span class="badge badge-info price">{{$kit->subscr_amount}} EUR</span> par semaine 

                @if($kit->subscr_amount != 0)   
                    avec un engagement initial de 4 semaines
                @else
                    pendant les 4 premières semaines
                @endif

            </p>

            <div class="row text-center mb-4">
                <div class="order-btn">
                    <a href="{{url('boutique/souscription/'.$kit->slug)}}" class="btn btn-danger" style="word-wrap: break-word;">Je veux souscrire à cet abonnement</a>
                </div>
            </div>

        </div>
    </div>
    @else
    <div class="col-md-2">
    </div>
    @endif

    <div class="col-md-5 text-center">
        <div class="box-order">
            <img src="{{URL::to('img/ico-simple.png')}}" alt="achat" />
            <h2 class="text-info">
                Vous aimez ce produit et vous voulez l'adopter ?
            </h2>
            <p class="lead mt-3">
                @if($kit->subscr_amount_ref != 0)     
                    Vous pouvez aussi l'acheter pour 
                @else
                   Vous pouvez l'acheter pour 
                @endif

                @if($kit->selling_price != $kit->selling_price_ref)
                <span class="badge badge-danger price"><s>{{$kit->selling_price_ref}} EUR</s></span>
                @endif

                <span class="badge badge-info price">{{$kit->selling_price}} EUR</span>
            </p>

            <div class="row text-center mb-5">
                <div class="order-btn">
                    <a href="{{url('boutique/achat/'.$kit->slug)}}" class="btn btn-danger">Je veux l'acheter</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3 pb-3">
    <div class="col-md-12  text-center">
        <p class="lead">
            <b>Pas de frais cachés : </b>Les prix indiqués incluent les frais d'envoi postal en France métropolitaine.
        </p>

        <p class="lead mt-1 text-center">
            <!--<a href="modalites">Voir les modalités d'achat et de livraison</a> -->
            <a href="{{url('faq')}}" class="btn btn-secondary">Consultez la rubrique 'FAQ'</a>
        </p>
    </div>
</div>



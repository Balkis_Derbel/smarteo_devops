@extends('layouts.default')

@section('title',''.$kit->title.'- SMARTEO - Apprendre en s\'amusant avec des robots')

@section('description',$kit->description)

@section('keywords',$kit->tags)

@section('image', URL::to('img/kits/dash.png'))

@section('content')

<section class="bg-blue" style="margin-top:50px;">

    <div class="container py-5">
        <div class="row text-justify">
            <div class="col-md-6">


                <div class="container my-4 pb-5">

                    <!--Carousel Wrapper-->
                    <div id="carousel-thumb" class="row carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">

                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                         @for ($i = 1; $i <= $kit->nb_images; $i++)
                         <div class="carousel-item {{ $i === 1 ? 'active' : '' }}">
                            <img class="d-block w-100" src="{{URL::to('products/'.$kit->reference.'/img-'.$i.'.jpg')}}" alt="Image-{{$i}}"/>
                        </div>
                        @endfor
                    </div>
                    <!--/.Slides-->

                    <!--Controls-->
                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Précédent</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Suivant</span>
                    </a>
                    <!--/.Controls-->


                    <ol class="carousel-indicators">
                        @for ($i = 1; $i <= $kit->nb_images; $i++)
                        <li data-target="#carousel-thumb" data-slide-to="{{$i-1}}" class="{{ $i === 1 ? 'active' : '' }}"> 
                            <img class="d-block w-100" src="{{URL::to('products/'.$kit->reference.'/img-'.$i.'.jpg')}}"
                            class="img-fluid mt-5">
                        </li>
                        @endfor
                    </ol>
                </div>
                <!--/.Carousel Wrapper-->

            </div>




        </div>

        <div class="col-md-6 text-white">
            <div class="text-center">
                <h1 class="col-md-12 center-block mt-3">
                    {{$kit->title}}
                </h1>
            </div>

            <p class="lead mt-3">
                {{$kit->description}}
            </p>

        </div>
    </div>
</div>
</section>
<section>
    @include('kits/detailed/order',$kit)
</section>

@include('kits/detailed/related_products', $related_products)
@include('kits/detailed/related_contents', $related_contents)

@stop
<div class="section-head mt-3" id="smart-kits">Ces autres produits peuvent aussi vous intéresser</div>

<section class="bg-clouds">
    <div class="container container-fluid py-3">
        <div class="row">
            <div class="col-md-12 text-center d-inline-block">
                <ul class="list">
                    @foreach($related_products as $item)
                    <li>
                        @include('kits/card', $item)
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center explore-btn mt-3">
                <a href="{{url('robots-educatifs')}}" class="button">Voir tous les kits</a>
            </div>
        </div>

    </div>
</section>
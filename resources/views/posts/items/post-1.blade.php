<div class="row" style="width:100%;padding:10px;margin-top:40px;">
    <div class="col-md-12 order-md-1">
        <img src="{{url('img/actualites/entrepreneuriat-ecole.jpg')}}" style="width:100%;border-radius:15px;" alt="elearning expo" />
    </div>
    <div class="col-md-12 order-md-2">
        <h5 style="color:#1289A7;">20/03/2018 au 22/03/2018</h5>
        <h4 style="color:#1289A7;font-weight:bold;margin: 10px 0;">Le Salon de la Formation et du Digital Learning</h4>
        <p class="font-weight-light" style="text-align:justify;">
            Digital learning, mobile learning, social learning, serious games, moocs, gamification, réalité virtuelle, intelligence artificielle et immersive learning …
            Le salon ELearning Expo était l'occasion d'échanger avec les différents acteurs sur les sujets de l'éducation et du digital.
            <br>
            <a href="http://www.e-learning-expo.com/">http://www.e-learning-expo.com/</a>
        </p>
    </div>
</div>

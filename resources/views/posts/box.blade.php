<div class="smart-link news" onclick="location.href='{{url('posts/'.$item['url'])}}'">
    <img src="{{url('img/actualites/'.$item['thumb'])}}" alt="{{$item['reference']}}" />
    <div class='title'>
        <h3>{{$item['title']}}</h3>
    </div>
</div>
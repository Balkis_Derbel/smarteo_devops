@extends('layouts.default')

@section('title','Actualités - '.$data['title'])

@*section('description',$data['description'])*@

@section('keywords',$data['keywords'])

@section('image', URL::to('img/actualites/'.$data['thumb']))

@section('content')

<section class="bg-light text-center">
    <div class="container pt-5 pb-5">

        <div class="row">
            <div class="col-lg-8">
                @include('posts/items/post-1', $data)
            </div>

            <div class="col-lg-4">
                <a href="{{url('actualites')}}" class="nounderline"><h2 class="w-100 bg-info p-3"> Dernières actualités</h2></a>

                @foreach($alldata as $item)
                @include('posts/box', $item)
                @endforeach

            </div>
        </div>
    </div>

</section>

@stop
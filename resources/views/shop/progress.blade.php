<div class="row">
	<div class="col-md-12 text-center">
		<div class="progress" style="height:40px">
			<div class="progress-bar bg-info" role="progressbar" style="width:20%">
				<span style="font-size:16px;">Produit</span>
			</div>
			<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
				<span style="font-size:16px;">Compte</span>
			</div>
			<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
				<span style="font-size:16px;">Adresse</span>
			</div>
			<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
				<span style="font-size:16px;">Paiement</span>
			</div>
			<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
				<span style="font-size:16px;">Confirmation</span>
			</div>					
		</div>
	</div>
</div>

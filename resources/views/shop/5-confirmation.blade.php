@extends('layouts.default')

@section('title','Catalogue de kits robotiques éducatifs - SMARTEO')

@section('css')
<style>
  .progress-bar {height:40px;}
</style>
@stop

@section('content')

<section class="bg-light"  style="margin-top:46px;">

	<div class="container text-center pt-5 pb-5">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="progress" style="height:40px">
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Produit</span>
					</div>
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Compte</span>
					</div>
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Adresse</span>
					</div>
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Paiement</span>
					</div>
					<div class="progress-bar bg-success" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Confirmation</span>
					</div>					
				</div>
			</div>
		</div>

    <div class="row pt-3">
      <div class="col-md-12">
        <h3 class="text-success lead">Merci pour votre commande. Votre paiement est confirmé.</h3>
      </div>
    </div>

    <div class="row pt-3">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <h3 class="p-3 text-white bg-info">Confirmation</h3>
        <h3>Référence : {{$data['reference']}}</h3>
        <p>Date : {{$data['date']}} </p>
        <p>{{$data['name']}}</p>
      </div>
    </div>
    <div class="row pt-3">
      <div class="col-md-2"></div>
      <div class="col-md-4">
        <h2>SMARTEO S.A.S</h2>
        <p>
          10 Avenue de Camberwell, 92330 SCEAUX
        </p>
        <p>service-commercial@smarteo.co</p>
        <p>01.84.19.64.56</p>
      </div>

      <div class="col-md-4">
        <h3>{{$data['name']}}</h3>
        <p>{{$data['address']}}</p>
        <p>{{$data['email']}}</p>
        <p>{{$data['phone']}}</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8 text-center">
        <table class="table table-bordered">
          <thead style="background-color: lightgray;">
            <tr>
              <th>Description</th>
              <th>Prix HT EUR</th>
              <th>Total TTC EUR</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$data['description']}}</td>
              <td>{{$data['price_excl_vat']}}</td>
              <td>{{$data['price']}}</td>
            </tr>
          </tbody>
        </table>

        {!! Form::open(['url' => 'boutique/facture']) !!}
        {{ Form::hidden('key', $data['key']) }}
        {{ Form::hidden('price', $data['price']) }}
        {!! Form::submit('Imprimer', ['class' => 'btn btn-lg btn-success mt-5']) !!}
        {!! Form::close() !!}

      </div>
    </div>

  </div>
</section>

@stop
@extends('layouts.default')

@section('title','Catalogue de kits robotiques éducatifs - SMARTEO')

@section('css')
<style>
	.progress-bar {height:40px;}
</style>
@stop

@section('content')

<section class="bg-light" style="margin-top:46px;">

	<div class="container text-center pt-5 pb-5">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="progress" style="height:40px">
					<div class="progress-bar bg-success" role="progressbar" style="width:20%;">
						<span style="font-size:16px;">Produit</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Compte</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Adresse</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Paiement</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Confirmation</span>
					</div>					
				</div>
			</div>
		</div>


		<div class="row pt-3 mt-3">
			<div class="col-md-2 text-center"></div>
			<div class="col-md-8 text-center">


				<div class="card">
					<div class="card-header">
						<h2 class="text-info text-bold">{{$data['title']}}</h2>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-8">
								<img style="max-height:200px;" src="{{url($data['thumb'])}}"/>
							</div>
							<div class="col-md-4">
								<div class="card p-3">

									@if($data['amount'] != $data['discounted_amount'])
									<span class="display-5 text-danger"><s>{{$data['amount']}} EUR</s></span>
									@endif

									<div class="display-5 text-info text-bold">
										@if($data['subscr_frequency'] == 'none')
										{{$data['discounted_amount']}} &euro;
										@elseif($data['subscr_frequency'] == 'weekly') 
										{{$data['discounted_amount']}} &euro; par semaine
										@endif
									</div>
									<img src="{{url('img/paiement-securise.png')}}" style="width:80%;margin-left:10%;"/>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<p class="card-text lead">{{$data['description']}}.</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row pt-3">
			<div class="col-md-12 text-center">
				{!! Form::open(['url' => 'boutique/achat']) !!}
				{{ Form::hidden('reference', $data['reference']) }}
				{{ Form::hidden('amount', $data['discounted_amount']) }}
				{{ Form::hidden('discount_code', $data['discount_code']) }}				
				{{ Form::hidden('order_type', $data['order_type']) }}
				{!! Form::submit('Suivant', ['class' => 'btn btn-lg btn-success']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</section>

	@stop
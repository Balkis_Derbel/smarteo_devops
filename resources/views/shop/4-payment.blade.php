@extends('layouts.default')

@section('title','Catalogue de kits robotiques éducatifs - SMARTEO')

@section('css')
<style>
   .StripeElement, #card-holder-name
   {
      box-sizing: border-box;
      height: 40px;
      padding: 10px 12px;
      border: 1px solid transparent;
      border-radius: 4px;
      background-color: white;
      box-shadow: 0 1px 3px 0 #e6ebf1;
      -webkit-transition: box-shadow 150ms ease;
      transition: box-shadow 150ms ease;
        margin-bottom:10px;
  }

  .StripeElement--focus {
      box-shadow: 0 1px 3px 0 #cfd7df;
  }

  .StripeElement--invalid {
      border-color: #fa755a;
  }

  .StripeElement--webkit-autofill {
      background-color: #fefde5 !important;
  }

  #card-holder-name{
    color: #31325F;
    line-height: 20px;
    font-weight: 300;
    font-family: "Helvetica Neue", Helvetica, sans-serif;
    font-size: 20px;
}
</style>

<style>
  .progress-bar {height:40px;}
</style>

@stop

@section('scripts')

<!-- Inlude Stripe.js -->
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script>

    var stripe = Stripe("<?php echo env('STRIPE_KEY') ?>");
    var elements = stripe.elements();
    var cardElement = elements.create('card', {
        hidePostalCode: true,
        style: {
            base: {
              iconColor: '#666EE8',
              color: '#31325F',
              lineHeight: '20px',
              fontWeight: 300,
              fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
              fontSize: '20px',
              '::placeholder': {color: '#CFD7E0'},
          }, 
      } 
  });

    cardElement.mount('#card-element');

    const cardHolderName = document.getElementById('card-holder-name');
    const cardButton = document.getElementById('card-button');
    const clientSecret = cardButton.dataset.secret;

    cardButton.addEventListener('click', async (e) => {
        document.getElementById('error_msg').innerHTML = '';
        cardButton.disabled = true;  

        const { setupIntent, error } = await stripe.handleCardSetup(
            clientSecret, cardElement, {
                payment_method_data: {
                    billing_details: { name: cardHolderName.value }
                }
            }
            );

        if (error) {
            document.getElementById('error_msg').innerHTML = 'Une erreur est survenue. Veuillez vérifier votre saisie.';
            cardButton.disabled = false;  
        } else {
            var payment_method = setupIntent.payment_method;

            document.getElementById('payment_method').value = payment_method;
            document.getElementById('validate_payment').style.display='block';

        }
    });

</script>
@stop

@section('content')

<section class="bg-light"  style="margin-top:46px;">

    <div class="container text-center pt-5 pb-5">

        <div class="row">
            <div class="col-md-12 text-center">
                <div class="progress" style="height:40px">
                    <div class="progress-bar bg-info" role="progressbar" style="width:20%;">
                        <span style="font-size:16px;">Produit</span>
                    </div>
                    <div class="progress-bar bg-info" role="progressbar" style="width:20%">
                        <span style="font-size:16px;">Compte</span>
                    </div>
                    <div class="progress-bar bg-info" role="progressbar" style="width:20%">
                        <span style="font-size:16px;">Adresse</span>
                    </div>
                    <div class="progress-bar bg-success" role="progressbar" style="width:20%">
                        <span style="font-size:16px;">Paiement</span>
                    </div>
                    <div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
                        <span style="font-size:16px;">Confirmation</span>
                    </div>                  
                </div>
            </div>
        </div>


        <div class="row pt-3 mt-3">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading" >
                    <div class='panel-title text-center'> 
                        <h1 class="text-info text-bold" >Paiement</h1>
                        <h4 class="text-info text-bold">{{$amount}} &euro;</h4>
                        <div style='display:inline-block;'>                            
                            <img class="img-responsive pull-right" src="{{url('img/paiement-securise.png')}}" style="max-width:150px;">
                        </div>
                    </div>  
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                       <h2 class="text-info text-center w-100 text-bold">
                           Veuillez saisir votre moyen de paiement
                       </h2>

                       <input id="card-holder-name" type="text" class="w-100" placeholder="Titulaire de la carte">
                       <div id="card-element"></div>
                       <button id="card-button" class="btn btn-lg btn-block btn-primary" data-secret="{{ $client_secret }}">
                          Valider
                      </button>
                  </div>

                  <div id="error_msg" class="col-md-12 text-center text-danger">
                        
                  </div>

                  <div class="col-md-12" id="validate_payment" style="display:none;">
                    {!! Form::open(['url' => 'boutique/paiement', 'id' => 'payment-form']) !!}
                    <div class="form-group">
                        {!! Form::submit('Confirmer le paiement', ['class' => 'mt-3 btn btn-lg btn-block btn-success btn-order', 'id' => 'submitBtn', 'style' => 'margin-bottom: 10px;', 'data_secret' => '{{$stripe_intent}}']) !!}
                    </div>
                    {{ Form::hidden('key', $key) }}
                    {{ Form::hidden('payment_method', "", ['id'=>'payment_method']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</section>
@stop
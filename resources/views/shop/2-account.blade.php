@extends('layouts.default')

@section('title','Catalogue de kits robotiques éducatifs - SMARTEO')

@section('content')

<section id="account"  style="margin-top:46px;">

	<div class="bg"></div>
	<div class="bg1"></div>

	<div class="container-fluid text-center pt-5 pb-5">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="progress" style="height:40px">
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Produit</span>
					</div>
					<div class="progress-bar bg-success" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Compte</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Adresse</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Paiement</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Confirmation</span>
					</div>					
				</div>
			</div>
		</div>


		<div class="row pt-3">

			<div class="col-md-8"  style="margin:auto;">
				<div class="account-text">
					<h1> Vous n'êtes pas connecté</h1>
					<div class="row">
						<div class="col-md-12">
							<a href="{{url('login')}}" class="btn btn-lg btn-warning m-0">Créer ou accéder à votre compte</a>
						</div>

					</div>
				</div>
			</div>

		</div>

	</div>
</section>

@stop
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
</head>
<style>
    td {
        border-bottom: 1px solid #ddd;
        margin: 5px;
    }
    body{
        font-family: sans-serif;
    }
</style>
<body>
    <div style="width:100%;">
        <div>
            <div style="width:100%;text-align:center;">
                <h2>SMARTEO SAS</h2>
                <p>10 Avenue de Camberwell, 92330 Sceaux</p>
                <p>service-commercial@smarteo.co</p>
                <p>01.84.19.64.56</p>
            </div>

            <div style="width:100%;text-align: center; padding-top: 130px;">
                <h1>Confirmation de votre commande</h1>
                <p> {{$data['name']}}<br/>
                    {{$data['email']}}<br/>
                </p>
            </div>
        </div>

        <div>
            <table cellspacing="0">
                <thead style="background-color: #eeeeee; border: none;">
                    <tr>
                        <th width="150px" height="35px" style="margin: 5px">Date</th>
                        <th width="400px">Description</th>
                        <th width="150px">Montant</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td height="45px">{{$data['date']}}</td>
                        <td>{{$data['description']}} <br/>
                            {{$data['options']}}
                        </td>
                        <td>
                            {{$data['price']}} EUR TTC
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>           
@extends('layouts.default')

@section('title','Catalogue de kits robotiques éducatifs - SMARTEO')

@section('content')

<section class="bg-light"  style="margin-top:46px;">

	<div class="container text-center pt-5 pb-5">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="progress" style="height:40px">
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Produit</span>
					</div>
					<div class="progress-bar bg-info" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Compte</span>
					</div>
					<div class="progress-bar bg-success" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Adresse</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Paiement</span>
					</div>
					<div class="progress-bar bg-secondary" role="progressbar" style="width:20%">
						<span style="font-size:16px;">Confirmation</span>
					</div>					
				</div>
			</div>
		</div>


		{!! Form::open(['url' => 'boutique/livraison']) !!}
		<div class="row mt-3 pt-3">

			<div class="col-md-1"></div>
			<div class="col-md-10 text-center">
				<h1 class="text-info">
					Veuillez indiquer votre adresse et vos préférences pour la livraison
				</h1>

				<h2 class="bg-info mt-3 p-1">
					Vos coordonnées
				</h2>

				<div class="row">
					<div class="col-md-6">

						<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
							{!! Form::label('firstname', 'Prénom*') !!}
							{!! Form::text('firstname', $user_data['firstname'], [
							'class' => 'form-control name', 'required'=>'required',
							'placeholder' => 'Veuillez entrer votre prénom',
							'data-error' =>"Ce champ est requis."]) !!}
							{!! $errors->first('firstname', '<small class="help-block with-errors">:message</small>') !!}
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
							{!! Form::label('lastname', 'Nom*') !!}
							{!! Form::text('lastname', $user_data['lastname'], [
							'class' => 'form-control name', 'required'=>'required',
							'placeholder' => 'Veuillez entrer votre nom',
							'data-error' =>"Ce champs est requis."]) !!}
							{!! $errors->first('lastname', '<small class="help-block with-errors">:message</small>') !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
							{!! Form::label('email', 'Email*') !!}
							{!! Form::text('email', $user_data['email'], [
							'class' => 'form-control name', 'required'=>'required',
							'style' => 'pointer-events: none;',
							'placeholder' => 'Veuillez entrer votre email',
							'data-error' =>"Ce champs est requis."]) !!}
							{!! $errors->first('email', '<small class="help-block with-errors">:message</small>') !!}
						</div>					
					</div>
					<div class="col-md-6">
						<div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">
							{!! Form::label('phone', 'Numéro de téléphone*') !!}
							{!! Form::text('phone', $user_data['phone'], [
							'class' => 'form-control name', 'required'=>'required',
							'placeholder' => 'Veuillez entrer votre numéro',
							'data-error' =>"Ce champs est requis."]) !!}
							{!! $errors->first('phone', '<small class="help-block with-errors">:message</small>') !!}
						</div>	
					</div>
				</div>

				<h2 class="bg-info mt-3 p-1">
					Adresse
				</h2>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group {!! $errors->has('address') ? 'has-error' : '' !!}">
							{!! Form::label('address', 'Adresse*') !!}
							{!! Form::text('address', $user_data['address'], [
							'class' => 'form-control name', 'required'=>'required',
							'placeholder' => 'Veuillez entrer votre adresse',
							'data-error' =>"Ce champs est requis."]) !!}
							{!! $errors->first('address', '<small class="help-block with-errors">:message</small>') !!}
						</div>	
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group {!! $errors->has('zipcode') ? 'has-error' : '' !!}">
							{!! Form::label('zipcode', 'Code postal*') !!}
							{!! Form::text('zipcode', $user_data['zipcode'], [
							'class' => 'form-control name', 'required'=>'required',
							'placeholder' => 'Veuillez entrer votre code postal',
							'data-error' =>"Ce champs est requis."]) !!}
							{!! $errors->first('zipcode', '<small class="help-block with-errors">:message</small>') !!}
						</div>	
					</div>
					<div class="col-md-6">
						<div class="form-group {!! $errors->has('city') ? 'has-error' : '' !!}">
							{!! Form::label('city', 'Ville*') !!}
							{!! Form::text('city', $user_data['city'], [
							'class' => 'form-control name', 'required'=>'required',
							'placeholder' => 'Veuillez entrer votre ville',
							'data-error' =>"Ce champs est requis."]) !!}
							{!! $errors->first('city', '<small class="help-block with-errors">:message</small>') !!}
						</div>	
					</div>
				</div>
<!--
				<h2 class="bg-info mt-3 p-1">
					Préférences de livraison
				</h2>

				<div class="row">
					<div class="col-md-6">
						<input type="radio" id="dewey" name="delivery" value="dewey">
						<label for="dewey">Par colissimo à votre adresse</label>
						<div>
							<table style="width:100%;">
								<tr>
									<td>
										<div class="text-info text-bold">Participation aux frais de port</div>
										<div class="display-4 text-info text-bold">4.5 &euro;</div>
									</td>
								</tr>
							</table>

						</div>
					</div>

					<div class="col-md-6">
						<input type="radio" id="louie" name="delivery" value="louie">
						<label for="louie">Relais Colis</label>

						<div class="text-success text-bold">Veuillez choisir votre lieu de retrait</div>

						<div>

							<script type="text/javascript" src="{{url('js/widget-rc.js')}}"></script>
							<div id="relais-colis-widget-container" style="margin-left:45%;"></div>
							<script type="text/javascript">
								callback = function (data) {
									console.log('data here', data)
									document.getElementById('chosenAdr').innerHTML = ' Vous avez choisi le relais : </br>' +
									'<span style="font-weight:bold;">' + data.id + ' - ' + data.name +
									'</br> Adresse : ' + data.location.formattedAddressLine + ' ' + data.location.formattedCityLine;
								}
								$('#relais-colis-widget-container').ready = generateHtmlButton(callback);
							</script>

							<table style="width:100%;">
								<tr>
									<td><span style=" width:100%;text-align:center;" class="text-success font-weight-bold">Livraison gratuite en Relais Colis &#174;</span>
									</td>
								</tr>
								<tr>
									<td>
										<div id="chosenAdr" class="text-white bg-success"> </div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
-->
				<div class="row pt-3">
					<div class="col-md-12 text-center">
						{{ Form::hidden('key', $key) }}
						{!! Form::submit('Suivant', ['class' => 'btn btn-lg btn-success']) !!}
					</div>
				</div>

				<div class="row pt-2">
					<div class="col-md-12 text-center">
						<input type="checkbox" id="scales" name="scales"
						checked>
						<label for="scales" class="lead">J'ai lu et j'accepte <a href="{{url('cgv')}}" class="text-info">
						les conditions générales de vente</a>
					</label>

					{!! $errors->first('cgv', '<small class="help-block with-errors">:message</small>') !!}
				</div>
			</div>
		</div>
		{!! Form::close() !!}
	</div>


</section>

@stop
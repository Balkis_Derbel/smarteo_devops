@extends('layouts.default')

@section('title','Mes clés d\'accès - SMARTEO - Apprendre en s\'amusant avec des robots')

@section('content')


<section class="bg-light">

    @include('includes.masthead', ['which' => "order", 'title' =>"Mes accès aux contenus"])

    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col-md-12">


                <div class="widget-box">
                    <div class="widget-header widget-header-flat text-center">
                        <h3>
                            Activer mes clès d'accès
                        </h3>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main pt-2 pb-2">

                            {!! Form::open(['url' => 'activation-acces-contenus']) !!}
                            <div class="row">
                                <div class="col-md-6 p-2">
                                    {!! Form::text('key', null, ['class' => 'form-control w-100 h-100 text-center', 'placeholder' => 'Clé d\'activation']) !!}
                                </div>
                                <div class="col-md-6 p-2">
                                    {!! Form::submit('Activer !', ['class' => 'btn btn-info w-100 h-100']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-12">
                <table class="table table-bordered table-sm">
                    <thead>
                        <th>Intitulé</th>
                        <th>Clé</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Date d'activation</th>
                    </thead>
                    @foreach($data as $row)
                    <tr>
                        <td>{{$row->title}}</td>
                        <td>{{$row->value}}</td>
                        <td>{{date('d-M-Y', strtotime($row->startdate))}}</td>
                        <td>{{date('d-M-Y', strtotime($row->maxdate))}}</td>
                        <td>{{date('d-M-Y', strtotime($row->created_at))}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center pt-4">
                <a href="{{url('contenus-et-tutoriels')}}" class="btn btn-success mt-3 mb-3">Accéder à mes contenus et tutoriels</a>
                <a href="{{url('mon-espace-personnel')}}" class="btn btn-info"> Retourner à mon espace personnel</a>
            </div>
        </div>
    </div>

</section>
@endsection
@extends('layouts.default')

@section('script')
<!-- Event snippet for Inscription sur le site web SMARTEO conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-998479216/Qv1zCOmOgJMBEPCqjtwD',
      'value': 1.0,
      'currency': 'EUR'
  });
</script>

@endsection

@section('content')

<section class="bg-light">

    @include('includes.masthead', ['which' => "user-home", 'title' =>"Bienvenue dans votre espace personnel"])

    <div class="container pt-5 pd-5">

        <div class="row text-center" style="display:run-in;">
            <div class="col-md-6">
                <div class="button-img img-6  pt-4 pb-4" style="border:1px solid #eaeaea;">
                    <h1 class="text-white">Mes données</h1>
                    <a href="{{url('mes-donnees')}}" class="display-6 btn btn-lg btn-success p-3 m-3 rounded">Mettre à jour mes données</a>
                </div>

                <div class="button-img img-6  pt-4 pb-4" style="border:1px solid #eaeaea;">
                    <h1 class="text-white">Mes avantages</h1>
                    <div><a href="{{url('mes-avantages')}}" class="display-6 btn btn-lg btn-success p-3 m-3 rounded">Voir mes avantages</a>  </div>
                </div>

            </div>
            
            <div class="col-md-6">
                <div class="button-img img-6  pt-4 pb-4" style="border:1px solid #eaeaea;">
                    <h1 class="text-white">Mes commandes</h1>
                    <div><a href="{{url('mes-commandes')}}" class="display-6 btn btn-lg btn-danger p-3 m-3 rounded">Voir mes commandes</a>  </div>

                    <div><a href="{{url('robots-educatifs')}}" class="display-6 btn btn-lg btn-success p-3 m-3 rounded"> Explorer les kits et réserver<br> mon activité</a></div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12"  style="text-align:center;margin:50px 0;">

                @if(session('impersonated_by') )
                <a href="{{ url('impersonate_leave') }}" class="display-6 btn btn-lg btn-info p-3 m-3 rounded" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                    Revenir à mon compte
                </a>
                @endif
                
                <a href="{{ route('logout') }}" class="display-6 btn btn-lg btn-info p-3 m-3 rounded" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                    Quitter mon espace personnel
                </a>
                
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>


    </div>  
</section>
@endsection

@extends('layouts.default')

@section('title','Mes avantages')

@section('content')

<section class="bg-light">

    @include('includes.masthead', ['which' => "benefits", 'title' =>"Mes Avantages"])

    <div class="container pt-5 pb-5"  id="account">
        <div class="row">
            <div class="col-md-12">

                <?php if($partner_discount != 0 && $partner_discount != 100) { ?>
                    <div class="card">
                        <div class="card-header text-white bg-success text-center">
                            <h4>Vous bénéficiez d'une réduction de {{$partner_discount}}% sur toutes vos commandes d'achat grâce à votre code '{{$partner_code}}'</h4>
                        </div>
                    </div>

                    <div class="card mt-3">
                        <div class="card-header text-white bg-success text-center">
                            <h4>Vous bénéficiez d'une réduction de {{$partner_discount * 2}}% sur tous vos abonnements grâce à votre code '{{$partner_code}}'</h4>
                        </div>
                    </div>

                <?php } ?>

                <?php if($partner_discount == 100) { ?>
                    <div class="card">
                        <div class="card-header text-white bg-success text-center">
                            <h4>Vous bénéficiez d'un mois gratuit sur vos abonnements grâce à votre code  '{{$partner_code}}'</h4>
                        </div>
                    </div>

                <?php } ?>

                <div class="card mt-3">
                    <div class="card-header text-white bg-info text-center">
                        <h4>Bénéficiez de réductions exclusives grâçe à vos codes de réduction</h4>
                    </div>

                    <div class="card-body text-center">

                        <p class="lead">Vous avez un code de réduction partenaire ? Veuillez le renseigner ici :</p>


                        {!! Form::open(['url' => 'avantage-code-partenaire', 'style'=>'margin:0;']) !!}
                        <div class="form-group">
                            {!! Form::text('partner_code',null, ['class' => 'form-control', 'placeholder' => 'Votre Code Partenaire']) !!}
                        </div>

                        {!! Form::submit('Valider', ['class' => 'btn btn-success']) !!}
                        {!! Form::close() !!}

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center pt-4">
                <a href="{{url('mon-espace-personnel')}}" class="btn btn-info"> Retourner à mon espace personnel</a>
            </div>
        </div>
    </div>

</section>



@stop
@extends('layouts.default')

@section('title','Mes données')

@section('content')

<section class="bg-light">

    @include('includes.masthead', ['which' => "userdata", 'title' =>"Mes Données"])

    <div class="container py-5"  id="account">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Veuillez renseigner ou mettre à jour vos données: </h1>
            </div>
        </div>

        {!! Form::open(['url' => 'mes-donnees']) !!}
        <div class="row form">
            <div class="box">

                <div class="form-group {!! $errors->has('firstname') ? 'has-error' : '' !!}">

                    {!! Form::text('firstname', $data['firstname'], ['class' => 'form-control name', 'placeholder' => 'Votre Prénom']) !!}
                    {!! $errors->first('firstname', '<small class="help-block">:message</small>') !!}

                </div>

                <div class="form-group {!! $errors->has('lastname') ? 'has-error' : '' !!}">

                    {!! Form::text('lastname', $data['lastname'], ['class' => 'form-control name', 'placeholder' => 'Votre Nom']) !!}
                    {!! $errors->first('lastname', '<small class="help-block">:message</small>') !!}

                </div>


                <div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">

                    {!! Form::text('phone', $data['phone'], ['class' => 'form-control phone', 'placeholder' => 'Votre Numéro']) !!}
                    {!! $errors->first('phone', '<small class="help-block">:message</small>') !!}

                </div>


                <div class="form-group {!! $errors->has('address') ? 'has-error' : '' !!}">

                    {!! Form::text('address', $data['address'], ['class' => 'form-control mail', 'placeholder' => 'Votre Adresse']) !!}
                    {!! $errors->first('address', '<small class="help-block">:message</small>') !!}

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group {!! $errors->has('city') ? 'has-error' : '' !!}">

                            {!! Form::text('city', $data['city'], ['class' => 'form-control mail', 'placeholder' => 'Ville']) !!}
                            {!! $errors->first('city', '<small class="help-block">:message</small>') !!}

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {!! $errors->has('zipcode') ? 'has-error' : '' !!}">

                            {!! Form::text('zipcode', $data['zipcode'], ['class' => 'form-control mail', 'placeholder' => 'Code Postal']) !!}
                            {!! $errors->first('zipcode', '<small class="help-block">:message</small>') !!}

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group {!! $errors->has('country') ? 'has-error' : '' !!}">

                            {!! Form::text('country', null, ['class' => 'form-control mail', 'disabled' => 'true', 'placeholder' => 'France']) !!}
                            {!! $errors->first('country', '<small class="help-block">:message</small>') !!}

                        </div>
                    </div>
                </div>

                <?php /*
                <!--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <input id="password" type="password" class="form-control password" name="password" placeholder="Mot de passe" required>

                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif

                </div>

                <div class="form-group">
            
                    <input id="password-confirm" type="password" class="form-control password" name="password_confirmation" placeholder="Confirmation" required>
            
                </div>-->
                */?>
                <div class="row pt-4">
                    <div class="col-md-12 text-center">
                        {!! Form::submit('Enregistrer', ['class' => 'btn btn-info']) !!}
                        <a href="{{url('mon-espace-personnel')}}" class="btn btn-success">Retourner à mon espace personnel sans enregistrer</a>
                    </div>
                </div>
            </div>

        </div>
        {!! Form::close() !!}

    </div>

</section>



@stop
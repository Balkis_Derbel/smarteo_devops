@extends('layouts.default')

@section('title','Mes commandes - SMARTEO - Les kits robotiques pour apprendre à l\'ère du digital')

@section('content')


<section class="bg-light">

    @include('includes.masthead', ['which' => "order", 'title' =>"Mes Commandes"])

    <div class="container pt-5 pb-5">

        <div class="row">
            <div class="col-md-12">
                <?php
                if(empty($data)){
                    echo "<h2>Vous n'avez aucune commande active</h2>";
                }
                ?>
                <table class="table table-bordered table-sm">
                    <thead>
                        <th>Date</th>
                        <th>Nom</th>
                        <th>Prix*</th>
                        <th>Statut</th>
                    </thead>
                    @foreach($data as $row)
                    <tr>
                        <td>
                            <!--<a href="{{url('commandes/'.$row['date'])}}" data-toggle='tooltip' data-placement='top' class='icon-style pr-2'><i class='fa fa-plus-circle fa-lg text-info'></i></a>-->
                            {{date('d-M-Y', strtotime($row['date']))}} 
                        </td>
                        <td><a href="{{url('robots-educatifs/'.$row['product_slug'])}}" class="text-info">{{$row['title']}}</a></td>
                        <td>{{$row['price']}} EUR</td>
                        <td>
                            <span class="badge badge-success">{{$row['status']}}</span>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <p><em>* Pour les abonnements, le prix est indiqué par période d'utilisation.</em></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center pt-4">
                <a href="{{url('robots-educatifs')}}" class="btn btn-success mt-2"> Explorer les robots éducatifs et réserver une nouvelle activité</a>
                <a href="{{url('mon-espace-personnel')}}" class="btn btn-info mt-2"> Retourner à mon espace personnel</a>
                <!--<a href="{{url('mes-paiements')}}" class="btn btn-danger mt-2"> Consulter mes paiements</a>-->
            </div>
        </div>
    </div>

</section>
@stop
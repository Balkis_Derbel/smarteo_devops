@extends('layouts.default')

@section('title','Communauté - SMARTEO')

@section('content')
<div id="container">    
    <div class="row" id="bandereau-mbot">
        <div class="col-md-12">
            <h1>{{$data['title']}}</h1>
        </div>
    </div>
</div>

<div class="container">
    <?php
    foreach($rows as $row){ ?>

        <div class="row">
            <div class="col-md-12">

                <?php if($row['type']=='html'){ ?>
                    <p class='lead'>
                        <?php print_r($row['value']); ?>
                    </p>
                <?php } ?>

                <?php if($row['type']=='list_contents'){ ?>

                    <ul class="list w-100 text-center">
                        @foreach($row['value'] as $c)
                        <li>
                            @if($c != NULL)
                                @include('contents/item',['item'=>$c])
                            @endif
                        </li>
                        @endforeach
                    </ul>

                <?php } ?>

                <?php if($row['type']=='form_contact'){ ?>
                    @include('contact/contact')
                <?php } ?>
            </div>
        </div>

    <?php } ?>
</div>
@stop
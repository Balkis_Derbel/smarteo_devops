@extends('layouts.default')

@section('content')

<section class="contact" id="contact" style="padding-top:48px">
  <div class="bg"><div class="bg1"></div></div>

  <div class="container-fluid py-5">
    <div class="row no-gutters">

      <div class="col-md-2"></div>
      <div class="col-md-8 col-md-offset-2 pt-3">
        <div class="contact-text-1 gradient-background ">
          <h2> Vous ne trouvez pas toutes les réponses à vos questions ?</h2>
          <p class="lead"> Faites le nous savoir et nous serons ravis de vous apporter les réponses :) </p>
        </div>

        {!! Form::open(['url' => 'contactForm']) !!}
        <div class="row form" style="margin: 2rem 3rem;">
          <div class="box">

            <div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">

              {!! Form::text('nom', null, ['class' => 'form-control name', 'placeholder' => 'Votre nom']) !!}
              {!! $errors->first('nom', '<small class="help-block">:message</small>') !!}

            </div>

            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">

              {!! Form::email('email', null, ['class' => 'form-control mail', 'placeholder' => 'Votre email']) !!}
              {!! $errors->first('email', '<small class="help-block">:message</small>') !!}

            </div>

            <div class="form-group {!! $errors->has('texte') ? 'has-error' : '' !!}">

              {!! Form::textarea ('texte', null, ['class' => 'form-control', 'placeholder' => 'Votre message']) !!}
              {!! $errors->first('texte', '<small class="help-block">:message</small>') !!}

            </div>

            <div class="col-md-12 text-center">
              <div class="form-group d-inline-block {!! $errors->has('g-recaptcha-response') ? 'has-error' : '' !!}" >
                {!! NoCaptcha::display() !!}

                @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                  <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
                @endif
              </div>
            </div>

            <div class="col-md-12 text-center">
              {{ Form::hidden('route', '/home') }}
              {!! Form::submit('Envoyer !', ['class' => 'btn btn-info']) !!}
            </div>

          </div>
        </div>
        {!! Form::close() !!}

      </div>

      <div class="col-md-12 text-center">
            <div class="explore-btn">
                <a href="{{url('communaute')}}" class="button btn-warning font-weight-bold">RETOUR</a>
            </div>
      </div>
    </div>
  </div>

</section>

@stop
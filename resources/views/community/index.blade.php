@extends('layouts.default')

@section('title','Communauté - SMARTEO')

@section('scripts')

<script type="text/javascript">

	$(function() {
		$('body').on('click', '.pagination a', function(e) {
			e.preventDefault();

			$('#load a').css('color', '#dfecf6');

			var url = '{{url('communaute')}}' + $(this).attr('href');  
			getSubjects(url);
			window.history.pushState("", "", url);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});

		$(".search_button").click(function(e) {
			e.preventDefault();
			var url = '{{url('communaute')}}';
			getSubjects(url);
			window.history.pushState("", "", url);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});

		function getSubjects(url) {
			var name = $('#search_box').val();

			$.ajax({
				url 	: url,
				data 	: {
					search : name
				}
			}).done(function (data) {
				$('#subjects_list').empty().html(data);  
			}).fail(function () {
				alert('Une erreur est survenue ...');
			});
		}
	});

</script>

@stop

@section('content')

<!-- Subjects -->

@include('community/subjects-header')


<section class="bg-clouds">

	<div class="container container-fluid pt-5 pb-5">

		<div class="row">
			<div class="col-md-12">
				<form method="get" action="" autocomplete="on" class="search_box">
					<input type="text" id="search_box" class="from-control" placeholder=""/>
					<input type="submit" class="search_button" style="z-index:5;"/>
				</form>
			</div>
		</div>

		<div class="row text-center d-inline-block w-100" id="subjects_list">
			@include('community/subjects-list')
		</div>
	</div>

</section>

@stop
<div class="card card-subject" onclick="location.href='{{url('communaute/'.$item->slug)}}'">
	<div class="row no-gutters" style="display:table;">
		<div  style="display:table-cell;vertical-align:middle;float:none;width:25%;">
			<img src="{{URL::to('contents/thumbs/'.$item->thumb)}}" class="w-100" alt="{{$item->title}}">
		</div>
		<div  style="display:table-cell;vertical-align:middle;float:none;width:75%;">
			<div class="w-100 p-2">
				<h5 class="card-title">{{$item->title}}</h5>
				<div class="w-100 text-center">
					<?php
					$tags = explode(",", $item->tags);
					foreach($tags as $t) {?>
						<span class="badge badge-success"><?=$t?></span>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
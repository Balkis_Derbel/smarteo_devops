<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['logger','optimize']], function () {

	Route::get('/', 					'HomeController@index');

	Route::get('parents', 				'StaticController@parents');
	Route::get('enseignant', 			'StaticController@enseignants');
	Route::get('mentions-legales', 		'StaticController@mentions_legales');
	Route::get('cgv',			 		'StaticController@cgv');
	Route::get('faq', 					'StaticController@faq');
	Route::get('nos-engagements', 		'StaticController@engagements');
	Route::get('qui-sommes-nous', 		'StaticController@whoweare');
	Route::get('contact', 				'StaticController@contact');
	/*Route::get('options', 				'StaticController@options');	*/

	Route::get('surveys', 				'StaticController@surveys');
	Route::get('surveys/parents', 		'StaticController@surveys');
	Route::get('surveys/enseignant', 	'StaticController@surveys');

	Route::get('newsletter', 			'StaticController@newsletter');
	Route::get('newsletter/{title}', 	'StaticController@newsletter');

	Route::get('actualites', 			'NewsController@index');
	Route::get ('actualites/{item}', 	'NewsController@index' );

	Route::get('blog', 					'BlogController@index');
	Route::get('blog/{item}', 			'BlogController@post' );

	Route::get('robots-educatifs',		'KitsController@index');
	Route::get('kits-educatifs',		'KitsController@index');
	Route::get('robots-educatifs/{label}','KitsController@details');

	/* old links will be deleted once obsolete */
	Route::get('kits',			 		'KitsController@index');
	Route::get('kits/{label}', 			'KitsController@details');

	Route::get('contactForm', 			'ContactController@getForm');
	Route::post('contactForm', 			'ContactController@postForm');

	/*Route::get('communaute',			'PostsController@index');*/
	Route::get('posts/{title}',			'PostsController@index');


	Auth::routes();


	Route::get('contenus-et-tutoriels',				'ContentsController@index');
	Route::get('contenus-et-tutoriels/{content}', 	'ContentsController@content');
	Route::get('contenus-et-tutoriels/{content}/{section}', 'ContentsController@content');

	Route::get('communaute',				'CommunityController@index');
	Route::get('communaute/nouveau-sujet',	'CommunityController@create');
	Route::get('communaute/{subject}', 		'CommunityController@subject');

	Route::get('ressources/exemples-code-mblock', 'StaticController@ressources_mbot');

	Route::get('contentFeedbackForm', 	'ContentsController@getFeedbackForm');
	Route::post('contentFeedbackForm', 	'ContentsController@postFeedbackForm');

	//social login routes
	Route::get ( '/redirect/{service}', 'Auth\AuthController@redirectToProvider' );
	Route::get ( '/callback/{service}', 'Auth\AuthController@handleProviderCallback' );

	// Admin Routes
	Route::get ('impersonate_leave', 			'Auth\AuthController@impersonate_leave' );

	Route::get('animateurs',			'StaticController@animateurs');
	Route::get('grand-jeu-makers',		'StaticController@grand_jeu_makers');

	Route::get('contact/educatec',		'ContactController@contact_educatec');
	Route::get('contact/terroir',		'ContactController@contact_salon_terroir');

	Route::get('jeu-concours',			'GiveawayController@getGiveaway');
	Route::get('jeu-concours/{token}',	'GiveawayController@getGiveaway');	
	Route::post('jeu-concours',			'GiveawayController@postGiveaway');	
	Route::get('jeu-concours/augmenter-vos-chances/{token}', 'GiveawayController@referral');

	/*WLRC Challenge Smarteo */
	Route::get('challenge-wlrc-smarteo-dash-journey-through-time', 					'DjttController@index');
	Route::get('challenge-wlrc-smarteo-wonderful-journey-through-time', 			'DjttController@index');

	Route::get('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps', 							'DjttController@index');
	Route::post('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps', 							'DjttController@submitParticipation');
	Route::get('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/conditions-generales', 		'DjttController@cg');
	Route::get('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/en-savoir-plus', 				'DjttController@know_more');
	Route::post('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/en-savoir-plus', 			'DjttController@receiveDocumentation');
    Route::get('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/documentation',				'DjttController@getDownloadDocumentation');
    Route::get('challenge-wlrc-smarteo-un-magnifique-voyage-dans-le-temps/equipe-gagnante', 				'DjttController@winners');    
    Route::get('make-wonder-apps', 	'StaticController@makewonder_apps');    

});

/* ------------------------- *
** User Dashboard Routes 	 *
** ------------------------- */
Route::group([
    'middleware' => ['logger','optimize'],
], function () {

	Route::get('mon-espace-personnel', 		'UserDashboardController@index')->name('home');
	Route::get('mes-donnees', 				'UserDashboardController@getUserData');
	Route::post('mes-donnees', 				'UserDashboardController@setUserData');
	Route::get('mes-avantages', 			'UserDashboardController@getBenefits');
	Route::post('avantage-code-partenaire', 'UserDashboardController@postPartnerCode');	
	Route::get('activation-acces-contenus',	'UserDashboardController@getContentAccessKeys');
	Route::post('activation-acces-contenus','UserDashboardController@postContentAccessKeys');
	Route::get('mes-commandes', 			'ShopController@getOrders');
	Route::get('mes-paiements', 			'ShopController@getPayments');
});

/* ------------------------- *
** Shop routes 			 *
** ------------------------- */
Route::group([
        'prefix' => 'boutique',
    'middleware' => ['logger','optimize'],
], function () {
    Route::get('achat/{label}', 		'ShopController@GetBuyProduct');
    Route::get('souscription/{label}', 	'ShopController@GetSubscribeProduct');

    Route::post('achat', 				'ShopController@PostMakeOrder');
    Route::post('compte', 				'ShopController@PostAccount');    
	Route::post('livraison', 			'ShopController@PostAddress'); 
    Route::post('paiement', 			'ShopController@PostPayment'); 
	Route::post('facture', 				'ShopController@PostConfirmation'); 

    //
	Route::get('livraison/{key}', 		'ShopController@GetAddress');
    Route::get('paiement/{key}',		'ShopController@GetPayment');  
    Route::get('confirmation/{key}', 	'ShopController@GetConfirmation'); 
});

/* ------------------------- *
** Admin routes 			 *
** ------------------------- */
Route::group([
        'prefix' => 'admin',
    'middleware' => ['logger','optimize', 'admin'],
], function () {

	Route::get('', 'Admin\AdminController@index');	

	/* BACKUP */
    Route::get('backup', 			'Admin\BackupController@index');
    Route::post('backup/create', 	'Admin\BackupController@create');
    Route::post('backup/export_db', 'Admin\BackupController@export_db');
    Route::get('backup/delete/{file_name?}', 'Admin\BackupController@delete')
    			->where('file_name', '(.*)');

    /* SHOP */
    /* PRODUCTS */
    Route::get('products', 			'Admin\ShopController@getProducts');
	Route::get('products/{id}', 	'Admin\ShopController@getProducts');		
	Route::post('products', 		'Admin\ShopController@postProducts');

	/* DISCOUNTS */
	Route::get('discounts',		'Admin\DiscountController@getPartnerDiscounts');
	Route::get('discounts/{id}','Admin\DiscountController@getPartnerDiscounts');
	Route::post('discounts', 	'Admin\DiscountController@postPartnerDiscount');

	/* COUPONS */
	Route::get('coupons', 	 	'Admin\AdminController@getCoupons');
	Route::get('coupons/{id}', 	'Admin\AdminController@getCoupons');	
	Route::post('coupons', 	 	'Admin\AdminController@postCoupons');

	/* ORDERS */
	Route::get('orders', 	 	'Admin\ShopController@getOrders');
	Route::get('orders/{id}', 	'Admin\ShopController@getOrders');	
	Route::post('orders', 	 	'Admin\ShopController@postOrders');

	/* PAYMENTS */
	Route::get('payments', 	 	'Admin\ShopController@getPayments');
	Route::get('payments/{id}', 'Admin\ShopController@getPayments');	
	Route::post('payments', 	'Admin\ShopController@postPayments');

	/* PARCEL-TRACKING */
	Route::get('parcel-tracking', 	 	'Admin\ShopController@getParcelTracking');
	Route::get('parcel-tracking/{id}', 	'Admin\ShopController@getParcelTracking');	
	Route::post('parcel-tracking', 		'Admin\ShopController@postParcelTracking');
	/* COLISSIMO TRACKING */
	Route::get('colissimo_tracking/{id}', 	'Admin\ShopController@getColissimoTracking');

	/* NEWS */
	Route::get('news', 		'Admin\AdminController@getNews');
	Route::get('news/{id}', 'Admin\AdminController@getNews');		
	Route::post('news', 	'Admin\AdminController@postNews');	

	/* CONTENTS */
	Route::get('contents', 				'Admin\ContentsController@getContents');
	Route::get('contents/view/{id}', 	'Admin\ContentsController@viewContent');

	Route::get('contents/crawle_url',       'Admin\ContentsController@crawle_url');

	Route::get('contents/update',		'Admin\ContentsController@getUpdate');
	Route::get('contents/update/{id}', 	'Admin\ContentsController@getUpdate');
	Route::post('contents/update',	 	'Admin\ContentsController@postUpdate');
	Route::get('contents/delete/{id}', 	'Admin\ContentsController@getDelete');
	Route::post('contents/delete/{id}', 'Admin\ContentsController@postDelete');

	/* SUBJECTS */
	Route::get('subjects', 		'Admin\SubjectsController@getSubjects');
	Route::get('subjects/{id}', 'Admin\SubjectsController@getSubjects');		
	Route::post('subjects', 	'Admin\SubjectsController@postSubjects');


	/* QUERIES */
	Route::get('queries',  	'Admin\AdminController@getQueries');
	Route::post('queries', 	'Admin\AdminController@postQueries');
	Route::get('weekly', 	'Admin\AdminController@getWeeklyReport');

	/* SHOWDATA */
	Route::get('showdata', 	 			'Admin\AdminController@showData');
	Route::get('showdata/{TableName}', 	'Admin\AdminController@showData');

	/* USERS */
	Route::get('users', 				'Admin\UsersController@getUsers');
	Route::get('send_email', 			'Admin\AdminController@send_email');
	Route::get('users/logs/{id}', 		'Admin\UsersController@getLogs');
	Route::get('users/mail/{id}', 		'Admin\UsersController@getEmail');
	Route::post('users/mail_order_sent','Admin\UsersController@postEmail');

	Route::get ('impersonate/{user_id}', 	'Admin\UsersController@impersonate')->middleware('admin');

	/* LOGS */
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

	/* OTHER ROUTES */
	Route::get('giveaway', 	'Admin\UsersController@getGiveAway');
	Route::get('djtt', 		'Admin\UsersController@getDjtt');

	/* DASHBOARD */
	Route::get('dashboard/orders', 'Admin\DashboardController@getOrders');

	/* CHEAT SHEET */
	Route::get('cheat-sheet', 	 	'Admin\AdminController@cheatSheet');
});


/* ------------------------- *
** Sitemap 			 *
** ------------------------- */
Route::get('sitemap', 		'SitemapController@index');
Route::get('sitemap.xml', 	'SitemapController@index');
<?php

namespace App;
use Illuminate\Support\Facades\View;
use Dompdf\Dompdf;
use App\Models\Order;

class Invoice
{
	protected $pdf;

	public function __construct() {
		$this->pdf = new Dompdf;
	}

	public function generate0() {
		$this->pdf->loadHtml(
			View::make('shop\invoice')->render()
		);
		$this->pdf->render();
		return $this->pdf->output();
	}

	public function generate($data) {
		$this->pdf->loadHtml(
			View::make('shop.invoice')->with('data',$data)->render()
		);
		$this->pdf->render();
		$this->pdf->stream('invoice.pdf', ['Attachment' => false]);
	}
}
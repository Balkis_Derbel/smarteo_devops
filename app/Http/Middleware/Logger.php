<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class Logger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_name = '';
        $user_id = 0;

        $user = $request->user();
        if($user != null)
        {
            $user_name = $user->name;
            $user_id =   $user->id;
        }
        
        $request_uri    = $request->ip();
        $remote_addr    = $request->fullUrl();
        $request_method = $request->method();
        $user_agent     = $request->header('User-Agent');
        $comment        = "";
        $is_bot = false;

        if(strpos($remote_addr,"callback") != false)
        {
            $x = explode('?',$remote_addr);
            $remote_addr = $x[0];
        }

        if(isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT']))
        {
            $is_bot = true;
        }

        // Save in DB           
        DB::insert('insert into logs (request_uri, remote_addr, request_method, user_agent, is_bot,user_name,user_id,comment) values (?, ?, ?, ?, ?, ?, ?, ?)', [$request_uri, $remote_addr, $request_method, $user_agent, $is_bot, $user_name, $user_id, $comment]);

        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests\OrderDeliveryFormRequest;
use App\Helpers\ToolboxHelper;

use App\Services\ShopService;
use App\Services\ProductsService;
use App\Services\UserDataService;

use Auth;
use App\User;
use App\Models\Order;
use Session;
use Mail;

class ShopController extends Controller
{
	protected $shopService;
	protected $userDataService;
	protected $productsService;

	public function __construct(
		ShopService $shopService,
		UserDataService $userDataService,
		ProductsService $productsService)
	{
		$this->shopService = $shopService;
		$this->userDataService = $userDataService;	
		$this->productsService = $productsService;				
	}

	/* ---------------------------------------
	 *	Get BuyProduct
	 * --------------------------------------- */
	public function GetBuyProduct($slug)
	{
		try
		{
			ToolboxHelper::LogMessage('Debug',__METHOD__, __LINE__,'','
			{
				Slug = '.$slug.'
			}');

			$product = $this->productsService->GetProductBySlug($slug);
			if($product == null) 
			{
				ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,'Product was not found','
				{
					Slug = '.$slug.'
				}');
				flash('Une erreur est survenue !')->error()->important();
				return back();
			}

			$discounts = $this->userDataService->GetUserDiscount();
			$discount = $discounts['products'];
			$discount_code = '';
			if($discount != 0) $discount_code = $discount.'_pct_off';
			if($discount == 100) $discount_code = $discount.'_pct_off_1month';

			$data = array(
				'title'					=> $product->title,
				'thumb' 				=> 'products/'.$product->reference.'/'.$product->thumb,
				'reference'				=> $product->reference,
				'description' 			=> $product->description,
				'amount' 				=> $product->selling_price,
				'discount_code'			=> $discount_code,
				'discounted_amount'		=> round($product->selling_price * (1-  $discount/100),2),
				'order_type'			=> 'pay_once',				
				'subscr_frequency' 		=> 'none',				
			);

			return view('shop/1-product')->with('data',$data);
		}
		catch(Exception $e)
		{
			ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,$e->getMessage(),'
			{
				Slug = '.$slug.'
			}');
			flash('Une erreur est survenue !')->error()->important();
			return back();
		}
	}

	/* ---------------------------------------
	 *	Get SubscribeProduct
	 * --------------------------------------- */
	public function GetSubscribeProduct($slug)
	{
		$product = $this->productsService->GetProductBySlug($slug);
		if($product == null)
		{
			flash('Une erreur est survenue !')->error()->important();
			return back();
		}

		$discounts 	= $this->userDataService->GetUserDiscount();
		$discount 	= $discounts['subscriptions'];
		$discount_code = $discounts['plan'];

		$data = array(
			'title'					=> $product->title,
			'thumb' 				=> 'products/'.$product->reference.'/'.$product->thumb,
			'reference'				=> $product->reference,
			'description' 			=> $product->description,
			'amount' 				=> $product->subscr_amount,
			'discount_code'			=> $discount_code,
			'discounted_amount'		=> round($product->subscr_amount * (1- $discount/100),2),
			'order_type'			=> 'subscribe',
			'subscr_frequency'		=> 'weekly',							
		);

		return view('shop/1-product')->with('data',$data)->with('pricing_plan',"subscription");
	}

	/* ---------------------------------------
	 *	Post BuyProduct
	 * --------------------------------------- */
	public function PostMakeOrder(Request $request)
	{
		//create entry order
		$key = md5(microtime().rand());
		$user_id = Auth::id();

		DB::insert('insert into orders (
			user_id,
			product_reference,
			amount,
			discount_code,
			order_type,
			request_uri,
			unique_key,
			status) 
			values(?,?,?,?,?,?,?,?)',
			[ 
				$user_id, 
				$request->get('reference'), 
				$request->get('amount') * 100,
				$request->get('discount_code'),
				$request->order_type,
				$request->ip(),
				$key,
				'created'
			]);

		//if user is loggedin --> go to Address
		if($user_id == NULL || $user_id == 0)
		{
			//session variable
			session_start();
			$_SESSION["shop"]=$key;
			return view('shop/2-account')->with('key',$key);
		}
		else
			return ShopController::GetAddress($key);
	}

	/* ---------------------------------------
	 *	GetAddress
	 * --------------------------------------- */
	public function GetAddress($key)
	{
		$data = $this->userDataService->get_user_data(Auth::id());
		return view('shop/3-address')->with('key',$key)->with('user_data',$data);
	}

	/* ---------------------------------------
	 *	PostAccount
	 * --------------------------------------- */
	public function PostAccount()
	{
		session_start();
		if (isset($_SESSION['shop'])) 
		{
			$key = $_SESSION["shop"];
			unset($_SESSION["shop"]);

			$user_id = Auth::id();

			$data = array('user_id' => $user_id);
			try
			{
				$order = Order::where(['unique_key'=>$key])->firstOrFail();
				$order->update($data);
			}
			catch(Exception $e)
			{
				flash('Une erreur est survenue')->error()->important();
				return url('robots-educatifs');
			}
			
			return ShopController::GetAddress($key);	
		}
		else 
		{
			return url('robots-educatifs');
		}
	}

	/* ---------------------------------------
	 *	PostAddress
	 * --------------------------------------- */
	public function PostAddress(OrderDeliveryFormRequest $request)
	{
		$data = array(
			'firstname' => $request->get('firstname'),
			'lastname'  => $request->get('lastname'),
			'email'   	=> $request->get('email'),
			'phone' 	=> $request->get('phone'),
			'address' 	=> $request->get('address'),
			'zipcode' 	=> $request->get('zipcode'),
			'city' 		=> $request->get('city'),
			'status'	=> 'checkout'
		);
		try
		{			
			$key =  $request->get('key');
			
			ToolboxHelper::LogMessage('Debug',__METHOD__, __LINE__,'','
			{
				Key = '.$key.'
			}');

			$order = Order::where(['unique_key'=>$key])->firstOrFail();
			$order->update($data);
		}
		catch(Exception $e)
		{
			flash('Une erreur est survenue')->error()->important();
		}

		return ShopController::GetPayment($key);
	}

	/* ---------------------------------------
	 *	Show Payment form
	 * --------------------------------------- */
	public function GetPayment($key)
	{
		try{
			ToolboxHelper::LogMessage('Debug',__METHOD__, __LINE__,'','
			{
				Key = '.$key.'
			}');

			$order = Order::where(['unique_key'=>$key])->first();
			if(empty($order))
			{
				flash('Une erreur est survenue !')->error()->important();
				return back();
			}

			if(Auth::user()->stripe_id == null)
				Auth::user()->createAsStripeCustomer();

			$intent = Auth::user()->createSetupIntent();
			$client_secret = $intent->client_secret;

			return view('shop/4-payment')->with('key',$key)
			->with('amount',$order->amount / 100)
			->with('order_type', $order->order_type)
			->with('client_secret', $client_secret);
		}
		catch(Exception $e)
		{
			ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,$e->getMessage(),'
			{
				Key = '.$key.'
			}');
			flash('Une erreur est survenue !')->error()->important();
			return ShopController::GetAddress($key);
		}
	}

	/* ---------------------------------------
	 *	Post Payment Request
	 * --------------------------------------- */
	public function PostPayment(Request $request)
	{	
		// récupérer les paramètres
		$payment_method = $request->get('payment_method');
		$key = $request->get('key');

		// créer la transaction dans Stripe
		$res = $this->shopService->MakeStripeTransaction(Auth::user(),$key,$payment_method);

		//	status
		if($res == "success")
		{
			flash('Paiement accepté')->success()->important();
			return ShopController::GetConfirmation($key);
		}
		else
		{
			flash('Une erreur est survenue '.$res)->error()->important();
			return ShopController::GetPayment($key);
		}
	}

	/* ---------------------------------------
	 *	Display Order Confirmation
	 * --------------------------------------- */
	public function GetConfirmation($key)
	{
		$order = Order::where(['unique_key'=>$key])->first();
		if(empty($order))
		{
			flash('Une erreur est survenue !')->error()->important();
		}

		$data = $this->shopService->GetFormattedOrderData($order);
		
		//Send confirmation email
		try{
			Mail::send('emails/email_commande', ['order'=>$data], function($message)
			{
				$message->from('service-commercial@smarteo.co')
				->to('contact@smarteo.co')
				->subject('Commande sur SMARTEO.CO');
			});

			Mail::send('emails/email_commande', ['order'=>$data], function($message) use($data)
			{
				$message->from('service-commercial@smarteo.co')
				->to($data['email'])
				->subject('Confirmation de votre commande');
			});
		}
		catch(Exception $e)
		{

		}


		return view('shop/5-confirmation')->with('data',$data);
	}

	/* ---------------------------------------
	 *	Download Invoice PDF
	 * --------------------------------------- */
	public function PostConfirmation(Request $request)
	{
		$order = Order::where(['unique_key'=>$request->key])->first();
		if(empty($order))
		{
			flash('Une erreur est survenue !')->error()->important();
			return back();
		}

		$data = $this->shopService->GetFormattedOrderData($order);

		$pdf = new \App\Invoice;
		$pdf->generate($data);
	}


	/* ---------------------------------------
	 *  Get The List of Orders
	 * --------------------------------------- */
	public function GetOrders()
	{
		$user_id = Auth::id();
		$orders = Order::where(['user_id'=>$user_id])->get();

		$data = array();

		foreach($orders as $o)
		{
			$d = $this->shopService->GetFormattedOrderData($o);
			if($d['status'] != '')
			{
				array_push($data, [
					'date' 			=> $d['date'],
					'product_slug'	=> $d['product_slug'],		
					'title'			=> $d['description'],
					'price'			=> $d['price'],
					'status'		=> $d['status'],
				]);
			}
		}
		return view('user/orders')->with('data',$data);
	}

	/* ---------------------------------------
	 *  Get The List of Payments
	 * --------------------------------------- */
	public function GetPayments()
	{
		$user_id = Auth::id();

		// si l'utilisateur à des commandes courantes 
		// avec dernier payement > 7 jours alors MAJ depuis stripe
		
		$data = array();
		return view('user/payments')->with('data',$data);
	}

}

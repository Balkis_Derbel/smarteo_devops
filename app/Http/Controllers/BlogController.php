<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
	private $posts_list=array(
		"kit-dash",
		"kit-marty",
		"je-veux-adopter-un-robot",
	);

	private $posts = array(
		array(
			'title' 		=> 'Apprendre la programmation informatique : est-ce vraiment nécessaire ?',
			'description' 	=> '',
			'keywords' 		=> '',
			'image' 		=> '',
			'published_date'=> '',
			'reference'		=> 'post-1',
			'url'			=> 'je-veux-adopter-un-robot'
		),
		array(
			'title' 		=> 'L\'Intelligence Artificielle finira-t-elle par prendre le dessus sur l’Intelligence Humaine ?',
			'description' 	=> '',
			'keywords' 		=> '',
			'image' 		=> '',
			'published_date'=> '',
			'reference'		=> 'post-2',
			'url'			=> 'je-veux-adopter-un-robot'
		),
		array(
			'title' 		=> 'Je veux adopter un robot',
			'description' 	=> '',
			'keywords' 		=> '',
			'image' 		=> '',
			'published_date'=> '',
			'reference'		=> 'post-3',
			'url'			=> 'je-veux-adopter-un-robot'
		)
	);

	public function index()
	{
		$allposts = $this->posts;
		return view('blog/index')->with('data',$allposts);
	}

	public function post($label)
	{
		$allposts = $this->posts;

		$i = array_search($label, $this->posts_list); 
		if ($i != 0)
		{
			return view('blog/detailed')->with('alldata',$allposts)->with('selected',$i);
		} 
		else
		{		
			abort(404);
		}	
	}
}

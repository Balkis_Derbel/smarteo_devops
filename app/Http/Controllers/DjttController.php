<?php

namespace App\Http\Controllers;
use App\Http\Requests\DjttFormRequest;
use App\Http\Requests\Djtt2FormRequest;
use Illuminate\Http\Request;
use Response;
use Storage;
use Illuminate\Support\Facades\DB;
use Mail;

class DjttController extends Controller
{

	public function index()
	{
		return view('djtt/index');
	}

	public function cg()
	{
		return view('djtt/cg');
	}

	public function know_more()
	{
		return view('djtt/know_more');
	}

	public function receiveDocumentation(Djtt2FormRequest $request)
	{
		$data = array(
			'name'    => $request->get('name'),
			'email'   => $request->get('email'),
			'phone'   => $request->get('phone') ?? '',
			'textmsg' => $request->get('school') ?? '',
			'ip' 	  => $request->ip());

		$msg = "** MSG = wlrc/en_savoir_plus ".
		'** SCHOOL = '.$data['textmsg']." ".
		'** IP = '.$data['ip'];

		try{
			// Save in DB			
			DB::insert('insert into contact_requests (name, email, phone, message) values (?, ?, ?, ?)', [$data['name'], $data['email'], $data['phone'], $msg ]);

			// Send email
			Mail::send('emails/email_contact', ['data'=>$data], function($message) use($data)
			{
				$message->to('contact@smarteo.co')->subject('Contact DJGT');
			});

			Mail::send('emails/email_djgt_doc', ['data'=>$data], function($message) use($data)
			{
				$message->to($data['email'])->subject('Le Challenge Robotique WonderWorkshop/Smarteo : Un Magnifique Voyage Dans Le Temps');
			});

			// Flash message
			flash('Votre message a été envoyé avec succès ')->success()->important();
			/*flash('Une erreur est survenue, merci de renouveler ultérieurement ou de nous envoyer un email à l\'adresse: contact@smarteo.co')->error()->important();*/

		}
		catch(Exception $e){
			flash('Une erreur est survenue, merci de renouveler ultérieurement ou de nous envoyer un email à l\'adresse: contact@smarteo.co')->error()->important();			
		}

		return back();
	}

	public function submitParticipation(DjttFormRequest $request)
	{
		$cbcond = $request->get('cbcond');
		if($cbcond==null)
		{
			flash('Veuillez prendre connaissance des conditions générales')->error()->important();
			return back();
		}

		$data = array(
			'name'    => $request->get('name'),
			'email'   => $request->get('email'),
			'phone'   => $request->get('phone'),
			'school' => $request->get('school') ?? '',
			'projectname' => $request->get('projectname'),
			'nb_kids' => $request->get('nb_kids'),
			'age' => $request->get('age'));

		// Save in DB			
		DB::insert('insert into djtts (name, email, phone, school, projectname, nb_kids, age) values (?, ?, ?, ?, ?, ?, ?)', [$data['name'],$data['email'], $data['phone'],$data['school'], $data['projectname'], $data['nb_kids'], $data['age']]);

		// Flash message
		flash('Votre demande a bien été prise en compte :) Nous reviendrons vers vous dans les plus brefs délais.')->success()->important();

		return back();
	}

	public function getDownloadDocumentation()
	{
		$file= "http://localhost/smrt-2/dev/public/files/challenge-wlrc-smarteo.pdf";
		return Response()->file($file);
	}

	public function winners()
	{
		return view('djtt/winners');
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends Controller
{
	public function parents()
	{
		return view('static/parents');
	}

	public function enseignants()
	{
		return view('static/enseignants');
	}

	public function mentions_legales()
	{
		return view('static/mentions_legales');
	}

	public function cgv()
	{
		return view('static/cgv');
	}

	public function faq()
	{
		return view('static/faq');
	}

	public function engagements()
	{
		return view('static/engagements');
	}

	public function whoweare()
	{
		return view('static/whoweare');
	}

	public function options()
	{
		return view('static/options');
	}

	public function surveys()
	{
		return view('surveys/index');
	}


	public function contact()
	{
		return view('contact/index');
	}

	public function animateurs()
	{
		return view('surveys/mentors');
	}

	public function grand_jeu_makers()
	{
		return redirect('https://docs.google.com/forms/d/e/1FAIpQLSd-kPWqgNl4OJC2fX_cqN64jB7B_dLwD5r0F_f9xozBK3-uSg/viewform');
	}

	public function ressources_mbot()
	{
		return redirect('https://drive.google.com/drive/folders/1a9139tRUmXEYCbZMc8j88x9yv3t11RJy?usp=sharing');
	}

	public function makewonder_apps()
	{
		return redirect('https://www.makewonder.com/apps/');
	}

	public function newsletter($title = null)
	{
		$data=array(
			[
				'date'	=>'11/07/2019',
				'ref'	=>'newsletter-19-07-11',
				'url'	=>'la-semaine-derniere-nous-avons-annonce-le-gagnant-du-challenge-de-robotique-un-magnifique-voyage-dans-le-temps',
				'title'	=>'La semaine dernière, nous avons annoncé le gagnant du challenge de robotique \'Un Magnifique Voyage Dans Le Temps\''
			],[
				'date'	=>'24/06/2019',
				'ref'	=>'newsletter-19-06-24',
				'url'	=>'les-premiers-ateliers-de-robotique-pour-enfants-et-ados-vont-bientot-commencer',
				'title'	=>'Les premiers ateliers de robotique pour enfants et ados vont bientôt commencer !'

			],[
				'date'	=>'10/06/2019',
				'ref'	=>'newsletter-19-06-10',
				'url'	=>'les-robots-font-leur-cinema-le-23-06',
				'title'	=>'Les Robots Font Leur Cinéma le 23/06'

			],[
				'date'	=>'02/06/2019',
				'ref'	=>'newsletter-19-06-02',
				'url'	=>'stages-de-vacances-et-ateliers-creatifs',
				'title'	=>'Stages de Vacances et Ateliers Créatifs'

			],[
				'date'	=>'10/04/2019',
				'ref'	=>'newsletter-19-04-10',
				'url'	=>'trois-bonnes-nouvelles-et-une-surprise',
				'title'	=>'Trois bonnes nouvelles et une surprise ;-)'

			]);

		$selected = $data[0];
		foreach($data as $item)
		{
			if($item['url']==$title) $selected = $item;
		}

		return  view('newsletter/index')->with('data',$data)->with('selected',$selected);
	}
	
}

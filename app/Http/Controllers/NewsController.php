<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
	public function index($slug = null)
	{


		$data = News::orderBy('date','DESC')->get();

		if($slug == null)
		{			
			return view('news/index')->with('data',$data->toArray());
		}
		else
		{
			$x = News::where(['slug' => $slug])->first();
			if($x) {
				return view('news/detailed')->with('data',$x->toArray())->with('alldata',$data->take(5)->toArray());
			}
			else
			{
				abort(404); 
			}
		}
	}
}

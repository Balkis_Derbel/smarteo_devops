<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ToolboxHelper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Services\ProductsService;
use App\Services\ContentsService;
use App\Services\UserDataService;

class KitsController extends Controller
{
	protected $productsService;
	protected $userDataService;	

	public function __construct(
		ProductsService $productsService, 
		UserDataService $userDataService,
		ContentsService $contentsService)
	{
		$this->productsService = $productsService;		
		$this->userDataService = $userDataService;	
		$this->contentsService = $contentsService;				
	}

	public function index(Request $request)
	{
		$search = Input::get('search');

		if ($search != "") {
			$search = rawurldecode($search);

			$list_products = DB::table('products')
			->where('type','product')
			->where('status','live')
			->WhereRaw("MATCH(reference, title, subtitle,slug,description,tags) AGAINST('".$search."')")
			->get();
		}
		else{
			$list_products = DB::table('products')
			->where('type','product')
			->where('status','live')
			->get();	
		}

		$data = ToolboxHelper::paginate_collection($list_products, 6);

		if ($request->ajax()) {
			return view('kits.products-list', ['data' => $data])->render();
		}
		return view('kits/index',compact('data'));
	}

	public function details($ident)
	{
		/* if user logged in : check if discount is associated to his account */
		$discounts = $this->userDataService->GetUserDiscount();	

		/* get the product */
		$kit = $this->productsService->GetProductBySlug($ident);
		if($kit == null) abort(404);

		/*apply discount */
		$kit->subscr_amount_ref = $kit->subscr_amount;
		$kit->selling_price_ref = $kit->selling_price;

		$kit->subscr_amount = round($kit->subscr_amount * (1 - $discounts['subscriptions'] / 100),2);
		$kit->selling_price = round($kit->selling_price * (1 - $discounts['products'] / 100),2); 

		$related_products = $this->productsService->GetRelatedProducts($kit->reference);
		$related_contents = $this->contentsService->GetRelatedContents($kit->tags);

		return view('kits/detailed/index')->with('kit',$kit)
		->with('related_products',$related_products)
		->with('related_contents',$related_contents);	
	}
}

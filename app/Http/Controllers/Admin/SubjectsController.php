<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\AdminSubjectsFormRequest;

class SubjectsController extends Controller
{
	public function __construct()
	{
	}

	public function getSubjects($id = 0)
	{
		$selected = Subject::find($id); 
		if($selected == null) $selected = new Subject;

		return View('admin/subjects')
		->with('data',Subject::all())
		->with('selected',$selected);
	}

	public function postSubjects(Request $request)
	{
		$data = $request->all();
		$action = "";
		if(array_key_exists('action',$data)) $action = $data['action'];

		$id = $request->id;
		$selected = null;


		if($action == '')
		{
			flash('Veuillez choisir une action !')->warning()->important();
		}
		else if($action == 'delete')
		{
			Subject::destroy($request['id']);
			flash('Votre demande a bien été prise en compte')->success()->important();
		}
		else if($action == 'view')
		{
			return redirect('communaute/'.$data['slug']);
		}
		else 
		{
			try
			{
				$data['slug'] = $data['slug'] === null ? preg_replace("![^a-z0-9]+!i", "-", strtolower($data['title'])) : $data['slug'];

				if($action == 'update')
				{
					$selected = Subject::find($data['id']); 
					if($selected == null)
					{
						flash('ID non valide !!')->warning()->important();
					} 
					else
					{
						$selected->update($data); 
						flash('Votre demande a bien été prise en compte')->success()->important();
					}
				} 
				else if ($action == 'create')
				{
					$selected = Subject::create($data);
					flash('Votre demande a bien été prise en compte')->success()->important();
				}
			}
			catch(Exception $e)
			{
				flash('Une erreur est survenue : '.$e)->error()->important();
			}
		}

		if($selected == null) $selected = new Subject;

		return View('admin/subjects')
		->with('data',Subject::all())
		->with('selected',$selected);
	}
}

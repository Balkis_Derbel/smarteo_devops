<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Http\Requests\AdminUserEmailFormRequest;
use App\User;
use App\Models\Giveaway;
use App\Models\Djtt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class UsersController extends Controller
{
    /* IMPERSONATE LOGIN */
 	public function impersonate( $user_id ){  
 		if( $user_id != ' '){
 			$user = User::find($user_id);
 			Auth::user()->impersonate($user);
 			return redirect('/');
 		}
 		return redirect()->back();
 	}

 	/* USERS */
 	public function getUsers()
 	{
 		$sql_query = 'select id, name, email, role, created_at, confirmed from users';
 		$data = DB::select( DB::raw($sql_query) );
 		
 		$username = Auth::user()->name;

 		return View('admin/users')
 		->with('data',$data)
 		->with('currentuser',$username);
 	}

 	/* Get Users logs */
 	public function getLogs($user_id)
 	{
 		$sql_query = "select id,request_uri,remote_addr,request_method,is_bot,user_name,user_id,comment,created_at from logs where user_id = '".$user_id."' and created_at >'".date("Y-m-d", strtotime('last week monday'))."' "."order by created_at desc";

 		$cols = array('id','request_uri','remote_addr','request_method','is_bot','user_name','user_id','comment','created_at');

 		$data = DB::select( DB::raw($sql_query) );
 		flash('Votre demande a bien été prise en compte')->success()->important();
 		
 		return View('admin/users_logs')->with('data', $data)->with('cols',$cols);
 	}


 	/* SEND EMAIL */
 	public function getEmail($user_id)
 	{
 		$user_email='';
 		$user_name='';

 		if( $user_id != ' '){
 			$user = User::find($user_id);
 			$user_email = $user->email;
 			$user_name     = $user->name;
 		}

 		return View('admin/users_mail')
 		->with('user_name',$user_name)
 		->with('user_email',$user_email);
 	}

 	public function postEmail(AdminUserEmailFormRequest $request)
 	{
 		$email_type = $request->get('email_type');

 		$data = array(    'email'     => $request->get('email'),
 			'user_name' => $request->get('name'),
 			'subject'     => 'Votre commande a été envoyée !',
 			'colissimo_code'   => $request->get('colissimo_code'));

 		if($request->submitbutton == 'Envoyer !') 
 		{
             // Send email
 			Mail::send('emails/admin/'.$email_type, $data, function($message) use ($data) 
 			{
 				$message->from('contact@smarteo.co', $name = 'Smarteo : Apprendre en s\'amusant avec des robots !');
 				$message->replyTo('contact@smarteo.co', $name = 'Smarteo : Apprendre en s\'amusant avec des robots !');

 				$message->to($data['email']);
 				$message->bcc('contact@smarteo.co', $name = null);

 				$message->subject($data['subject']);
 			});

            // Flash message
 			flash('Votre message a été envoyé avec succès ')->success()->important();

 			return back();
 		}
 		else
 		{
 			return View('emails/admin/'.$email_type,$data);
 		}
 	}


 	public function getGiveAway()
	{
		return View('admin/giveaway')
		->with('data',GiveAway::all());
	}

 	public function getDjtt()
	{
		return View('admin/djtt')
		->with('data',Djtt::all());
	}

}

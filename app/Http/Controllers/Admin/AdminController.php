<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\AdminQueryFormRequest;
use App\Http\Requests\AdminNewsFormRequest;
use App\Http\Requests\AdminCouponsFormRequest;

use Auth;
use Request;
use App\Models\News;
use App\Models\Coupon;
use Illuminate\Support\Facades\DB;
use \TANIOS\Airtable\Airtable;
use Mail;

class AdminController extends Controller
{

	/* ----------------------------------------------
 	ADMIN : INDEX
 	-	-----------------------------------------------*/
 	public function index()
 	{
 		return view('admin/index');
 	}

	/* ----------------------------------------------
 	ADMIN : QUERIES
 	------------------------------------------------*/
 	public function getQueries()
 	{		
 		return view('admin/queries')->with('data',array())->with('cols',array())->with('sql_query','')->with('query_id','0');
 	}

 	public function postQueries(AdminQueryFormRequest $request)
 	{
 		$sql_query = $request->get('sql_query');
 		$query_id =  $request->get('query_id');
 		$data = array();
 		$cols = array();

 		switch ($query_id) {
 			case '1':
 			$sql_query = 'select id, name, email, role, created_at, confirmed from users';
 			$cols = array('id','name','email','role','created_at','confirmed');
 			break;

 			case '2':
 			$sql_query = 'select * from contact_requests';
 			$cols = array('id','name','email','phone','message','created_at');
 			break;

 			case '3':
 			$sql_query = 'select * from logs where created_at > "'.date("Y-m-d").'" order by created_at desc';
 			$cols = array('id','request_uri','remote_addr','request_method','user_agent','is_bot','user_name','user_id','comment','created_at');
 			break;

 			case '4':
 			$sql_query = 'select remote_addr, count(remote_addr) as cn from logs where created_at>"'.date("Y-m-d").'" group by (remote_addr) order by cn desc';
 			$cols = array('remote_addr','count');
 		} 


 		if($sql_query == '')
 		{
 			flash('La requête sql est vide !!!')->warning()->important();
 		}
 		else
 		{
 			try{
 				$data = DB::select( DB::raw($sql_query) );
 				flash('Votre demande a bien été prise en compte')->success()->important();
 			}
 			catch(Exception $e)
 			{
 				flash('Une erreur est survenue '.$e)->warning()->important();
 			}
		 }
		 
 		return View('admin/queries')->with('data', $data)->with('cols',$cols)->with('sql_query',$sql_query)->with('query_id',$query_id);
 	}

	/* ----------------------------------------------
 	ADMIN : SHOW TABLES
 	------------------------------------------------*/
 	public function showData($TableName = NULL)
 	{
 		if($TableName === NULL)
 		{
 			$sql_query = "show tables";
 			$data = DB::select( DB::raw($sql_query) );
 			$cols=array('table_name');
 			$TableName = "ALL";
 		}
 		else
 		{
 			try{
 				$sql_query = "select * from ".$TableName . " order by id desc";

				if($TableName == 'logs'){
 					$sql_query .= " limit 100";					
				}

 				$data = DB::select( DB::raw($sql_query) );

 				$sql_query = "show columns from ".$TableName;


 				$res = DB::select( DB::raw($sql_query) );

 				$cols = array();
 				foreach ($res as  $rowkey => $row) {
 					array_push($cols, $row->Field);
 				}

 				flash('Votre demande a bien été prise en compte')->success()->important();
 			}
 			catch(Exception $e)
 			{
 				flash('Une erreur est survenue '.$e)->warning()->important();
 			}


 		}

 		return View('admin/showdata')->with('tablename', $TableName)->with('data', $data)->with('cols',$cols);
 	}


	/* ----------------------------------------------
 	ADMIN : WEEKLY REPORT
 	------------------------------------------------*/
 	public function getWeeklyReport($startDate = 0)
 	{
 		$sql_query = "select remote_addr,week_nb, count(week_nb) as cn from (select remote_addr, floor(datediff(created_at, '2019-01-01')/7) as week_nb from logs where created_at> '2019-01-01') as tab group by remote_addr, week_nb order by cn desc";

 		$data = DB::select( DB::raw($sql_query));
 		
 		return view('admin/weekly')->with('data',$data);
 	}

	/* ----------------------------------------------
 	ADMIN : NEWS
 	------------------------------------------------*/
 	public function getNews($id = 0)
 	{
 		$selected = News::find($id); 
 		if($selected == null) $selected = new News;

 		return View('admin/news')
 		->with('data',News::all())
 		->with('selected',$selected);
 	}

 	public function postNews(AdminNewsFormRequest $request)
 	{
 		$action = $request->action;
 		$id = $request->id;
 		$selected = null;

 		if($action == '')
 		{
 			flash('Veuillez choisir une action !')->warning()->important();
 		}
 		else if($action == 'delete')
 		{
 			News::destroy($request->id);
 			flash('Votre demande a bien été prise en compte')->success()->important();
 		}
 		else 
 		{
 			$url = $request->url === null ? preg_replace("![^a-z0-9]+!i", "-", strtolower($request->title)) : $request->url;

 			$data = array(
 				'title' => $request->title,
 				'url' => $url,
 				'date' => $request->date,
 				'reference' => $request->reference,
 				'event_date' => $request->event_date,
 				'description' => $request->description,
 				'short' => $request->short,
 				'detailed' => $request->detailed,
 				'image' => $request->image,
 				'keywords' => $request->keywords
 			);

 			if($action == 'update')
 			{
 				$selected = News::find($request->id); 
 				if($selected == null)
 				{
 					flash('ID non valide !!')->warning()->important();
 				} 
 				else
 				{
 					$selected->update($data); 
 					flash('Votre demande a bien été prise en compte')->success()->important();
 				}
 			} 
 			else if ($action == 'create')
 			{
 				$selected = News::create($data);
 				flash('Votre demande a bien été prise en compte')->success()->important();
 			}


 		}

 		if($selected == null) $selected = new News;

 		return View('admin/news')
 		->with('data',News::all())
 		->with('selected',$selected);
 	}


	/* ----------------------------------------------
 	ADMIN : COUPONS
 	------------------------------------------------*/

 	public function getCoupons($id = 0)
 	{
 		$selected = Coupon::find($id); 
 		if($selected == null) $selected = new Coupon;

 		return View('admin/coupons')
 		->with('data',Coupon::all())
 		->with('selected',$selected);
 	}

 	public function postCoupons(AdminCouponsFormRequest $request)
 	{
 		$action = $request->action;
 		$id = $request->id;
 		$selected = null;

 		if($action == '')
 		{
 			flash('Veuillez choisir une action !')->warning()->important();
 		}
 		else if($action == 'delete')
 		{
 			Coupon::destroy($request->id);
 			flash('Votre demande a bien été prise en compte')->success()->important();
 		}
 		else 
 		{
 			$url = $request->url === null ? preg_replace("![^a-z0-9]+!i", "-", strtolower($request->title)) : $request->url;

 			$data = array(
 				'email' 		=> $request->email ?: '',
 				'coupon' 		=> $request->coupon ?: '',				
 				'token_rent' 	=> $request->token_rent ?: '',
 				'token_sell' 	=> $request->token_sell ?: '',				
 				'discount_rent' => $request->discount_rent ?: 0,
 				'discount_sell' => $request->discount_sell ?: 0,
 				'kit' 			=> $request->kit ?: '',
 				'status' 		=> $request->status ?: 'inactive'
 			);

 			if($action == 'update')
 			{
 				$selected = Coupon::find($request->id); 
 				if($selected == null)
 				{
 					flash('ID non valide !!')->warning()->important();
 				} 
 				else
 				{
 					$selected->update($data); 
 					flash('Votre demande a bien été prise en compte')->success()->important();
 				}
 			} 
 			else if ($action == 'create')
 			{
 				$selected = Coupon::create($data);
 				flash('Votre demande a bien été prise en compte')->success()->important();
 			}


 		}

 		if($selected == null) $selected = new Coupon;

 		return View('admin/coupons')
 		->with('data',Coupon::all())
 		->with('selected',$selected);
 	}


	/* ----------------------------------------------
 	ADMIN : CHEAT SHEET
 	------------------------------------------------*/

 	public function cheatSheet()
 	{
 		return View('admin/cheat-sheet');
 	}

 }

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class DashboardController extends Controller
{
	public function getOrders()
	{
		$orders = Order::all(); 

		$interest 			= array();
		$payment  			= array();
		$service_ongoing 	= array();
		$service_completed	= array();
		$archived 			= array();

		foreach ($orders as $o)
		{
			//interest
			if($o->status=='created') array_push($interest, $o);
			if($o->status=='checkout') array_push($interest, $o);	

			//payment
			if($o->status=='subscription_success') array_push($payment, $o);
			if($o->status=='payment_success') array_push($payment, $o);

			//service ongoing
			if($o->status=='product_sent') array_push($service_ongoing, $o);
			if($o->status=='product_delivered' && $o->order_type=='subscription') 
				array_push($service_ongoing, $o);
			if($o->status=='subscription_ongoing') array_push($service_ongoing, $o);
			if($o->status=='workshop_confirmed') array_push($service_ongoing, $o);			

			//service end
			if($o->status=='subscription_finished') array_push($service_completed, $o);
			if($o->status=='product_delivered' and $o->order_type='pay_once') 
				array_push($service_completed, $o);
			if($o->status=='processed') array_push($service_completed, $o);
			if($o->status=='workshop_completed') array_push($service_completed, $o);			

			//archived
			if($o->status=='archived') array_push($archived, $o);			
		}

		return View('admin/dashboard/orders')
		->with('interest'			,$interest)
		->with('payment'			,$payment)
		->with('service_ongoing'	,$service_ongoing)
		->with('service_completed'	,$service_completed)
		->with('archived'			,$archived);
	}
}

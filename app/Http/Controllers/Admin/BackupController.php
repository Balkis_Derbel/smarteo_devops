<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use Artisan;
use Exception;
use League\Flysystem\Adapter\Local;
use Log;
use Response;
use Storage;
use Config;

class BackupController extends Controller
{
    public function index()
    {
        if (!count(config('backup.backup.destination.disks'))) {
            dd(trans('backpack::backup.no_disks_configured'));
        }

        $this->data['backups'] = [];

        foreach (config('backup.backup.destination.disks') as $disk_name) {
            $disk = Storage::disk($disk_name);
            $adapter = $disk->getDriver()->getAdapter();
            $files = $disk->allFiles();

            // make an array of backup files, with their filesize and creation date
            foreach ($files as $k => $f) {
                // only take the zip files into account
                if ((substr($f, -4) == '.zip' || substr($f, -4) == '.sql') && $disk->exists($f)) {
                    $this->data['backups'][] = [
                        'file_path'     => $f,
                        'file_name'     => str_replace('backups/', '', $f),
                        'file_size'     => $disk->size($f),
                        'last_modified' => $disk->lastModified($f),
                        'disk'          => $disk_name,
                        'download'      => ($adapter instanceof Local) ? true : false,
                    ];
                }
            }
        }

        // reverse the backups, so the newest one would be on top
        $this->data['backups'] = array_reverse($this->data['backups']);
        $this->data['title'] = 'Backups';

        return view('admin.backup', $this->data);
    }

    public function create()
    {
        try {
            ini_set('max_execution_time', 600);
            // start the backup process
            info('Calling backup:run');
            Artisan::call('backup:run');

            $output = Artisan::output();

            // log the results
            Log::info("BackupController/create \r\n Nouveau backup réalisé avec succès - ".$output);
            // return the results as a response to the ajax call
            
            flash('Opération réalisée avec succès')->success()->important();

        } catch (Exception $e) {
            Log::error("BackupController/create \r\n".$e);
            flash('Une erreur est survenue '.$e)->warning()->important();
        }

        return back();
    }

    /**
     * Deletes a backup file.
     */
    public function delete($file_name)
    {
        $disk = Storage::disk('backups');

        if ($disk->exists($file_name)) {
            $disk->delete($file_name);

            flash('Opération réalisée avec succès')->success()->important();
        } else {
            flash('Une erreur est survenue '.$e)->error()->important();
        }
        return back();
    }

    /**
     * Backup sql database 
     */
    public function export_db()
    {
        try{
            //Enter your database information here and the name of the backup file
            $mysqlDatabaseName =    Config::get('database.connections.mysql.database');
            $mysqlUserName =        Config::get('database.connections.mysql.username');
            $mysqlPassword =        Config::get('database.connections.mysql.password');
            $mysqlHostName =        Config::get('database.connections.mysql.host');
            $mysqlExportPath =      storage_path('backups')
            .'/'.config('app.name').'-3.0'
            .'/db_export_'
            .$mysqlDatabaseName.'_'
            .date('Y-m-d_H-i-s')
            .'.sql';

            //Export of the database and output of the status
            /*$command='C:\MAMP\htdocs\smrt-2\storage\backups\mysqldump --opt -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' > ' .$mysqlExportPath;*/

            $command='mysqldump --opt -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' > ' .$mysqlExportPath;

            $output=array();
            exec($command,$output,$worked);
            
            $message = "";

            if($worked === 0 ){
                flash('La base a été exportée avec succès')->success()->important();
                $message = 'The database ' .$mysqlDatabaseName .' was successfully stored in the following path : '.$mysqlExportPath;
                Log::info("BackupController/export_db \r\n".$command." \r\n".$message);
            } else {
                flash('Une erreur est survenue')->warning()->important();
                $message = 'An error occurred when exporting ' .$mysqlDatabaseName .' to '.$mysqlExportPath ;
                Log::error("BackupController/export_db \r\n".$command." \r\n".$message);
            }          
        }
        catch (Exception $e) {
            flash('Une erreur est survenue')->warning()->important();
            Log::error("BackupController/export_db \r\n".$command." \r\n".$e);
        }
        return back();
    } 
}

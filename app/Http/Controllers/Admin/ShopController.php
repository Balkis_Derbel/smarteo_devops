<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Order;
use App\Models\Payment;
use App\Models\ParcelTracking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\ShopService;

use App\Http\Requests\AdminProductFormRequest;

class ShopController extends Controller
{
	protected $shopService;

	public function __construct(ShopService $shopService)
	{
		$this->shopService = $shopService;
	}

	public function getProducts($id = 0)
	{
		$selected = Product::find($id); 
		if($selected == null) $selected = new Product;

		return View('admin/products')
		->with('data',Product::all())
		->with('selected',$selected);
	}

	public function postProducts(Request $request)
	{
		$data = $request->all();
		$action = "";
		if(array_key_exists('action',$data)) $action = $data['action'];

		$id = $request->id;
		$selected = null;


		if($action == '')
		{
			flash('Veuillez choisir une action !')->warning()->important();
		}
		else if($action == 'delete')
		{
			Product::destroy($request['id']);
			flash('Votre demande a bien été prise en compte')->success()->important();
		}
		else if($action == 'create_update_in_stripe')
		{
			
			$product_id = $data['id'];
			$res = $this->shopService->UpdateProductInStripe($product_id);
			
			if ($res == 'ok') 
				flash('Votre demande a bien été prise en compte')->success()->important();
			else 
				flash('Une erreur est survenue '.$res)->error()->important();
		}
		else if($action == 'view_product')
		{
			return redirect('robots-educatifs/'.$data['slug']);
		}
		else 
		{
			try
			{
				$data['slug'] = $data['slug'] === null ? preg_replace("![^a-z0-9]+!i", "-", strtolower($data['title'])) : $data['slug'];

				if($action == 'update')
				{
					$selected = Product::find($data['id']); 
					if($selected == null)
					{
						flash('ID non valide !!')->warning()->important();
					} 
					else
					{
						$selected->update($data); 
						flash('Votre demande a bien été prise en compte')->success()->important();
					}
				} 
				else if ($action == 'create')
				{
					$selected = Product::create($data);
					flash('Votre demande a bien été prise en compte')->success()->important();
				}
			}
			catch(Exception $e)
			{
				flash('Une erreur est survenue : '.$e)->error()->important();
			}
		}

		if($selected == null) $selected = new Product;

		return View('admin/products')
		->with('data',Product::all())
		->with('selected',$selected);
	}

	public function getOrders($id = 0)
	{
		$selected = Order::find($id); 
		if($selected == null) $selected = new Order;

		return View('admin/orders')
		->with('data',Order::all())
		->with('selected',$selected);
	}

	public function getPayments($id = 0)
	{
		$selected = Payment::find($id); 
		if($selected == null) $selected = new Payment;

		return View('admin/payments')
		->with('data',Payment::all())
		->with('selected',$selected);
	}


	public function postOrders(Request $request)
	{
		$data = $request->all();
		$action = "";
		if(array_key_exists('action',$data)) $action = $data['action'];
		$id = $data['id'];
		$selected = null;


		if($action == '')
		{
			flash('Veuillez choisir une action !')->warning()->important();
		}
		else if($action == 'delete')
		{
			Order::destroy($data['id']);
			flash('Votre demande a bien été prise en compte')->success()->important();
		}
		else 
		{
			try
			{
				if($action == 'update')
				{
					$selected = Order::find($data['id']); 
					if($selected == null)
					{
						flash('ID non valide !!')->warning()->important();
					} 
					else
					{
						$selected->update($data); 
						flash('Votre demande a bien été prise en compte')->success()->important();
					}
				} 
				else if ($action == 'create')
				{
					$selected = Order::create($data);
					flash('Votre demande a bien été prise en compte')->success()->important();
				}
			}
			catch(Exception $e)
			{
				flash('Une erreur est survenue : '.$e)->error()->important();
			}
		}

		if($selected == null) $selected = new Order;

		return View('admin/orders')
		->with('data',Order::all())
		->with('selected',$selected);
	}

	public function postPayments(Request $request)
	{
		$data = $request->all();
		$action = "";
		if(array_key_exists('action',$data)) $action = $data['action'];
		$id = $data['id'];
		$selected = null;


		if($action == '')
		{
			flash('Veuillez choisir une action !')->warning()->important();
		}
		else if($action == 'delete')
		{
			Payment::destroy($data['id']);
			flash('Votre demande a bien été prise en compte')->success()->important();
		}
		else 
		{
			try
			{
				if($action == 'update')
				{
					$selected = Payment::find($data['id']); 
					if($selected == null)
					{
						flash('ID non valide !!')->warning()->important();
					} 
					else
					{
						$selected->update($data); 
						flash('Votre demande a bien été prise en compte')->success()->important();
					}
				} 
				else if ($action == 'create')
				{
					$selected = Payment::create($data);
					flash('Votre demande a bien été prise en compte')->success()->important();
				}
			}
			catch(Exception $e)
			{
				flash('Une erreur est survenue : '.$e)->error()->important();
			}
		}

		if($selected == null) $selected = new Payment;

		return View('admin/payments')
		->with('data',Payment::all())
		->with('selected',$selected);
	}


	public function getParcelTracking($id = 0)
	{
		$selected = ParcelTracking::find($id); 
		if($selected == null) $selected = new ParcelTracking;

		return View('admin/parcel-tracking')
		->with('data',ParcelTracking::all())
		->with('selected',$selected);
	}

	public function postParcelTracking(Request $request)
	{
		$data = $request->all();
		$action = "";
		if(array_key_exists('action',$data)) $action = $data['action'];

		$id = $request->id;
		$selected = null;


		if($action == '')
		{
			flash('Veuillez choisir une action !')->warning()->important();
		}
		else if($action == 'delete')
		{
			ParcelTracking::destroy($request['id']);
			flash('Votre demande a bien été prise en compte')->success()->important();
		}
		else 
		{
			try
			{
				if($action == 'update')
				{
					$selected = ParcelTracking::find($data['id']); 
					if($selected == null)
					{
						flash('ID non valide !!')->warning()->important();
					} 
					else
					{
						$selected->update($data); 
						flash('Votre demande a bien été prise en compte')->success()->important();
					}
				} 
				else if ($action == 'create')
				{
					$selected = ParcelTracking::create($data);
					flash('Votre demande a bien été prise en compte')->success()->important();
				}
			}
			catch(Exception $e)
			{
				flash('Une erreur est survenue : '.$e)->error()->important();
			}
		}

		if($selected == null) $selected = new Product;

		return View('admin/parcel-tracking')
		->with('data',ParcelTracking::all())
		->with('selected',$selected);
	}

	public function getColissimoTracking($tracking_id)
	{
		$x = $this->shopService->GetColissimoTrackingInfo($tracking_id);
		$data = $x['shipment']['event'];
		
		return view('admin/colissimo-tracking')->with('data',$data);
	}

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Http\Requests\AdminContentsFormRequest;
use App\Helpers\ToolboxHelper;

class ContentsController extends Controller
{
	public function getContents()
	{
		$contents = Content::all();
		return view('admin.contents.home', [
			'contents' => $contents]);
	}

    public function crawle_url(Request $request)
    {
        $data = $request->all();

        $url = $data['search'];
        $tags = ToolboxHelper::GetMetaTags($url);

        $title          = isset($tags['og:title'])      ? $tags['og:title']        : (
                          isset($tags['title'])         ? $tags['title']     : ''); 

        $description    = isset($tags['og:description'])? $tags['og:description']  : (
                          isset($tags['description'])   ? $tags['description'] : '');

        $keywords       = isset($tags['og:keywords'])   ? $tags['og:keywords']  : (
                          isset($tags['keywords'])      ? $tags['keywords'] : '');

        $image          = isset($tags['og:image'])      ? $tags['og:image']  : (
                          isset($tags['image'])         ? $tags['image'] : '');

        $parameters = 'url>'.$url;
        $slug = preg_replace("![^a-z0-9]+!i", "-", strtolower($title));

        $params = (object) array(
            'reference'     => ToolboxHelper::GenerateRandomString(5),
            'title'         => $title, 
            'description'   => $description,
            'keywords'      => $keywords,
            'image'         => $image,
            'parameters'    => $parameters,
            'slug'          => $slug,
            'integration'   => 'href'
        );

        if ($request->ajax()) {
            return json_encode($params);
        }
        else return view('admin.contents.update');
    }

    public function viewContent($id)
    {
        $content = Content::findorfail($id);
        return redirect('contenus-et-tutoriels/'.$content->slug);
    }

    public function insert(Request $request){
        //validate post data
     $this->validate($request, [
      'reference' => 'required',
      'title' => 'required',
      'description',
      'slug',
      'keywords',
      'tags', 
      'tags_1',    
      'thumb',
      'integration',
      'parameters',
      'access_type',
      'status'
  ]);

     $postData = $request->all();

        //insert post data
     Content::create($postData);

     return redirect()->action('AdminController@getContents');
 }

 public function getUpdate($id = null)
 {
    $content = new Content();
    if($id != null)
       $content = Content::where('id', $id)->firstOrFail();

   return view('admin.contents.update', compact('content'));
}

public function postUpdate(AdminContentsFormRequest $request)
{
    try{
        $postData = $request->all();
        $id = $request->id;

        $postData['slug'] = $postData['slug'] === null ? 
        preg_replace("![^a-z0-9]+!i", "-", strtolower($postData['title'])) 
        : $postData['slug'];

        if($id == null)
            Content::create($postData);
        else
        {
            $content = Content::where('id', $id)->firstOrFail(); 
            $content->update($postData);
        }
        flash('Votre demande a bien été prise en compte')->success()->important();
    }
    catch(Exception $e)
    {
        flash('Une erreur est survenue : '.$e)->error()->important();
    }
    return redirect()->action('Admin\ContentsController@getUpdate',[$id]);
}

public function getDelete($id)
{
    $content = Content::findorfail($id);
    return view('admin.contents.delete', [
        'content' => $content]);
}

public function postDelete($id)
{
   Content::destroy($id);
   flash('Votre demande a bien été prise en compte')->success()->important();
   return redirect()->action('Admin\ContentsController@getContents');
}


}

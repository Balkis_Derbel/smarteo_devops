<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PartnerDiscount;
//use App\Http\Requests\PartnerDiscountFormRequest;

class DiscountController extends Controller
{
	public function getPartnerDiscounts($id = 0)
	{
		$selected = PartnerDiscount::find($id); 
		if($selected == null) $selected = new PartnerDiscount;

		return View('admin/discounts')
		->with('data',PartnerDiscount::all())
		->with('selected',$selected);
	}

	public function postPartnerDiscount(Request $request)
	{
		$action = $request->action;
		$id = $request->id;
		$selected = null;


		if($action == '')
		{
			flash('Veuillez choisir une action !')->warning()->important();
		}
		else if($action == 'delete')
		{
			PartnerDiscount::destroy($request->id);
			flash('Votre demande a bien été prise en compte')->success()->important();
		}
		else 
		{
			$data = array(
				'userid' => $request->userid,
				'discount_code' => $request->discount_code,
				'discount' => $request->discount
			);

			if($action == 'update')
			{
				$selected = PartnerDiscount::find($request->id); 
				if($selected == null)
				{
					flash('ID non valide !!')->warning()->important();
				} 
				else
				{
					$selected->update($data); 
					flash('Votre demande a bien été prise en compte')->success()->important();
				}
			} 
			else if ($action == 'create')
			{
				$selected = PartnerDiscount::create($data);
				flash('Votre demande a bien été prise en compte')->success()->important();
			}
		}

		if($selected == null) $selected = new PartnerDiscount;

		return View('admin/discounts')
		->with('data',PartnerDiscount::all())
		->with('selected',$selected);
	}

}

<?php

namespace App\Http\Controllers;
use App\Http\Requests\ContactFormRequest;
use Mail;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
	public function getForm()
	{
		return view('home/index');
	}


	public function postForm(ContactFormRequest $request)
	{

		$data = array('name'    => $request->get('nom'),
			'email'   => $request->get('email'),
			'phone'   => $request->get('phone'),
			'textmsg' => $request->get('texte'),
			'route' 	=> $request->get('route'),
			'ip' 		=> $request->ip());

		$msg = "** ROUTE = ".$data['route']." ".
		'** MSG = '.$data['textmsg']." ";
		'** IP = '.$data['ip'];

		// Save in DB			
		DB::insert('insert into contact_requests (name, email, phone, message) values (?, ?, ?, ?)', [$data['name'], $data['email'],$data['phone'], $msg]);

		// Send email
		Mail::send('emails/email_contact', ['data'=>$data], function($message) 
		{
			$message->to('contact@smarteo.co')->subject('Contact');
		});

		// Flash message
		flash('Votre message a été envoyé avec succès ')->success()->important();

		return back();

	}


	public function contact_educatec()
	{
		return view('contact/contact_educatec');
	}

	public function contact_salon_terroir()
	{
		return redirect('https://airtable.com/shr43G4QxfNj6nEbd');
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Helpers\ToolboxHelper;
use App\Models\Subject;
use App\Models\Content;

class CommunityController extends Controller
{

	public function index(Request $request)
	{
		$search = Input::get('search');

		if ($search != "") {
			$search = rawurldecode($search);

			$subjects = DB::table('subjects')
			->where('status','live')
			->WhereRaw("MATCH(title, slug,keywords,tags,details) AGAINST('".$search."')")
			->get();
		}
		else{
			$subjects = DB::table('subjects')
			->where('status','live')
			->get();
		}

		$data = ToolboxHelper::paginate_collection($subjects, 6);

		if ($request->ajax()) {
			return view('community.subjects-list', ['data' => $data])->render();
		}

		return view('community/index',compact('data'));
	}

	public function subject($slug)
	{
		$item = Subject::where(array('slug'=> $slug))->first();
		if ($item) 
		{
			$rows = json_decode($item['details'], true);
			$rows = $rows['rows'];

			foreach($rows as $selected => $row)
			{
				if($row['type']=='list_contents')
				{ 
					foreach($row['value'] as $key => $ref){
						$content = Content::where('reference',$ref)->first();
						if($content) 
							$rows[$selected]['value'][$key] = $content;
						else
						{
							$rows[$selected]['value'][$key] = NULL;
							ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,
							' --> Content not found !',
							'{
								SubjectSlug = '.$slug.',
								ContentRef = '.$ref.'
							}');
						}
					}
				}
			}
			return view('community/subject')->with('data', $item)->with('rows',$rows);
		}
		else
		{
			Log::error(__METHOD__.' at line '.__LINE__.' --> Subject not found !
			{
				SubjectSlug = '.$slug.',
			}');
			return redirect()->action('CommunityController@index');;
		}

	}

	public function create()
	{
		return view('community/new-subject');
	}

}

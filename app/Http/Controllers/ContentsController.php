<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Requests\ContentFeedbackFormRequest;
use App\Helpers\ToolboxHelper;
use App\Models\Content;
use App\Services\ContentsService;
use Mail;

class ContentsController extends Controller
{
	protected $contentsService;

	public function __construct(ContentsService $contentsService)
	{
		$this->contentsService 	= $contentsService;		
		//$this->middleware('auth');
	}

	public function index(Request $request)
	{
		$search = Input::get('search');

		if ($search != "") {
			$search = rawurldecode($search);

			$usercontents = DB::table('user_contents')
			->where('userid',Auth::id())
			->join('contents', 'user_contents.contentid', '=', 'contents.id')
			->select('contents.*')
			->WhereRaw("MATCH(reference, title, description,slug,keywords,tags,tags_1,parameters) AGAINST('".$search."')")
			->get();

			$publiccontents = DB::table('contents')
			->where('access_type','public')
			->where('status','ready')
			->WhereRaw("MATCH(reference, title, description,slug,keywords,tags,tags_1,parameters) AGAINST('".$search."')")
			->get();
		}
		else
		{
			$usercontents = DB::table('user_contents')
			->where('userid',Auth::id())
			->join('contents', 'user_contents.contentid', '=', 'contents.id')
			->select('contents.*')
			->get();

			$publiccontents = DB::table('contents')
			->where('access_type','public')
			->where('status','ready')
			->get();
		}

		$selectedcontents = $usercontents->merge($publiccontents)->unique();

		$data = ToolboxHelper::paginate_collection($selectedcontents, 6);

		if ($request->ajax()) {
			return view('contents.items-list', ['data' => $data])->render();
		}

		return view('contents/index',compact('data'));
	}

	public function content($content, $section = "")
	{
		$item = Content::where(array('slug'=> $content))->first();
		if ($item) 
		{
			$integration = $item->integration;
			$params = $this->contentsService->GetContentAdditionalParameters($item->parameters);

			if($integration == 'local')
			{
				if($section == null)
				{
					return view('contents/'.$item->reference.'/index')->with('item', $item->toArray());
				}
				else
				{
					return view('contents/'.$item->reference.'/pages/'.$section)->with('item', $item->toArray());
				}
			}
			elseif($integration == 'href')
			{
				$url = $params['url'];
				return redirect()->to($url);
			}
			elseif($integration == 'iframe')
			{
				$url = $params['url'];
				return view('contents/link-iframe/index')->with('url', $url);
			}

		}
		else
		{
			abort(404);
		}
	}



	public function getFeedbackForm()
	{
		return view('home/index');
	}


	public function postFeedbackForm(ContentFeedbackFormRequest $request)
	{

		$data = array('name' => $request->get('nom'),
			'email'   		=> $request->get('email'),
			'phone'			=> '',
			'textmsg' 		=> $request->get('texte'),
			'route' 		=> $request->get('route'),
			'content_ref' 	=> $request->get('content_ref'),
			'ip' 			=> $request->ip());

		$msg = "*** CONTENT FEEDBACK *".
		"** CONTENT = ".$data['content_ref']." ".
		"** MESSAGE = ".$data['textmsg']." ";
		"** IP = ".$data['ip'];

		// Save in DB			
		DB::insert('insert into contact_requests (name, email, phone, message) values (?, ?, ?, ?)', [$data['name'], $data['email'],'', $msg]);

		// Send email
		Mail::send('emails/email_contact', ['data'=>$data], function($message) 
		{
			$message->to('contact@smarteo.co')->subject('Contact');
		});

		// Flash message
		flash('Nous vous remercions, votre message a été envoyé avec succès ')->success()->important();

		return back();

	}
}

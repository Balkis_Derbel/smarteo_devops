<?php

namespace App\Http\Controllers;
use App\Models\Order;
use Mail;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\CouponFormRequest;
use Illuminate\Support\Facades\Redirect;
use \TANIOS\Airtable\Airtable;


class OrderController extends Controller
{
	private function array_sort($array, $on, $order=SORT_ASC)
	{
		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
				asort($sortable_array);
				break;
				case SORT_DESC:
				arsort($sortable_array);
				break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	private function GetEventBadge($event)
	{
		if($event == "Paiement")
			return "badge-info";
		elseif($event == "Colis livré")
			return "badge-warning";
		elseif($event == "Colis renvoyé")
			return "badge-warning";
		else
			return "badge-success";
	}

	private function GetFieldComment($fields)
	{
		if(array_key_exists('Comment',$fields))
		{
			return $fields->Comment;
		}
		else
		{
			return "";
		}
	}

	private function GetFieldAmount($fields)
	{
		if(array_key_exists('Amount',$fields))
		{
			$amount = $fields->Amount * 1.2 / 100;
			return $amount . " EUR";
		}
		else
		{
			return "";
		}
	}

	private function GetOrderStatus($fields)
	{
		if(array_key_exists('Status',$fields))
		{
			return $fields->Status;
		}
		else
		{
			return "En cours de préparation";
		}
	}

	private function GetOrderType($fields)
	{
		if(array_key_exists('Unit of recurrence',$fields))
		{
			$unit = $fields->{'Unit of recurrence'};
			if($unit === 'Week') return 'Location';
			else return '??';
		}
		else
		{
			return "Achat";
		}
	}

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function BuyOrder($label)
	{
		$data = array('user_id' => Auth::id(),
			'order_type'=> 'buy',
			'kit_label' => $label
		);

		return redirect('boutique/achat/'.$label);
	}

	public function RentOrder($label)
	{
		$data = array('user_id' => Auth::id(),
			'order_type'=> 'rent',
			'kit_label' => $label
		);

		// Save in Orders table		
		DB::insert('insert into orders (user_id, order_type, kit_reference, kit_label, kit_price, latest_status_id) values (?, ?, ?, ?, ?, ?)', [$data['user_id'], $data['order_type'],$data['kit_label'], $data['kit_label'], 0 , 1]);

		$order_id = DB::getPdo()->lastInsertId();

		// Save in Orders status table
		DB::insert('insert into order_status (order_id, status_id) values (?, ?)', [$order_id, 1]);

		$link = 'https://www.payfacile.com/smarteo/si/souscription-'.$label;
		return view('kits/checkout')->with('link',$link);
		//return Redirect::to($link);
	}


	public function GetOrders()
	{
		$airtable = new Airtable(array(
			'api_key' => 'keyAijfVB7pKtrNzW',
			'base'    => 'appCY4cjwDqFkTxPV'
		));

		$user_email = Auth::user()->email;
		$params = array(
			"filterByFormula" => "AND( {Customer Email} = '".$user_email."' )"
		);

		$request = $airtable->getContent( 'Orders' , $params );

		do {
			$response = $request->getResponse();
			$records = $response[ 'records' ];
		}
		while( $request = $response->next() );


		$data = array();

		if(!empty($records))
		{
			foreach ($records as $record) {
				$fields = $record->fields;
				$data = array_merge($data, array([
					'date' 		=> $fields->{"Date Update"},
					'type' 		=> $this->GetOrderType($fields),					
					'label'		=> $fields->{"Product Name"},
					'price'		=> $fields->{"Unitary Price"} * 1.2 / 100,
					'status'	=> $this->GetOrderStatus($fields)
				]));
			}
		}

		return view('order/orders')->with('data',$data);
	}

	public function GetOrderHistory($ref)
	{
		$airtable = new Airtable(array(
			'api_key' => 'keyAijfVB7pKtrNzW',
			'base'    => 'appCY4cjwDqFkTxPV'
		));

		//look in 'orders' table
		$params = array(
			"filterByFormula" => "AND( {Date Update} = '".$ref."' )"
		);

		$request = $airtable->getContent( 'Orders' , $params );

		do {
			$response = $request->getResponse();
			$records = $response[ 'records' ];
		}
		while( $request = $response->next() );

		$data0 = array();
		$order_key = '%zzz%';

		if(!empty($records))
		{
			foreach ($records as $record) {
				$fields = $record->fields;
				$data0 = array_merge($data0, array([
					'date' 		=> $fields->{"Date Update"},
					'type' 		=> $this->GetOrderType($fields),					
					'label'		=> $fields->{"Product Name"},
					'price'		=> $fields->{"Unitary Price"} * 1.2 / 100,
					'status'	=> $this->GetOrderStatus($fields)
				]));
				$order_key = $fields->{"Key"};
			}
		}
		else
		{
			flash('Référence non valide !')->warning()->important();
		}

		//look in 'ordershistory' table
		$params = array(
			"filterByFormula" => "AND( {Order} = '".$order_key."' )"
		);

		$request = $airtable->getContent( 'OrderHistory' , $params );

		do {
			$response = $request->getResponse();
			$records = $response[ 'records' ];
		}
		while( $request = $response->next() );


		$data = array();

		if(!empty($records))
		{
			foreach ($records as $record) {
				$fields = $record->fields;
				$data = array_merge($data, array([
					'date' 		=> $fields->{"Date"},
					'event' 	=> $fields->{"Event"},					
					'amount'	=> $this->GetFieldAmount($fields),
					'comment'	=> $this->GetFieldComment($fields),
					'badge'		=> $this->GetEventBadge($fields->{"Event"})
				]));
			}
			$data = $this->array_sort($data, 'date', SORT_ASC);
		}
		elseif(count($data0)>0)
		{
			$data00 = $data0[0];
			$data = array([
				'date' 		=> $data00['date'],
				'event' 	=> 'Commande effectuée',					
				'amount'	=> '',
				'comment'	=> $data00['label'],
				'badge'		=> $this->GetEventBadge('Commande effectuée')
			]);
		}

		return view('order/order-history')->with('data',$data);
	}

}

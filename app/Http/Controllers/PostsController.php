<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostsController extends Controller
{
	public function index($title = null)
	{


		$data = Post::orderBy('date','DESC')->get();

		if($title == null)
		{			
			return view('posts/index')->with('data',$data->toArray());
		}
		else
		{
			$x = Post::where(['url' => $title])->first();
			if($x) {
				return view('posts/detailed')->with('data',$x->toArray())->with('alldata',$data->take(5)->toArray());
			}
			else
			{
				abort(404); 
			}
		}
	}

}

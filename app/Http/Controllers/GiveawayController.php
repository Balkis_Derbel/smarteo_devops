<?php

namespace App\Http\Controllers;
use App\Http\Requests\GiveawayFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GiveawayController extends Controller
{

	public function getGiveaway($token = '')
	{
		if($token == "conditions-generales")
			return view('giveaway/cg');

		$token_0 = openssl_random_pseudo_bytes(16);
		$token_0 = bin2hex($token_0);
		return view('giveaway/index')->with('token',$token)->with('token_0',$token_0);
	}

	public function postGiveaway(GiveawayFormRequest $request)
	{

		$cbcond = $request->get('cbcond');
		if($cbcond==null)
		{
					flash('Veuillez prendre connaissance des conditions générales du tirage au sort :)')->error()->important();
		return back();
}

		$data = array('firstname'    => $request->get('firstname'),
			'lastname'   => $request->get('lastname'),
			'email'   => $request->get('email'),
			'token' => $request->get('token') ?? '',
			'cbnsl' => $request->get('cbnsl'));

		$token_0 = openssl_random_pseudo_bytes(16);
		$token_0 = bin2hex($token_0);

		if($data['cbnsl'] == null) $data['cbnsl']='';
		// Save in DB			
		DB::insert('insert into giveaways (firstname, lastname, email, usertoken, invitertoken, newsletter) values (?, ?, ?, ?, ?, ?)', [$data['firstname'],$data['lastname'], $data['email'],$token_0,$data['token'], $data['cbnsl']]);

		// Flash message
		flash('Votre participation a bien été prise en compte :) Vous pouvez augmenter encore vos chances de gagner en invitant des amis')->success()->important();

		return back();
	}

}

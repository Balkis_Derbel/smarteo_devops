<?php

namespace App\Http\Controllers;
use App\Http\Requests\UserDataFormRequest;
use App\Http\Requests\ContentAccessKeysFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\Order;
use App\Models\UserInfo;
use App\Models\UserContent;
use App\Models\ContentAccessKey;
use App\Models\PartnerDiscount;
use App\Services\UserDataService;


class UserDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    protected $userDataService;

    public function __construct(UserDataService $userDataService)
    {
        $this->userDataService = $userDataService;  
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session_start();
        if (isset($_SESSION['shop'])) 
        {
            $key = $_SESSION["shop"];
            unset($_SESSION["shop"]);
            
            return redirect('boutique/livraison/'.$key);        
        }

        $userid = Auth::id();

        return view('user/dashboard');
    }

    public function getUserData()
    {
        $userid = Auth::id();
        $data = $this->userDataService->get_user_data(Auth::id());
        return view('user/userdata')->with('data', $data);
    }

    public function setUserData(UserDataFormRequest $request)
    {
        $userid = Auth::id();

        $firstname = array(
            'userid'=> $userid,
            'key'   => 'firstname',
            'value' => $request->firstname
        );

        $lastname = array(
            'userid'=> $userid,
            'key'   => 'lastname',
            'value' => $request->lastname
        );

        $phone = array(
            'userid'=> $userid,
            'key'   => 'phone',
            'value' => $request->phone
        );

        $address = array(
            'userid'=> $userid,
            'key'   => 'address',
            'value' => $request->address
        );

        $zipcode = array(
            'userid'=> $userid,
            'key'   => 'zipcode',
            'value' => $request->zipcode
        );


        $city = array(
            'userid'=> $userid,
            'key'   => 'city',
            'value' => $request->city
        );

        UserInfo::firstOrCreate(['userid' => $userid, 'key' => 'firstname'])->update($firstname);
        UserInfo::firstOrCreate(['userid' => $userid, 'key' => 'lastname'])->update($lastname);
        UserInfo::firstOrCreate(['userid' => $userid, 'key' => 'phone'])->update($phone);
        UserInfo::firstOrCreate(['userid' => $userid, 'key' => 'address'])->update($address);
        UserInfo::firstOrCreate(['userid' => $userid, 'key' => 'zipcode'])->update($zipcode);
        UserInfo::firstOrCreate(['userid' => $userid, 'key' => 'city'])->update($city);


        $data = $this->userDataService->get_user_data(Auth::id());

        flash('Vos données ont été mises à jour')->success()->important();

        return view('user/userdata')->with('data', $data);
    }

    public function getContentAccessKeys()
    {

        $accesskeys = DB::table('content_access_keys')
        ->leftjoin('user_contents', 'user_contents.keyid', '=', 'content_access_keys.id')
        ->select('content_access_keys.*', 'user_contents.userid')
        ->where('user_contents.userid', '=', Auth::id())
        ->orderBy('content_access_keys.maxdate', 'desc');

        return view('user/content_access_keys')->with('data',$accesskeys->get()->unique()->toArray());
    }

    public function postContentAccessKeys(ContentAccessKeysFormRequest $request)
    {
        $key = $request->get('key');
        $accesskey = ContentAccessKey::where(array('value'=> $key))->first();

        if($accesskey == null)
        {
            flash('Cette clé n\'est pas valide, veuiller vérifier votre saisie')->warning()->important();
        }
        else
        {
            $contentids = $accesskey->contentids;
            $ids = explode(";",$contentids);

            UserContent::where('userid','=',Auth::id())
            ->where('keyid','=',$accesskey->id)
            ->delete();
            

            foreach($ids as $id)
            {
                DB::table('user_contents')->insert([
                    [
                        'userid'    =>  Auth::id(),
                        'contentid' =>  $id,
                        'keyid'     =>  $accesskey->id,
                        'maxdate'   =>  $accesskey->maxdate
                    ],
                ]);
            }

            flash('Votre demande a bien été prise en compte')->success()->important();   
        }

        return back();
    }


    public function getBenefits()
    {
        $user_id = Auth::id();

        $partner_code = "";
        $partner_discount = 0;

        $data = PartnerDiscount::where('userid',$user_id)->get();
        
        if(!$data->isEmpty())
        {
            $partner_code = $data[0]->discount_code;
            $partner_discount = $data[0]->discount;
        }

        return view('user/userbenefits')->with('partner_code', $partner_code)
        ->with('partner_discount',$partner_discount);
    }

    public function postPartnerCode(Request $request)
    {
        $user_id = Auth::id();
        $partner_code = $request->get('partner_code');

        if($partner_code != "" )
        {
            $d = PartnerDiscount::where('userid',$user_id)->get();
            if(!$d->isEmpty())
            {
                PartnerDiscount::where('userid','=',$user_id)->delete();
            }
        }

        if($partner_code == "NPSCIE" || $partner_code == "JOYEUXNOEL")
        {  
            DB::table('partner_discounts')->insert([
                [
                    'userid'       => $user_id,
                    'discount_code' =>  $partner_code,
                    'discount'      =>  5
                ],
            ]);
            flash('Votre demande a bien été prise en compte')->success()->important();
        }
        elseif($partner_code == "ARCENCIEL" || $partner_code == "WEAREMAKERS")
        {
            DB::table('partner_discounts')->insert([
                [
                    'userid'       => $user_id,
                    'discount_code' =>  $partner_code,
                    'discount'      =>  10
                ],
            ]);
            flash('Votre demande a bien été prise en compte')->success()->important();
        }
        elseif($partner_code == "WEAREWINNERS")
        {
            DB::table('partner_discounts')->insert([
                [
                    'userid'       => $user_id,
                    'discount_code' =>  $partner_code,
                    'discount'      =>  100
                ],
            ]);
            flash('Votre demande a bien été prise en compte')->success()->important();
        }
        else
        {
            flash('Votre code partenaire n\'a pas été reconnu')->error()->important();  
        }
        return redirect('mes-avantages');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $latestnews = News::orderBy('date','DESC')->take(2)->get()->toArray();
        $kits = Product::where('status','live')->where('type','product')->where('status','live')->inRandomOrder()->take(3)->get();
         
        return view('home/index')->with('latestnews',$latestnews)
		->with('kits',$kits);
    }
}

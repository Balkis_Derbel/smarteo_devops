<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\News;
use Illuminate\Support\Facades\DB;

class SitemapController extends Controller
{
	public function index()
	{	
		$newsletters = array(
			[
				'url'	=>'la-semaine-derniere-nous-avons-annonce-le-gagnant-du-challenge-de-robotique-un-magnifique-voyage-dans-le-temps'
			],[
				'url'	=>'les-premiers-ateliers-de-robotique-pour-enfants-et-ados-vont-bientot-commencer'
			],[
				'url'	=>'les-robots-font-leur-cinema-le-23-06'
			],[
				'url'	=>'stages-de-vacances-et-ateliers-creatifs'
			],[
				'url'	=>'trois-bonnes-nouvelles-et-une-surprise'
			]);


		$pages = array(
			
			//static pages
			array('uri' => ''),
			array('uri' => 'parents'),
			array('uri' => 'enseignant'),
			array('uri' => 'surveys'),
			array('uri' => 'contact'),
			array('uri' => 'qui-sommes-nous'),
			array('uri' => 'nos-engagements'),
			array('uri' => 'actualites'),
			array('uri' => 'contenus-et-tutoriels'),
			array('uri' => 'faq'),			
		);

		//actualités
		$news = News::all()->toArray();

		foreach($news as $item)
		{
			array_push($pages, array('uri' => 'actualites/'.$item['url']));
		}

		//contenus et tutoriels
		$publiccontents = DB::table('contents')
		->where('access_type','public')
		->where('status','ready')
		->get();

		foreach($publiccontents as $item)
		{
			array_push($pages, array('uri' => 'contenus-et-tutoriels/'.$item->slug));
		}

		//robots educatifs
		$products = DB::table('products')
		->where('type','product')
		->where('status','live')
		->get();

		foreach($products as $item)
		{
			array_push($pages, array('uri' => 'robots-educatifs/'.$item->slug));
		}


		foreach($newsletters as $item)
		{
			array_push($pages, array('uri' => 'newsletter/'.$item['url']));
		}

		return response()->view('sitemap/index', compact('pages'))->header('Content-Type', 'text/xml');
	}
}

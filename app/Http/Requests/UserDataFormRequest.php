<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserDataFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'         => 'required|min:3|max:40',
            'lastname'          => 'required|min:3|max:40',
            'phone'             => 'required|max:15',
            'address'           => 'required|min:3|max:40',
            'zipcode'           => 'required|min:5|max:5',
            'city'              => 'required|min:3|max:40',
            'country'           => '',
            'password'          => 'max:250',
            'passwordconfirm'   => 'max:250'
        ];
    }
}

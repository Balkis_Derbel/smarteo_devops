<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Djtt extends Model
{
    protected $fillable = [
        'id', 
        'name', 
        'email',
        'phone', 
        'school',
        'projectname',
        'nb_kids',
        'age',
        'created_at'
    ];
}

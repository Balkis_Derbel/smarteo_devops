<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContent extends Model
{
	protected $fillable = [
		'id', 
		'userid', 
		'contentid', 
		'keyid',
		'maxdate'
	];
}

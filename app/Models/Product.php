<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable=[  
		'id',
		'reference',
		'type',            
		'title',
		'subtitle',		
		'slug',
		'description',
		'thumb',
		'nb_images',
		'featured',
		'tags',
		'selling_price',       
		'subscr_amount',
		'subscr_frequency',
		'stripe_product_ref',
		'stripe_subscr_plan',
		'status'
	];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
	protected $fillable = [
		'id', 
		'email', 
		'coupon', 
		'token_rent',
		'token_sell',
		'discount_rent',
		'discount_sell',
		'kit',
		'status'
	];
    //
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentAccessKey extends Model
{
	protected $fillable = [
		'id',
		'title',
		'value',
		'startdate',
		'maxdate',
		'contentids',
	];
}

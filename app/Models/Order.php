<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
		protected $fillable=[
	    		'id',
    			'user_id',
    			'product_reference',
    			'amount',
                'discount_code',
    			'order_type',
    			'created_at',
    			'updated_at',
    			'request_uri',
    			'unique_key',
                'firstname',
                'lastname',
                'email',
                'phone',
                'address',
                'zipcode',
                'city',
                'options',
                'other_infos',
                'status'
    		];
}
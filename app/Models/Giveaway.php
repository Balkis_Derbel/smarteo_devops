<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Giveaway extends Model
{
	protected $fillable = [
        'id', 
        'firstname', 
        'lastname',
        'email', 
        'usertoken',
        'invitertoken',
        'newsletter',
        'created_at'
    ];
}

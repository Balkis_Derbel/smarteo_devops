<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $fillable = [
		'id', 
		'title', 
		'slug',
		'keywords',
		'tags',
		'thumb',
		'status',
		'details'
	];
}
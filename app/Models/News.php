<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	protected $fillable = [
        'id', 
        'title', 
        'slug', 
        'date',
        'reference',
        'event_date',
        'description',
        'short',
        'detailed',
        'thumb',
        'cover',
        'keywords'
    ];
    //
}

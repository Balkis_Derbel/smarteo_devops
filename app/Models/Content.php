<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	protected $fillable = [
		'id', 
		'reference', 
		'title', 
		'description',
		'slug',
		'keywords',
		'tags',
		'tags_1',		
		'thumb',
		'integration',
		'parameters',
		'access_type',
		'status'
	];
}
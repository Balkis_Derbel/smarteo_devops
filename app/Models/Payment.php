<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
protected $fillable=[
	    		'id',
    			'user_id',
    			'order_id',
    			'product_ref',
    			'amount',
    			'stripe_customer_id',
    			'stripe_transaction_id'
    		];
}

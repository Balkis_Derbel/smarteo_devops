<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParcelTracking extends Model
{
		protected $fillable=[
	    		'id',
    			'orderid',
    			'firstname',
    			'lastname',
    			'email',
    			'phone',
    			'address',
    			'zipcode',
    			'city',
    			'status',
    			'transporter',
    			'tracking_id',
    			'date_sent',
                'date_delivered',
    			'comment'
    		];
}

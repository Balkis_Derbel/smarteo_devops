<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerDiscount extends Model
{
		protected $fillable=[
	    		'id',
    			'userid',
    			'discount_code',
    			'discount'
    		];
}

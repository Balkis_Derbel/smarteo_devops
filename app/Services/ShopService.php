<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

use App\Models\Order;
use App\Models\Product;
use App\Models\PartnerDiscount;

use App\Helpers\ToolboxHelper;
use Stripe;
use Log;

class ShopService
{

    private function GetOrderOptions($order)
    {
        return "";
    }

    private function GetProductInfo($ref)
    {
        $selected = Product::where(['reference'=>$ref])->first();
        if($selected != null) return array
            (
                'title'=>$selected->title,
                'slug'=>$selected->slug
            );
        else return array
            (
                'title'=> $ref,
                'slug' => ''
            );
    }

    public function GetDiscount($user_id)
    {
        $discount = 0;
        $d = PartnerDiscount::where('userid',$user_id)->get();
        if(!$d->isEmpty()) 
        {
            $discount = $d[0]->discount;
        }

        return $discount;
    }


    public function GetFormattedOrderData($order)
    {
        try
        {
            $data = array(
                'key'           => '',
                'name'          => '',
                'email'         => '',
                'phone'         => '',
                'address'       => '',
                'zipcode'       => '',
                'city'          => '',
                'order_type'    => '',
                'description'   => '',
                'product_slug'  => '',                
                'options'       => '',
                'price_excl_vat' => '',
                'price'         => '',
                'reference'     => '',
                'date'          => '',
                'status'        => ''
            );

            if(empty($order)) return $data;

            $data['key'] = $order->unique_key;
            $data['name'] = $order->firstname." ".$order->lastname;
            $data['address'] = $order->address.", ".$order->zipcode." ".$order->city;
            $data['email'] = $order->email;
            $data['phone'] = $order->phone;

            $info = $this->GetProductInfo($order->product_reference);
            $description =  $info['title'];
            $slug =         $info['slug'];

            if($order->order_type == 'pay_once')
            {
                $description = "Achat : ".$description;
            }
            if($order->order_type == 'subscribe')
            {
                $description = "Abonnement hebdomadaire : ".$description;
            }
            $data['description'] = $description;
            $data['product_slug'] = $slug;         

            $data['options'] = $this->GetOrderOptions($order);
            $data['price'] = number_format($order->amount / 100, 2, ',', ' ');
            $data['price_excl_vat'] = number_format($order->amount / 100 / 1.2, 2, ',', '');

            $data['order_type'] = $order->order_type;

            $data['reference'] = date("y.m.m").".".$order->id;
            $data['date'] = $order->created_at;

            $s = $order->status;

            $status_list=array(
                'created'               => '',
                'checkout'              => '',
                'payment_success'       => 'Commande validée',
                'subscription_success'  => 'Abonnement validé',
                'product_sent'          => 'Produit envoyé',
                'product_delivered'     => 'Produit livré',
                'subscription_ongoing'  => 'Abonnement en cours',
                'subscription_finished' => 'Abonnement achevé',
                'processed'             => 'Traité',
                'archived'              => 'Archivé');

            if(array_key_exists($s,$status_list)) $data['status'] = $status_list[$s];

            return $data;
        }
        catch(Exception $e)
        {
            Log::error('ShopService::GetFormattedOrderData --> '.$e->getMessage());
        }
    }

    /* ----------------------------------
    ** Get Order Data From Key
    ** ---------------------------------- */
    public function GetOrderDataFromKey($key)
    {
        $order = Order::where(['unique_key'=>$key])->first();
        
        if(empty($order)) throw('unknown order key');

        $data = array(
            'order_id'          => $order->id,
            'firstname'         => $order->firstname,
            'lastname'          => $order->lastname,
            'email'             => $order->email,
            'user_id'           => $order->user_id,
            'amount'            => $order->amount,
            'order_type'        => $order->order_type,    
            'product'           => $order->product_reference,
            'discount_code'     => $order->discount_code
        );

        return $data;
    }

    /* ----------------------------------
    ** Get or create new Stripe Id
    ** ---------------------------------- */
    public function GetCustomerStripeId($user, $stripe_token)
    {
        $customer_stripe_id = "";
        try {

            $customer_stripe_id = $user->customer_stripe_id;

            if(empty($customer_stripe_id))
            {
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $customer = \Stripe\Customer::create([
                    'source' => $stripe_token,
                    'email' => $user->email,
                    'metadata' => [
                        "User Name" => $user->name
                    ]
                ]);

                $customer_stripe_id = $customer->id;
            }

            $user->customer_stripe_id = $customer_stripe_id;

            return $customer_stripe_id;

        } catch (\Stripe\Error\Card $e) {
            // log error
        }
    }

    /* ----------------------------------
    ** Create ¨Payment Intent
    ** ---------------------------------- */
    public function CreateStripePaymentIntent($user, $stripe_token, $amount)
    {

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $intent = \Stripe\PaymentIntent::create([
            'amount' => $amount,
            'currency' => 'eur',
            'payment_method_types' => ['card'],
            'receipt_email' => 'jenny.rosen@example.com'
        ]);

        return $intent->client_secret;
    }
    /* ----------------------------------
    ** MakeStripeSubscription
    ** ---------------------------------- */
    public function MakeStripeTransaction($user, $order_key, $payment_method)
    {      
        $res = "";
        
        try
        {
            ToolboxHelper::LogMessage('Debug',__METHOD__, __LINE__,'','
            {
                OrderKey = '        .$order_key.',
                PaymentMethod = '   .$payment_method.'
            }');

            // mettre à jour la méthode de payement dans stripe
            $user->updateDefaultPaymentMethod($payment_method);

            // récupérer infos
            $order_data = $this->GetOrderDataFromKey($order_key);

            if($order_data['order_type']=='pay_once')
            {
                $res = $this->MakeStripePayment($user, $payment_method, $order_data);

                if($res == 'success')
                {
                // Update order status
                    $order = Order::where(['id'=>$order_data['order_id']])->firstOrFail();
                    $order->update(array('status'=>'payment_success'));
                }
            }
            else
            {
                $product = Product::Where('reference',$order_data['product'])->first();
                $discount_code = $order_data['discount_code'];

                $res = $this->MakeStripeSubscription(
                    $user, 
                    $payment_method, 
                    $product->stripe_subscr_plan, 
                    $discount_code);

                // Update order status
                if($res == 'success')
                {
                    $order = Order::where(['id'=>$order_data['order_id']])->firstOrFail();
                    $order->update(array('status'=>'subscription_success'));
                }
            }
        }
        catch(Exception $e)
        {
            ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,$e->getMessage(),'
            {
                OrderKey = '        .$order_key.',
                PaymentMethod = '   .$payment_method.'
            }');
        }

        return $res;
    }

    /* ----------------------------------
    ** MakeStripePayment
    ** ---------------------------------- */
    public function MakeStripePayment($user, $payment_method, $order)
    {
        try 
        {
            ToolboxHelper::LogMessage('Debug',__METHOD__, __LINE__,'','
            {
                OrderId = '             .$order['order_id'].',
                CustomerStripeId = '    .$user->stripe_id.'
            }');

            $charge = $user->charge($order['amount'], $payment_method);
        } 
        catch (\Stripe\Error\Card $e) 
        {
            ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,$e->getMessage(),'
            {
                OrderId = '         .$order['order_id'].',
                PaymentMethod = '   .$payment_method.'
            }');
        }

        // Créer une entrée dans la table 'payements'
        DB::insert('insert into payments (
            user_id,
            order_id,
            product_ref,
            amount,
            stripe_customer_id,
            stripe_transaction_id) 
            values(?,?,?,?,?,?)',
            [ 
                $order['user_id'], 
                $order['order_id'], 
                $order['product'],
                $order['amount'],
                $user->stripe_id,
                $charge->id
            ]);

        return "success";

    }

    /* ----------------------------------
    ** MakeStripeSubscription
    ** ---------------------------------- */
    public function MakeStripeSubscription($user, $payment_method, $plan, $discount_code)
    {
        try 
        {
            ToolboxHelper::LogMessage('Debug',__METHOD__, __LINE__,'','
            {
                CustomerStripeId = '    .$user->stripe_id.',
                DiscountCode = '        .$discount_code.',
                Plan = '                .$plan.',
                PaymentMethod = '       .$payment_method.'
            }');

            $user->newSubscription('main', $plan)
            ->withCoupon($discount_code)
            ->create($payment_method);
        }
        catch (Exception $e) 
        {
            ToolboxHelper::LogMessage('Error',__METHOD__, __LINE__,$e->getMessage(),'
            {
                CustomerStripeId = '    .$user->stripe_id.',
                DiscountCode = '        .$discount_code.',
                Plan = '                .$plan.',
                PaymentMethod = '       .$payment_method.'
            }');
            return $e->getMessage();
        }

        return "success";
    }

    /* ----------------------------------
    ** UpdateProductInStripe
    ** ---------------------------------- */
    public function UpdateProductInStripe($product_id)
    {
        $selected = Product::where(['id'=>$product_id])->firstOrFail();

        if($selected == null) return 'product_ref invalide !';

        $stripe_product_ref = $selected->stripe_product_ref;
        $stripe_subscr_plan = $selected->stripe_subscr_plan;        

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        try
        {
            if($stripe_subscr_plan != "")
            {
                /* plans are deleted since it is not possible to update price of existing plans */
                $plan = \Stripe\Plan::retrieve($stripe_subscr_plan);
                $plan->delete();
            }

            if($stripe_product_ref == "")
            {
                $product = \Stripe\Product::create([
                    'name'          =>  $selected->title,
                    'type'          =>  'service',
                    'attributes'    =>  ['name']
                ]);

                $selected->update([
                    'stripe_product_ref'=>$product['id'],
                ]);

                $stripe_product_ref = $product['id'];
            }
            else
            {
                \Stripe\Product::update(
                    $stripe_product_ref,
                    [
                        'name'          =>  $selected->title,
                        'attributes'    =>  ['name']
                    ]);
            }

            $plan = \Stripe\Plan::create([
                'product'   => $stripe_product_ref,
                'nickname'  => 'Abonnement hebdomadaire',
                'interval'  => 'week',
                'currency'  => 'eur',
                'amount'    => $selected->subscr_amount * 100,
            ]);

            $selected->update([
                'stripe_subscr_plan'=>$plan['id']
            ]);
        }
        catch(Exception $e)
        {
            return $e;
        }

        return 'ok';
    }

    /* ----------------------------------
    ** GetColissimoTrackingInfo
    ** ---------------------------------- */
    public function GetColissimoTrackingInfo($tracking_id)
    {
        $colissimo = new \Hedii\ColissimoApi\ColissimoApi();

        try 
        {
            $result = $colissimo->get($tracking_id);
            return $result;
        } 
        catch (\Hedii\ColissimoApi\ColissimoApiException $e) {
    // ...
        }
    }
    
}

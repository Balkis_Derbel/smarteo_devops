<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\Content;
use Log;

class ContentsService
{

    public function GetContentAdditionalParameters($str)
    {
        $p = array('type'=>'');

        $tags = explode(";",$str);
        foreach($tags as $tag)
        {
            $x = explode(">",$tag);
            if (count($x) > 1) $p[$x[0]]=$x[1];
        }

        return $p;
    }

    public function GetRelatedContents($tags)
    {
        if($tags == null || $tags == '') $tags='NULL';

        $list_tags = explode(',',$tags);

        $c = Content::where('status','ready');

        foreach($list_tags as $tag)
        {
            $c = $c->where('tags', 'LIKE', '%'.$tag.'%');
        }

        return $c->inRandomOrder()->take(3)->get();
    }
    
}

<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\UserInfo;
use App\Models\PartnerDiscount;
use Auth;


class UserDataService
{

    public function get_user_data($userid)
    {
        $data = array(
            'firstname' =>$this->get_user_data_bykey($userid, 'firstname'),
            'lastname'  =>$this->get_user_data_bykey($userid, 'lastname'),
            'phone'     =>$this->get_user_data_bykey($userid, 'phone'),
            'address'   =>$this->get_user_data_bykey($userid, 'address'),
            'zipcode'   =>$this->get_user_data_bykey($userid, 'zipcode'),
            'city'      =>$this->get_user_data_bykey($userid, 'city'),
            'email'     => Auth::user() ? Auth::user()->email : ''
        );
        return $data;
    }

    private function get_user_data_bykey($userid, $key)
    {
        $res="";
        $x = UserInfo::where(['userid' => $userid, 'key' => $key])->first();
        if($x) {$res=$x->value;}
        return $res;
    }

    public function GetUserDiscount($user_id = 0)
    {
        $discount = array(
            'subscriptions' => 0,
            'products'      => 0,
            'duration'      => '',
            'plan'          => ''
        );

        $uid = $user_id;
        
        if($uid == 0)
        {
            if(Auth::check()) $user_id = Auth::id();
        }

        $d = PartnerDiscount::where('userid',$user_id)->get();
        if(!$d->isEmpty()) 
        {

            if($d[0]->discount_code == 'WEAREWINNERS')
            {
                $discount['subscriptions']  = 100;
                $discount['duration']       = '1 mois';
                $discount['plan']           = '100_pct_off_1_month';
            }
            else
            {
                $discount['products']       = $d[0]->discount;
                $discount['subscriptions']  = 2 * $d[0]->discount;
                $discount['plan']           = $discount['subscriptions'].'_pct_off';
            }
        }

        return $discount;
    }
}

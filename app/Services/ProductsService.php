<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\Product;
use Log;

class ProductsService
{

    public function GetAllProductsByType($type)
    {
        return Product::where('status','live')->where('type',$type)->get();
    }
    
    public function GetProductBySlug($slug)
    {
        return Product::where('slug',$slug)->where('status','live')->first();
    }

    public function GetRelatedProducts($ref)
    {
        return Product::where('status','live')->where('type','product')->where('reference','<>',$ref)->inRandomOrder()->take(3)->get();
    }
}

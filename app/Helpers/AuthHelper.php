<?php


if (!function_exists('HasAdminRights')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function HasAdminRights()
    {
		if(Auth::user() && Auth::user()->role == 'admin') return true;
		return false;
    }
}

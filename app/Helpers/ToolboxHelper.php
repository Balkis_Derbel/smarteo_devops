<?php
namespace App\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use Log;

class ToolboxHelper{

    public static function paginate_collection($items, $perPage = 4, $page = null, 
        $baseUrl = null, 
        $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? 
        $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), 
            $items->count(),
            $perPage, $page, $options);

        if ($baseUrl) {
            $lap->setPath($baseUrl);
        }

        return $lap;
    }

    public static function getIp()
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function LogMessage($type, $method, $line, $description, $params)
    {
        $user_id    = Auth::user() ? Auth::user()->id : 0;
        $user_name  = Auth::user() ? Auth::user()->name : '';
        $user_email = Auth::user() ? Auth::user()->email : '';

        $message = $method.' at line '.$line.' : '.$description.'
        '.$params.'
        {
            UserId :    '.$user_id.'
            UserName :  '.$user_name.'
            UserEmail : '.$user_email.'
            IPAdress : ' .ToolboxHelper::getIp().'
        }';

        if      (strtoupper($type)=='INFO')     Log::info($message);
        elseif  (strtoupper($type)=='ERROR')    Log::error($message);
        elseif  (strtoupper($type)=='DEBUG')    Log::debug($message);
        else                                    Log::error($message);
    }

    public static function GetMetaTags($url)
    {
        $content = file_get_contents($url);
        $doc = new \DOMDocument();
        $tags = [];

        // squelch HTML5 errors
        @$doc->loadHTML($content);

        $meta = $doc->getElementsByTagName('meta');
        foreach ($meta as $element) 
        {
            $name = "";
            $value = "";
            foreach ($element->attributes as $node) {
                if(strtolower($node->name) == 'name' || strtolower($node->name) == 'property')
                    $name = $node->value;
                if(strtolower($node->name) == 'content')
                    $value = $node->value;
            }
            $tags [$name]= $value;
        }

        return $tags;
    }

    public static function GenerateRandomString($length = 10) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

